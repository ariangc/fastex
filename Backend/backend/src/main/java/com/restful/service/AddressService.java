package com.restful.service;

import com.restful.entity.Address;
import com.restful.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("addressService")
public class AddressService {
    @Autowired
    @Qualifier("addressRepository")
    private AddressRepository addressRepository;

    public Long createAddress(Address a){
        addressRepository.save(a);
        return a.getId();
    }
}
