package com.restful.model;

import com.restful.entity.ProgrammedFlight;
import com.restful.entity.Shipping;
import com.restful.entity.ShippingXProgrammedFlight;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class ShippingXProgrammedFlightModel {
    private Long id;
    private int orderInRoute;
    private boolean active;
    private ShippingModel shipping;
    private ProgrammedFlightModel programmedFlight;

    public ShippingXProgrammedFlightModel(Long id, int orderInRoute, boolean active) {
        this.id = id;
        this.orderInRoute = orderInRoute;
        this.active = active;
    }

    public ShippingXProgrammedFlightModel(ShippingXProgrammedFlight a){
        this.id = a.getId();
        this.orderInRoute = a.getOrderInRoute();
        this.active = a.isActive();
        this.shipping = new ShippingModel(a.getShipping());
        this.programmedFlight = new ProgrammedFlightModel(a.getProgrammedFlight());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getOrder() {
        return orderInRoute;
    }

    public void setOrder(int order) {
        this.orderInRoute = order;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public ShippingModel getShipping() {
        return shipping;
    }

    public void setShipping(ShippingModel shipping) {
        this.shipping = shipping;
    }

    public ProgrammedFlightModel getProgrammedFlight() {
        return programmedFlight;
    }

    public void setProgrammedFlight(ProgrammedFlightModel programmedFlight) {
        this.programmedFlight = programmedFlight;
    }
}
