import React from "react";

// reactstrap components
import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
} from "reactstrap";
import IconButton from '@material-ui/core/IconButton';
import Search from '@material-ui/icons/Search';
import {apiURL} from "variables/Variables.js"
// core components
import SideBar from 'components/sidebar/SideBar.js';
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './Clients.css';

class FirstStepClient extends React.Component {
  constructor(props) {
    super(props);
    this.loadPersonData = this.loadPersonData.bind(this);
    this.togglerDocumentNumber = this.togglerDocumentNumber.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleDocumentTypeChange = this.handleDocumentTypeChange.bind(this);

    this.state = {
      countries:[],
      documentTypes:[],
      country:0,
      documentType:0,
      documentNumber:"",
      names:"",
      surnames:"",
      birthday:"",
      personFound:false,
      countrySelected:0,
      documentTypeSelected:0,
      documentNumberClassname:"",
      documentNumberHasError:false,
      documentTypesWithRegex: [],
      documentTypeRegex: "",
      personID:0,
      person: {}
    }
}

togglerDocumentNumber(event){
  this.setState({
    documentNumber:event.target.value
  }, () => {this.props.documentNumberFunction(this.state.documentNumber)
    let regex = new RegExp(this.state.documentTypeRegex);
    console.log(this.state.documentTypeRegex)
    if (regex.test(this.state.documentNumber)) {
      this.setState({
        documentNumberClassname: "",
        documentNumberHasError:false
      }, () => {this.props.documentNumberHasErrorFunction(this.state.documentNumberHasError)});
    } else {
      this.setState({
        documentNumberClassname: "has-danger",
        documentNumberHasError:true
      }, () => {this.props.documentNumberHasErrorFunction(this.state.documentNumberHasError)});
    }})
}

handleCountryChange(event){
  this.setState({countrySelected:event.target.value, documentTypeSelected:0}, () => {
    var body = {idCountry:parseInt(this.state.countrySelected)};
    axios.post(apiURL.value + 'documentType/listByCountry', body).then((res)=>{
      if(res.status === 200){
        const documentTypesRes = res.data.map((documentType) =>
                    <option value={documentType['id']}>{documentType['name']}</option>
                );
        this.setState({documentTypes:documentTypesRes,documentTypesWithRegex:res.data,
        documentTypeRegex:res.data[0]['regex']})
      }
    })
  });
}

handleDocumentTypeChange(event){
  this.setState({documentTypeSelected:event.target.value}, () => {
    this.state.documentTypesWithRegex.map((value,index) => {
      if(value.id == this.state.documentTypeSelected){this.setState({documentTypeRegex:value.regex})}})
  });

}

loadPersonData(){
  var body = {idCountry: this.state.countrySelected.toString(), idDocumentType: this.state.documentTypeSelected.toString(),
              document:this.props.documentNumber};
  

  axios.post(apiURL.value + 'person/getByDocument', body).then((res)=>{
    if(res.status === 200){
      if(res.data.length === 0){
        toast.error('Persona no encontrada', { type: toast.TYPE.ERROR, autoClose: 5000 })
        this.props.namesFunction("")
        this.props.surnamesFunction("")
        this.props.birthdayFunction("")
        this.setState({personFound:false}, () => {
          this.props.personFoundFunction(this.state.personFound)
        })
      } else {
        console.log(res.data)
        this.setState({personFound:true, person: res.data[0], names: res.data[0].names, surnames: res.data[0].surnames,
          birthday: res.data[0].birthday.slice(0,10), personID: res.data[0].id}, () => {
          this.props.personFoundFunction(this.state.personFound)
          this.props.namesFunction(this.state.names)
          this.props.surnamesFunction(this.state.surnames)
          this.props.birthdayFunction(this.state.birthday.slice(0,10))
          this.props.personIDFunction(this.state.personID)
          this.props.personFunction(this.state.person)
        })
      }
        
      
    }
  }).catch(() => {this.setState({personFound:true, names: "error de servidor", surnames: "",
  birthday: ""}, () => {
  this.props.personFoundFunction(this.state.personFound)
  this.props.namesFunction(this.state.names)
  this.props.surnamesFunction(this.state.surnames)
  this.props.birthdayFunction(this.state.birthday.slice(0,10))})})

  
}

componentWillMount(){
  axios.post(apiURL.value + 'country/list').then((res)=>{
    console.log(res);
    if(res.status === 200){
      const countriesRes = res.data.map((country) =>
                  <option value={country.id}>{country.name}</option>
              );
      this.setState({countries:countriesRes})
    }
  })
}

render(){
  return (
    <>
    <div className="App">
          <Col>
            <ToastContainer /> 
            <Form>
              <Row>
                <h3 className="stepTitle">Datos Generales</h3>
              </Row>
              <Row>
                <Col>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlSelect1">País de Nacimiento</label>
                        <Input id="countrySelect" type="select" onChange={this.handleCountryChange} value={this.state.countrySelected}>
                          <option disabled value={0} selected>Seleccione un País</option>
                          {this.state.countries}
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                    <FormGroup>
                      <label htmlFor="exampleFormControlSelect2">Tipo de Documento</label>
                      <Input id="documentTypeSelect" type="select" onChange={this.handleDocumentTypeChange} value={this.state.documentTypeSelected}>
                          <option disabled value={0} selected>Seleccione un Tipo de Documento</option>
                          {this.state.documentTypes}
                      </Input>
                    </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                      <FormGroup className = {this.state.documentNumberClassname}>
                        <label htmlFor="exampleFormControlInput1">Número de Documento</label>
                        <Input
                          id="documentNumber"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerDocumentNumber}
                          value = {this.props.documentNumber}
                        ></Input>
                      </FormGroup>
                      </Col>
                      <Col >
                      <Row>
                        <br></br>
                      </Row>
                      <Row>
                      <IconButton 
                        color='primary' 
                        component="span"
                        onClick={this.loadPersonData}
                      >
                        <Search fontSize="large"/>
                      </IconButton >
                      </Row> 
                    </Col>
                  </Row>
                </Col>
                <Col >
                  <Row>
                    <Col sm={{ size: 8, offset: 1 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput1">Nombres</label>
                        <Input
                          id="namesInput"
                          placeholder=""
                          type="text"
                          value ={this.props.names}
                          readonly="readonly"
                        ></Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 1 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput1">Apellidos</label>
                        <Input
                          id="lastNamesInput"
                          placeholder=""
                          type="text"
                          value = {this.props.surnames}
                          readonly="readonly"
                        ></Input>
                      </FormGroup>
                    </Col> 
                  </Row> 
                  <Row>
                    <Col sm={{ size: 5, offset: 1 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput1">Fecha de Nacimiento</label>
                        <Input
                          id="birthdayInput"
                          placeholder=""
                          type="text"
                          value = {this.props.birthday}
                          readonly="readonly"
                        ></Input>
                      </FormGroup>
                    </Col>
                  </Row>
                </Col>
              </Row>  
            </Form>
          </Col>
                              
  
    </div>       
    </>
  );
  }
}

export default FirstStepClient;
