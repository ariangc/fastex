import React from "react";

// reactstrap components
import { Grid } from "react-bootstrap";
import {
  Button,
  CustomInput,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormText,
  Label,
  Card,
  CardTitle,
  Modal,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from "reactstrap";
import { Link } from "react-router-dom";
import Tooltip2 from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import HelpOutline from '@material-ui/icons/HelpOutline';
import classNames from 'classnames';
import {currentUser, shippingList,
  apiURL} from "variables/Variables.js"
// core components
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import SideBar from 'components/sidebar/SideBar.js';
import MaterialTable from "material-table";
import axios from "axios";
import './Shipping.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {PassThrough} from 'stream';
import {BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, 
      Tooltip, Legend, LineChart, Line} from 'recharts';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'
import Datetime from 'react-datetime';

class ShippingUpload extends React.Component {
  constructor(props) {
    super(props);
    this.loadShippingsFile = this.loadShippingsFile.bind(this);
    this.clearFile = this.clearFile.bind(this);
    this.loadFile = this.loadFile.bind(this);
    this.cancelLoad = this.cancelLoad.bind(this);
    this.openModal = this.openModal.bind(this);
    this.handlerCloseModal = this.handlerCloseModal.bind(this);
    this.handleWarehouseChange = this.handleWarehouseChange.bind(this);
    this.filterBar = this.filterBar.bind(this); 
    this.handlerDate = this.handlerDate.bind(this)
    this.showDateTime = this.showDateTime(this);
    this.state = {
      selectedFile: null,
      fileName:'Seleccionar archivo',
      fileError:false,
      modalOpen: false,
      warehouses:[],
      response:null,
      loadSuccess:false,
      warehouseSelected:'',
      isLoading:false,
      isLoading2:false,
      loadingFile:false,
      goodView:true,
      badView:true,
      selectedDate:'',
      secondGraph:null,
      datetimeOpen:null
    }
  }
  showDateTime(){
    this.setState({
      datetimeOpen:true
    })
  }
  handlerDate(event){
    console.log(event.format("YYYYMMDD").toString())
    this.setState({
      selectedDate:event.format("YYYYMMDD").toString(),
      isLoading2:true,
      datetimeOpen:false
    }, ()=>{
      if(event.format("YYYYMMDD").toString()!=''){
        var aux = {icaoCode:this.state.warehouseSelected, date:event.format("YYYYMMDD").toString()}
        axios.post(apiURL.value + 'warehouse/massiveHour', aux).then((res)=>{
          if(res.status==200){
            console.log(res.data)
            this.setState({
              secondGraph:res.data,
              isLoading2:false
            })
          }
          else{
            toast.error('Ocurrió un error en la consulta', { type: toast.TYPE.ERROR, autoClose: 5000 })
          }
        })
        .catch(()=>{
          toast.error('Ocurrió un error en el servidor', { type: toast.TYPE.ERROR, autoClose: 5000 })
        })
      }
    })
  }
  filterBar(event){
    console.log(event.dataKey)
    if(event.dataKey=='totalGood' || event.dataKey=='totalGoodHidden'){
      this.setState({
        goodView:!this.state.goodView
      })
    }
    else if(event.dataKey=='totalBad' || event.dataKey=='totalBadHidden'){
      this.setState({
        badView:!this.state.badView
      })
    }
  }
  handleWarehouseChange(event){
    this.setState({
      warehouseSelected: event.target.value,
      selectedDate:'',
      isLoading:true,
      response:null,
      secondGraph:null
    }, ()=>{
      var body ={"icaoCode" : this.state.warehouseSelected.toString()}
      axios.post(apiURL.value + 'warehouse/massiveTest', body).then((res)=>{
        if(res.status==200){
          this.setState({
            response:res.data,
            isLoading:false
          }, ()=>{
            console.log('DATA', this.state.response)
          })
        }
        else{
          toast.error('Ocurrió un error en la consulta', { type: toast.TYPE.ERROR, autoClose: 5000 })
          this.setState({
            isLoading:false,
            response:null
          })
        }
      })
      .catch(()=>{
        toast.error('Ocurrió un error en el servidor', { type: toast.TYPE.ERROR, autoClose: 5000 })
        this.setState({
          isLoading:false,
          response:null
        })
      })
    })
  }
  handlerCloseModal(){
    this.setState({
      modalOpen:false
    })
  }
  openModal(){
    if(!this.state.selectedFile){
      this.setState({
        fileError:true
      })
      toast.error('No ha seleccionado un archivo para la carga', { type: toast.TYPE.ERROR, autoClose: 5000 })
    }
    else{
      this.setState({
        fileError:false,
        modalOpen:true
      })
    }
  }
  cancelLoad(){
    this.props.history.push('/'+ shippingList.url)
  }
  async loadFile(){
    if(!this.state.selectedFile){
      this.setState({
        fileError:true
      })
      toast.error('No ha seleccionado un archivo para la carga', { type: toast.TYPE.ERROR, autoClose: 5000 })
    }
    else{
      
      const data = new FormData()
      console.log(this.state.selectedFile);
      data.append('file', this.state.selectedFile)
      console.log(data);
      console.log(this.state.selectedFile);
      await this.setState({
        modalOpen:false,
        loadingFile:true,
      })
      axios.post(apiURL.value + 'warehouse/massive', data).then((res)=>{
        if(res.status==200){
          toast.success('Se cargó el archivo correctamente', { type: toast.TYPE.SUCCESS, autoClose: 5000 })
          this.setState({
            fileError:false,
            selectedFile:null,
            fileName:'Seleccionar archivo',
            loadingFile:false,
            loadSuccess:true
          }, ()=>{
            axios.post(apiURL.value + 'city/list').then((res)=>{
              if(res.status==200){
                var cities = res.data.map((city, index)=>
                  <option value={city.airportCode}>{city.name}</option>
                )
                this.setState({
                  warehouses:cities
                })
              }
            })
          });
          
        }
        else{
          toast.error('Ocurrió un error en la carga del archivo', { type: toast.TYPE.ERROR, autoClose: 5000 })
          this.setState({
            loadingFile:false
          })
        }
      })
      .catch((resp)=>{
        toast.error('Ocurrió un error en el servidor', { type: toast.TYPE.ERROR, autoClose: 5000 })
        this.setState({
          loadingFile:false
        })
      })
      
    }
  }
  async clearFile(){
    await this.setState({
      selectedFile: null
    });
    this.setState({
      fileName: this.state.selectedFile?this.state.selectedFile.name:'Seleccionar archivo'
    })
  }
  async loadShippingsFile(event){
    var file = event.target.files[0];
    console.log(file);
    // if return true allow to setState
    await this.setState({
      selectedFile: file
    });
    await this.setState({
      fileName: file?file.name:'Seleccionar archivo',
      fileError: file?false:true
    });
    console.log(this.state.selectedFile)
  }
  render(){
    var yesterday = Datetime.moment().subtract( 1, 'day' );
    var valid = function( current ){
        return !current.isAfter( yesterday );
    };
  return (
    <>
    
    <div className="App">
      
      <SideBar fluid toggle={this.sidebarIsClose} isOpen={true}/>
      
      <Container fluid className={classNames('content', {'is-open': true})}>
        <NavbarRedexAdmin style={{position:'fixed', marginTop:'0'}}/>
        <Container style={{marginTop:'10%', backgroundColor:'#FAFAFA'}}>
          <ToastContainer />
          <Card>
            <Col md={{size:6, offset:3}}>
              <div className='text-center'><h4>Simulación</h4></div>
              <Form>
                {this.state.loadingFile?
                <div className='text-center'  style={{width:'100%', weight:'100%', backgroundColor:'#FFFFFF'
                }}>
                  <Loader
                    type="ThreeDots"
                    color="#03256C"
                    height={100}
                    width={100}
                    timeout={3000000}
                  />
                </div> :PassThrough}
                <FormGroup>
                  <Label for="exampleCustomFileBrowser">Plantilla</Label>
                  <a href='https://drive.google.com/drive/u/1/folders/1TlWop8x6q2K-FpHdI6lsD0QuoCjmqkCH' target='_blank'>
                  <u><p style={{fontSize:'14px', color:'#03256C'}}>Link de referencia</p></u>
                  </a>                
                </FormGroup>
               
                <FormGroup>
                  <Label for="exampleCustomFileBrowser">Cargar archivo</Label>
                  <InputGroup>
                    {(this.state.selectedFile)?
                      <InputGroupAddon id="exampleCustomFileBrowser"  addonType="prepend">
                        <InputGroupText  id="exampleCustomFileBrowser"style={this.state.fileError?{padding:'0%', borderColor:'#dc3545'}:{padding:'0%'}} >
                          <Tooltip2 title='Borrar archivo' onClick={this.clearFile}>
                            <Delete style={{color:'#FF4136'}}/>
                          </Tooltip2>
                        </InputGroupText>
                      </InputGroupAddon>:PassThrough
                    } 
                  <CustomInput accept='.zip,.rar,.7zip' style={{backgroundColor: '#000000'}} invalid = {this.state.fileError} 
                    type="file" id="exampleCustomFileBrowser" name="customFile" label={this.state.fileName}
                    value={!this.state.selectedFile?null:PassThrough} 
                    onChange={this.loadShippingsFile}/>
                  </InputGroup>
                </FormGroup>
                <Row style={{marginBottom:'5%', marginTop:'5%'}}>
                  <Button 
                    size="lg"
                    color="warning"
                    outline="warning"
                    style={{marginLeft:'20%'}}
                    onClick={this.cancelLoad}>
                    Cancelar
                  </Button>
                  <Button
                      size="lg"
                      variant="contained"
                      color="primary"
                      style={{marginLeft:'5%'}}
                      onClick={this.openModal}>
                    Cargar
                  </Button>
                </Row>
                {this.state.loadSuccess?<FormGroup style={{marginBottom:'7%'}}>
                  <label htmlFor="exampleFormControlSelect1">Almacén</label>
                  <Input id="countrySelect" type="select" onChange={this.handleWarehouseChange}>
                      <option disabled value={0} selected>Seleccionar</option>
                      {this.state.warehouses}
                  </Input>
                </FormGroup>:PassThrough}
                {(this.state.isLoading && !this.state.response)?
                <div className='text-center'  style={{width:'100%'}}>
                  <Loader
                    type="ThreeDots"
                    color="#03256C"
                    height={100}
                    width={100}
                    timeout={3000000}
                  />
                </div> :PassThrough}
              </Form> 
            </Col>
           {(!this.state.isLoading && this.state.response)? 
              <BarChart
                width= {window.innerWidth-250-window.innerWidth*0.07}
                height={300}
                data={this.state.response[this.state.warehouseSelected]}
                margin={{
                top: 20, bottom: 80,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="arrivalDate" interval={2} angle={-45} textAnchor='end' />
                <YAxis yAxisId="left" orientation="left" stroke="#8884d8" />
                
                <Tooltip />
                <Legend wrapperStyle={{top: 250}} onClick={this.filterBar}/>
                <Bar yAxisId="left" name='Recepcionados' dataKey={this.state.goodView?"totalGood":"totalGoodHidden"} fill="#4c9141" />
                <Bar yAxisId="left" name='No Recepcionados' dataKey={this.state.badView?"totalBad":"totalBadHidden"} fill="#B20004" />
            </BarChart>:PassThrough}
            {(!this.state.isLoading && this.state.response)? 
            <Col md={{size:6, offset:3}}>
              <FormGroup>
                <label >Día de consulta</label>
                <Datetime
                  isValidDate={ valid }
                  dateFormat="DD/MM/YYYY"
                  timeFormat={false}
                  value={this.state.selectedDate !== "" ? (Datetime.moment(this.state.selectedDate)) : ""}
                  inputProps={{ placeholder: "Seleccione fecha de consulta" }}
                  onChange={this.handlerDate}
                  closeOnSelect
                />
              </FormGroup>
            </Col>:PassThrough}
            {this.state.isLoading2?
            <div className='text-center'  style={{width:'100%'}}>
              <Loader
                type="ThreeDots"
                color="#03256C"
                height={100}
                width={100}
                timeout={3000000}
              />
            </div>
            :PassThrough}
            {(!this.state.isLoading2 && this.state.secondGraph)?
            <LineChart
              width={window.innerWidth-250-window.innerWidth*0.07}
              height={400}
              data={this.state.secondGraph[this.state.warehouseSelected]}
              margin={{
                top: 20, bottom: 80,
              }}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="hour" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Line type="monotone" name='Recepcionados' dataKey="totalGood" stroke="#4c9141" activeDot={{ r: 8 }} />
              <Line type="monotone" name='Flujo Total' dataKey="total" stroke="#B20004" />
            </LineChart>:PassThrough}
            
            <Modal toggle={this.handlerCloseModal} isOpen={this.state.modalOpen}>
              <Col  sm="12" md={{ size: 5, offset: 4 }}>
                <HelpOutline style={{ fontSize: 100, color:'#1768AC' }}/>
              </Col>
              <div className="modal-header">
              <div className='text-center'  style={{width:'100%'}}><h5 /* style={{textAlign:'center'}} */  id="exampleModalLiveLabel">
                  ¿Desea confirmar la carga masiva de envíos?
                </h5>
              </div>
                <button aria-label="Close" className="close" type="button" 
                        onClick={this.handlerCloseModal}>
                  <span aria-hidden={true}>×</span>
                </button>
              </div>
              <Col  sm="12" md={{ size: 8, offset: 2 }}>
                <div className="modal-footer">
                  <Button
                    color="danger"
                    type="button"
                    size="lg"
                    onClick={this.handlerCloseModal}>
                    Cancelar
                  </Button>
                  <Button
                    color="primary"
                    type="button"
                    size="lg"
                    onClick={this.loadFile}>
                    Aceptar
                  </Button>
                </div>
              </Col>
            </Modal>
          </Card>   
        </Container>
      </Container>
  
    </div>       
    </>
  );
  }
}

export default ShippingUpload;
