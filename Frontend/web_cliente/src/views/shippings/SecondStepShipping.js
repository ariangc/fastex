import React from "react";

// reactstrap components
import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
} from "reactstrap";
import IconButton from '@material-ui/core/IconButton';
import Search from '@material-ui/icons/Search';
import {apiURL} from "variables/Variables.js"
// core components
import SideBar from 'components/sidebar/SideBar.js';
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './Shipping.css';
import {PassThrough} from 'stream';

class SecondStepShipping extends React.Component {
  constructor(props) {
    super(props);
    this.loadCustomerAddresseData = this.loadCustomerAddresseData.bind(this);
    this.togglerDocumentNumber = this.togglerDocumentNumber.bind(this);
    this.togglerEmail = this.togglerEmail.bind(this);
    this.togglerTelephone = this.togglerTelephone.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.handleDocumentTypeChange = this.handleDocumentTypeChange.bind(this);

    this.state = {
      countries:[],
      documentTypes:[],
      documentNumberAddressee:"",
      namesAddressee:"",
      emailAddressee:"",
      telephoneAddressee:"",
      personAddresseeFound:false,
      countrySelected:0,
      cities:[],
      citiesList:[],
      cityAddresseeSelected:0,
      citySelected:null,
      documentNumberClassname:"",
      documentNumberAddresseeHasError:false,
      documentTypesWithRegex: [],
      documentTypeRegex: "",
      documentTypeSelected:0,
      personIDAddressee:0,
      personAddressee: {},
      emailAddresseeHasError: false,
      emailClassname:'',
      customerFound:false
    }
}

togglerDocumentNumber(event){
  this.setState({
    documentNumberAddressee:event.target.value 
  }, () => {this.props.documentNumberAddresseeFunction(this.state.documentNumberAddressee)
    let regex = new RegExp(this.state.documentTypeRegex);
    console.log(this.state.documentTypeRegex)
    if (regex.test(this.state.documentNumberAddressee)) {
      this.setState({
        documentNumberClassname: "",
        documentNumberAddresseeHasError:false
      }, () => {this.props.documentNumberAddresseeHasErrorFunction(this.state.documentNumberAddresseeHasError)});
    } else {
      this.setState({
        documentNumberClassname: "has-danger",
        documentNumberAddresseeHasError:true
      }, () => {this.props.documentNumberAddresseeHasErrorFunction(this.state.documentNumberAddresseeHasError)});
    }})
}

togglerEmail(event){
    if(event.target.value.replace(/\s/g, "").length>0 && event.target.value.includes("@") && event.target.value.substring(0, event.target.value.indexOf("@")).length>0 && 
    event.target.value.substring(event.target.value.indexOf("@"), event.target.value.length ).length>0 && event.target.value.substring(event.target.value.indexOf("@"), event.target.value.length ).includes(".")){
        this.setState({emailAddresseeHasError:false, emailClassname:'', emailAddressee:event.target.value
    }, () => {this.props.emailAddresseeFunction(this.state.emailAddressee)
                this.props.emailAddresseeHasErrorFunction(this.state.emailAddresseeHasError)})
    }else{
        this.setState({emailAddresseeHasError:true, emailClassname:"has-danger", emailAddressee:event.target.value
    }, () => {this.props.emailAddresseeFunction(this.state.emailAddressee)
                this.props.emailAddresseeHasErrorFunction(this.state.emailAddresseeHasError)})
    } 
}

togglerTelephone(event){
    const {value } = event.target;
    let regex = new RegExp("^[0-9]*$");
    if (regex.test(value) && value.length < 12) {
      this.setState({
        telephoneAddressee: event.target.value
      }, () => {this.props.telephoneAddresseeFunction(this.state.telephoneAddressee)});
    } else if (regex.test(value)) {
      this.setState({
        telephoneAddressee: event.target.value.substring(0, 12)
      }, () => {this.props.telephoneAddresseeFunction(this.state.telephoneAddressee)});
    }
}

handleDocumentTypeChange(event){
  this.setState({documentTypeSelected:event.target.value}, () => {
    this.state.documentTypesWithRegex.map((value,index) => {
      if(value.id == this.state.documentTypeSelected){this.setState({documentTypeRegex:value.regex})}})
  });

}

handleCountryChange(event){
  this.setState({countrySelected:event.target.value, documentTypeSelected:0}, () => {
    var body = {idCountry:parseInt(this.state.countrySelected)};
    axios.post(apiURL.value + 'documentType/listByCountry', body).then((res)=>{
      if(res.status === 200){
        const documentTypesRes = res.data.map((documentType,index) =>
                    <option value={documentType['id']}>{documentType['name']}</option>
                );    
        this.setState({documentTypes:documentTypesRes,documentTypesWithRegex:res.data,
        documentTypeRegex:res.data[0]['regex']})
      }
    })
  });
}

handleCityChange(event){
    this.setState({cityAddresseeSelected:event.target.value}, () => {
      this.state.citiesList.map((city) => {
        if(city.id == this.state.cityAddresseeSelected){
          this.setState({citySelected:city}, () =>{
          console.log(this.state.citySelected)
          this.props.cityAddresseeFunction(this.state.citySelected)})        
        }
    })
   
  })
}

loadCustomerAddresseData(){
  var body = {idCountry: this.state.countrySelected.toString(), idDocumentType: this.state.documentTypeSelected.toString(),
    document:this.props.documentNumberAddressee};

  console.log(this.state.countrySelected.toString(),this.state.documentTypeSelected.toString(),this.props.documentNumberAddressee) 
  axios.post(apiURL.value + 'customer/getCustomerByDocument', body).then((res)=>{
    if(res.status === 200){
      console.log(res.data)
      if(res.data.length === 0){
        toast.error('El destinatario no es cliente', { type: toast.TYPE.WARNING, autoClose: 5000 })
        this.props.nameAddresseeFunction("")
        this.setState({customerAddresseeFound:false}, () => {
        this.props.customerAddresseeFoundFunction(this.state.customerAddresseeFound)
        console.log(this.state.customerAddresseeFound)
        //buscar persona
        axios.post(apiURL.value + 'person/getByDocument', body).then((res)=>{
          if(res.status === 200){
            if(res.data.length === 0){
              toast.error('Persona no encontrada', { type: toast.TYPE.ERROR, autoClose: 5000 })
              this.props.nameAddresseeFunction("")
              this.setState({personAddresseeFound:false}, () => {
                this.props.personAddresseeFoundFunction(this.state.personAddresseeFound)
              })
            } else {
              console.log(res.data)
              this.setState({personAddresseeFound:true, namesAddressee: res.data[0].names + ' ' + res.data[0].surnames,
                              personAddressee:res.data[0]}, () => {
                this.props.personAddresseeFoundFunction(this.state.personAddresseeFound)
                this.props.nameAddresseeFunction(this.state.namesAddressee)
                this.props.personAddresseeFunction(this.state.personAddressee)
              })
            }
            this.props.emailAddresseeFunction("")
            this.props.telephoneAddresseeFunction("")
          }
        }).catch(() => {this.setState({personAddresseeFound:true, namesAddressee: "error de servidor"}, () => {
          this.props.personAddresseeFoundFunction(this.state.personAddresseeFound)
          this.props.nameAddresseeFunction(this.state.namesAddressee)})})    
      })
      } else {
        console.log("es cliente")
        console.log(res.data)
        this.setState({personAddresseeFound:true, customerAddresseeFound:true, customerAddreessee: res.data[0], 
          namesAddressee: res.data[0].person.names + ' ' + res.data[0].person.surnames, personAddressee:res.data[0].person}, () => {
            this.props.personAddresseeFoundFunction(this.state.personAddresseeFound)
            this.props.customerAddresseeFoundFunction(this.state.customerAddresseeFound)
            this.props.nameAddresseeFunction(this.state.namesAddressee)
            this.props.customerAddresseeFunction(this.state.customerAddreessee)
            this.props.emailAddresseeFunction(this.state.customerAddreessee.mail)
            this.props.telephoneAddresseeFunction(this.state.customerAddreessee.phone)
            this.props.personAddresseeFunction(this.state.personAddressee)
      })
      }
    }
  }).catch(() => {this.setState({customerAddresseeFound:false, namesAddressee: "Sin conexión"}, () => {
    this.props.customerAddresseeFoundFunction(this.state.customerAddresseeFound)
    this.props.nameAddresseeFunction(this.state.namesAddressee)
    this.props.customerAddresseeFunction(this.state.customerAddreessee)
  })}) 
}

componentWillMount(){
  axios.post(apiURL.value + 'country/list').then((res)=>{
    console.log(res);
    if(res.status === 200){
      const countriesRes = res.data.map((country) =>
                  <option value={country.id}>{country.name}</option>
              );
      this.setState({countries:countriesRes})
    }
  })
  axios.post(apiURL.value + 'city/list').then((res)=>{
    console.log(res);
    if(res.status === 200){
      const citiesRes = res.data.map((city) =>
                  <option value={city.id}>{city.name}</option>
              );
      this.setState({cities:citiesRes,citiesList:res.data})
    }
  })
}

componentDidMount(){
  if(this.props.emailAddressee.length===0){
    this.props.emailAddresseeHasErrorFunction(true)
  }
}

render(){
  return (
    <>
    <div className="App">
          <Col>
            <ToastContainer /> 
            <Form>
              <Row>
                <h3 className="stepTitle">Registrar Destinatario</h3>
              </Row>
              <Row>
                <Col>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlSelect1">País de Nacimiento</label>
                        <Input id="countrySelect" type="select" onChange={this.handleCountryChange} value={this.state.countrySelected}>
                          <option disabled value={0} selected>Seleccione un País</option>
                          {this.state.countries}
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                    <FormGroup>
                      <label htmlFor="exampleFormControlSelect2">Tipo de Documento</label>
                      <Input id="documentTypeSelect" type="select" onChange={this.handleDocumentTypeChange} value={this.state.documentTypeSelected}>
                          <option disabled value={0} selected>Seleccione un Tipo de Documento</option>
                          {this.state.documentTypes}
                      </Input>
                    </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                      <FormGroup className = {this.state.documentNumberClassname}>
                        <label htmlFor="exampleFormControlInput1">Número de Documento</label>
                        <Input
                          id="documentNumber"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerDocumentNumber}
                          value = {this.props.documentNumberAddressee}
                        ></Input>
                      </FormGroup>
                      </Col>
                      <Col >
                      <Row>
                        <br></br>
                      </Row>
                      <Row>
                      <IconButton 
                        color='primary' 
                        component="span"
                        onClick={this.loadCustomerAddresseData}
                      >
                        <Search fontSize="large"/>
                      </IconButton >
                      </Row> 
                    </Col>
                  </Row>
                </Col>
                <Col >
                  <Row>
                    <Col sm={{ size: 8, offset: 1 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput1">Destinatario</label>
                        <Input
                          id="namesInput"
                          placeholder=""
                          type="text"
                          value ={this.props.nameAddressee}
                          readonly="readonly"
                        ></Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 1 }}>
                        <FormGroup>
                        <label htmlFor="exampleFormControlSelect2">Ciudad de Destino (*)</label>
                        <Input id="citySelect" type="select" onChange={this.handleCityChange} value={this.state.cityAddresseeSelected}>
                            <option disabled value={0} selected>Seleccione una Ciudad de Destino</option>
                            {this.state.cities}
                        </Input>
                        </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 1 }}>
                      <FormGroup className={this.state.emailClassname}>
                        <label htmlFor="exampleFormControlInput1">Correo (*)</label>
                        <Input
                          id="emailInput"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerEmail}
                          value = {this.props.emailAddressee}
                          disabled={this.props.customerAddresseeFound?true:false}
                        ></Input>
                      </FormGroup>
                    </Col> 
                  </Row> 
                  <Row>
                    <Col sm={{ size: 5, offset: 1 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput1">Teléfono</label>
                        <Input
                          id="telephoneInput"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerTelephone}
                          value = {this.props.telephoneAddressee}
                          disabled={this.props.customerAddresseeFound?true:false}
                        ></Input>
                      </FormGroup>
                    </Col>
                  </Row>
                </Col>
              </Row>  
            </Form>
          </Col>
                              
  
    </div>       
    </>
  );
  }
}

export default SecondStepShipping;
