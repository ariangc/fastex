package com.restful.repository;

import com.restful.entity.ProgrammedFlight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository("programmedFlightRepository")
public interface ProgrammedFlightRepository extends JpaRepository<ProgrammedFlight, Long> {
    public abstract List<ProgrammedFlight> findByStartDateGreaterThanEqual(Date startDate);
}
