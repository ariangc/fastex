package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "WarehouseStatus")
@Entity
public class WarehouseStatus {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idWarehouseStatus")
    private Long id;

    private String description;

    @OneToMany(mappedBy = "warehouseStatus", cascade = CascadeType.ALL)
    private List<Warehouse> warehouses = new ArrayList<>();

    protected WarehouseStatus(){}

    public WarehouseStatus(Long id, String description, List<Warehouse> warehouses) {
        this.id = id;
        this.description = description;
        this.warehouses = warehouses;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Warehouse> getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(List<Warehouse> warehouses) {
        this.warehouses = warehouses;
    }
}
