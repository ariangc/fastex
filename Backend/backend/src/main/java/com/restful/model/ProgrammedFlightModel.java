package com.restful.model;

import com.restful.entity.Flight;
import com.restful.entity.ProgrammedFlight;
import com.restful.entity.ProgrammedFlightStatus;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

public class ProgrammedFlightModel {
    private Long id;

    private Date startDate;
    private int filledCapacity;
    private int totalCapacity;

    private ProgrammedFlightStatusModel programmedFlightStatus;
    private FlightModel flight;

    public ProgrammedFlightModel(Long id, Date startDate, int filledCapacity, int totalCapacity) {
        this.id = id;
        this.startDate = startDate;
        this.filledCapacity = filledCapacity;
        this.totalCapacity = totalCapacity;
    }

    public ProgrammedFlightModel(ProgrammedFlight a){
        this.id = a.getId();
        this.startDate = a.getStartDate();
        this.filledCapacity = a.getFilledCapacity();
        this.totalCapacity = a.getTotalCapacity();
        this.programmedFlightStatus = new ProgrammedFlightStatusModel(a.getProgrammedFlightStatus());
        this.flight = new FlightModel(a.getFlight());
    }

    public Long getId() {
        return id;
    }

    public boolean isFull(){
        return (this.filledCapacity == this.totalCapacity);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getFilledCapacity() {
        return filledCapacity;
    }

    public void setFilledCapacity(int filledCapacity) {
        this.filledCapacity = filledCapacity;
    }

    public int getTotalCapacity() {
        return totalCapacity;
    }

    public void setTotalCapacity(int totalCapacity) {
        this.totalCapacity = totalCapacity;
    }

    public ProgrammedFlightStatusModel getProgrammedFlightStatus() {
        return programmedFlightStatus;
    }

    public void setProgrammedFlightStatus(ProgrammedFlightStatusModel programmedFlightStatus) {
        this.programmedFlightStatus = programmedFlightStatus;
    }

    public FlightModel getFlight() {
        return flight;
    }

    public void setFlight(FlightModel flight) {
        this.flight = flight;
    }
}
