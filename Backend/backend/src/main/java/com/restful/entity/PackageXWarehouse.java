package com.restful.entity;

import javax.persistence.*;
import java.util.Date;

@Table(name = "PackageXWarehouse")
@Entity
public class PackageXWarehouse {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idPackageXWarehouse")
    private Long id;

    @Column(name = "checkin", columnDefinition="DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkin;

    @Column(name = "checkout", columnDefinition="DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkout;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idWarehouse")
    private Warehouse warehouse;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPackage")
    private Package pack;

    protected PackageXWarehouse(){}

    public PackageXWarehouse(Long id, Date checkin, Date checkout, Warehouse warehouse, Package pack) {
        this.id = id;
        this.checkin = checkin;
        this.checkout = checkout;
        this.warehouse = warehouse;
        this.pack = pack;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCheckin() {
        return checkin;
    }

    public void setCheckin(Date checkin) {
        this.checkin = checkin;
    }

    public Date getCheckout() {
        return checkout;
    }

    public void setCheckout(Date checkout) {
        this.checkout = checkout;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Package getPack() {
        return pack;
    }

    public void setPack(Package pack) {
        this.pack = pack;
    }
}
