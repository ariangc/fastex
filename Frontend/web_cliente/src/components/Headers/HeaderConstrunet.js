import React from "react";

// reactstrap components
import { Button, Container } from "reactstrap";

// core components

function HeaderConstrunet() {
  let pageHeader = React.createRef();

  React.useEffect(() => {
    if (window.innerWidth > 700) {
      const updateScroll = () => {
        let windowScrollTop = window.pageYOffset / 4;
        pageHeader.current.style.transform =
          "translate3d(0," + windowScrollTop + "px,0)";
      };
      window.addEventListener("scroll", updateScroll);
      return function cleanup() {
        window.removeEventListener("scroll", updateScroll);
      };
    }
  });
  return (
    <>
      <div className="page-header page-header-small"
      style={{
        minHeight: '20vh',
        backgroundColor: '#03256C'
        //backgroundImage: "url(" + require("assets/img/banner.jpg") + ")"
      }}>
        <div
          className="page-header-image"

          ref={pageHeader}
        ></div>
        <div className="content-center">
          <Container>
            <h1 className="title">FastEx</h1>
          </Container>
        </div>
      </div>
    </>
  );
}

export default HeaderConstrunet;
