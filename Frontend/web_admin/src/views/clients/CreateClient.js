import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import Typography from '@material-ui/core/Typography';
import SideBar from 'components/sidebar/SideBar.js';
import classNames from 'classnames';
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import FirstStepClient from 'views/clients/FirstStepClient.js';
import SecondStepClient from 'views/clients/SecondStepClient.js';
import {PassThrough} from 'stream';
import ThirdStepClient from 'views/clients/ThirdStepClient.js';
import HelpOutline from '@material-ui/icons/HelpOutline';
import { ToastContainer, toast } from 'react-toastify';
import axios from "axios";
import {apiURL} from "variables/Variables.js"
import 'react-toastify/dist/ReactToastify.css';
import './Clients.css';
import history from "./history";
import {
    Modal,
    Container,
    Row,
    Col,
    Card,
    Button
  } from "reactstrap";


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    //marginRight: theme.spacing(1),
    marginLeft: '10%'
  },
  backButton: {
    //marginRight: theme.spacing(2),
    marginLeft: '35%'
  },
  buttonFirstStep:{
    marginLeft: '42%'
  },
  completed: {
    display: 'inline-block',
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return ['Datos generales', 'Datos de contacto', 'Confirmación del registro'];
}

export default function CreateClient(props) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [completed, setCompleted] = React.useState(new Set());
  const [documentNumber, setDocumentNumber] = React.useState("");
  const [names, setNames] = React.useState("");
  const [surnames, setSurnames] = React.useState("");
  const [birthday, setBirthday] = React.useState("");
  const [address, setAddress] = React.useState("");
  const [postalCode, setPostalCode] = React.useState("");
  const [inside, setInside] = React.useState("");
  const [reference, setReference] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [telephone, setTelephone] = React.useState("");
  const [username, setUsername] = React.useState("");
  const [isOpen, setIsOpen] = React.useState(false);
  const [emailHasError, setEmailHasError] = React.useState(false);
  const [usernameHasError, setUsernameHasError] = React.useState(false);
  const [documentNumberHasError, setDocumentNumberHasError] = React.useState(false);
  const [personFound, setPersonFound] = React.useState(false);
  const [personID, setPersonID] = React.useState(0);
  const [person, setPerson] = React.useState({});
  const [addressCity, setAddressCity] = React.useState(0);
  const [isCustomer, setIsCustomer] = React.useState(false);
  const steps = getSteps();

  const totalSteps = () => {
    return getSteps().length;
  };

  const completedSteps = () => {
    return completed.size;
  };

  const allStepsCompleted = () => {
    return completedSteps() === totalSteps() 
  };

  const isLastStep = () => {
    return activeStep === totalSteps() - 1;
  };

  const handleLast = () => {
    setIsOpen(true);
  }

  const openFalse = () => {
    setIsOpen(false);
  }

  const handleNext = () => {
    const newActiveStep =
      isLastStep() && !allStepsCompleted()
        ? // It's the last step, but not all steps have been completed
          // find the first step that has been completed
          steps.findIndex((step, i) => !completed.has(i))
        : activeStep + 1;

    setActiveStep(newActiveStep);    
  };

  const handlePersonNotFound = () => {
    toast.error('No ha buscado una persona', { type: toast.TYPE.ERROR, autoClose: 5000 })
  }

  const handlerCustomerFound = () => {
    toast.error('La persona ya es cliente', { type: toast.TYPE.WARNING, autoClose: 5000 })
  }
  const handleSecondStepHasError = () => {
    toast.error('Complete los campos obligatorios (*)', { type: toast.TYPE.ERROR, autoClose: 5000 });
 /*    setUsernameHasError(true);
    setEmailHasError(true); */
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const createClient = () => {
    var body = {id:null, user:username, password: null,
                mail:email, phone:telephone, phonePrefix: null,
                infoEmail: null, infoPhone: null, 
                person:person, 
                customerStatus:{id:2,name:"ACTIVO"},
                address:{id:1, street:"Test", inside:"Test", zipCode:"Test", reference:"Test",
                         city:{id:1}}};
    axios.post(apiURL.value + 'customer/create', body).then((res)=>{
      if(res.status === 200){
        setIsOpen(false);
        history.push("/client-list");
      }
    })
  }

  const handleComplete = () => {
    const newCompleted = new Set(completed);
    newCompleted.add(activeStep);
    setCompleted(newCompleted);

    /**
     * Sigh... it would be much nicer to replace the following if conditional with
     * `if (!this.allStepsComplete())` however state is not set when we do this,
     * thus we have to resort to not being very DRY.
     */
    if (completed.size !== totalSteps()) {
      handleNext();
    }
  };

  return (
    <div className="App">
    <SideBar fluid toggle={false} isOpen={true}/>
    <Container fluid className={classNames('content', {'is-open': true})} style={{backgroundColor:'#FAFAFA'}}>
        <NavbarRedexAdmin />
        <Container style={{padding:'0%', margin:'0%', backgroundColor:'#FAFAFA'}}>
        <Stepper alternativeLabel nonLinear activeStep={activeStep} style={{position:'fixed', width:'85%', marginTop:'5.5%',
                                                                            zIndex:800, backgroundColor:'#FAFAFA'}}>
            {steps.map((label, index) => {
            const stepProps = {};
            const buttonProps = {};
            return (
                <Step key={label} {...stepProps}>
                <StepButton disabled className="svg.MuiStepIcon-root.MuiStepIcon-active"
                >
                    {label}
                </StepButton>
                </Step>
            );
            })}
        </Stepper>
        </Container>

        <Container style={{marginTop:'18%'}}>
            <Card > 
              <Col> 
              <div>
                      <Typography className={classes.instructions}>{activeStep==0?<FirstStepClient 
                      documentNumberFunction = {setDocumentNumber} 
                      documentNumber = {documentNumber}
                      namesFunction = {setNames}
                      names = {names}
                      surnamesFunction = {setSurnames}
                      surnames = {surnames}
                      birthdayFunction = {setBirthday}
                      birthday = {birthday}
                      personFoundFunction = {setPersonFound}
                      documentNumberHasErrorFunction = {setDocumentNumberHasError}
                      documentNumberHasError = {documentNumberHasError}
                      personIDFunction = {setPersonID}
                      personFunction = {setPerson}
                      isCustomerFunction = {setIsCustomer}
                      />:
                      activeStep==1?<SecondStepClient 
                      addressFunction = {setAddress}
                      address = {address}
                      postalCodeFunction = {setPostalCode}
                      postalCode = {postalCode}
                      insideFunction = {setInside}
                      inside = {inside}
                      referenceFunction = {setReference}
                      reference = {reference}
                      emailFunction = {setEmail}
                      email = {email}
                      telephoneFunction = {setTelephone}
                      telephone = {telephone}
                      usernameFunction = {setUsername}
                      username = {username}
                      emailHasErrorFunction = {setEmailHasError}
                      emailHasError = {emailHasError}
                      usernameHasErrorFunction = {setUsernameHasError}
                      usernameHasError = {usernameHasError}
                      addressCityFunction = {setAddressCity}
                      personID = {personID}
                      />:
                      activeStep==2?<ThirdStepClient
                      names = {names}
                      surnames = {surnames}
                      email = {email}
                      username = {username}
                      />:
                      PassThrough}</Typography>
                      <Row>
                        <Col>
                      {activeStep === 0 ?PassThrough:
                      <Button onClick={handleBack} 
                          size="lg"
                          color="warning"
                          outline="warning"
                          className={classes.backButton}
                          >
                          Volver
                      </Button>}
                      {activeStep === 0 ?
                      <Button
                          size="lg"
                          variant="contained"
                          color="primary"
                          onClick={!personFound?handlePersonNotFound:isCustomer?handlerCustomerFound:handleNext}
                          className={classes.buttonFirstStep}
                      >{isLastStep()? 'Terminar' : 'Siguiente'}
                      </Button>:
                      <Button
                          size="lg"
                          variant="contained"
                          color="primary"
                          onClick={isLastStep()?handleLast:(emailHasError || usernameHasError)?handleSecondStepHasError:handleNext}
                          className={classes.button}
                      >{isLastStep()? 'Terminar' : 'Siguiente'}
                      </Button>}
                          
                      </Col>
                      </Row>  
                      <Modal toggle={openFalse} isOpen={isOpen}>
                        <Col  sm="12" md={{ size: 5, offset: 4 }}><HelpOutline style={{ fontSize: 100, color:'#1768AC' }}/></Col>
                          <div className="modal-header">
                          
                            <h5 style={{textAlign:'center'}} className="modal-title" id="exampleModalLiveLabel">
                              ¿Desea confirmar el registro del cliente {names}?
                            </h5>
                            <button
                              aria-label="Close"
                              className="close"
                              type="button"
                              onClick={openFalse}
                            >
                              <span aria-hidden={true}>×</span>
                            </button>
                          </div>
                          <Col  sm="12" md={{ size: 8, offset: 2 }}>
                          <div className="modal-footer">
                            <Button
                              color="danger"
                              type="button"
                              size="lg"
                              onClick={openFalse}
                            >
                              Cancelar
                            </Button>
                            <Button
                              color="primary"
                              type="button"
                              size="lg"
                              onClick={createClient}
                            >
                              Aceptar
                            </Button>
                          </div>
                        </Col>
                      </Modal>                
              </div>
              </Col>
            </Card>  
        </Container>
        
        </Container>
    </div>
  );
}
