package com.restful.model;

import com.restful.entity.Package;
import com.restful.entity.Shipping;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class PackageModel {
    private Long id;
    private String type;
    private boolean active;
    private String description;
    private ShippingModel shipping;

    public PackageModel(Long id, String type, boolean active, String description) {
        this.id = id;
        this.type = type;
        this.active = active;
        this.description = description;
    }

    public PackageModel() {
    }

    public PackageModel(Package a){
        this.id = a.getId();
        this.type = a.getType();
        this.active = a.isActive();
        this.description = a.getDescription();
        this.shipping = new ShippingModel(a.getShipping());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ShippingModel getShipping() {
        return shipping;
    }

    public void setShipping(ShippingModel shipping) {
        this.shipping = shipping;
    }
}
