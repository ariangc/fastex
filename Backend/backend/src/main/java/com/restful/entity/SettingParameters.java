package com.restful.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;

@Table(name = "SettingParameters")
@Entity
public class SettingParameters {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String name;
    private int value;
    private boolean active;

    protected SettingParameters(){}

    public SettingParameters(Long id, String name, int value, boolean active) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.active = active;
    }

    @Override
    public String toString(){
        return String.format("SettingParameters=[id=%d, name='%s', value=%d, active=%d]", id, name, value, active);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
