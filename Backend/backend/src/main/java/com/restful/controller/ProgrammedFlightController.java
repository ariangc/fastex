package com.restful.controller;

import com.restful.model.ProgrammedFlightModel;
import com.restful.service.ProgrammedFlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RequestMapping("/programmedFlight")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
public class ProgrammedFlightController {
    @Autowired
    @Qualifier("programmedFlightService")
    private ProgrammedFlightService programmedFlightService;

    @CrossOrigin
    @PostMapping("/getAll")
    public @ResponseBody Map<String, List<ProgrammedFlightModel>> listProgrammedFlights(){
        return programmedFlightService.getProgrammedFlights();
    }
}
