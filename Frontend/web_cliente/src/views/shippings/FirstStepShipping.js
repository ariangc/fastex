import React from "react";

// reactstrap components
import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
} from "reactstrap";
import IconButton from '@material-ui/core/IconButton';
import Search from '@material-ui/icons/Search';
import {apiURL} from "variables/Variables.js"
// core components
import SideBar from 'components/sidebar/SideBar.js';
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './Shipping.css';

class FirstStepShipping extends React.Component {
  constructor(props) {
    super(props);
    this.loadCustomerData = this.loadCustomerData.bind(this);
    this.togglerDocumentNumber = this.togglerDocumentNumber.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleDocumentTypeChange = this.handleDocumentTypeChange.bind(this);

    this.state = {
      countries:[],
      documentTypes:[],
      countrySelected:0,
      documentNumber:"",
      names:"",
      email:"",
      telephone:"",
      personFound:false,
      documentTypeSelected:0,
      documentNumberClassname:"",
      documentNumberHasError:false,
      documentTypesWithRegex: [],
      documentTypeRegex: "",
      person: {},
      customer: {},
      customerFound:false
    }
}

togglerDocumentNumber(event){
  this.setState({
    documentNumber:event.target.value
  }, () => {this.props.documentNumberFunction(this.state.documentNumber)
    let regex = new RegExp(this.state.documentTypeRegex);
    console.log(this.state.documentTypeRegex)
    if (regex.test(this.state.documentNumber)) {
      this.setState({
        documentNumberClassname: "",
        documentNumberHasError:false
      }, () => {this.props.documentNumberHasErrorFunction(this.state.documentNumberHasError)});
    } else {
      this.setState({
        documentNumberClassname: "has-danger",
        documentNumberHasError:true
      }, () => {this.props.documentNumberHasErrorFunction(this.state.documentNumberHasError)});
    }})
}

handleCountryChange(event){
  this.setState({countrySelected:event.target.value, documentTypeSelected:0}, () => {
    var body = {idCountry:parseInt(this.state.countrySelected)};
    axios.post(apiURL.value + 'documentType/listByCountry', body).then((res)=>{
      if(res.status === 200){
        const documentTypesRes = res.data.map((documentType) =>
                    <option value={documentType['id']}>{documentType['name']}</option>
                );
        
        this.setState({documentTypes:documentTypesRes,documentTypesWithRegex:res.data,
        documentTypeRegex:res.data[0]['regex']})
      }
    })
  });
}

handleDocumentTypeChange(event){
  this.setState({documentTypeSelected:event.target.value}, () => {
    this.state.documentTypesWithRegex.map((value,index) => {
      if(value.id == this.state.documentTypeSelected){this.setState({documentTypeRegex:value.regex})}})
  });

}

loadCustomerData(){

  var body = {idCountry: this.state.countrySelected.toString(), idDocumentType: this.state.documentTypeSelected.toString(),
              document:this.props.documentNumber};
  
  console.log(this.state.countrySelected.toString(),this.state.documentTypeSelected.toString(),this.props.documentNumber) 
  axios.post(apiURL.value + 'customer/getCustomerByDocument', body).then((res)=>{
    if(res.status === 200){
      console.log(res.data)
      if(res.data.length === 0){
        toast.error('Cliente no encontrado', { type: toast.TYPE.ERROR, autoClose: 5000 })
        this.props.namesFunction("")
        this.setState({customerFound:false}, () => {
          this.props.customerFoundFunction(this.state.customerFound)
        })
      } else {
        console.log(res.data)
        this.setState({customerFound:true, customer: res.data[0], names: res.data[0].person.names + ' ' + res.data[0].person.surnames}, () => {
          this.props.customerFoundFunction(this.state.customerFound)
          this.props.namesFunction(this.state.names)
          this.props.customerFunction(this.state.customer)
        })
      }
    }
  }).catch(() => {this.setState({customerFound:false, names: "Sin conexión"}, () => {
  this.props.customerFoundFunction(this.state.customerFound)
  this.props.namesFunction(this.state.names)
  this.props.customerFunction(this.state.customer)
  })}) 
            
}

componentWillMount(){
  axios.post(apiURL.value + 'country/list').then((res)=>{
    console.log(res);
    if(res.status === 200){
      const countriesRes = res.data.map((country) =>
                  <option value={country.id}>{country.name}</option>
              );
      this.setState({countries:countriesRes})
    }
  })
}

render(){
  return (
    <>
    <div className="App">
          <Col>
            <ToastContainer /> 
            <Form>
              <Row>
                <h3 className="stepTitle">Registrar Remitente</h3>
              </Row>
              <Row>
                <Col>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlSelect1">País de Nacimiento</label>
                        <Input id="countrySelect" type="select" onChange={this.handleCountryChange} value={this.state.countrySelected}>
                          <option disabled value={0} selected>Seleccione un País</option>
                          {this.state.countries}
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                    <FormGroup>
                      <label htmlFor="exampleFormControlSelect2">Tipo de Documento</label>
                      <Input id="documentTypeSelect" type="select" onChange={this.handleDocumentTypeChange} value={this.state.documentTypeSelected}>
                          <option disabled value={0} selected>Seleccione un Tipo de Documento</option>
                          {this.state.documentTypes}
                      </Input>
                    </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                      <FormGroup className = {this.state.documentNumberClassname}>
                        <label htmlFor="exampleFormControlInput1">Número de Documento</label>
                        <Input
                          id="documentNumber"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerDocumentNumber}
                          value = {this.props.documentNumber}
                        ></Input>
                      </FormGroup>
                      </Col>
                      <Col >
                      <Row>
                        <br></br>
                      </Row>
                      <Row>
                      <IconButton 
                        color='primary' 
                        component="span"
                        onClick={this.loadCustomerData}
                      >
                        <Search fontSize="large"/>
                      </IconButton >
                      </Row> 
                    </Col>
                  </Row>
                </Col>
                <Col >
                  <Row>
                    <Col sm={{ size: 8, offset: 1 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput1">Cliente</label>
                        <Input
                          id="namesInput"
                          placeholder=""
                          type="text"
                          value ={this.props.names}
                          readonly="readonly"
                        ></Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 1 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput1">Correo</label>
                        <Input
                          id="emailInput"
                          placeholder=""
                          type="text"
                          value = {this.props.customer.mail}
                          readonly="readonly"
                        ></Input>
                      </FormGroup>
                    </Col> 
                  </Row> 
                  <Row>
                    <Col sm={{ size: 5, offset: 1 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput1">Teléfono</label>
                        <Input
                          id="telephoneInput"
                          placeholder=""
                          type="text"
                          value = {this.props.customer.phone}
                          readonly="readonly"
                        ></Input>
                      </FormGroup>
                    </Col>
                  </Row>
                </Col>
              </Row>  
            </Form>
          </Col>
                              
  
    </div>       
    </>
  );
  }
}

export default FirstStepShipping;
