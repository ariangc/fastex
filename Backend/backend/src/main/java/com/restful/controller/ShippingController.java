package com.restful.controller;

import com.restful.entity.ProgrammedFlight;
import com.restful.entity.Shipping;
import com.restful.model.ProgrammedFlightModel;
import com.restful.model.ShippingModel;
import com.restful.service.ShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/shipping")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
public class ShippingController {
    @Autowired
    @Qualifier("shippingService")
    private ShippingService shippingService;

    @CrossOrigin
    @PostMapping("/create")
    public @ResponseBody Map<String, ShippingModel> createShipping(@RequestBody Map<String, Shipping> a){
        System.out.println(a.get("shipping").toString());
        return shippingService.createShipping(a.get("shipping"));
    }

    @CrossOrigin
    @PostMapping("/list")
    public @ResponseBody Map<String, List<ShippingModel>> listShippings(){
        return shippingService.listShippings();
    }

    @CrossOrigin
    @PostMapping("/getByTrackingCode")
    public @ResponseBody Map<String, List<ShippingModel>> getShippingByTrackingCode(@RequestBody Map<String, String> data){
        return shippingService.getShippingByTrackingNumber(data.get("trackingCode"));
    }

    @CrossOrigin
    @PostMapping("/getRoute")
    public @ResponseBody Map<String, List<ProgrammedFlightModel>> getRoute(@RequestBody Map<String, Long> data){
        return shippingService.getRoute(data.get("idShipping"));
    }

    @CrossOrigin
    @PostMapping("/cancel")
    public @ResponseBody boolean cancelShipping(@RequestBody Map<String, Shipping> data){
        return shippingService.cancelShipping(data.get("shipping"));
    }

    @CrossOrigin
    @PostMapping("/deliver")
    public @ResponseBody boolean deliverShipping(@RequestBody Map<String, Shipping> data){
        return shippingService.deliverShipping(data.get("shipping"));
    }

    @CrossOrigin
    @PostMapping("/quote")
    public @ResponseBody Map<String, Float> quoteShipping(@RequestBody Map<String, Long> data){
        return shippingService.quoteShipping(data.get("idStartCity"), data.get("idEndCity"));
    }
}
