package com.restful.service;

import com.restful.entity.Country;
import com.restful.model.CountryModel;
import com.restful.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("countryService")
public class CountryService {
    @Autowired
    @Qualifier("countryRepository")
    private CountryRepository countryRepository;

    public List<CountryModel> getCountries(){
        List<Country> l = countryRepository.findAll();
        List<CountryModel> lModel = new ArrayList<CountryModel>();
        for(Country country: l){
            CountryModel x = new CountryModel(country);
            lModel.add(x);
        }
        return lModel;
    }

}
