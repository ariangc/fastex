package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "Package")
@Entity
public class Package {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idPackage")
    private Long id;

    private String type;
    private boolean active;
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idShipping")
    private Shipping shipping;

    @OneToMany(mappedBy = "pack", cascade = CascadeType.ALL)
    private List<PackageXWarehouse> packageXWarehouses = new ArrayList<>();

    protected Package(){}

    public Package(Long id, String type, boolean active, String description, Shipping shipping, List<PackageXWarehouse> packageXWarehouses) {
        this.id = id;
        this.type = type;
        this.active = active;
        this.description = description;
        this.shipping = shipping;
        this.packageXWarehouses = packageXWarehouses;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    public List<PackageXWarehouse> getPackageXWarehouses() {
        return packageXWarehouses;
    }

    public void setPackageXWarehouses(List<PackageXWarehouse> packageXWarehouses) {
        this.packageXWarehouses = packageXWarehouses;
    }
}
