import React from "react";

// reactstrap components
import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label
} from "reactstrap";
import {apiURL} from "variables/Variables.js"
// core components
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './Clients.css';

class SecondStepClient extends React.Component {
  constructor(props) {
    super(props);
    this.togglerAddressStreet = this.togglerAddressStreet.bind(this);
    this.togglerPostalCode = this.togglerPostalCode.bind(this);
    this.togglerInside = this.togglerInside.bind(this);
    this.togglerReference = this.togglerReference.bind(this);
    this.togglerEmail = this.togglerEmail.bind(this);
    this.togglerTelephone = this.togglerTelephone.bind(this);
    this.togglerUsername = this.togglerUsername.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.state = {
      countries:[],
      cities:[],
      telephoneCodes:[],
      addressStreet:"",
      postalCode:"",
      inside:"",
      reference:"",
      email:"",
      telephoneCode:0,
      telephone:"",
      username: "",
      emailClassname: "",
      emailHasError:false,
      telephoneClassname: "",
      telephoneHasError:false,
      usernameClassname: "",
      usernameHasError:false,
      countrySelected: 0,
      citySelected:0,
      usernamesList:[],
      usernamesLabelsList:[]
    }
}

togglerAddressStreet(event){
    this.setState({
        addressStreet:event.target.value
    }, () => {this.props.addressFunction(this.state.addressStreet)})
}

togglerPostalCode(event){
    this.setState({
      postalCode:event.target.value
    }, () => {this.props.postalCodeFunction(this.state.postalCode)})
}

togglerInside(event){
    this.setState({
      inside:event.target.value
    }, () => {this.props.insideFunction(this.state.inside)})
}

togglerReference(event){
    this.setState({
      reference:event.target.value
    }, () => {this.props.referenceFunction(this.state.reference)})
}

togglerEmail(event){
  if(event.target.value.replace(/\s/g, "").length>0 && event.target.value.includes("@") && event.target.value.substring(0, event.target.value.indexOf("@")).length>0 && 
  event.target.value.substring(event.target.value.indexOf("@"), event.target.value.length ).length>0 && event.target.value.substring(event.target.value.indexOf("@"), event.target.value.length ).includes(".")){
    this.setState({emailHasError:false, emailClassname:'', email:event.target.value
  }, () => {this.props.emailFunction(this.state.email)
            this.props.emailHasErrorFunction(this.state.emailHasError)})
  }else{
    this.setState({emailHasError:true, emailClassname:"has-danger", email:event.target.value
  }, () => {this.props.emailFunction(this.state.email)
            this.props.emailHasErrorFunction(this.state.emailHasError)})
  } 
}

togglerTelephone(event){
  const {value } = event.target;
    let regex = new RegExp("^[0-9]*$");
    if (regex.test(value) && this.state.telephone.length < 12) {
      this.setState({
        telephone: event.target.value
      }, () => {this.props.telephoneFunction(this.state.telephone)});
    } else if (regex.test(value)) {
      this.setState({
        telephone: event.target.value.substring(0, 12)
      }, () => {this.props.telephoneFunction(this.state.telephone)});
    }
}

togglerUsername(event){
  if(event.target.value.length===0){
    this.setState({usernameClassname:'has-danger',usernameHasError:true,
    username:event.target.value
  }, () => {this.props.usernameFunction(this.state.username) 
            this.props.usernameHasErrorFunction(this.state.usernameHasError)})
  }else{
    this.setState({
      usernameClassname:'',usernameHasError:false,
      username:event.target.value
    }, () => {this.props.usernameFunction(this.state.username)
              this.props.usernameHasErrorFunction(this.state.usernameHasError)})
  } 
}

handleCountryChange(event){
  this.setState({countrySelected:event.target.value}, () => {
    var body = {idCountry:parseInt(this.state.countrySelected)};
    axios.post(apiURL.value + 'city/listByCountry', body).then((res)=>{
      if(res.status === 200){
        console.log(res.data)
        const cityListsRes = res.data.map((city) =>
                    <option value={city['id']}>{city['name']}</option>
                );
        this.setState({cities:cityListsRes})
      }
    })
  });
}

handleCityChange(event){
  this.setState({citySelected:event.target.value}, () => {this.props.addressCityFunction(this.state.citySelected)});
}

componentWillMount(){
    this.setState({emailHasError:this.props.emailHasError, usernameHasError:this.props.usernameHasError});

    axios.post(apiURL.value + 'country/list').then((res)=>{
      if(res.status === 200){
        const countriesRes = res.data.map((country) =>
                    <option value={country.id}>{country.name}</option>
                );
        this.setState({countries:countriesRes})
      }
    })
    
    var body = {idPerson:parseInt(this.props.personID)};
    axios.post(apiURL.value + 'customer/getUsernames', body).then((res)=>{
      console.log(res);
      if(res.status === 200){
        this.setState({usernamesList:res.data})
        const usernamesListRes = res.data.map((value,index) =>
                <label id={index}>{value}</label>
                );
        this.setState({usernamesLabelsList:usernamesListRes})        
      }
    })

}

componentDidMount(){
  if(this.props.username.length===0){
    this.props.usernameHasErrorFunction(true)
  }
  if(this.props.email.length===0){
    this.props.emailHasErrorFunction(true)
  }
}

render(){
  return (
    <>
    <div className="App">
          <Col> 
            <ToastContainer />
            <Form>
              <Row>
                <h3 className="stepTitle">Datos de Contacto</h3>
              </Row>
              <Row>
                <Col>
                  <Row>
                    <Col sm={{ size: 6, offset: 2 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlSelect1">País</label>
                        <Input id="countrySelect" type="select" onChange={this.handleCountryChange}>
                          <option disabled value={0} selected>Seleccione un País</option>
                          {this.state.countries}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col sm={{ size: 6, offset: 2 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlSelect1">Ciudad</label>
                        <Input id="citySelect" type="select" onChange={this.handleCityChange}>
                        <option disabled value={0} selected>Seleccione una Ciudad</option>
                          {this.state.cities}
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 10, offset: 2 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput1">Dirección de calle</label>
                        <Input
                          id="addressStreet"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerAddressStreet}
                          value = {this.props.address}
                        ></Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 4, offset: 2 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput1">Código postal/ZIP</label>
                        <Input
                          id="postalCode"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerPostalCode}
                          value = {this.props.postalCode}
                        ></Input>
                      </FormGroup>
                    </Col>
                    <Col sm={{ size: 3, offset: 1 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput1">Interior</label>
                        <Input
                          id="inside"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerInside}
                          value = {this.props.inside}
                        ></Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 10, offset: 2 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput1">Referencia</label>
                        <Input
                          id="reference"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerReference}
                          value = {this.props.reference}
                        ></Input>
                      </FormGroup>
                    </Col>
                  </Row>
                </Col>
                <Col >
                  <Row>
                    <Col sm={{ size: 8, offset: 1 }}>
                      <FormGroup className={this.state.emailClassname}>
                        <label htmlFor="exampleFormControlInput1">Correo electrónico (*)</label>
                        <Input
                          id="email"
                          placeholder=""
                          type="email"
                          onChange = {this.togglerEmail}
                          value = {this.props.email}
                        ></Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 3, offset: 1 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlSelect1">Código</label>
                        <Input id="telephoneCode" type="select">
                          <option>+51</option>
                          <option>+52</option>
                          <option>+53</option>
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col sm={{ size: 5 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput1">Teléfono</label>
                        <Input
                          id="telephone"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerTelephone}
                          value = {this.props.telephone}
                        ></Input>
                      </FormGroup>
                    </Col> 
                  </Row> 
                  <Row>
                    <Col sm={{ size: 8, offset: 1 }}>
                      <FormGroup className={this.state.usernameClassname}>
                        <label htmlFor="exampleFormControlInput1">Usuario (*)</label>
                        <Input
                          id="username"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerUsername}
                          value = {this.props.username}
                        ></Input>
                        <Label style={{marginTop:'1.5%'}}>Usuarios recomendados:</Label>
                        {this.state.usernamesList.map((value,index) =>
                         { return (<p style={{fontSize:"14px", marginBottom:"5px"}} id={index}>{value}</p>)}
                        )}
                      </FormGroup>
                    </Col>
                  </Row>
                </Col>
              </Row>  
            </Form>
          </Col> 
    </div>       
    </>
  );
  }
}

export default SecondStepClient;
