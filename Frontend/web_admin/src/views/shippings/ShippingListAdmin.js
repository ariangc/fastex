import React from "react";

// reactstrap components
import { Grid } from "react-bootstrap";
import {
  Button,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormText,
  Label,
  Card,
  CardTitle,
  Modal
} from "reactstrap";
import {Tabs} from 'react-bootstrap';
import {Tab} from 'react-bootstrap';
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import HelpOutline from '@material-ui/icons/HelpOutline';
import PublishIcon from '@material-ui/icons/Publish';
import classNames from 'classnames';
import {currentUser,shippingUpload, shipping, shippingDetail, deliverShipping,
  apiURL} from "variables/Variables.js"
// core components
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import SideBar from 'components/sidebar/SideBar.js';
import MaterialTable from "material-table";
import axios from "axios";
import './Shipping.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'
import {PassThrough} from 'stream';

const columns = [
{
  title: 'N°',
  field: 'id'
},
{
  title: 'Remitente',
  field: 'senderName'
},
{
  title: 'Destinatario',
  field: 'receiverName'
}, 
{
  title: 'Origen',
  field: 'source'
},
{
  title: 'Destino',
  field: 'destination'
},
{
  title: 'Tracking',
  field: 'trackingCode'
},
{
  title: 'Estado',
  field: 'status'
}
];

class ShippingListAdmin extends React.Component {
  constructor(props) {
    super(props);
    this.sidebarIsOpen = this.sidebarIsOpen.bind(this);
    this.sidebarIsClose = this.sidebarIsClose.bind(this);
    this.handlerCloseEditModal = this.handlerCloseEditModal.bind(this);
    this.handlerCloseModal = this.handlerCloseModal.bind(this);
    this.cancelShipping = this.cancelShipping.bind(this);
    this.state = {
      selectedRow: null,
      selectedRowCancel: null,
      data:null,
      sidebarOpen:true,
      modalOpen:false,
      clientDeleteNames:'',
      shippings:[],
      loading:true
    }
}
async cancelShipping(){
  await this.setState({
    loading:true,
    modalOpen:false
  })
  var shipping = {shipping:this.state.selectedRowCancel.original}
  axios.post(apiURL.value + 'shipping/cancel', shipping).then((res)=>{
    if(res.data){
      this.setState({
        loading:false
      })
      toast.success('Se canceló el envío con éxito', { type: toast.TYPE.SUCCESS, autoClose: 5000 })
      axios.post(apiURL.value + 'shipping/list').then((res)=>{
        if(res.status==200){
          var auxShippings=[];
          res.data['shippings'].forEach((shipping, index)=>{
            var aux ={idShipping: parseInt(shipping.id)}
            axios.post(apiURL.value + 'shipping/getRoute', aux).then((resp)=>{
              if(resp.status==200){
                var ship = {'id':index+1, 'senderName':shipping.customer.person.names + ' ' + shipping.customer.person.surnames,
                      'receiverName':shipping.receiver.person.names + ' ' + shipping.receiver.person.surnames,
                      'source': resp.data['route'][0].flight.startCity.name + ' - ' + resp.data['route'][0].flight.startCity.country.name,
                      'destination': resp.data['route'][resp.data['route'].length-1].flight.endCity.name + ' - ' + resp.data['route'][resp.data['route'].length-1].flight.endCity.country.name,
                      'trackingCode': shipping.trackingCode, 'status':shipping.shippingStatus.description, original:shipping
                      }
                auxShippings.push(ship)
                this.setState({
                  data:auxShippings.sort(function(a, b){return b.original.id - a.original.id}),
                  loading:false
                })
              }
            })
          })
        }
        
        
      })
    }
    else{
      toast.error('No se pudo cancelar el envío', { type: toast.TYPE.ERROR, autoClose: 5000 })
    }
  })
  .catch(()=>{
    toast.error('Ocurrió un error en el servidor', { type: toast.TYPE.ERROR, autoClose: 5000 })
  })
}
handlerCloseModal(){
  this.setState({
    modalOpen:false
  })
}
handlerCloseEditModal(){
  this.setState({
    modalOpen:false
  })
}
sidebarIsClose(){
  this.setState({
    sidebarOpen:!this.state.sidebarOpen
  })
}
sidebarIsOpen(){
  this.setState({
    sidebarOpen:true
  })
}

async componentWillMount(){
  shipping.value = null;
  await axios.post(apiURL.value + 'shipping/list').then((res)=>{
    if(res.status==200){
      console.log('respuesta: ', res.data['shippings'])
      if(res.data['shippings'].length==0){
        this.setState({
          loading:false
        })
      }
      else{
      var auxShippings=[];
      res.data['shippings'].forEach((shipping, index)=>{
        var aux ={idShipping: parseInt(shipping.id)}
        axios.post(apiURL.value + 'shipping/getRoute', aux).then((resp)=>{
          if(resp.status==200){
            var ship = {'id':index+1, 'senderName':shipping.customer.person.names + ' ' + shipping.customer.person.surnames,
                  'receiverName':shipping.receiver.person.names + ' ' + shipping.receiver.person.surnames,
                  'source': resp.data['route'][0].flight.startCity.name + ' - ' + resp.data['route'][0].flight.startCity.country.name,
                  'destination': resp.data['route'][resp.data['route'].length-1].flight.endCity.name + ' - ' + resp.data['route'][resp.data['route'].length-1].flight.endCity.country.name,
                  'trackingCode': shipping.trackingCode, 'status':shipping.shippingStatus.description, original:shipping
                  }
            auxShippings.push(ship)
            this.setState({
              data:auxShippings.sort(function(a, b){return b.original.id - a.original.id}),
              loading:false
            })
          }
        })
        
      })
    }
      console.log('nueva data', auxShippings)
    }
  })

  if(this.props.location.state){
    if(this.props.location.state.activeToast){
      toast.error('Envío registrado', { type: toast.TYPE.SUCCESS, autoClose: 5000 })
    }
    
  }

}
  render(){
  return (
    <>
    <div className="App">
      <SideBar fluid toggle={this.sidebarIsClose} isOpen={this.state.sidebarOpen}/>  
      <Container
      fluid className={classNames('content', {'is-open': this.state.sidebarOpen})}>
      <NavbarRedexAdmin style={{position:'fixed', marginTop:'0'}}/>
        <Container style={{marginTop:'10%', backgroundColor:'#FAFAFA'}}>
          <ToastContainer />           
            <Col>
              {this.state.loading?
              <div className='text-center'  style={{width:'100%', weight:'100%', backgroundColor:'#FFFFFF'
            }}>
              <Loader
                type="ThreeDots"
                color="#03256C"
                height={100}
                width={100}
                timeout={3000000}
              />
            </div> :
            
              <MaterialTable
              
                title="Envíos"
                data={this.state.data?this.state.data:[]}
                columns={columns}
                onRowClick={(evt, selectedRow) => {
                  (evt.target).ondblclick = () =>{this.setState({selectedRow}, ()=>{
                    shipping.value=this.state.selectedRow.original;
                    this.props.history.push('/' + shippingDetail.url)
                    })
                  }
                }}
                actions={[
                  {
                    icon:'edit',
                    iconButton:{color:'red'},
                    color:'red',
                    tooltip: 'Save User'
                  },
                  {
                    icon: 'delete',
                    root: 'blue',
                    tooltip: 'Delete User'
                  }
                ]}
                components={{
                  Action: 
                    props => {
                      if(props.action.icon === 'edit'){
                        if(props.data.original.shippingStatus.id==4){
                        return(
                          <Tooltip title='Entregar Envío'>
                            <IconButton 
                            style={{color:'#0074D9'}} 
                            component="span"
                            size = 'small'
                            onClick= {(event, rowData) => {
                              this.setState({selectedRow:props.data}, ()=>{
                              shipping.value=this.state.selectedRow.original;
                              this.props.history.push('/' + deliverShipping.url)
                              })
                            }}
                            >
                              <CheckCircleIcon fontSize="large"/>
                            </IconButton >
                          </Tooltip>
                        )}
                        else{
                          return(PassThrough)
                        }
                      }
                      if(props.action.icon === 'delete'){
                        if(props.data.original.shippingStatus.id==2){
                        return(
                          
                          <Tooltip title='Cancelar Envío'>
                            <IconButton 
                            style={{color:'#FF4136'}}
                            component="span"
                            onClick = {(event, rowData) => {this.setState({
                              selectedRowCancel:props.data,
                              modalOpen:true
                            })}}
                            >
                              <Delete fontSize="large"/>
                            </IconButton >
                          </Tooltip>
                        )}
                        else{
                          return(PassThrough)
                        }
                      }
                    }
                }}
                options={{actionsColumnIndex: -1,filtering:true, exportButton: true,
                  rowStyle: rowData => ({
                    backgroundColor: (this.state.selectedRow && this.state.selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
                  })
                }}
                localization={{
                  toolbar: {
                    searchPlaceholder: "Buscar"
                  },
                  body: {
                    emptyDataSourceMessage: 'No hay datos'
                  },
                  header:{
                    actions:'Acciones'
                  }
                }}
              />}
            </Col> 
            <Modal toggle={this.handlerCloseModal} isOpen={this.state.modalOpen}>
              <Col  sm="12" md={{ size: 5, offset: 4 }}>
                <HelpOutline style={{ fontSize: 100, color:'#1768AC' }}/>
              </Col>
              <div className="modal-header">
              <div className='text-center'  style={{width:'100%'}}><h5 /* style={{textAlign:'center'}} */  id="exampleModalLiveLabel">
                    ¿Desea confirmar la cancelación del envío {this.state.selectedRowCancel?this.state.selectedRowCancel.trackingCode:''}?
                </h5>
              </div>
                <button aria-label="Close" className="close" type="button" 
                        onClick={this.handlerCloseModal}>
                  <span aria-hidden={true}>×</span>
                </button>
              </div>
              <Col  sm="12" md={{ size: 8, offset: 2 }}>
                <div className="modal-footer">
                  <Button
                    color="danger"
                    type="button"
                    size="lg"
                    onClick={this.handlerCloseModal}>
                    Cancelar
                  </Button>
                  <Button
                    color="primary"
                    type="button"
                    size="lg"
                    onClick={this.cancelShipping}>
                    Aceptar
                  </Button>
                </div>
              </Col>
            </Modal>  
          </Container>
      </Container>
  
    </div>       
    </>
  );
  }
}

export default ShippingListAdmin;
