/*eslint-disable*/
import React from "react";

// reactstrap components
import { Container } from "reactstrap";

class TransparentFooterConstrunet extends React.Component {
  render(){
  return (
    <footer className="footer">
      <Container>
          <nav>
            <ul>
              <li>
                <a
                  href="http://www.construnet.com.pe"
                  target="_blank"
                >
                  About Us
                </a>
              </li>
              <li>
                <a
                  href="https://www.construnet.com.pe/blog"
                  target="_blank"
                >
                  Blog
                </a>
              </li>
            </ul>
          </nav>
          <div className="copyright" id="copyright">
            © {new Date().getFullYear()}, Todos los derechos reservados.
            
          </div>
        </Container>
    </footer>
  );
  }
}

export default TransparentFooterConstrunet;
