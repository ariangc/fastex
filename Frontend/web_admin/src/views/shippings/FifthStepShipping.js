import React from "react";

// reactstrap components
import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Button
} from "reactstrap";
import IconButton from '@material-ui/core/IconButton';
import PersonIcon from '@material-ui/icons/Person';
import FlagIcon from '@material-ui/icons/Flag';
import LocalAtmIcon from '@material-ui/icons/LocalAtm';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import {apiURL,originWarehouse} from "variables/Variables.js"
// core components
import SideBar from 'components/sidebar/SideBar.js';
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './Shipping.css';
import MaterialTable from "material-table";
import { EmitFlags } from "typescript";
import { Page, Text, View, Document, StyleSheet, PDFDownloadLink } from '@react-pdf/renderer';
import ReactPDF from '@react-pdf/renderer';
import {PassThrough} from 'stream';
import html2canvas from 'html2canvas'
import jsPDF from 'jspdf'
import { constants } from "buffer";

const data = [
  {
    id:1,
    type:'Regular',
    description: 'Maleta negra de 25kg'
  },
  {
    id:2,
    type:'Frágil',
    description: null
  },
];
const columns = [
  {
    title: 'Tipo',
    field: 'type'
  },
  {
    title: 'Descripción',
    field: 'description'
  }
  ];

class FifthStepShipping extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      senderName:'Renato Cruzado Arroyo',
      senderDocumentNumber:'12345678',
      senderCity:'Lima - Perú',
      receiverName:'Arian Gallardo Callalli',
      receiverDocumentNumber:'87654321',
      receiverCity:'Brasilia - Brasil',
      price:'500 PEN'
    }
}

componentDidMount(){
  console.log(this.props.packageShipping)
}

render(){
  /* const styles = StyleSheet.create({
    movieContainer: {
      backgroundColor: "#f6f6f5",
      display: "flex",
      flexDirection: "row",
      padding: 5
  },
    page: {
      
      backgroundColor: '#FFFFFF'
    },
    section: {
      marginTop: '10%',
      padding: 10,
      flexGrow: 1
    }
  }); */
  
  // Create Document Component
  /* const MyDocument = () => (
    <Document>
      <Page size="A4" style={styles.page}>
        <View>
          <Text style={{backgroundColor:'#03256C', textAlign:'center', color:'#FFFFFF'}}>RedEx</Text>
            <Text style={{textAlign:'center'}}>
              Remitente&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              Destinatario
            </Text>
            <Text style={{textAlign:'center'}}>
              {this.state.senderName}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              {this.state.receiverName}
            </Text>
            <Text style={{textAlign:'center'}}>
              {this.state.senderDocumentNumber}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              {this.state.receiverDocumentNumber}
            </Text>
            <Text style={{textAlign:'center'}}>
              {this.state.senderCity}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              {this.state.receiverCity}
            </Text>
        </View>    
      </Page>
    </Document>
  ); */
  return (
    <>
    <div className="App">
          <Col>
            <ToastContainer /> 
            <Form>
              <Row>
              <Col md={{size:8}}>
                <h4>Resumen</h4>
                <Row>
                  <Col md={{size:5, offset:1}}>
                    <div className='text-center'><label htmlFor="exampleFormControlSelect1">Remitente</label></div>
                  </Col>
                  <Col md={{size:5, offset:1}}>
                    <div className='text-center'><label htmlFor="exampleFormControlSelect1">Destinatario</label></div>
                  </Col>
                </Row>
                <Row>
                  <Col md={{size:1}}> 
                    <PersonIcon style={{color:'#03256C'}}/>
                  </Col>
                  <Col md={{size:6}}>
                    <label>{this.props.names}</label>
                  </Col>
                  <Col md={{size:5}}>
                    <label>{this.props.nameAddressee}</label>
                  </Col>
                </Row>
                <Row>
                  <Col md={{size:1}}> 
                    <PersonIcon style={{color:'#03256C'}}/>
                  </Col>
                  <Col md={{size:6}}>
                    <label>{this.props.documentNumber}</label>
                  </Col>
                  <Col md={{size:5}}>
                    <label>{this.props.documentNumberAddressee}</label>
                  </Col>
                </Row>
                <Row>
                  <Col md={{size:1}}> 
                    <FlagIcon style={{color:'#03256C'}}/>
                  </Col>
                  <Col md={{size:6}}>
                    <label>{originWarehouse.name}</label>
                  </Col>
                  <Col md={{size:5}}>
                    <label>{this.props.cityAddressee.name+' - '+this.props.cityAddressee.country.name}</label>
                  </Col>
                </Row>
                <Row>
                  <Col md={{size:1}}>
                    <LocalAtmIcon style={{color:'#03256C'}}/>
                  </Col>
                  <Col md={{size:5}}>
                    <label>{this.props.price}</label>
                  </Col>
                </Row>
                <div  id="divToPrint">
                <MaterialTable
                     
                  title="Paquetes"
                  data={this.props.packageShipping}
                  columns={columns}
                  localization={{
                    toolbar: {
                      searchPlaceholder: "Buscar"
                    },
                    body: {
                      emptyDataSourceMessage: 'No hay datos'
                    }
                  }}
                  options={{
                    paging: false
                  }}
                />
                </div>
              </Col>
              
              </Row>
            </Form>
          </Col>
                              
  
    </div>       
    </>
  );
  }
}

export default FifthStepShipping;
