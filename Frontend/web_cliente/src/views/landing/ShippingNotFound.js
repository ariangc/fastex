import React from "react";

// reactstrap components
import {
  Button,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  FormGroup,
  Card,
  Row,
  Col
} from "reactstrap";
import {login,apiURL,shipping} from "variables/Variables.js"
import { Link } from "react-router-dom";
// core components
import ExamplesNavbar from "components/Navbars/ExamplesNavbar.js";
import HeaderConstrunet from "components/Headers/HeaderConstrunet.js";
import FooterConstrunet from "components/Footers/FooterConstrunet.js";
import IconButton from '@material-ui/core/IconButton';
import Search from '@material-ui/icons/Search';
import AccessTime from '@material-ui/icons/AccessTime';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import axios from "axios";
import history from "../clients/history";

class ShippingNotFound extends React.Component {
    constructor(props) {
        super(props);
        this.homepage = this.homepage.bind(this);
    }

    homepage(){
        this.props.history.push({pathname: '/landing'})
    }
    componentWillMount(){
        console.log("landing")
    }
  render(){
  return (
    <>
      <main ref="main">
      <div className="wrapper" >
        <HeaderConstrunet/>
        <div className="container-fluid text-center">
            <Row>
                <Col sm={{ size:8, offset:2}}>
                  <br></br>
                  <h2 style={{marginBottom:"1vh",fontWeight:"bold"}}>Buscamos, pero no lo encontramos</h2>
                  <h5>El número de rastreo es incorrecto</h5>

                  <h4 style={{marginTop:"1vh",marginBottom:"1vh"}}>Sigue los siguientes pasos para buscar tu envío</h4>
                </Col>
            </Row>
            <Row>
                <Col sm={{ size:3,offset:3}}>
                  <Card style={{height: "40vh", width: "40vh",backgroundImage:"url(" + require("assets/img/shippingNotFound1.jpg") + ")",backgroundSize: "cover"}}>
                  </Card>
                  <label style={{marginLeft:"2vh",fontSize:"16px"}}>1. Ubique su número de rastreo en su comprobante de pago</label>
                </Col>
                <Col sm={{ size:3}}>
                  <Card style={{height: "40vh", width: "40vh",backgroundImage:"url(" + require("assets/img/shippingNotFound2.PNG") + ")",backgroundSize: "cover"}}>
                  </Card>
                  <label style={{marginLeft:"2vh",fontSize:"16px"}}>2. Ingrese el número de tracking</label>
                </Col>

            </Row>
            <Row>
                <Col>
                  <Button
                    size="lg"
                    variant="contained"
                    color="primary"
                    onClick={this.homepage}
                    style={{fontSize:"18px"}}
                    >Inicio
                  </Button>
                </Col>
            </Row>
        </div>
        <FooterConstrunet/>
      </div>
    </main>
    </>
  );
  }
}

export default ShippingNotFound;
