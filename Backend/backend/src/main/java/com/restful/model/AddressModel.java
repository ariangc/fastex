package com.restful.model;

import com.restful.entity.Address;

public class AddressModel {
    private Long id;
    private String street;
    private String inside;
    private String zipCode;
    private String reference;
    private CityModel city;

    public AddressModel(Long id, String street, String inside, String zipCode, String reference) {
        this.id = id;
        this.street = street;
        this.inside = inside;
        this.zipCode = zipCode;
        this.reference = reference;
    }

    public AddressModel(Address a){
        this.id = a.getId();
        this.street = a.getStreet();
        this.inside = a.getInside();
        this.zipCode = a.getZipCode();
        this.reference = a.getReference();
        this.city = new CityModel(a.getCity());
    }

    public AddressModel(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getInside() {
        return inside;
    }

    public void setInside(String inside) {
        this.inside = inside;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public CityModel getCity() {
        return city;
    }

    public void setCity(CityModel city) {
        this.city = city;
    }
}
