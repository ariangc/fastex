package com.restful.repository;

import com.restful.entity.Shipping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("shippingRepository")
public interface ShippingRepository extends JpaRepository<Shipping, Long> {
    public abstract List<Shipping> findByTrackingCode(String trackingCode);
}
