import React from "react";

// reactstrap components
import { Grid } from "react-bootstrap";
import {
  Button,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormText,
  Label,
  Card,
  CardTitle,
  Modal
} from "reactstrap";
import {Tabs} from 'react-bootstrap';
import {Tab} from 'react-bootstrap';
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import HelpOutline from '@material-ui/icons/HelpOutline';
import classNames from 'classnames';
import {apiURL, warehouse, warehouseList, editedWarehouse} from "variables/Variables.js"
// core components
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import SideBar from 'components/sidebar/SideBar.js';
import MaterialTable from "material-table";
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {PassThrough} from 'stream';
const data = [];

class EditWarehouse extends React.Component {
  constructor(props) {
    super(props);
    this.sidebarIsOpen = this.sidebarIsOpen.bind(this);
    this.sidebarIsClose = this.sidebarIsClose.bind(this);
    this.handlerCloseEditModal = this.handlerCloseEditModal.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.togglerCantOcup = this.togglerCantOcup.bind(this);
    this.togglerCantTotal = this.togglerCantTotal.bind(this);
    this.togglerLimit = this.togglerLimit.bind(this);
    this.openModal = this.openModal.bind(this);
    this.editWarehouse = this.editWarehouse.bind(this)
    this.cancelEdit = this.cancelEdit.bind(this)
    this.state = {
      selectedRow: null,
      data:[],
      countries:[],
      countrySelected:0,
      cities:[],
      citySelected:0,
      sidebarOpen:true,
      modalOpen:false,
      cantOcup:'',
      cantTotal:'',
      limit:"",
      toast:true
    }
}
cancelEdit(){
  this.props.history.push('/' + warehouseList.url)
}
editWarehouse(){
  if(warehouse.value) {
    var warehouseAux ={id: warehouse.value.id, totalCapacity: parseInt(this.state.cantTotal),
      filledCapacity:  parseInt(this.state.cantOcup), capacityLimitPercentage:  parseInt(this.state.limit),
      icaoCode: warehouse.value.icaoCode, iataCode: warehouse.value.iataCode, city: warehouse.value.city,
      warehouseStatus: warehouse.value.warehouseStatus}
    axios.post(apiURL.value + 'warehouse/update', warehouseAux).then((res)=>{
      if(res.status==200){
        this.setState({
          modalOpen:false,
          toast:false
        }, ()=>{
          editedWarehouse.value=1;
          toast.success('Se actualizó el almacén correctamente', { type: toast.TYPE.SUCCESS, autoClose: 5000 })
          this.props.history.push('/'+ warehouseList.url)
        })
        
      }
      else{
        toast.error('Ocurrió un problema al actualizar el almacén', { type: toast.TYPE.ERROR, autoClose: 5000 })
      }
    })
    .catch(()=>{
      toast.error('Ocurrió un problema con el servidor', { type: toast.TYPE.ERROR, autoClose: 5000 })
    })
  }
  else{
    toast.error('Debe ingresar desde el listado de almacenes', { type: toast.TYPE.ERROR, autoClose: 5000 })
  }
}
openModal(){
  if(parseInt(this.state.cantOcup)>parseInt(this.state.cantTotal)){
    toast.error('La cantidad ocupada no puede ser mayor a la cantidad total', { type: toast.TYPE.ERROR, autoClose: 5000 })
    this.setState({
      modalOpen:false
    })
  }else{
    this.setState({
      modalOpen:true
    })
  }
}
handleCountryChange(event){
  this.setState({countrySelected:event.target.value}, () => {
    var body = {idCountry:parseInt(this.state.countrySelected)};
    axios.post(apiURL.value + 'city/listByCountry', body).then((res)=>{
      if(res.status === 200){
        console.log(res.data)
        const cityListsRes = res.data.map((city) =>
                    <option value={city['id']}>{city['name']}</option>
                );
        this.setState({cities:cityListsRes})
      }
    })
  });
}

handleCityChange(event){

}

togglerCantOcup(event){
   const {value } = event.target;
    let regex = new RegExp("^[0-9]*$");
    if (regex.test(value) && this.state.cantOcup.length < 5) {
      this.setState({
        cantOcup: event.target.value
      });
    } else if (regex.test(value)) {
      this.setState({
        cantOcup: event.target.value.substring(0, 5)
      });
    }
}

togglerCantTotal(event){
    const {value } = event.target;
    let regex = new RegExp("^[0-9]*$");
    if (regex.test(value) && this.state.cantTotal.length < 5) {
      this.setState({
        cantTotal: event.target.value
      });
    } else if (regex.test(value)) {
      this.setState({
        cantTotal: event.target.value.substring(0, 5)
      });
    }
}

togglerLimit(event){
    const {value } = event.target;
    let regex = new RegExp("^[0-9]*$");
    console.log(event.tregex);
    if (regex.test(event.target.value) && event.target.value <= 100) {
      this.setState({
        limit: event.target.value
      });
    }
}

handlerCloseEditModal(){
  this.setState({
    modalOpen:false
  })
}
sidebarIsClose(){
  this.setState({
    sidebarOpen:!this.state.sidebarOpen
  })
}
sidebarIsOpen(){
  this.setState({
    sidebarOpen:true
  })
}

componentWillMount(){
  editedWarehouse.value=0;
  console.log(warehouse);
  if(warehouse.value) {
    this.setState({
      limit: warehouse.value.capacityLimitPercentage,
      cantOcup: warehouse.value.filledCapacity,
      cantTotal: warehouse.value.totalCapacity
    })
  }


}

  render(){
  return (
    <>
    <div className="App"> 
      <SideBar fluid toggle={this.sidebarIsClose} isOpen={this.state.sidebarOpen}/>
      <Container
      fluid className={classNames('content', {'is-open': this.state.sidebarOpen})}>
      <NavbarRedexAdmin/>
        <Container style={{marginTop:'10%', backgroundColor:'#FAFAFA'}}>
        <ToastContainer /> 
            <Card>
            <Col md={{size:6, offset:3}}>
            {this.state.toast?<ToastContainer />:PassThrough}
            <Form>
              <Row>
                <h3 className="stepTitle">Editar Almacén</h3>
              </Row>
              <Row>
                <Col>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlSelect1">País</label>
                        <Input id="countrySelect" type="text" value={warehouse.value?warehouse.value.city.country.name:''} disabled>
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                    <FormGroup>
                      <label htmlFor="exampleFormControlSelect2">Ciudad</label>
                      <Input id="citySelect" type="text" value={warehouse.value?warehouse.value.city.name:''} disabled>
                      </Input>
                    </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                      <FormGroup className = {this.state.cantOcup}>
                        <label htmlFor="exampleFormControlInput1">Cant. Ocupada</label>
                        <Input
                          id="cantOcup"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerCantOcup}
                          value = {this.state.cantOcup}
                        ></Input>
                      </FormGroup>
                      </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                      <FormGroup className = {this.state.cantTotal}>
                        <label htmlFor="exampleFormControlInput2">Cant. Total</label>
                        <Input
                          id="cantTotal"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerCantTotal}
                          value = {this.state.cantTotal}
                        ></Input>
                      </FormGroup>
                      </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput3">% Limite</label>
                        <Input
                          id="limit"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerLimit}
                          value = {this.state.limit}
                        ></Input>
                      </FormGroup>
                      </Col>                      
                  </Row>
                </Col>
              </Row>  
            <Row style={{marginBottom:'10%', marginTop:'5%'}}>
                  <Button 
                    size="lg"
                    color="warning"
                    outline="warning"
                    style={{marginLeft:'20%'}}
                    onClick={this.cancelEdit}>
                    Cancelar
                  </Button>
                  <Button
                      size="lg"
                      color="primary"
                      style={{marginLeft:'5%'}}
                      onClick={this.openModal}>
                    Actualizar
                  </Button>
                </Row>
            </Form>
            <Modal toggle={this.handlerCloseEditModal} isOpen={this.state.modalOpen}>
              <Col  sm="12" md={{ size: 5, offset: 4 }}>
                <HelpOutline style={{ fontSize: 100, color:'#1768AC' }}/>
              </Col>
              <div className="modal-header">
              <div className='text-center'  style={{width:'100%'}}><h5 /* style={{textAlign:'center'}} */  id="exampleModalLiveLabel">
                  ¿Desea confirmar la actualización del almacén de {warehouse.value?warehouse.value.city.name:''}?
                </h5>
              </div>
                <button aria-label="Close" className="close" type="button" 
                        onClick={this.handlerCloseEditModal}>
                  <span aria-hidden={true}>×</span>
                </button>
              </div>
              <Col  sm="12" md={{ size: 8, offset: 2 }}>
                <div className="modal-footer">
                  <Button
                    color="danger"
                    type="button"
                    size="lg"
                    onClick={this.handlerCloseEditModal}>
                    Cancelar
                  </Button>
                  <Button
                    color="primary"
                    type="button"
                    size="lg"
                    onClick={this.editWarehouse}>
                    Aceptar
                  </Button>
                </div>
              </Col>
            </Modal>
            </Col> 
            </Card>  
          </Container>
      </Container>
    
    </div>       
    </>
  );
  }
}

export default EditWarehouse;
