package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table (name = "Customer")
@Entity
public class Customer {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idCustomer")
    private Long id;

    private String user;
    private String password;
    private String mail;
    private String phone;
    private String phonePrefix;
    private boolean infoEmail;
    private boolean infoPhone;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPerson")
    private Person person;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idCustomerStatus")
    private CustomerStatus customerStatus;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    private List<Shipping> shippings = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idAddress")
    private Address address;

    protected Customer(){}

    public Customer(Long id, String user, String password, String mail, String phone, String phonePrefix, boolean infoEmail, boolean infoPhone, Person person, CustomerStatus customerStatus, List<Shipping> shippings, Address address) {
        this.id = id;
        this.user = user;
        this.password = password;
        this.mail = mail;
        this.phone = phone;
        this.phonePrefix = phonePrefix;
        this.infoEmail = infoEmail;
        this.infoPhone = infoPhone;
        this.person = person;
        this.customerStatus = customerStatus;
        this.shippings = shippings;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public CustomerStatus getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(CustomerStatus customerStatus) {
        this.customerStatus = customerStatus;
    }

    public List<Shipping> getShippings() {
        return shippings;
    }

    public void setShippings(List<Shipping> shippings) {
        this.shippings = shippings;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean isInfoEmail() {
        return infoEmail;
    }

    public void setInfoEmail(boolean infoEmail) {
        this.infoEmail = infoEmail;
    }

    public boolean isInfoPhone() {
        return infoPhone;
    }

    public void setInfoPhone(boolean infoPhone) {
        this.infoPhone = infoPhone;
    }
}
