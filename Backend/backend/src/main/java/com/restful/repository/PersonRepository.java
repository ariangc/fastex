package com.restful.repository;

import com.restful.entity.Country;
import com.restful.entity.DocumentType;
import com.restful.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("personRepository")
public interface PersonRepository extends JpaRepository<Person, Integer> {
    public abstract List<Person> findAll();
    public abstract List<Person> findById(Long id);
    public abstract List<Person> findByDocument(String document);
    public abstract List<Person> findByDocumentAndCountryAndDocumentType(String document, Country country, DocumentType documentType);
}
