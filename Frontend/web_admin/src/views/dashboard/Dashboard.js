import React from "react";

// reactstrap components
import {
  Row,
  Col,
  Form,
  FormGroup,
  Container,
  Input,
  Card,
  Label,
} from "reactstrap";
import IconButton from '@material-ui/core/IconButton';
import Search from '@material-ui/icons/Search';
import {apiURL,currentUser,originWarehouse} from "variables/Variables.js"
// core components
import SideBar from 'components/sidebar/SideBar.js';
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import classNames from 'classnames';
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import {BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, 
    Tooltip, Legend, LineChart, Line} from 'recharts';
import {PassThrough} from 'stream';
import packages from './packages.svg';
import warehouse from './warehouse.svg'

const data = [{"day":"10/07/2020","totalGood":10,"total":30,"referencia":40},
                {"day":"11/07/2020","totalGood":20,"total":50,"referencia":40},
                {"day":"12/07/2020","totalGood":30,"total":50,"referencia":40}, 
                {"day":"13/07/2020","totalGood":30,"total":30,"referencia":40},
                {"day":"14/07/2020","totalGood":28,"total":50,"referencia":40},
                {"day":"15/07/2020","totalGood":40,"total":40,"referencia":40}]

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.handleWarehouseChange = this.handleWarehouseChange.bind(this);


    this.state = {
        warehouseSelected:'',
        warehouses:[],
        isLoading:true,
        response:null,
        warehouse:{},
        graphic:null,
        ampliationDate:"",
        warehousesList:[]
    }
}

handleWarehouseChange(event){
    this.setState({
      warehouseSelected: event.target.value
    }, ()=>{
      var body={'icaoCode':this.state.warehouseSelected}
      axios.post(apiURL.value + 'warehouse/getHistory',body).then((res)=>{
      if(res.status==200){
        console.log(res.data.history)
        this.setState({graphic:res.data.history})
      }
      })
      axios.post(apiURL.value + 'warehouse/predict',body).then((res)=>{
        if(res.status==200){
          console.log(res.data)
          this.setState({ampliationDate:res.data.prediction})
        }
      })
    })   
    
    this.state.warehousesList.map((warehouse) => {
      if(warehouse.airportCode == event.target.value){
        console.log(warehouse)
        var body={"idCity":parseInt(warehouse.id)}
        axios.post(apiURL.value + 'warehouse/getByCity',body).then((res)=>{
          if(res.status==200){
            if(res.data.warehouse[0]){
              this.setState({warehouse:res.data.warehouse[0]},() => {
                console.log(res.data.warehouse[0])
              })
            }else{
              toast.error('El almacén se encuentra inactivo', { type: toast.TYPE.ERROR, autoClose:false})
              this.setState({warehouse:{}})
            }
          }
          
        })        
      }
    })
    
}

componentDidMount(){
  var body={"idCity":parseInt(originWarehouse.id)}
    axios.post(apiURL.value + 'warehouse/getByCity',body).then((res)=>{
      if(res.status==200){
        if(res.data.warehouse[0]){
          this.setState({warehouse:res.data.warehouse[0]},() => {
            console.log(res.data.warehouse[0])
          })
        }else{
          toast.error('El almacén se encuentra inactivo', { type: toast.TYPE.ERROR, autoClose: false})
          this.setState({warehouse:{}})
        }
      }
      
    })
    console.log(originWarehouse.icao)
    var body={'icaoCode':originWarehouse.icao}
    axios.post(apiURL.value + 'warehouse/getHistory',body).then((res)=>{
      if(res.status==200){
        console.log(res.data.history)
        this.setState({graphic:res.data.history})
      }
    })

    var body={'icaoCode':originWarehouse.icao}
    axios.post(apiURL.value + 'warehouse/predict',body).then((res)=>{
      if(res.status==200){
        console.log(res.data)
        this.setState({ampliationDate:res.data.prediction})
      }
    })


}

componentWillMount(){ 
  if(currentUser.role == "Administrador"){
    axios.post(apiURL.value + 'city/list').then((res)=>{
    if(res.status==200){
      console.log(res.data)
      var cities = res.data.map((city, index)=>
        <option value={city.airportCode}>{city.name}</option>
      )
      this.setState({
        warehouses:cities,
        warehousesList:res.data
      })
    }
    })
  }
    
  
}

render(){
  return (
    <>
     <div className="App">
        <SideBar fluid toggle={false} isOpen={true}/>
        <Container fluid className={classNames('content', {'is-open': true})} style={{backgroundColor:'#FAFAFA'}}>
            <NavbarRedexAdmin />
            <Container style={{padding:'0%', marginTop:'10%', backgroundColor:'#FAFAFA'}}>
            <ToastContainer />
          <Col>
              <Row>
                <h3 className="stepTitle">Dashboard</h3>
              </Row>
              <Row>
                <Col sm={{ size: 6, offset:3}} style={{textAlign:"center"}}>
                  <Row>
                  <img src={packages} width="25" height="25"/>
                  <label style={{fontSize:"18px",fontWeight:"bold"}}>Capacidad ocupada / Capacidad total:&nbsp;</label>
                  <label style={{fontSize:"18px"}}>{this.state.warehouse.filledCapacity}/{this.state.warehouse.totalCapacity}</label> <br></br>
                  {this.state.ampliationDate != ""?<div><img src={warehouse} width="25" height="25"/><label style={{fontSize:"18px",fontWeight:"bold"}}>El almacén requiere ampliación el día:&nbsp;</label>
                  <label style={{fontSize:"18px"}}>{this.state.ampliationDate}</label></div>:PassThrough}
                  </Row>
                    {currentUser.role == "Administrador"?
                        <FormGroup style={{marginTop:'5%',marginBottom:'7%'}}>
                        <label htmlFor="exampleFormControlSelect1">Almacén</label>
                        <Input id="countrySelect" type="select" onChange={this.handleWarehouseChange}>
                            <option disabled value={0} selected>Seleccionar</option>
                            {this.state.warehouses}
                        </Input>
                    </FormGroup>:PassThrough}
                </Col>
                <LineChart
                    width={window.innerWidth-250-window.innerWidth*0.07}
                    height={400}
                    data={this.state.graphic}
                    margin={{
                        top: 20, bottom: 80,
                    }}
                    >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="arrivalDate" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Line type="monotone" name='Recepcionados' dataKey="totalGood" stroke="#4c9141" activeDot={{ r: 8 }} />
                    <Line type="monotone" name='No recepcionados' dataKey="totalBad" stroke="#B20004" />
                    {/* <Line type="monotone" name='Límite' dataKey="referencia" stroke="#B20004" /> */}
                </LineChart>  
              </Row>  
          </Col>
          </Container>
    </Container>
                              
  
    </div>       
    </>
  );
  }
}

export default Dashboard;
