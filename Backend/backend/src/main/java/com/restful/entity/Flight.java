package com.restful.entity;

import javax.persistence.*;

@Table(name = "Flight")
@Entity
public class Flight {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idFlight")
    private Long id;

    String departureTime; //Change both in DBD
    String landingTime;
    String actualLandingTime;
    String description;

    @ManyToOne
    @JoinColumn(name = "id_start_city")
    private City startCity;

    @ManyToOne
    @JoinColumn(name = "id_end_city")
    private City endCity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idFlightStatus")
    private FlightStatus flightStatus;

    protected Flight() {}


    public Flight(Long id, String departureTime, String landingTime, String actualLandingTime, String description, City startCity, City endCity, FlightStatus flightStatus) {
        this.id = id;
        this.departureTime = departureTime;
        this.landingTime = landingTime;
        this.actualLandingTime = actualLandingTime;
        this.description = description;
        this.startCity = startCity;
        this.endCity = endCity;
        this.flightStatus = flightStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getLandingTime() {
        return landingTime;
    }

    public void setLandingTime(String landingTime) {
        this.landingTime = landingTime;
    }

    public String getActualLandingTime() {
        return actualLandingTime;
    }

    public void setActualLandingTime(String actualLandingTime) {
        this.actualLandingTime = actualLandingTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public City getStartCity() {
        return startCity;
    }

    public void setStartCity(City startCity) {
        this.startCity = startCity;
    }

    public City getEndCity() {
        return endCity;
    }

    public void setEndCity(City endCity) {
        this.endCity = endCity;
    }

    public FlightStatus getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(FlightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }
}
