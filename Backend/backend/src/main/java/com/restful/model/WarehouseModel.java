package com.restful.model;

import com.restful.entity.City;
import com.restful.entity.Warehouse;
import com.restful.entity.WarehouseStatus;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class WarehouseModel {
    private Long id;
    private int totalCapacity;
    private int filledCapacity;
    private int capacityLimitPercentage;
    private int totalBad;
    private String icaoCode;
    private String iataCode;
    private CityModel city;
    private WarehouseStatusModel warehouseStatus;

    public WarehouseModel(Long id, int totalCapacity, int filledCapacity, int capacityLimitPercentage, int totalBad, String icaoCode, String iataCode) {
        this.id = id;
        this.totalCapacity = totalCapacity;
        this.filledCapacity = filledCapacity;
        this.capacityLimitPercentage = capacityLimitPercentage;
        this.totalBad = totalBad;
        this.icaoCode = icaoCode;
        this.iataCode = iataCode;
    }

    public WarehouseModel(Warehouse a){
        this.id = a.getId();
        this.totalCapacity = a.getTotalCapacity();
        this.filledCapacity = a.getFilledCapacity();
        this.capacityLimitPercentage = a.getCapacityLimitPercentage();
        this.totalBad = a.getTotalBad();
        this.icaoCode = a.getIcaoCode();
        this.iataCode = a.getIataCode();
        this.city = new CityModel(a.getCity());
        this.warehouseStatus = new WarehouseStatusModel(a.getWarehouseStatus());
    }

    public int getTotalBad() {
        return totalBad;
    }

    public void setTotalBad(int totalBad) {
        this.totalBad = totalBad;
    }

    public WarehouseModel(){ super(); }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTotalCapacity() {
        return totalCapacity;
    }

    public void setTotalCapacity(int totalCapacity) {
        this.totalCapacity = totalCapacity;
    }

    public int getFilledCapacity() {
        return filledCapacity;
    }

    public void setFilledCapacity(int filledCapacity) {
        this.filledCapacity = filledCapacity;
    }

    public int getCapacityLimitPercentage() {
        return capacityLimitPercentage;
    }

    public void setCapacityLimitPercentage(int capacityLimitPercentage) {
        this.capacityLimitPercentage = capacityLimitPercentage;
    }

    public String getIcaoCode() {
        return icaoCode;
    }

    public void setIcaoCode(String icaoCode) {
        this.icaoCode = icaoCode;
    }

    public String getIataCode() {
        return iataCode;
    }

    public void setIataCode(String iataCode) {
        this.iataCode = iataCode;
    }

    public CityModel getCity() {
        return city;
    }

    public void setCity(CityModel city) {
        this.city = city;
    }

    public WarehouseStatusModel getWarehouseStatus() {
        return warehouseStatus;
    }

    public void setWarehouseStatus(WarehouseStatusModel warehouseStatus) {
        this.warehouseStatus = warehouseStatus;
    }
}
