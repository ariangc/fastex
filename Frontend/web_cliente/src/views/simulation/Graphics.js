import React from "react";

// reactstrap components

import {
  Button,
  CustomInput,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormText,
  Label,
  Card
} from "reactstrap";
import { Link } from "react-router-dom";
import {currentUser, shippingList,
  apiURL} from "variables/Variables.js"
// core components
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import SideBar from 'components/sidebar/SideBar.js';
import MaterialTable from "material-table";
import axios from "axios";
import './Simulation.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {PassThrough} from 'stream';
import classNames from 'classnames';
import {BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';

const data = 
    {"SPIM":[{"arrivalDate": '22/04/2020', "totalGood": 9, "totalBad":8}, 
{"arrivalDate": '23/04/2020', "totalGood": 31, "totalBad":5}, 
{"arrivalDate": '24/04/2020', "totalGood": 5, "totalBad":23}, 
{"arrivalDate": '25/04/2020', "totalGood": 16, "totalBad":7}, 
{"arrivalDate": '26/04/2020', "totalGood": 29, "totalBad":32}, 
{"arrivalDate": '27/04/2020', "totalGood": 21, "totalBad":25}, 
{"arrivalDate": '28/04/2020', "totalGood": 45, "totalBad":41}, 
{"arrivalDate": '29/04/2020', "totalGood": 24, "totalBad":15}, 
{"arrivalDate": '30/04/2020', "totalGood": 7, "totalBad":5}, 
{"arrivalDate": '01/05/2020', "totalGood": 10, "totalBad":27}], 
"SBBR":[{"arrivalDate": '22/04/2020', "totalGood": 36, "totalBad":44}, 
{"arrivalDate": '23/04/2020', "totalGood": 15, "totalBad":15}, 
{"arrivalDate": '24/04/2020', "totalGood": 32, "totalBad":36}, 
{"arrivalDate": '25/04/2020', "totalGood": 19, "totalBad":32}, 
{"arrivalDate": '26/04/2020', "totalGood": 16, "totalBad":6}, 
{"arrivalDate": '27/04/2020', "totalGood": 32, "totalBad":47}, 
{"arrivalDate": '28/04/2020', "totalGood": 35, "totalBad":41}, 
{"arrivalDate": '29/04/2020', "totalGood": 34, "totalBad":44}, 
{"arrivalDate": '30/04/2020', "totalGood": 33, "totalBad":2}, 
{"arrivalDate": '01/05/2020', "totalGood": 19, "totalBad":21}], 
"SKBO":[{"arrivalDate": '22/04/2020', "totalGood": 35, "totalBad":5}, 
{"arrivalDate": '23/04/2020', "totalGood": 30, "totalBad":10}, 
{"arrivalDate": '24/04/2020', "totalGood": 50, "totalBad":20}, 
{"arrivalDate": '25/04/2020', "totalGood": 28, "totalBad":6}, 
{"arrivalDate": '26/04/2020', "totalGood": 3, "totalBad":47}, 
{"arrivalDate": '27/04/2020', "totalGood": 20, "totalBad":37}, 
{"arrivalDate": '28/04/2020', "totalGood": 26, "totalBad":19}, 
{"arrivalDate": '29/04/2020', "totalGood": 46, "totalBad":50}, 
{"arrivalDate": '30/04/2020', "totalGood": 28, "totalBad":7}, 
{"arrivalDate": '01/05/2020', "totalGood": 40, "totalBad":48}], 
"SBBR":[{"arrivalDate": '22/04/2020', "totalGood": 43, "totalBad":46}, 
{"arrivalDate": '23/04/2020', "totalGood": 19, "totalBad":30}, 
{"arrivalDate": '24/04/2020', "totalGood": 36, "totalBad":16}, 
{"arrivalDate": '25/04/2020', "totalGood": 25, "totalBad":25}, 
{"arrivalDate": '26/04/2020', "totalGood": 6, "totalBad":29}, 
{"arrivalDate": '27/04/2020', "totalGood": 20, "totalBad":40}, 
{"arrivalDate": '28/04/2020', "totalGood": 15, "totalBad":23}, 
{"arrivalDate": '29/04/2020', "totalGood": 13, "totalBad":39}, 
{"arrivalDate": '30/04/2020', "totalGood": 33, "totalBad":45}, 
{"arrivalDate": '01/05/2020', "totalGood": 39, "totalBad":19}], 
"UMMS":[{"arrivalDate": '22/04/2020', "totalGood": 28, "totalBad":21}, 
{"arrivalDate": '23/04/2020', "totalGood": 36, "totalBad":15}, 
{"arrivalDate": '24/04/2020', "totalGood": 17, "totalBad":18}, 
{"arrivalDate": '25/04/2020', "totalGood": 16, "totalBad":46}, 
{"arrivalDate": '26/04/2020', "totalGood": 46, "totalBad":1}, 
{"arrivalDate": '27/04/2020', "totalGood": 25, "totalBad":8}, 
{"arrivalDate": '28/04/2020', "totalGood": 45, "totalBad":32}, 
{"arrivalDate": '29/04/2020', "totalGood": 3, "totalBad":20}, 
{"arrivalDate": '30/04/2020', "totalGood": 41, "totalBad":17}, 
{"arrivalDate": '01/05/2020', "totalGood": 48, "totalBad":19}] 


    };

class Graphics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      icao:''
    }
  }
  componentWillMount(){

  }
  render(){
  return (
    <>
    
    <div className="App">
      
      <SideBar fluid toggle={this.sidebarIsClose} isOpen={true}/>
      
      <Container fluid className={classNames('content', {'is-open': true})}>
        <NavbarRedexAdmin style={{position:'fixed', marginTop:'0'}}/>
        <Container style={{marginTop:'10%', backgroundColor:'#FAFAFA'}}>
          <ToastContainer />
          <Card>
            <Col>
              <div className='text-center'><h4>Simulación</h4></div>
              <FormGroup>
                <label htmlFor="exampleFormControlSelect1">Almacén</label>
                <Input id="countrySelect" type="select" onChange={this.handleCountryChange}>
                    <option disabled value={0} selected>Seleccionar</option>
                    {this.state.countries}
                </Input>
              </FormGroup>
              <BarChart
                width= {window.innerWidth-250-window.innerWidth*0.07}
                height={300}
                data={data["SPIM"]}
                margin={{
                top: 20, bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="arrivalDate" />
                <YAxis yAxisId="left" orientation="left" stroke="#8884d8" />
                
                <Tooltip />
                <Legend />
                <Bar yAxisId="left" name='Recepcionados' dataKey="totalGood" fill="#8884d8" />
                <Bar yAxisId="left" name='No Recepcionados' dataKey="totalBad" fill="#82ca9d" />
            </BarChart>
            </Col>
            
          </Card>   
        </Container>
      </Container>
  
    </div>       
    </>
  );
  }
}

export default Graphics;
