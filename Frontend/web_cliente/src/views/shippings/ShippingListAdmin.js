import React from "react";

// reactstrap components
import { Grid } from "react-bootstrap";
import {
  Button,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormText,
  Label,
  Card,
  CardTitle,
  Modal
} from "reactstrap";
import {Tabs} from 'react-bootstrap';
import {Tab} from 'react-bootstrap';
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import HelpOutline from '@material-ui/icons/HelpOutline';
import PublishIcon from '@material-ui/icons/Publish';
import classNames from 'classnames';
import {currentUser,shippingUpload,
  apiURL} from "variables/Variables.js"
// core components
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import SideBar from 'components/sidebar/SideBar.js';
import MaterialTable from "material-table";
import axios from "axios";
import './Shipping.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const data = [];
const columns = [
{
  title: 'N°',
  field: 'id'
},
{
  title: 'Remitente',
  field: 'customer.person.names'
},
{
  title: 'Destinatario',
  field: 'receiver.person.names'
}, 
{
  title: 'Origen',
  field: 'person.surnames'
},
{
  title: 'Destino',
  field: 'person.country.name'
},
{
  title: 'Tracking',
  field: 'trackingCode'
},
{
  title: 'Estado',
  field: 'shippingStatus.description'
}
];

class ShippingListAdmin extends React.Component {
  constructor(props) {
    super(props);
    this.sidebarIsOpen = this.sidebarIsOpen.bind(this);
    this.sidebarIsClose = this.sidebarIsClose.bind(this);
    this.togglerADName = this.togglerADName.bind(this);
    this.handlerCloseEditModal = this.handlerCloseEditModal.bind(this);
    this.handlerShippingUpload = this.handlerShippingUpload.bind(this);
    this.state = {
      selectedRow: null,
      data:[],
      sidebarOpen:true,
      modalOpen:false,
      clientDeleteNames:'',
      shippings:[]
    }
}
handlerShippingUpload(){
  this.props.history.push("/"+shippingUpload.url);
}
handlerCloseEditModal(){
  this.setState({
    modalOpen:false
  })
}
togglerADName(event){
  this.setState({
    ADName:event.target.value
  })
}
sidebarIsClose(){
  this.setState({
    sidebarOpen:!this.state.sidebarOpen
  })
}
sidebarIsOpen(){
  this.setState({
    sidebarOpen:true
  })
}
componentWillMount(){
  /* if(!currentUser.logged){
    this.props.history.push("");
  } */
  console.log(currentUser.role)
  axios.post(apiURL.value + 'shipping/list').then((res)=>{
    console.log(res.data)
    this.setState({
      shippings:res.data['shippings']
    })
  })
}
  render(){
  return (
    <>
    <div className="App">
      <SideBar fluid toggle={this.sidebarIsClose} isOpen={this.state.sidebarOpen}/>  
      <Container
      fluid className={classNames('content', {'is-open': this.state.sidebarOpen})}>
      <NavbarRedexAdmin style={{position:'fixed', marginTop:'0'}}/>
        <Container style={{marginTop:'10%', backgroundColor:'#FAFAFA'}}>
          <ToastContainer />
          
          <Tabs defaultActiveKey="envios" id="uncontrolled-tab-example">
            
            <Tab eventKey="envios" title="Entrantes">
            <Button style={{marginRight:'1%', marginTop:'-6.5%', paddingLeft:'2%', paddingRight:'2%'}}
            className="float-right"
                    color="primary"
                    type="button"
                    size="lg"
                    onClick={this.handlerShippingUpload}
                  >
                  <PublishIcon/>  Carga masiva
                  </Button>
            <Col> 
                    <MaterialTable
                    
                      title="Envíos"
                      data={this.state.shippings}
                      columns={columns}
                      onRowClick={(evt, selectedRow) => {
                        (evt.target).ondblclick = () =>{this.setState({selectedRow}, ()=>{console.log(this.state.selectedRow)})
                        toast.error('Ha seleccionado a ' + selectedRow.person.names, { type: toast.TYPE.ERROR, autoClose: 5000 });}
                        }}
                      options={{actionsColumnIndex: -1,filtering:true,
                        rowStyle: rowData => ({
                          backgroundColor: (this.state.selectedRow && this.state.selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
                        })
                      }}
                      localization={{
                        toolbar: {
                          searchPlaceholder: "Buscar"
                        },
                        body: {
                          emptyDataSourceMessage: 'No hay datos'
                        }
                      }}
                    />
                  </Col>
            </Tab>
            </Tabs>
            
              
          </Container>
      </Container>
  
    </div>       
    </>
  );
  }
}

export default ShippingListAdmin;
