import React from "react";

// reactstrap components
import {
  Button,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  FormGroup,
  Card,
  Row,
  Col
} from "reactstrap";
import {login,apiURL,shipping} from "variables/Variables.js"
import { Link } from "react-router-dom";
// core components
import ExamplesNavbar from "components/Navbars/ExamplesNavbar.js";
import HeaderConstrunet from "components/Headers/HeaderConstrunet.js";
import FooterConstrunet from "components/Footers/FooterConstrunet.js";
import IconButton from '@material-ui/core/IconButton';
import Search from '@material-ui/icons/Search';
import AccessTime from '@material-ui/icons/AccessTime';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import axios from "axios";
import history from "../clients/history";

class LandingPage extends React.Component {
  constructor(props) {
    super(props);
    this.togglerTrackingCode = this.togglerTrackingCode.bind(this);
    this.searchShipping = this.searchShipping.bind(this);
    this.quoteShipping = this.quoteShipping.bind(this);
    this.queryTimes = this.queryTimes.bind(this);

    this.state = {
      trackingCode:'',
      shipping:null
    }
}

  togglerTrackingCode(event){
    this.setState({trackingCode:event.target.value})
  }

  quoteShipping(){
    this.props.history.push({pathname: '/quote-shipping'})
  }

  queryTimes(){
    this.props.history.push({pathname: '/query-times'})
  }

  searchShipping(){
    var body={trackingCode:this.state.trackingCode}
    axios.post(apiURL.value + 'shipping/getByTrackingCode', body).then((res)=>{
      if(res.status === 200){
        console.log(res.data.shipping[0])
        if(res.data.shipping[0]){
          this.setState({shipping:res.data.shipping[0]},()=>{
            this.props.history.push({
              pathname : '/shipping-detail',
                state :{
                  shipping: this.state.shipping
                }
            });
            })
        }else{
          this.props.history.push({pathname: '/shipping-not-found'})
        }        
      }
    }).catch(error => {
                  console.log(error)
                 })
  }
  
  componentWillMount(){
    console.log("landing")
  }
  render(){
  return (
    <>
      <main ref="main">
      <div className="wrapper" >
        <HeaderConstrunet/>
        <div className="section section-team text-center"  style={{backgroundSize: "cover",backgroundImage: "url(" + require("assets/img/fondo2.jpg") + ")"}}>
        <div className="container-fluid">
            <Row>
                <Col sm={{ size:12}}>
                  <Card style={{height: "18vh", width: "22vh"}}>
                    <IconButton 
                        color='primary' 
                        component="span"
                        onClick={this.queryTimes}
                      >
                        <AccessTime fontSize="large" style={{color:"#03256C"}}/>
                    </IconButton >
                    <p className="description" style={{color:"#000000"}}>
                      Tiempos
                    </p>
                  </Card>

                  <Card style={{backgroundColor: "#03256C", height: "20vh", width: "22vh",paddingTop:"2vh"}}>
                    <IconButton 
                        color='primary' 
                        component="span"
                      >
                        <Search fontSize="large" style={{color:"#ffffff"}}/>
                    </IconButton >
                    <p className="description" style={{color:"#ffffff"}}>
                      Rastrear
                    </p>
                  </Card>

                  <Card style={{height: "18vh", width: "22vh"}}>
                    <IconButton 
                        color='primary' 
                        component="span"
                        onClick={this.quoteShipping}
                      >
                        <MonetizationOnIcon fontSize="large" style={{color:"#03256C"}}/>
                    </IconButton >
                    <p className="description" style={{color:"#000000"}}>
                      Cotizar
                    </p>
                  </Card>
              </Col>
              </Row>
              <Row>
                <Col sm={{ size: 4, offset: 4 }}>
                  <FormGroup >
                    
                    <Input
                      id="trackingCode"
                      placeholder="Ingrese el número de rastreo"
                      type="text"
                      onChange = {this.togglerTrackingCode}
                      value = {this.state.trackingCode}
                      style={{backgroundColor: "#ffffff", height:"7vh", fontSize:"16px"}}
                    ></Input>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Button
                    size="lg"
                    variant="contained"
                    color="primary"
                    onClick={this.searchShipping}
                    style={{fontSize:"16px"}}
                    >Buscar
                  </Button>
                </Col>
              </Row>
        </div>
        </div>
        <FooterConstrunet/>
      </div>
    </main>
    </>
  );
  }
}

export default LandingPage;
