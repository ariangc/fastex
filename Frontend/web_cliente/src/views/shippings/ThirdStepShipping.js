import React from "react";

// reactstrap components
import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Button,
  Modal
} from "reactstrap";
import IconButton from '@material-ui/core/IconButton';
import Search from '@material-ui/icons/Search';
import {apiURL} from "variables/Variables.js"
// core components
import SideBar from 'components/sidebar/SideBar.js';
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './Shipping.css';
import MaterialTable from "material-table";
import Tooltip from '@material-ui/core/Tooltip'
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import HelpOutline from '@material-ui/icons/HelpOutline';

const columns = [
  {
    title: 'Tipo',
    field: 'type'
  },
  {
    title: 'Descripción',
    field: 'description'
  }
  ];
class ThirdStepShipping extends React.Component {
  constructor(props) {
    super(props);
    this.handlerRegisterModalOpen = this.handlerRegisterModalOpen.bind(this);
    this.handlerCloseModal = this.handlerCloseModal.bind(this);
    this.handlerEditModalOpen = this.handlerEditModalOpen.bind(this);
    this.handlerDeleteModalOpen = this.handlerDeleteModalOpen.bind(this);
    this.togglerType = this.togglerType.bind(this);
    this.togglerDescription = this.togglerDescription.bind(this);
    this.togglerTypeEdit = this.togglerTypeEdit.bind(this);
    this.togglerDescriptionEdit = this.togglerDescriptionEdit.bind(this);
    this.handlerAddPackage = this.handlerAddPackage.bind(this);
    this.handlerEditPackage = this.handlerEditPackage.bind(this);    
    this.handlerDeletePackage = this.handlerDeletePackage.bind(this);
    this.state = {
      selectedRow:null,
      packageID:0,
      package: {},
      editModalOpen:false,
      deleteModalOpen:false,
      registerModalOpen:false,
      type:"",
      description:"",
      newType:"",
      newDescription:"",
      data:[],
      count:0,
    }
}

handlerRegisterModalOpen(){
  this.setState({registerModalOpen:true})
}
handlerEditModalOpen(){
  this.setState({editModalOpen:true})
}
handlerDeleteModalOpen(){
  this.setState({deleteModalOpen:true})
}
handlerCloseModal(){
  this.setState({editModalOpen:false,deleteModalOpen:false,
    registerModalOpen:false});  
}

togglerType(event){
  this.setState({type:event.target.value});
}

togglerDescription(event){
  this.setState({
    description:event.target.value
  })
}

togglerTypeEdit(event){
/*   var newRow = this.state.selectedRow
  newRow.type = event.target.value */
  this.setState({newType:event.target.value});
}

togglerDescriptionEdit(event){
/*   var newRow = this.state.selectedRow
  newRow.description = event.target.value */
  this.setState({newDescription:event.target.value});
}

handlerAddPackage(){
  const countAux = this.state.count + 1
  const dataAux = this.state.data
  var aux = {
    id: countAux,
    type: this.state.type,
    active: true,
    description:this.state.description,
    shipping:{id:null}}
  dataAux.push(aux)

  this.setState({count: countAux, type:"", description:"",
    data: dataAux,
    registerModalOpen:false},() => this.props.packageShippingFunction(this.state.data));  
}

handlerEditPackage(){
  console.log(this.state.selectedRow)
  var newData = this.state.data
  var newRows = []
  newData.forEach((row)=>{
    if(row.id==this.state.selectedRow.id){
      row.type = this.state.newType
      row.description = this.state.newDescription
    }
    newRows.push(row)
  })
  this.setState({editModalOpen:false,data:newRows})
}

handlerDeletePackage(){
  var newData = this.state.data
  var newRows = []
  newData.forEach((row)=>{
    if(row.id!=this.state.selectedRow.id){
      newRows.push(row)
    }
  })
  const countAux = this.state.count - 1
  this.setState({data:newRows,deleteModalOpen:false,count:countAux})
}

componentWillMount(){

}

render(){
  return (
    <>
    <div className="App">
          <Col>
            <ToastContainer /> 
            <Row>
              <Col sm={{ size: 4}}>
                <h5>Registrar Paquetes</h5>
                <Row>
                  <Col>
                  <FormGroup>
                    <label htmlFor="exampleFormControlSelect1">Cantidad</label>
                    <Input id="countitive" type="text" readonly ="readonly" value = {this.state.count}>                   
                    </Input>
                  </FormGroup>
                  </Col>
                  <Col style= {{ marginTop: '6%'}}>
                    <IconButton style ={{ color: '#1768AC' }} onClick={this.handlerRegisterModalOpen} component = "span">
                      <AddCircleIcon fontSize= "large" />
                    </IconButton>
                  </Col>
                </Row>
              </Col>
            </Row>            
            <MaterialTable
                      title="Paquetes"
                      data={this.state.data}
                      columns={columns}
                      actions={[
                        {
                          icon:'edit',
                          iconButton:{color:'red'},
                          color:'red',
                          tooltip: 'Save User'/* ,
                          onClick: (event, rowData) => alert("You saved " + rowData.name) */
                        },
                        {
                          icon: 'delete',
                          root: 'blue',
                          tooltip: 'Delete User'/* ,
                          onClick: (event, rowData) => confirm("You want to delete " + rowData.name),
                          disabled: rowData.birthYear < 2000 */
                        }
                      ]}
                      components={{
                        Action: 
                          props => {
                            if(props.action.icon === 'edit'){
                              return(
                                <Tooltip title='Editar Paquete'>
                                  <IconButton 
                                  style={{color:'#0074D9'}} 
                                  component="span"
                                  size = 'small'
                                  onClick = {(event, rowData) => {
                                    this.setState({selectedRow:props.data, editModalOpen:true,newType:props.data.type,newDescription:props.data.description})
                                  }}
                                  >
                                  <Edit fontSize="large"/>
                                  </IconButton >
                                </Tooltip>
                              )
                            }
                            if(props.action.icon === 'delete'){
                              return(
                                <Tooltip title='Eliminar Paquete'>
                                  <IconButton 
                                  style={{color:'#FF4136'}}
                                  component="span"
                                  onClick = {(event, rowData) => {this.setState({selectedRow:props.data,
                                    deleteModalOpen:true
                                  })}}
                                  >
                                    <Delete fontSize="large"/>
                                  </IconButton >
                                </Tooltip>
                              )
                            }
                          }
                      }}
                      options={{actionsColumnIndex: -1,filtering:true,
                        rowStyle: rowData => ({
                          //backgroundColor: (this.state.selectedRow && this.state.selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
                        })
                      }}
                      localization={{
                        header:{
                          actions:'Acciones'
                        },
                        body: {
                          emptyDataSourceMessage: 'No hay datos'
                        }
                      }}
                    />
          </Col>
          <Modal toggle={this.handlerCloseModal} isOpen={this.state.deleteModalOpen}>
            <Col  sm="12" md={{ size: 5, offset: 4 }}>
              <HelpOutline style={{ fontSize: 100, color:'#1768AC' }}/>
            </Col>
              <div className="modal-header">
              <div className='text-center'  style={{width:'100%'}}><h5 id="exampleModalLiveLabel">
                ¿Desea eliminar el registro del paquete?
                </h5>
              </div>
                <button
                  aria-label="Close"
                  className="close"
                  type="button"
                  onClick={this.handlerCloseModal}
                >
                  <span aria-hidden={true}>×</span>
                </button>
                </div>
              <Col  sm="12" md={{ size: 8, offset: 2 }}>
              <div className="modal-footer">
                <Button
                  color="danger"
                  type="button"
                  size="lg"
                  onClick={this.handlerCloseModal}
                >
                  Cancelar
                </Button>
                <Button
                  color="primary"
                  type="button"
                  size="lg"
                  onClick={this.handlerDeletePackage}
                >
                  Aceptar
                </Button>
              </div>
              </Col>
          </Modal> 
          <Modal toggle={this.handlerCloseModal} isOpen={this.state.editModalOpen}>
            <Col>
              <div className="modal-header">
                <Col sm="12" md={{ size: 6, offset: 4}} >
                  <Row>
                    <h5 style={{textAlign:'center'}} className="modal-title" id="exampleModalLiveLabel">
                      Editar Paquete
                    </h5> 
                  </Row>
                </Col>
                </div>
                <Row>
                  <Col sm="12" md={{ size: 6}}>
                    <FormGroup>
                      <label htmlFor="exampleFormControlSelect1">Tipo</label>
                      <Input id="typeSelect" type="select" onChange={this.togglerTypeEdit} value={this.state.selectedRow?this.state.newType:''}>
                        <option disabled value={""} selected>Seleccione un Tipo</option>
                        <option value={"Frágil"} >Frágil</option>
                        <option value={"Pequeño"} >Pequeño </option>
                        <option value={"Grande"} >Grande</option>                     
                      </Input>
                    </FormGroup>
                    </Col>
                </Row>
                <Row>
                  <Col sm="12" md={{ size: 12}}>
                    <FormGroup>
                      <label htmlFor="exampleFormControlSelect1">Descripción</label>
                      <Input id="typeSelect" type="text" onChange={this.togglerDescriptionEdit} value={this.state.selectedRow?this.state.newDescription:''}>
                      </Input>
                    </FormGroup>
                    </Col>
                </Row>           
              </Col>
              <Col  sm="12" md={{ size: 8, offset: 2 }}>
              <div className="modal-footer">
                <Button
                  color="danger"
                  type="button"
                  size="lg"
                  onClick={this.handlerCloseModal}
                >
                  Cancelar
                </Button>
                <Button
                  color="primary"
                  type="button"
                  size="lg"
                  onClick={this.handlerEditPackage}
                >
                  Aceptar
                </Button>
              </div>
            </Col>
          </Modal> 
          <Modal toggle={this.handlerCloseModal} isOpen={this.state.registerModalOpen}>
            <Col>
              <div className="modal-header">
                <Col sm="12" md={{ size: 6, offset: 4}} >
                  <Row>
                    <h5 style={{textAlign:'center'}} className="modal-title" id="exampleModalLiveLabel">
                      Registrar Paquete
                    </h5>
                  </Row>
                </Col>
                </div>
                <Row>
                  <Col sm="12" md={{ size: 6}}>
                    <FormGroup>
                      <label htmlFor="exampleFormControlSelect1">Tipo</label>
                      <Input id="typeSelect" type="select" onChange={this.togglerType} value={this.state.type}>
                        <option disabled value={""} selected>Seleccione un Tipo</option>
                        <option value={"Frágil"} >Frágil</option>
                        <option value={"Pequeño"} >Pequeño </option>
                        <option value={"Grande"} >Grande</option>
                      </Input>
                    </FormGroup>
                    </Col>
                </Row>
                <Row>
                  <Col sm="12" md={{ size: 12}}>
                    <FormGroup>
                      <label htmlFor="exampleFormControlSelect1">Descripción</label>
                      <Input id="typeSelect" type="text" onChange={this.togglerDescription} value={this.state.description}>
                      </Input>
                    </FormGroup>
                    </Col>
                </Row>          

              </Col>
              <Col  sm="12" md={{ size: 8, offset: 2 }}>
              <div className="modal-footer">
                <Button
                  color="danger"
                  type="button"
                  size="lg"
                  onClick={this.handlerCloseModal}
                >
                  Cancelar
                </Button>
                <Button
                  color="primary"
                  type="button"
                  size="lg"
                  onClick={this.handlerAddPackage}
                >
                  Aceptar
                </Button>
              </div>
            </Col>
          </Modal> 
    </div>       
    </>
  );
  }
}

export default ThirdStepShipping;
