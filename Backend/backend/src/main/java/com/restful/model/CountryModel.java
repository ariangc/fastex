package com.restful.model;

import com.restful.entity.Country;

public class CountryModel {
    private Long id;
    private String name;
    private String code;
    private ContinentModel continent;

    public CountryModel(Long id, String name, String code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    public CountryModel(Country a){
        this.id = a.getId();
        this.name = a.getName();
        this.code = a.getCode();
        this.continent = new ContinentModel(a.getContinent());
    }

    public CountryModel(){
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ContinentModel getContinent() {
        return continent;
    }

    public void setContinent(ContinentModel continent) {
        this.continent = continent;
    }
}
