package com.restful.service;

import com.restful.entity.DocumentType;
import com.restful.entity.Person;
import com.restful.entity.Receiver;
import com.restful.model.ReceiverModel;
import com.restful.repository.PersonRepository;
import com.restful.repository.ReceiverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service("receiverService")
public class ReceiverService {
    @Autowired
    @Qualifier("personRepository")
    private PersonRepository personRepository;

    @Autowired
    @Qualifier("receiverRepository")
    private ReceiverRepository receiverRepository;

    public Map<String, ReceiverModel> createReceiver(Receiver receiver){
        Map<String, ReceiverModel> response = new HashMap<>();

        Person person = receiver.getPerson();
        List<Receiver> receiverList = receiverRepository.findByPerson(person);
        if(!receiverList.isEmpty()){
            response.put("receiver", new ReceiverModel(receiverList.get(0)));
            return response;
        }
        receiver = receiverRepository.save(receiver);
        response.put("receiver", new ReceiverModel(receiver));
        return response;
    }
}
