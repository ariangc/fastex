import React from "react";

// reactstrap components
import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
} from "reactstrap";
import IconButton from '@material-ui/core/IconButton';
import Search from '@material-ui/icons/Search';
import {apiURL} from "variables/Variables.js"
// core components
import SideBar from 'components/sidebar/SideBar.js';
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import Loader from 'react-loader-spinner'
import MaterialTable from "material-table";
import 'react-toastify/dist/ReactToastify.css';
import './Shipping.css';

const columns = [
  {
    title: 'Origen',
    field: 'origen'
  },
  {
    title: 'Destino',
    field: 'destino'
  },
  {
    title: 'F.Salida',
    field: 'fechaSalida'
  }, 
  {
    title: 'Vuelo',
    field: 'vuelo'
  }
  ];

class FourthStepShipping extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      flights:[],
      selectedRow:null,
      arrivalDate:"",
      price:150.00,
      route:null,
      loaderActive:true
    }
}


componentWillMount(){
  var body = {idStartCity:44,idEndCity:this.props.cityAddressee.id}
  axios.post(apiURL.value + 'algorithm/run',body).then((res)=>{
    console.log(res.data);
    if(res.status === 200){
      var flightsAux=[]
      res.data.route.forEach(flight => {
        var dictFlight = {origen:flight.flight.startCity.name,destino:flight.flight.endCity.name,
          fechaSalida:flight.startDate.toString().slice(0,10),vuelo:"PK" + flight.flight.id.toString()}
          flightsAux.push(dictFlight)
      });
      console.log(res.data.route[res.data.route.length-1])
      this.setState({loaderActive:false,route:res.data.route,flights:flightsAux,
        arrivalDate:res.data.route[res.data.route.length-1].startDate.toString().slice(0,10)},() =>{
          this.props.routeFunction(this.state.route)
        })
    }
  })
}

render(){
  return (
    <>
    <div className="App">
          <Col>
            <ToastContainer /> 
            <Form>
              <Row>
                  <h3 className="stepTitle">Ruta de Envío</h3>
              </Row>
              <Row>
                <Col sm={{ size: 3}}>
                  <FormGroup>
                    <label htmlFor="exampleFormControlSelect1">Origen</label>
                    <Input id="originCity" type="text" value= "Lima-Perú" readonly="readonly"></Input>  
                  </FormGroup>
                </Col>
                <Col sm={{ size: 3}}>
                <FormGroup>
                    <label htmlFor="exampleFormControlSelect1">Destino</label>
                    <Input id="destinityCity" type="text" value={this.props.cityAddressee.name+'-'+this.props.cityAddressee.country.name} readonly="readonly"></Input>  
                  </FormGroup>
                </Col>
                <Col sm={{ size: 3}}>
                  <FormGroup>
                    <label htmlFor="exampleFormControlSelect1">Fecha de llegada</label>
                    <Input id="arriveDate" type="text" value={this.state.arrivalDate} readonly="readonly"></Input>  
                  </FormGroup>
                </Col>
                <Col sm={{ size: 3}}>
                <FormGroup>
                    <label htmlFor="exampleFormControlSelect1">Costo</label>
                    <Input id="cost" type="text" value={this.props.packageShipping.length*50} readonly="readonly"></Input>  
                  </FormGroup>
                </Col>
              </Row>
              {this.state.loaderActive?
                <div className='text-center'  style={{width:'100%', weight:'100%', backgroundColor:'#FFFFFF'}}>
                  <Loader
                    type="ThreeDots"
                    color="#03256C"
                    height={100}
                    width={100}
                    timeout={3000000}
                  />
                </div>:<MaterialTable
                title="Hoja de Ruta"
                data={this.state.flights}
                columns={columns}
                onRowClick={(evt, selectedRow) => {
                  (evt.target).ondblclick = () =>{this.setState({selectedRow}, ()=>{console.log(this.state.selectedRow)})
                  }
                  }}
                options={{actionsColumnIndex: -1,filtering:true,
                  rowStyle: rowData => ({
                    backgroundColor: (this.state.selectedRow && this.state.selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
                  })
                }}
                localization={{
                  toolbar: {
                    searchPlaceholder: "Buscar"
                  },
                  body: {
                    emptyDataSourceMessage: 'No hay datos'
                  }
                }}
              />}
            </Form>
          </Col>
                              
  
    </div>       
    </>
  );
  }
}

export default FourthStepShipping;
