package com.restful.entity;

import javax.persistence.*;
import java.util.Date;

@Table(name = "Employee")
@Entity
public class Employee {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idEmployee")
    private Long id;

    private String user;
    private String password;
    private Date startDate;
    private Date endDate;
    private String email;
    private String phone;
    private String phonePrefix;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPerson")
    private Person person;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idRole")
    private Role role;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idCity")
    private City city;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idEmployeeStatus")
    private EmployeeStatus employeeStatus;

    protected Employee(){}

    public Employee(Long id, String user, String password, Date startDate, Date endDate, String email, String phone, String phonePrefix, Person person, Role role, City city, EmployeeStatus employeeStatus) {
        this.id = id;
        this.user = user;
        this.password = password;
        this.startDate = startDate;
        this.endDate = endDate;
        this.email = email;
        this.phone = phone;
        this.phonePrefix = phonePrefix;
        this.person = person;
        this.role = role;
        this.city = city;
        this.employeeStatus = employeeStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public EmployeeStatus getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmployeeStatus(EmployeeStatus employeeStatus) {
        this.employeeStatus = employeeStatus;
    }
}
