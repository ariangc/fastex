package com.restful.model;

import com.restful.entity.Continent;

public class ContinentModel {
    private Long id;
    private String name;

    public ContinentModel(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public ContinentModel(Continent a){
        this.id = a.getId();
        this.name = a.getName();
    }

    public ContinentModel(){
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
