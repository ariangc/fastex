package com.restful.model;

import com.restful.entity.Customer;

public class CustomerModel {
    private Long id;
    private String user;
    private String password;
    private String mail;
    private String phone;
    private String phonePrefix;
    private boolean infoEmail;
    private boolean infoPhone;
    private PersonModel person;
    private CustomerStatusModel customerStatus;
    private AddressModel address;

    public CustomerModel(Long id, String user, String password, String mail, String phone, String phonePrefix, boolean infoEmail, boolean infoPhone) {
        this.id = id;
        this.user = user;
        this.password = password;
        this.mail = mail;
        this.phone = phone;
        this.phonePrefix = phonePrefix;
        this.infoEmail = infoEmail;
        this.infoPhone = infoPhone;
    }

    public CustomerModel(Customer a){
        this.id = a.getId();
        this.user = a.getUser();
        this.password = a.getPassword();
        this.mail = a.getMail();
        this.phone = a.getPhone();
        this.phonePrefix = a.getPhonePrefix();
        this.infoEmail = a.isInfoEmail();
        this.infoPhone = a.isInfoPhone();
        this.person = new PersonModel(a.getPerson());
        this.customerStatus = new CustomerStatusModel(a.getCustomerStatus());
        this.address = new AddressModel(a.getAddress());
    }

    public CustomerModel(){
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public PersonModel getPerson() {
        return person;
    }

    public void setPerson(PersonModel person) {
        this.person = person;
    }

    public CustomerStatusModel getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(CustomerStatusModel customerStatus) {
        this.customerStatus = customerStatus;
    }

    public AddressModel getAddress() {
        return address;
    }

    public void setAddress(AddressModel address) {
        this.address = address;
    }

    public boolean isInfoEmail() {
        return infoEmail;
    }

    public void setInfoEmail(boolean infoEmail) {
        this.infoEmail = infoEmail;
    }

    public boolean isInfoPhone() {
        return infoPhone;
    }

    public void setInfoPhone(boolean infoPhone) {
        this.infoPhone = infoPhone;
    }
}
