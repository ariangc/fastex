package com.restful.entity;

import javax.persistence.*;
import java.util.Date;

@Table(name = "WarehouseHistory")
@Entity
public class WarehouseHistory {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idWarehouseHistory")
    private Long id;

    private int currentCapacity;
    private int newCapacity;
    private Date updateDate;
    private boolean valid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idWarehouse")
    private Warehouse warehouse;

    protected WarehouseHistory(){}

    public WarehouseHistory(Long id, int currentCapacity, int newCapacity, Date updateDate, boolean valid, Warehouse warehouse) {
        this.id = id;
        this.currentCapacity = currentCapacity;
        this.newCapacity = newCapacity;
        this.updateDate = updateDate;
        this.valid = valid;
        this.warehouse = warehouse;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCurrentCapacity() {
        return currentCapacity;
    }

    public void setCurrentCapacity(int currentCapacity) {
        this.currentCapacity = currentCapacity;
    }

    public int getNewCapacity() {
        return newCapacity;
    }

    public void setNewCapacity(int newCapacity) {
        this.newCapacity = newCapacity;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }
}
