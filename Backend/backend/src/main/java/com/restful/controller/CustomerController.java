package com.restful.controller;

import com.restful.entity.Customer;
import com.restful.model.CustomerModel;
import com.restful.model.PersonModel;
import com.restful.service.CustomerService;
import com.restful.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/customer")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
public class CustomerController {
    @Autowired
    @Qualifier("customerService")
    private CustomerService customerService;

    @Autowired
    @Qualifier("personService")
    private PersonService personService;

    @CrossOrigin
    @PostMapping("/list")
    public @ResponseBody List<CustomerModel> getCustomers(){
        return customerService.getCustomers();
    }

    @CrossOrigin
    @PostMapping("/getUsernames")
    public @ResponseBody List<String> getUsernames(@RequestBody Map<String, Long> data){
        return personService.getUsernames(data.get("idPerson"));
    }

    @CrossOrigin
    @PostMapping("/create")
    public boolean createCustomer(@RequestBody Customer a){
        return customerService.createCustomer(a);
    }

    @CrossOrigin
    @PostMapping("/getCustomerByDocument")
    public @ResponseBody List<CustomerModel> getCustomerByPersonData(@RequestBody Map<String, String> data){
        try {
            return customerService.getCustomerByPersonData(Long.parseLong(data.get("idCountry")), Long.parseLong(data.get("idDocumentType")), data.get("document"));
        }catch(Exception e){
            e.printStackTrace();
        }
        return new ArrayList<CustomerModel>();
    }
}
