import React from "react";

// reactstrap components
import { Grid } from "react-bootstrap";
import {
  Button,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormText,
  Label,
  Card,
  CardTitle,
  Modal
} from "reactstrap";
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import HelpOutline from '@material-ui/icons/HelpOutline';
import classNames from 'classnames';
import {apiURL, warehouse, editWarehouse, editedWarehouse} from "variables/Variables.js"
// core components
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import SideBar from 'components/sidebar/SideBar.js';
import MaterialTable from "material-table";
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
    ZoomableGroup,
    ComposableMap,
    Geographies,
    Geography
  } from "react-simple-maps";
import Map from "./Map";
import ReactTooltip from "react-tooltip";
import * as data1 from './Mapa.json'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'
import {PassThrough} from 'stream';

  const geoUrl = data1;

  const rounded = num => {
    if (num > 1000000000) {
      return Math.round(num / 100000000) / 10 + "Bn";
    } else if (num > 1000000) {
      return Math.round(num / 100000) / 10 + "M";
    } else {
      return Math.round(num / 100) / 10 + "K";
    }
  };
  

class WarehouseMap extends React.Component {
  constructor(props) {
    super(props);
    this.sidebarIsOpen = this.sidebarIsOpen.bind(this);
    this.sidebarIsClose = this.sidebarIsClose.bind(this);
    this.handlerContent = this.handlerContent.bind(this);
    this.state = {
      sidebarOpen:true,
      content:'',
      aux:'',
      warehouses:[],
      loading:true
    }
}
handlerContent(content){
  
  this.setState({
    content
  }, ()=>{
    this.setState({
      aux:''
    })
  })
}
sidebarIsClose(){
    this.setState({
      sidebarOpen:!this.state.sidebarOpen
    })
  }
  sidebarIsOpen(){
    this.setState({
      sidebarOpen:true
    })
  }
  componentDidMount(){
    axios.post(apiURL.value + 'warehouse/list').then((res)=>{
        if(res.status === 200){
            var list = []
            geoUrl.default.objects.ne_110m_admin_0_countries.geometries.map((data, index)=>{
                var aux = {active:false, name:'', filled:0, total:0, statusId:0, status:''}
                res.data.map((warehouse, i)=>{
                    if(warehouse.city.country.name.localeCompare(data.properties['NAME'])==0){
                        aux.active=true;
                        aux.name=warehouse.city.name;
                        aux.filled=warehouse.filledCapacity;
                        aux.total=warehouse.totalCapacity;
                        aux.statusId=warehouse.warehouseStatus.id;
                        aux.status=warehouse.warehouseStatus.description;
                    }
                })
                list.push(aux)
            })
            this.setState({
            warehouses:list,
            loading:false
            })
        }
      })
    .catch(()=>{
        toast.error('Ha ocurrido un error con el servidor', { type: toast.TYPE.ERROR, autoClose: 5000 })
    })
}
render(){
  return (
    <>
    <div className="App">
      
      <SideBar fluid toggle={this.sidebarIsClose} isOpen={this.state.sidebarOpen}/>
      <Container
      fluid className={classNames('content', {'is-open': this.state.sidebarOpen})}>
      <NavbarRedexAdmin style={{position:'fixed', marginTop:'0'}}/>
        <Container style={{marginTop:'10%', padding:'0%', backgroundColor:'#FAFAFA'}}>
        <ToastContainer /> 
          <Card>
            <div className='text-center' style ={{marginBottom:'-5%'}}><h4>Almacenes</h4></div>
                <Col> 
                {this.state.loading?
                      <div className='text-center'  style={{width:'100%', weight:'100%', backgroundColor:'#FFFFFF'
                    }}>
                      <Loader
                        type="ThreeDots"
                        color="#03256C"
                        height={100}
                        width={100}
                        timeout={3000000}
                      />
                    </div> :
                    <div>
                <Map setTooltipContent={this.handlerContent} data={this.state.warehouses}/>
                  <ReactTooltip>{this.state.content}</ReactTooltip> </div>}
                </Col>
              </Card>
          </Container>
      </Container>
                  
    </div>       
    </>
  );
  }
}

export default WarehouseMap;
