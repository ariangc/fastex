package com.restful.repository;

import com.restful.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restful.entity.Customer;

import java.util.List;

@Repository("customerRepository")
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    public abstract List<Customer> findAll();
    public abstract List<Customer> findByUser(String user);
    public abstract List<Customer> findByPerson(Person person);
}
