package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "City")
@Entity
public class City {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idCity")
    private Long id;
    private String name;
    private String cityCode;
    private String airportCode;
    private Float latitude;
    private Float longitude;
    private Float exponent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idCountry")
    private Country country;

    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL)
    private List<Warehouse> warehouses = new ArrayList<>();

    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL)
    private List<Address> addresses = new ArrayList<>();

    protected City(){}

    public City(Long id, String name, String cityCode, String airportCode, Float latitude, Float longitude, Float exponent, Country country, List<Warehouse> warehouses, List<Address> addresses) {
        this.id = id;
        this.name = name;
        this.cityCode = cityCode;
        this.airportCode = airportCode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.country = country;
        this.warehouses = warehouses;
        this.addresses = addresses;
        this.exponent = exponent;
    }

    public Float getExponent() {
        return exponent;
    }

    public void setExponent(Float exponent) {
        this.exponent = exponent;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<Warehouse> getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(List<Warehouse> warehouses) {
        this.warehouses = warehouses;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }
}
