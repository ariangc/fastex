package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "EmployeeStatus")
@Entity
public class EmployeeStatus {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idEmployeeStatus")
    private Long id;

    private String description;

    @OneToMany(mappedBy = "employeeStatus", cascade = CascadeType.ALL)
    private List<Employee> employees = new ArrayList<>();

    protected EmployeeStatus(){}

    public EmployeeStatus(Long id, String description, List<Employee> employees) {
        this.id = id;
        this.description = description;
        this.employees = employees;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
