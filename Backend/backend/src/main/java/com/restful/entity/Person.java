package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Table(name = "Person")
@Entity
public class Person {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idPerson")
    private Long id;

    private String names;
    private String surnames;
    private String document;
    private Date birthday;
    private boolean isCustomer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idCountry")
    private Country country;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idDocumentType")
    private DocumentType documentType;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    private List<Customer> customers = new ArrayList<>();

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    private List<Receiver> receivers = new ArrayList<>();

    protected Person(){}
    @Override
    public String toString(){
        return String.format("[id=%d, names='%s', surnames='%s', document='%s', documentType='%s', birthday='%s'" +
                "isCustomer='%d", id, names, surnames, document, documentType.getName(), birthday.toString(), isCustomer);
    }

    public Person(Long id, String names, String surnames, String document, Date birthday, boolean isCustomer, Country country, DocumentType documentType, List<Customer> customers, List<Receiver> receivers) {
        this.id = id;
        this.names = names;
        this.surnames = surnames;
        this.document = document;
        this.birthday = birthday;
        this.isCustomer = isCustomer;
        this.country = country;
        this.documentType = documentType;
        this.customers = customers;
        this.receivers = receivers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public boolean isCustomer() {
        return isCustomer;
    }

    public void setCustomer(boolean customer) {
        isCustomer = customer;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public List<Receiver> getReceivers() {
        return receivers;
    }

    public void setReceivers(List<Receiver> receivers) {
        this.receivers = receivers;
    }
}
