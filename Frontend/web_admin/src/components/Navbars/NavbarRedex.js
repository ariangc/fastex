import React from "react";
// reactstrap components
import {
  Navbar,
  Container,
} from "reactstrap";

function NavbarRedex(props) {
  
  return (
    <>
      
      <Navbar className={"top"} expand="lg" style={{backgroundColor:'#03256C'}}>
        <Container>
          <h5 style={{fontFamily:'Lato', marginTop:'1%', marginBottom:'1%'}}><b>{props.title}</b></h5>
          
        </Container>
      </Navbar>
    </>
  );
}

export default NavbarRedex;