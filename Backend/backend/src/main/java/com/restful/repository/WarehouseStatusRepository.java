package com.restful.repository;

import com.restful.entity.WarehouseStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WarehouseStatusRepository extends JpaRepository<WarehouseStatus, Long> {

}
