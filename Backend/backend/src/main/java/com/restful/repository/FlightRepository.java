package com.restful.repository;

import com.restful.entity.City;
import com.restful.entity.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("flightRepository")
public interface FlightRepository extends JpaRepository<Flight, Long> {
    public abstract List<Flight> findByStartCityAndEndCity(City startCity, City endCity);
}
