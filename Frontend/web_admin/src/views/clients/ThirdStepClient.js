import React from "react";

// reactstrap components
import {
  Row,
  Col,
  Form,
  FormGroup,
} from "reactstrap";
import {apiURL} from "variables/Variables.js"
// core components
import axios from "axios";
import './Clients.css';
import logo from "assets/img/trato-hecho.jpg"

const data = [];
class ThirdStepClient extends React.Component {
  constructor(props) {
    super(props);    
    this.togglerUser = this.togglerUser.bind(this);
    this.state = {
      selectedRow: null,
      data:[],
      User:""
    }
}
togglerUser(event){
  this.setState({
    User:event.target.value
  })
}

componentWillMount(){
  /* if(!currentUser.logged){
    this.props.history.push("");
  } */
}
componentWillUnmount(){
  /* console.log("login")
  console.log(this.props)
  if(this.props.location.pathname=='/'+landingConstrunetAdmision.url ){
    if(this.props.history.action == 'POP'){
      if(this.props.history.location.pathname == '/'+loginConstrunetAdmision.url){
        this.props.history.push(landingConstrunetAdmision.url);
      }
    }
  } */
}

  render(){
  return (
    <>
    <div className="App">
          <Col> 
            <Form>
              <Row>
                <h3 className="stepTitle">Confirmación del Registro</h3>
              </Row>
                <Row sm={{ size: 16}}>
                <Col sm={{ size: 5, offset: 1 }}>
                <FormGroup>
                    <label>
                        <h5 className="StepContainer">
                            Datos del Cliente:
                        </h5>
                    </label>
                </FormGroup>
                <FormGroup>
                    <label classname="StepCheck">
                        Nombres:&nbsp;&nbsp;&nbsp;
                    </label>
                    <label classname="StepCheck"><b>{this.props.names}</b></label>
                </FormGroup>
                <FormGroup>
                    <label classname="StepCheck">
                        Apellidos:&nbsp;&nbsp;&nbsp;
                    </label >
                    <label classname="StepCheck"><b>{this.props.surnames}</b></label>
                </FormGroup>
                <FormGroup>
                    <label classname="StepCheck">
                        Correo:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </label>
                    <label classname="StepCheck"><b>{this.props.email}</b></label>
                </FormGroup>
                <FormGroup>
                    <label classname="StepCheck">
                        Usuario:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </label>
                    <label classname="StepCheck"><b>{this.props.username}</b></label>
                </FormGroup>
                <FormGroup>
                 <div className="custom-control custom-control-alternative custom-checkbox">
                      <input
                        className="custom-control-input"
                        id="customCheckEmail"
                        type="checkbox"
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="customCheckEmail"
                      >
                        <h5 className="stepCheck">
                        El cliente desea recibir información sobre sus paquetes enviados 
                        y/o a ser recibidos, en su correo electrónico proporcionado en este registro.
                        </h5>
                      </label>
                    </div> 
                </FormGroup>
                <FormGroup>
                    <div className="custom-control custom-control-alternative custom-checkbox">
                      <input
                        className="custom-control-input"
                        id="customCheckTel"
                        type="checkbox"
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="customCheckTel"
                      >
                        <h5 className="stepCheck">
                        El cliente desea recibir información sobre sus paquetes enviados y/o a ser 
                        recibidos, en el número telefónico proporcionado en este registro.
                        </h5>
                      </label>
                    </div> 
                </FormGroup>
                </Col>
                <Col sm={{ size: 6, offset: 0 }}>
                <FormGroup>  
                    <img src={logo} alt="logo" style={{height:"70%", width:"90%", alignContent:"center"}}></img>
                </FormGroup>
                </Col>
                </Row> 
                <FormGroup>
                    <label>                        
                        <h5 className="stepFinal"><b>
                        El cliente debe revisar su correo electrónico para configurar por primera 
                        vez su constraseña, para darle acceso al sistema de clientes de Redex
                        </b>
                        </h5>                        
                    </label>
                </FormGroup>
            </Form>
          </Col>                        
    </div>       
    </>
  );
  }
}

export default ThirdStepClient;
