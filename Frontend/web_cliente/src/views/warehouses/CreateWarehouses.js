import React from "react";

// reactstrap components
import { Grid } from "react-bootstrap";
import {
  Button,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormText,
  Label,
  Card,
  CardTitle,
  Modal
} from "reactstrap";
import {Tabs} from 'react-bootstrap';
import {Tab} from 'react-bootstrap';
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import HelpOutline from '@material-ui/icons/HelpOutline';
import classNames from 'classnames';
import {currentUser, loginConstrunetAdmision, landingConstrunetAdmision, table,
  apiURL} from "variables/Variables.js"
// core components
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import SideBar from 'components/sidebar/SideBar.js';
import MaterialTable from "material-table";
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const data = [];

class CreateWarehouses extends React.Component {
  constructor(props) {
    super(props);
    this.sidebarIsOpen = this.sidebarIsOpen.bind(this);
    this.sidebarIsClose = this.sidebarIsClose.bind(this);
    this.handlerCloseEditModal = this.handlerCloseEditModal.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.togglerCantOcup = this.togglerCantOcup.bind(this);
    this.togglerCantTotal = this.togglerCantTotal.bind(this);
    this.togglerLimit = this.togglerLimit.bind(this);
    this.createWarehouse = this.createWarehouse.bind(this); 
    this.state = {
      selectedRow: null,
      data:[],
      countries:[],
      countrySelected:0,
      cities:[],
      fullCities:[],
      citySelected:0,
      sidebarOpen:true,
      modalOpen:false,
      cantOcup:0,
      cantTotal:0,
      limit:"",
      cityDic:[]
    }
}
createWarehouse(){
  var warehouseBody={id:null, totalCapacity: parseInt(this.state.cantTotal),
    filledCapacity: parseInt(this.state.cantOcup), capacityLimitPercentage:parseInt(this.state.limit),
    icaoCode:null, iataCode:null, city:this.state.cityDic, 
    warehouseStatus:{id: 2, description: "ACTIVO"}}
    console.log(warehouseBody)
    axios.post(apiURL.value + 'warehouse/create', warehouseBody).then((res)=>{
      if(res.status==200){

      }
    })
}
handleCountryChange(event){
  this.setState({countrySelected:event.target.value}, () => {
    var body = {idCountry:parseInt(this.state.countrySelected)};
    axios.post(apiURL.value + 'city/listByCountry', body).then((res)=>{
      if(res.status === 200){
        console.log(res.data)
        const cityListsRes = res.data.map((city) =>
                    <option value={city['id']}>{city['name']}</option>
                );
        this.setState({cities:cityListsRes, fullCities:res.data})
      }
    })
  });
}

handleCityChange(event){
  console.log(event.target.value)
  this.state.fullCities.map((value, index)=>{
    console.log(value.props, event.target.value)
    if(value.id==event.target.value){
      this.setState({
        cityDic:value
      })
    }
  })
}

togglerCantOcup(event){
   const {value } = event.target;
    let regex = new RegExp("^[0-9]*$");
    if (regex.test(value) && this.state.cantOcup.length < 5) {
      this.setState({
        cantOcup: event.target.value
      });
    } else if (regex.test(value)) {
      this.setState({
        cantOcup: event.target.value.substring(0, 5)
      });
    }
}

togglerCantTotal(event){
    const {value } = event.target;
    let regex = new RegExp("^[0-9]*$");
    if (regex.test(value) && this.state.cantTotal.length < 5) {
      this.setState({
        cantTotal: event.target.value
      });
    } else if (regex.test(value)) {
      this.setState({
        cantTotal: event.target.value.substring(0, 5)
      });
    }
}

togglerLimit(event){
    const {value } = event.target;
    let regex = new RegExp("^[0-9]{1,3}(\.[0-9]{0,2})?$");
    console.log(event.tregex);
    if (/^[0-9]{1,3}(\.[0-9]{0,2})?$/.test(event.target.value) && this.state.limit.length < 6) {
      this.setState({
        limit: event.target.value
      });
    } else if (regex.test(event.target.value)) {
      this.setState({
        limit: event.target.value.substring(0, 6)
      });
    }
}

handlerCloseEditModal(){
  this.setState({
    modalOpen:false
  })
}
sidebarIsClose(){
  this.setState({
    sidebarOpen:!this.state.sidebarOpen
  })
}
sidebarIsOpen(){
  this.setState({
    sidebarOpen:true
  })
}

componentWillMount(){
  axios.post(apiURL.value + 'country/list').then((res)=>{
    console.log(res);
    if(res.status === 200){
      const countriesRes = res.data.map((country) =>
                  <option value={country.id}>{country.name}</option>
              );
      this.setState({countries:countriesRes})
    }
  })
}

  render(){
  return (
    <>
    <div className="App"> 
      <SideBar fluid toggle={this.sidebarIsClose} isOpen={this.state.sidebarOpen}/>
      <Container
      fluid className={classNames('content', {'is-open': this.state.sidebarOpen})}>
      <NavbarRedexAdmin/>
        <Container style={{marginTop:'10%', backgroundColor:'#FAFAFA'}}>
            <Card>
            <Col md={{size:6, offset:3}}>
            <ToastContainer /> 
            <Form>
              <Row>
                <h3 className="stepTitle">Registrar Almacén</h3>
              </Row>
              <Row>
                <Col>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlSelect1">País</label>
                        <Input id="countrySelect" type="select" onChange={this.handleCountryChange}>
                          <option disabled value={0} selected>Seleccione un País</option>
                          {this.state.countries}
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                    <FormGroup>
                      <label htmlFor="exampleFormControlSelect2">Ciudad</label>
                      <Input id="citySelect" type="select" onChange={this.handleCityChange}>
                          <option disabled value={0} selected>Seleccione una Ciudad</option>
                          {this.state.cities}
                      </Input>
                    </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                      <FormGroup className = {this.state.cantOcup}>
                        <label htmlFor="exampleFormControlInput1">Cant. Ocupada</label>
                        <Input
                          id="cantOcup"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerCantOcup}
                          value = {this.state.cantOcup}
                        ></Input>
                      </FormGroup>
                      </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                      <FormGroup className = {this.state.cantTotal}>
                        <label htmlFor="exampleFormControlInput2">Cant. Total</label>
                        <Input
                          id="cantTotal"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerCantTotal}
                          value = {this.state.cantTotal}
                        ></Input>
                      </FormGroup>
                      </Col>
                  </Row>
                  <Row>
                    <Col sm={{ size: 8, offset: 2 }}>
                      <FormGroup>
                        <label htmlFor="exampleFormControlInput3">% Limite</label>
                        <Input
                          id="limit"
                          placeholder=""
                          type="text"
                          onChange = {this.togglerLimit}
                          value = {this.state.limit}
                        ></Input>
                      </FormGroup>
                      </Col>                      
                  </Row>
                </Col>
              </Row>  
            <Row style={{marginBottom:'10%', marginTop:'5%'}}>
                  <Button 
                    size="lg"
                    color="warning"
                    outline="warning"
                    style={{marginLeft:'20%'}}
                    onClick={this.cancelLoad}>
                    Cancelar
                  </Button>
                  <Button
                      size="lg"
                      variant="contained"
                      color="primary"
                      style={{marginLeft:'5%'}}
                      onClick={this.createWarehouse}>
                    Cargar
                  </Button>
                </Row>
            </Form>

            </Col> 
            </Card>  
          </Container>
      </Container>
    
    </div>       
    </>
  );
  }
}

export default CreateWarehouses;
