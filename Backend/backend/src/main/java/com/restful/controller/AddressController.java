package com.restful.controller;

import com.restful.entity.Address;
import com.restful.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/address")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
public class AddressController {
    @Autowired
    @Qualifier("addressService")
    private AddressService addressService;

    @CrossOrigin
    @PostMapping("/create")
    public @ResponseBody Map<String, Long> createAddress(Address a){
        Long idAddress = addressService.createAddress(a);
        Map<String, Long> response = new HashMap<String, Long>();
        response.put("idAddress", idAddress);
        return response;
    }
}
