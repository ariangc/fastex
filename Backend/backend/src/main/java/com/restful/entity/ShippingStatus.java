package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "ShippingStatus")
@Entity
public class ShippingStatus {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idShippingStatus")
    private Long id;

    private String description;

    @OneToMany(mappedBy = "shippingStatus", cascade = CascadeType.ALL)
    private List<Shipping> shippings = new ArrayList<>();

    protected ShippingStatus(){}

    public ShippingStatus(Long id, String description, List<Shipping> shippings) {
        this.id = id;
        this.description = description;
        this.shippings = shippings;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Shipping> getShippings() {
        return shippings;
    }

    public void setShippings(List<Shipping> shippings) {
        this.shippings = shippings;
    }
}
