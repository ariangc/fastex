package com.restful.controller;

import com.restful.entity.Receiver;
import com.restful.model.ReceiverModel;
import com.restful.service.ReceiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/receiver")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
public class ReceiverController {
    @Autowired
    @Qualifier("receiverService")
    private ReceiverService receiverService;

    @CrossOrigin
    @PostMapping("/create")
    public @ResponseBody Map<String, ReceiverModel> createReceiver(@RequestBody Map<String, Receiver> data){
        return receiverService.createReceiver(data.get("receiver"));
    }
}
