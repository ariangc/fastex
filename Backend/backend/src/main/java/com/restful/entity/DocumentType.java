package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "DocumentType")
@Entity
public class DocumentType {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idDocumentType")
    private Long id;

    private String name;
    private String regex;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idCountry")
    private Country country;

    @OneToMany(mappedBy = "documentType", cascade = CascadeType.ALL)
    private List<Person> persons = new ArrayList<>();


    public DocumentType(Long id, String name, String regex, Country country, List<Person> persons) {
        this.id = id;
        this.name = name;
        this.regex = regex;
        this.country = country;
        this.persons = persons;
    }

    public DocumentType(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
