package com.restful.repository;

import com.restful.entity.ProgrammedFlight;
import com.restful.entity.Shipping;
import com.restful.entity.ShippingXProgrammedFlight;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ShippingXProgrammedFlightRepository extends JpaRepository<ShippingXProgrammedFlight, Long> {
    public abstract List<ShippingXProgrammedFlight> findByShipping(Shipping shipping);
}
