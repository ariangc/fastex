package com.restful.controller;

import com.restful.model.CountryModel;
import com.restful.service.CountryService;
import com.restful.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/country")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
public class CountryController {
    @Autowired
    @Qualifier("countryService")
    private CountryService countryService;

    @CrossOrigin
    @PostMapping("/list")
    public @ResponseBody
    List<CountryModel> getCountries(){
        return countryService.getCountries();
    }

}
