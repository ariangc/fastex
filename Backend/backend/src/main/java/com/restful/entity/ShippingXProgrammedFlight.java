package com.restful.entity;

import javax.persistence.*;

@Table(name = "ShippingXProgrammedFlight")
@Entity
public class ShippingXProgrammedFlight {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idShippingXProgrammedFlight")
    private Long id;

    private int orderInRoute;
    private boolean active;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idShipping")
    private Shipping shipping;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idProgrammedFlight")
    private ProgrammedFlight programmedFlight;

    public ShippingXProgrammedFlight(Long id, int orderInRoute, boolean active, Shipping shipping, ProgrammedFlight programmedFlight) {
        this.id = id;
        this.orderInRoute = orderInRoute;
        this.active = active;
        this.shipping = shipping;
        this.programmedFlight = programmedFlight;
    }

    public ShippingXProgrammedFlight(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getOrderInRoute() {
        return orderInRoute;
    }

    public void setOrderInRoute(int orderInRoute) {
        this.orderInRoute = orderInRoute;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    public ProgrammedFlight getProgrammedFlight() {
        return programmedFlight;
    }

    public void setProgrammedFlight(ProgrammedFlight programmedFlight) {
        this.programmedFlight = programmedFlight;
    }
}
