package com.restful.model;



public class DayCharge {
    String arrivalDate;
    int totalGood;
    int totalBad;

    protected DayCharge() {}

    public DayCharge(String arrivalDate, int totalGood, int totalBad) {
        this.arrivalDate = arrivalDate;
        this.totalGood = totalGood;
        this.totalBad = totalBad;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public int getTotalGood() {
        return totalGood;
    }

    public void setTotalGood(int totalGood) {
        this.totalGood = totalGood;
    }

    public int getTotalBad() {
        return totalBad;
    }

    public void setTotalBad(int totalBad) {
        this.totalBad = totalBad;
    }
}
