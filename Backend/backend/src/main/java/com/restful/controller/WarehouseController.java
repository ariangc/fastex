package com.restful.controller;

import com.restful.entity.Warehouse;
import com.restful.model.DayCharge;
import com.restful.model.HourCharge;
import com.restful.model.WarehouseModel;
import com.restful.service.WarehouseService;
import net.lingala.zip4j.exception.ZipException;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.ParseException;
import java.util.*;

import net.lingala.zip4j.ZipFile;


@RestController
@RequestMapping("/warehouse")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
public class WarehouseController {
    @Autowired
    @Qualifier("warehouseService")
    private WarehouseService warehouseService;

    @PostMapping("/list")
    private @ResponseBody List<WarehouseModel> listWarehouses(){
	 	  System.out.println("Entrando a listWarehouses");
        return warehouseService.getWarehouses();
    }

    @PostMapping("/create")
    private @ResponseBody boolean createWarehouse(@RequestBody Warehouse a){
        return warehouseService.createWarehouse(a);
    }

    @PostMapping("/update")
    private @ResponseBody boolean updateWarehouse(@RequestBody Warehouse a){
        return warehouseService.updateWarehouse(a);
    }

    @PostMapping("/delete")
    private @ResponseBody boolean deleteWarehouse(@RequestBody Warehouse a){
        return warehouseService.deleteWarehouse(a);
    }

    @CrossOrigin
    @RequestMapping(value = "/massive", method=RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    private @ResponseBody boolean uploadHistory(@RequestParam("file") MultipartFile file) throws Exception {
        //Lectura de ZIP
        File zip = File.createTempFile(UUID.randomUUID().toString(), "temp");
        FileOutputStream o = new FileOutputStream(zip);
        IOUtils.copy(file.getInputStream(), o);
        o.close();

        String destination = "./tempFiles";
        try{
            ZipFile zipFile = new ZipFile(zip);
            zipFile.extractAll(destination);
        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        } finally {
            zip.delete();
        }

        return true;
    }

    @CrossOrigin
    @RequestMapping("/massiveTest")
    private Map<String, List<DayCharge>> processFiles(@RequestBody Map<String, String> data) throws Exception{
	 	  System.out.println(data.get("icaoCode"));
        try {
            return warehouseService.processFiles(data.get("icaoCode"));
        } catch(Exception e){
            e.printStackTrace();
            return new HashMap<>();
        }
    }

    @CrossOrigin
    @RequestMapping("/massiveHour")
    private Map<String, List<HourCharge>> processHours(@RequestBody Map<String, String> data) throws Exception{
        System.out.println(data.get("icaoCode") + " " + data.get("date"));
        try {
            return warehouseService.processFiles(data.get("icaoCode"), data.get("date"));
        } catch (Exception e){
            e.printStackTrace();
            return new HashMap<>();
        }
    }

    @CrossOrigin
    @RequestMapping("/massiveLoading")
    private @ResponseBody boolean processFilesSave(@RequestBody Map<String, String> data) throws Exception{
	 	  System.out.println(data.get("icaoCode"));
        try {
            return warehouseService.processFilesSave(data.get("icaoCode"));
        } catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @CrossOrigin
    @RequestMapping("/getByCity")
    private @ResponseBody Map<String, List<WarehouseModel>> getByCity(@RequestBody Map<String, Long> data){
        return warehouseService.getWarehousesByCity(data.get("idCity"));
    }

    @CrossOrigin
    @RequestMapping("/getHistory")
    private @ResponseBody Map<String, List<DayCharge>> getHistory(@RequestBody Map<String, String> data){
        return warehouseService.getHistory(data.get("icaoCode"));
    }

    @CrossOrigin
    @RequestMapping("/predict")
    private @ResponseBody Map<String, String> predict(@RequestBody Map<String, String> data) throws ParseException {
        return warehouseService.predictCollision(data.get("icaoCode"));
    }

    @CrossOrigin
    @RequestMapping("/predictDate")
    private @ResponseBody Map<String, Integer> predictDate(@RequestBody Map<String, String> data) throws ParseException{
        return warehouseService.getPrediction(data.get("icaoCode"), data.get("pickedDate"));
    }
}
