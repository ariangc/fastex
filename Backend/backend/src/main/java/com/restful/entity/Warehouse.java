package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "Warehouse")
@Entity
public class Warehouse {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idWarehouse")
    private Long id;

    private int totalCapacity;
    private int filledCapacity;
    private int capacityLimitPercentage;
    private int totalBad;
    private String icaoCode;
    private String iataCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idCity")
    private City city;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idWarehouseStatus")
    private WarehouseStatus warehouseStatus;

    @OneToMany(mappedBy = "warehouse", cascade = CascadeType.ALL)
    private List<WarehouseHistory> warehouseHistories = new ArrayList<>();

    @OneToMany(mappedBy = "warehouse", cascade = CascadeType.ALL)
    private List<PackageXWarehouse> packageXWarehouses = new ArrayList<>();

    protected Warehouse(){}

    public Warehouse(Long id, int totalCapacity, int filledCapacity, int capacityLimitPercentage, int totalBad, String icaoCode, String iataCode, City city, WarehouseStatus warehouseStatus, List<WarehouseHistory> warehouseHistories, List<PackageXWarehouse> packageXWarehouses) {
        this.id = id;
        this.totalCapacity = totalCapacity;
        this.filledCapacity = filledCapacity;
        this.capacityLimitPercentage = capacityLimitPercentage;
        this.totalBad = totalBad;
        this.icaoCode = icaoCode;
        this.iataCode = iataCode;
        this.city = city;
        this.warehouseStatus = warehouseStatus;
        this.warehouseHistories = warehouseHistories;
        this.packageXWarehouses = packageXWarehouses;
    }

    public Long getId() {
        return id;
    }

    public boolean isFull(){
        return (totalCapacity == filledCapacity);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTotalCapacity() {
        return totalCapacity;
    }

    public void setTotalCapacity(int totalCapacity) {
        this.totalCapacity = totalCapacity;
    }

    public int getFilledCapacity() {
        return filledCapacity;
    }

    public int getTotalBad() {
        return totalBad;
    }

    public void setTotalBad(int totalBad) {
        this.totalBad = totalBad;
    }

    public void setFilledCapacity(int filledCapacity) {
        this.filledCapacity = filledCapacity;
    }

    public int getCapacityLimitPercentage() {
        return capacityLimitPercentage;
    }

    public void setCapacityLimitPercentage(int capacityLimitPercentage) {
        this.capacityLimitPercentage = capacityLimitPercentage;
    }

    public String getIcaoCode() {
        return icaoCode;
    }

    public void setIcaoCode(String icaoCode) {
        this.icaoCode = icaoCode;
    }

    public String getIataCode() {
        return iataCode;
    }

    public void setIataCode(String iataCode) {
        this.iataCode = iataCode;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public WarehouseStatus getWarehouseStatus() {
        return warehouseStatus;
    }

    public void setWarehouseStatus(WarehouseStatus warehouseStatus) {
        this.warehouseStatus = warehouseStatus;
    }

    public List<WarehouseHistory> getWarehouseHistories() {
        return warehouseHistories;
    }

    public void setWarehouseHistories(List<WarehouseHistory> warehouseHistories) {
        this.warehouseHistories = warehouseHistories;
    }

    public List<PackageXWarehouse> getPackageXWarehouses() {
        return packageXWarehouses;
    }

    public void setPackageXWarehouses(List<PackageXWarehouse> packageXWarehouses) {
        this.packageXWarehouses = packageXWarehouses;
    }
}
