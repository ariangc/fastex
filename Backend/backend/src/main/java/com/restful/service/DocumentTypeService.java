package com.restful.service;

import com.restful.entity.Country;
import com.restful.entity.DocumentType;
import com.restful.model.DocumentTypeModel;
import com.restful.repository.CountryRepository;
import com.restful.repository.DocumentTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("documentTypeService")
public class DocumentTypeService {
    @Autowired
    @Qualifier("documentTypeRepository")
    private DocumentTypeRepository documentTypeRepository;

    @Autowired
    @Qualifier("countryRepository")
    private CountryRepository countryRepository;

    public List<DocumentTypeModel> getDocumentTypes(){
        List<DocumentType> l = documentTypeRepository.findAll();
        List<DocumentTypeModel> lModel = new ArrayList<DocumentTypeModel>();
        for(DocumentType documentType: l){
            DocumentTypeModel x = new DocumentTypeModel(documentType);
            lModel.add(x);
        }
        return lModel;
    }

    public List<DocumentTypeModel> getDocumentTypesByCountry(Long idCountry){
        List<Country> lCountry = countryRepository.findById(idCountry);
        List<DocumentType> l = documentTypeRepository.findByCountry(lCountry.get(0));
        List<DocumentTypeModel> lModel = new ArrayList<DocumentTypeModel>();
        for(DocumentType documentType: l){
            DocumentTypeModel x = new DocumentTypeModel(documentType);
            lModel.add(x);
        }
        return lModel;
    }
}
