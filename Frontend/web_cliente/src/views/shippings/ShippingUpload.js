import React from "react";

// reactstrap components
import { Grid } from "react-bootstrap";
import {
  Button,
  CustomInput,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormText,
  Label,
  Card,
  CardTitle,
  Modal,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from "reactstrap";
import { Link } from "react-router-dom";
import Tooltip2 from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import HelpOutline from '@material-ui/icons/HelpOutline';
import classNames from 'classnames';
import {currentUser, shippingList,
  apiURL} from "variables/Variables.js"
// core components
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import SideBar from 'components/sidebar/SideBar.js';
import MaterialTable from "material-table";
import axios from "axios";
import './Shipping.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {PassThrough} from 'stream';
import {BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, 
      Tooltip, Legend, LineChart, Line} from 'recharts';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'
import Datetime from 'react-datetime';

const data = 
{"SPIM":[{"arrivalDate": '22/04/2020', "totalGood": 48, "totalBad":6}, 
{"arrivalDate": '23/04/2020', "totalGood": 45, "totalBad":50}, 
{"arrivalDate": '24/04/2020', "totalGood": 49, "totalBad":32}, 
{"arrivalDate": '25/04/2020', "totalGood": 47, "totalBad":47}, 
{"arrivalDate": '26/04/2020', "totalGood": 40, "totalBad":22}, 
{"arrivalDate": '27/04/2020', "totalGood": 9, "totalBad":47}, 
{"arrivalDate": '28/04/2020', "totalGood": 9, "totalBad":46}, 
{"arrivalDate": '29/04/2020', "totalGood": 28, "totalBad":9}, 
{"arrivalDate": '30/04/2020', "totalGood": 13, "totalBad":19}, 
{"arrivalDate": '01/05/2020', "totalGood": 6, "totalBad":49}, 
{"arrivalDate": '02/05/2020', "totalGood": 22, "totalBad":8}, 
{"arrivalDate": '03/05/2020', "totalGood": 28, "totalBad":9}, 
{"arrivalDate": '04/05/2020', "totalGood": 35, "totalBad":13}, 
{"arrivalDate": '05/05/2020', "totalGood": 12, "totalBad":25}, 
{"arrivalDate": '06/05/2020', "totalGood": 10, "totalBad":26}, 
{"arrivalDate": '07/05/2020', "totalGood": 9, "totalBad":3}, 
{"arrivalDate": '08/05/2020', "totalGood": 1, "totalBad":35}, 
{"arrivalDate": '09/05/2020', "totalGood": 25, "totalBad":35}, 
{"arrivalDate": '10/05/2020', "totalGood": 38, "totalBad":24}, 
{"arrivalDate": '11/05/2020', "totalGood": 28, "totalBad":37}, 
{"arrivalDate": '12/05/2020', "totalGood": 33, "totalBad":8}, 
{"arrivalDate": '13/05/2020', "totalGood": 48, "totalBad":30}, 
{"arrivalDate": '14/05/2020', "totalGood": 17, "totalBad":9}, 
{"arrivalDate": '15/05/2020', "totalGood": 30, "totalBad":21}, 
{"arrivalDate": '16/05/2020', "totalGood": 39, "totalBad":13}, 
{"arrivalDate": '17/05/2020', "totalGood": 7, "totalBad":16}, 
{"arrivalDate": '18/05/2020', "totalGood": 16, "totalBad":12}, 
{"arrivalDate": '19/05/2020', "totalGood": 17, "totalBad":9}, 
{"arrivalDate": '20/05/2020', "totalGood": 5, "totalBad":14}, 
{"arrivalDate": '21/05/2020', "totalGood": 26, "totalBad":8}, 
{"arrivalDate": '22/05/2020', "totalGood": 33, "totalBad":31}, 
{"arrivalDate": '23/05/2020', "totalGood": 30, "totalBad":10}, 
{"arrivalDate": '24/05/2020', "totalGood": 13, "totalBad":3}, 
{"arrivalDate": '25/05/2020', "totalGood": 11, "totalBad":19}, 
{"arrivalDate": '26/05/2020', "totalGood": 32, "totalBad":47}, 
{"arrivalDate": '27/05/2020', "totalGood": 32, "totalBad":9}, 
{"arrivalDate": '28/05/2020', "totalGood": 5, "totalBad":48}, 
{"arrivalDate": '29/05/2020', "totalGood": 49, "totalBad":23}, 
{"arrivalDate": '30/05/2020', "totalGood": 37, "totalBad":48}, 
{"arrivalDate": '31/05/2020', "totalGood": 42, "totalBad":4}, 
{"arrivalDate": '01/06/2020', "totalGood": 31, "totalBad":39}, 
{"arrivalDate": '02/06/2020', "totalGood": 21, "totalBad":8}, 
{"arrivalDate": '03/06/2020', "totalGood": 36, "totalBad":6}, 
{"arrivalDate": '04/06/2020', "totalGood": 31, "totalBad":24}, 
{"arrivalDate": '05/06/2020', "totalGood": 39, "totalBad":24}, 
{"arrivalDate": '06/06/2020', "totalGood": 28, "totalBad":24}, 
{"arrivalDate": '07/06/2020', "totalGood": 9, "totalBad":28}, 
{"arrivalDate": '08/06/2020', "totalGood": 10, "totalBad":20}, 
{"arrivalDate": '09/06/2020', "totalGood": 13, "totalBad":42}, 
{"arrivalDate": '10/06/2020', "totalGood": 2, "totalBad":24}, 
{"arrivalDate": '11/06/2020', "totalGood": 31, "totalBad":29}, 
{"arrivalDate": '12/06/2020', "totalGood": 1, "totalBad":42}, 
{"arrivalDate": '13/06/2020', "totalGood": 26, "totalBad":4}, 
{"arrivalDate": '14/06/2020', "totalGood": 14, "totalBad":23}, 
{"arrivalDate": '15/06/2020', "totalGood": 2, "totalBad":5}, 
{"arrivalDate": '16/06/2020', "totalGood": 15, "totalBad":26}, 
{"arrivalDate": '17/06/2020', "totalGood": 31, "totalBad":4}, 
{"arrivalDate": '18/06/2020', "totalGood": 47, "totalBad":35}, 
{"arrivalDate": '19/06/2020', "totalGood": 36, "totalBad":46}, 
{"arrivalDate": '20/06/2020', "totalGood": 43, "totalBad":40}, 
{"arrivalDate": '21/06/2020', "totalGood": 17, "totalBad":24}, 
{"arrivalDate": '22/06/2020', "totalGood": 6, "totalBad":12}, 
{"arrivalDate": '23/06/2020', "totalGood": 26, "totalBad":11}, 
{"arrivalDate": '24/06/2020', "totalGood": 26, "totalBad":11}, 
{"arrivalDate": '25/06/2020', "totalGood": 7, "totalBad":8}, 
{"arrivalDate": '26/06/2020', "totalGood": 1, "totalBad":22}, 
{"arrivalDate": '27/06/2020', "totalGood": 5, "totalBad":35}, 
{"arrivalDate": '28/06/2020', "totalGood": 5, "totalBad":50}, 
{"arrivalDate": '29/06/2020', "totalGood": 1, "totalBad":43}, 
{"arrivalDate": '30/06/2020', "totalGood": 20, "totalBad":50}, 
{"arrivalDate": '01/07/2020', "totalGood": 6, "totalBad":1}, 
{"arrivalDate": '02/07/2020', "totalGood": 1, "totalBad":41}, 
{"arrivalDate": '03/07/2020', "totalGood": 11, "totalBad":10}, 
{"arrivalDate": '04/07/2020', "totalGood": 16, "totalBad":19}, 
{"arrivalDate": '05/07/2020', "totalGood": 13, "totalBad":36}, 
{"arrivalDate": '06/07/2020', "totalGood": 17, "totalBad":6}, 
{"arrivalDate": '07/07/2020', "totalGood": 12, "totalBad":31}, 
{"arrivalDate": '08/07/2020', "totalGood": 22, "totalBad":43}, 
{"arrivalDate": '09/07/2020', "totalGood": 9, "totalBad":40}, 
{"arrivalDate": '10/07/2020', "totalGood": 21, "totalBad":24}, 
{"arrivalDate": '11/07/2020', "totalGood": 44, "totalBad":39}, 
{"arrivalDate": '12/07/2020', "totalGood": 7, "totalBad":19}, 
{"arrivalDate": '13/07/2020', "totalGood": 23, "totalBad":9}, 
{"arrivalDate": '14/07/2020', "totalGood": 14, "totalBad":22}, 
{"arrivalDate": '15/07/2020', "totalGood": 13, "totalBad":40}, 
{"arrivalDate": '16/07/2020', "totalGood": 45, "totalBad":13}, 
{"arrivalDate": '17/07/2020', "totalGood": 17, "totalBad":41}, 
{"arrivalDate": '18/07/2020', "totalGood": 25, "totalBad":36}, 
{"arrivalDate": '19/07/2020', "totalGood": 14, "totalBad":3}, 
{"arrivalDate": '20/07/2020', "totalGood": 35, "totalBad":2}], 
"SBBR":[{"arrivalDate": '22/04/2020', "totalGood": 36, "totalBad":36}, 
{"arrivalDate": '23/04/2020', "totalGood": 50, "totalBad":17}, 
{"arrivalDate": '24/04/2020', "totalGood": 45, "totalBad":33}, 
{"arrivalDate": '25/04/2020', "totalGood": 28, "totalBad":49}, 
{"arrivalDate": '26/04/2020', "totalGood": 21, "totalBad":14}, 
{"arrivalDate": '27/04/2020', "totalGood": 32, "totalBad":34}, 
{"arrivalDate": '28/04/2020', "totalGood": 31, "totalBad":27}, 
{"arrivalDate": '29/04/2020', "totalGood": 48, "totalBad":43}, 
{"arrivalDate": '30/04/2020', "totalGood": 31, "totalBad":42}, 
{"arrivalDate": '01/05/2020', "totalGood": 5, "totalBad":29}, 
{"arrivalDate": '02/05/2020', "totalGood": 37, "totalBad":7}, 
{"arrivalDate": '03/05/2020', "totalGood": 6, "totalBad":23}, 
{"arrivalDate": '04/05/2020', "totalGood": 48, "totalBad":45}, 
{"arrivalDate": '05/05/2020', "totalGood": 17, "totalBad":28}, 
{"arrivalDate": '06/05/2020', "totalGood": 2, "totalBad":28}, 
{"arrivalDate": '07/05/2020', "totalGood": 12, "totalBad":37}, 
{"arrivalDate": '08/05/2020', "totalGood": 21, "totalBad":25}, 
{"arrivalDate": '09/05/2020', "totalGood": 43, "totalBad":39}, 
{"arrivalDate": '10/05/2020', "totalGood": 43, "totalBad":12}, 
{"arrivalDate": '11/05/2020', "totalGood": 39, "totalBad":41}, 
{"arrivalDate": '12/05/2020', "totalGood": 33, "totalBad":38}, 
{"arrivalDate": '13/05/2020', "totalGood": 13, "totalBad":6}, 
{"arrivalDate": '14/05/2020', "totalGood": 3, "totalBad":32}, 
{"arrivalDate": '15/05/2020', "totalGood": 39, "totalBad":22}, 
{"arrivalDate": '16/05/2020', "totalGood": 43, "totalBad":5}, 
{"arrivalDate": '17/05/2020', "totalGood": 10, "totalBad":43}, 
{"arrivalDate": '18/05/2020', "totalGood": 24, "totalBad":13}, 
{"arrivalDate": '19/05/2020', "totalGood": 44, "totalBad":28}, 
{"arrivalDate": '20/05/2020', "totalGood": 47, "totalBad":37}, 
{"arrivalDate": '21/05/2020', "totalGood": 13, "totalBad":13}, 
{"arrivalDate": '22/05/2020', "totalGood": 32, "totalBad":32}, 
{"arrivalDate": '23/05/2020', "totalGood": 27, "totalBad":33}, 
{"arrivalDate": '24/05/2020', "totalGood": 45, "totalBad":9}, 
{"arrivalDate": '25/05/2020', "totalGood": 13, "totalBad":4}, 
{"arrivalDate": '26/05/2020', "totalGood": 13, "totalBad":23}, 
{"arrivalDate": '27/05/2020', "totalGood": 32, "totalBad":31}, 
{"arrivalDate": '28/05/2020', "totalGood": 44, "totalBad":48}, 
{"arrivalDate": '29/05/2020', "totalGood": 1, "totalBad":30}, 
{"arrivalDate": '30/05/2020', "totalGood": 23, "totalBad":42}, 
{"arrivalDate": '31/05/2020', "totalGood": 19, "totalBad":41}, 
{"arrivalDate": '01/06/2020', "totalGood": 41, "totalBad":50}, 
{"arrivalDate": '02/06/2020', "totalGood": 15, "totalBad":11}, 
{"arrivalDate": '03/06/2020', "totalGood": 40, "totalBad":7}, 
{"arrivalDate": '04/06/2020', "totalGood": 4, "totalBad":45}, 
{"arrivalDate": '05/06/2020', "totalGood": 45, "totalBad":49}, 
{"arrivalDate": '06/06/2020', "totalGood": 29, "totalBad":30}, 
{"arrivalDate": '07/06/2020', "totalGood": 33, "totalBad":5}, 
{"arrivalDate": '08/06/2020', "totalGood": 9, "totalBad":15}, 
{"arrivalDate": '09/06/2020', "totalGood": 16, "totalBad":40}, 
{"arrivalDate": '10/06/2020', "totalGood": 26, "totalBad":34}, 
{"arrivalDate": '11/06/2020', "totalGood": 18, "totalBad":12}, 
{"arrivalDate": '12/06/2020', "totalGood": 21, "totalBad":45}, 
{"arrivalDate": '13/06/2020', "totalGood": 21, "totalBad":2}, 
{"arrivalDate": '14/06/2020', "totalGood": 46, "totalBad":16}, 
{"arrivalDate": '15/06/2020', "totalGood": 17, "totalBad":16}, 
{"arrivalDate": '16/06/2020', "totalGood": 40, "totalBad":28}, 
{"arrivalDate": '17/06/2020', "totalGood": 24, "totalBad":2}, 
{"arrivalDate": '18/06/2020', "totalGood": 36, "totalBad":3}, 
{"arrivalDate": '19/06/2020', "totalGood": 10, "totalBad":23}, 
{"arrivalDate": '20/06/2020', "totalGood": 29, "totalBad":16}, 
{"arrivalDate": '21/06/2020', "totalGood": 4, "totalBad":14}, 
{"arrivalDate": '22/06/2020', "totalGood": 4, "totalBad":6}, 
{"arrivalDate": '23/06/2020', "totalGood": 50, "totalBad":30}, 
{"arrivalDate": '24/06/2020', "totalGood": 24, "totalBad":47}, 
{"arrivalDate": '25/06/2020', "totalGood": 15, "totalBad":35}, 
{"arrivalDate": '26/06/2020', "totalGood": 23, "totalBad":28}, 
{"arrivalDate": '27/06/2020', "totalGood": 11, "totalBad":24}, 
{"arrivalDate": '28/06/2020', "totalGood": 47, "totalBad":28}, 
{"arrivalDate": '29/06/2020', "totalGood": 44, "totalBad":23}, 
{"arrivalDate": '30/06/2020', "totalGood": 7, "totalBad":36}, 
{"arrivalDate": '01/07/2020', "totalGood": 1, "totalBad":15}, 
{"arrivalDate": '02/07/2020', "totalGood": 30, "totalBad":27}, 
{"arrivalDate": '03/07/2020', "totalGood": 45, "totalBad":31}, 
{"arrivalDate": '04/07/2020', "totalGood": 5, "totalBad":43}, 
{"arrivalDate": '05/07/2020', "totalGood": 3, "totalBad":30}, 
{"arrivalDate": '06/07/2020', "totalGood": 40, "totalBad":29}, 
{"arrivalDate": '07/07/2020', "totalGood": 4, "totalBad":43}, 
{"arrivalDate": '08/07/2020', "totalGood": 41, "totalBad":6}, 
{"arrivalDate": '09/07/2020', "totalGood": 17, "totalBad":43}, 
{"arrivalDate": '10/07/2020', "totalGood": 8, "totalBad":10}, 
{"arrivalDate": '11/07/2020', "totalGood": 27, "totalBad":19}, 
{"arrivalDate": '12/07/2020', "totalGood": 47, "totalBad":6}, 
{"arrivalDate": '13/07/2020', "totalGood": 23, "totalBad":38}, 
{"arrivalDate": '14/07/2020', "totalGood": 26, "totalBad":8}, 
{"arrivalDate": '15/07/2020', "totalGood": 24, "totalBad":9}, 
{"arrivalDate": '16/07/2020', "totalGood": 48, "totalBad":28}, 
{"arrivalDate": '17/07/2020', "totalGood": 27, "totalBad":42}, 
{"arrivalDate": '18/07/2020', "totalGood": 49, "totalBad":35}, 
{"arrivalDate": '19/07/2020', "totalGood": 18, "totalBad":7}, 
{"arrivalDate": '20/07/2020', "totalGood": 32, "totalBad":25}], 
"SKBO":[{"arrivalDate": '22/04/2020', "totalGood": 16, "totalBad":20}, 
{"arrivalDate": '23/04/2020', "totalGood": 42, "totalBad":17}, 
{"arrivalDate": '24/04/2020', "totalGood": 15, "totalBad":4}, 
{"arrivalDate": '25/04/2020', "totalGood": 32, "totalBad":29}, 
{"arrivalDate": '26/04/2020', "totalGood": 42, "totalBad":46}, 
{"arrivalDate": '27/04/2020', "totalGood": 32, "totalBad":3}, 
{"arrivalDate": '28/04/2020', "totalGood": 41, "totalBad":26}, 
{"arrivalDate": '29/04/2020', "totalGood": 21, "totalBad":46}, 
{"arrivalDate": '30/04/2020', "totalGood": 38, "totalBad":44}, 
{"arrivalDate": '01/05/2020', "totalGood": 16, "totalBad":18}, 
{"arrivalDate": '02/05/2020', "totalGood": 12, "totalBad":22}, 
{"arrivalDate": '03/05/2020', "totalGood": 1, "totalBad":2}, 
{"arrivalDate": '04/05/2020', "totalGood": 19, "totalBad":37}, 
{"arrivalDate": '05/05/2020', "totalGood": 11, "totalBad":46}, 
{"arrivalDate": '06/05/2020', "totalGood": 49, "totalBad":46}, 
{"arrivalDate": '07/05/2020', "totalGood": 29, "totalBad":19}, 
{"arrivalDate": '08/05/2020', "totalGood": 46, "totalBad":16}, 
{"arrivalDate": '09/05/2020', "totalGood": 44, "totalBad":16}, 
{"arrivalDate": '10/05/2020', "totalGood": 23, "totalBad":31}, 
{"arrivalDate": '11/05/2020', "totalGood": 16, "totalBad":2}, 
{"arrivalDate": '12/05/2020', "totalGood": 3, "totalBad":24}, 
{"arrivalDate": '13/05/2020', "totalGood": 38, "totalBad":17}, 
{"arrivalDate": '14/05/2020', "totalGood": 32, "totalBad":3}, 
{"arrivalDate": '15/05/2020', "totalGood": 24, "totalBad":38}, 
{"arrivalDate": '16/05/2020', "totalGood": 35, "totalBad":39}, 
{"arrivalDate": '17/05/2020', "totalGood": 9, "totalBad":43}, 
{"arrivalDate": '18/05/2020', "totalGood": 23, "totalBad":31}, 
{"arrivalDate": '19/05/2020', "totalGood": 34, "totalBad":38}, 
{"arrivalDate": '20/05/2020', "totalGood": 20, "totalBad":14}, 
{"arrivalDate": '21/05/2020', "totalGood": 2, "totalBad":36}, 
{"arrivalDate": '22/05/2020', "totalGood": 33, "totalBad":19}, 
{"arrivalDate": '23/05/2020', "totalGood": 4, "totalBad":42}, 
{"arrivalDate": '24/05/2020', "totalGood": 11, "totalBad":45}, 
{"arrivalDate": '25/05/2020', "totalGood": 7, "totalBad":45}, 
{"arrivalDate": '26/05/2020', "totalGood": 32, "totalBad":21}, 
{"arrivalDate": '27/05/2020', "totalGood": 10, "totalBad":27}, 
{"arrivalDate": '28/05/2020', "totalGood": 27, "totalBad":3}, 
{"arrivalDate": '29/05/2020', "totalGood": 43, "totalBad":19}, 
{"arrivalDate": '30/05/2020', "totalGood": 19, "totalBad":45}, 
{"arrivalDate": '31/05/2020', "totalGood": 35, "totalBad":6}, 
{"arrivalDate": '01/06/2020', "totalGood": 46, "totalBad":18}, 
{"arrivalDate": '02/06/2020', "totalGood": 1, "totalBad":40}, 
{"arrivalDate": '03/06/2020', "totalGood": 44, "totalBad":2}, 
{"arrivalDate": '04/06/2020', "totalGood": 36, "totalBad":21}, 
{"arrivalDate": '05/06/2020', "totalGood": 3, "totalBad":40}, 
{"arrivalDate": '06/06/2020', "totalGood": 11, "totalBad":11}, 
{"arrivalDate": '07/06/2020', "totalGood": 28, "totalBad":21}, 
{"arrivalDate": '08/06/2020', "totalGood": 1, "totalBad":31}, 
{"arrivalDate": '09/06/2020', "totalGood": 4, "totalBad":23}, 
{"arrivalDate": '10/06/2020', "totalGood": 6, "totalBad":45}, 
{"arrivalDate": '11/06/2020', "totalGood": 36, "totalBad":28}, 
{"arrivalDate": '12/06/2020', "totalGood": 38, "totalBad":10}, 
{"arrivalDate": '13/06/2020', "totalGood": 26, "totalBad":37}, 
{"arrivalDate": '14/06/2020', "totalGood": 49, "totalBad":2}, 
{"arrivalDate": '15/06/2020', "totalGood": 18, "totalBad":5}, 
{"arrivalDate": '16/06/2020', "totalGood": 18, "totalBad":16}, 
{"arrivalDate": '17/06/2020', "totalGood": 33, "totalBad":14}, 
{"arrivalDate": '18/06/2020', "totalGood": 49, "totalBad":1}, 
{"arrivalDate": '19/06/2020', "totalGood": 2, "totalBad":4}, 
{"arrivalDate": '20/06/2020', "totalGood": 49, "totalBad":15}, 
{"arrivalDate": '21/06/2020', "totalGood": 17, "totalBad":17}, 
{"arrivalDate": '22/06/2020', "totalGood": 6, "totalBad":9}, 
{"arrivalDate": '23/06/2020', "totalGood": 21, "totalBad":27}, 
{"arrivalDate": '24/06/2020', "totalGood": 40, "totalBad":7}, 
{"arrivalDate": '25/06/2020', "totalGood": 21, "totalBad":2}, 
{"arrivalDate": '26/06/2020', "totalGood": 25, "totalBad":50}, 
{"arrivalDate": '27/06/2020', "totalGood": 20, "totalBad":44}, 
{"arrivalDate": '28/06/2020', "totalGood": 20, "totalBad":20}, 
{"arrivalDate": '29/06/2020', "totalGood": 20, "totalBad":45}, 
{"arrivalDate": '30/06/2020', "totalGood": 31, "totalBad":43}, 
{"arrivalDate": '01/07/2020', "totalGood": 47, "totalBad":46}, 
{"arrivalDate": '02/07/2020', "totalGood": 50, "totalBad":32}, 
{"arrivalDate": '03/07/2020', "totalGood": 27, "totalBad":20}, 
{"arrivalDate": '04/07/2020', "totalGood": 46, "totalBad":49}, 
{"arrivalDate": '05/07/2020', "totalGood": 12, "totalBad":30}, 
{"arrivalDate": '06/07/2020', "totalGood": 22, "totalBad":43}, 
{"arrivalDate": '07/07/2020', "totalGood": 22, "totalBad":41}, 
{"arrivalDate": '08/07/2020', "totalGood": 10, "totalBad":8}, 
{"arrivalDate": '09/07/2020', "totalGood": 23, "totalBad":12}, 
{"arrivalDate": '10/07/2020', "totalGood": 7, "totalBad":11}, 
{"arrivalDate": '11/07/2020', "totalGood": 40, "totalBad":44}, 
{"arrivalDate": '12/07/2020', "totalGood": 41, "totalBad":20}, 
{"arrivalDate": '13/07/2020', "totalGood": 37, "totalBad":2}, 
{"arrivalDate": '14/07/2020', "totalGood": 34, "totalBad":40}, 
{"arrivalDate": '15/07/2020', "totalGood": 2, "totalBad":4}, 
{"arrivalDate": '16/07/2020', "totalGood": 2, "totalBad":20}, 
{"arrivalDate": '17/07/2020', "totalGood": 16, "totalBad":2}, 
{"arrivalDate": '18/07/2020', "totalGood": 18, "totalBad":42}, 
{"arrivalDate": '19/07/2020', "totalGood": 44, "totalBad":12}, 
{"arrivalDate": '20/07/2020', "totalGood": 7, "totalBad":2}], 
"SUAA":[{"arrivalDate": '22/04/2020', "totalGood": 38, "totalBad":43}, 
{"arrivalDate": '23/04/2020', "totalGood": 21, "totalBad":17}, 
{"arrivalDate": '24/04/2020', "totalGood": 41, "totalBad":15}, 
{"arrivalDate": '25/04/2020', "totalGood": 25, "totalBad":18}, 
{"arrivalDate": '26/04/2020', "totalGood": 33, "totalBad":33}, 
{"arrivalDate": '27/04/2020', "totalGood": 23, "totalBad":49}, 
{"arrivalDate": '28/04/2020', "totalGood": 10, "totalBad":23}, 
{"arrivalDate": '29/04/2020', "totalGood": 40, "totalBad":49}, 
{"arrivalDate": '30/04/2020', "totalGood": 23, "totalBad":41}, 
{"arrivalDate": '01/05/2020', "totalGood": 26, "totalBad":23}, 
{"arrivalDate": '02/05/2020', "totalGood": 8, "totalBad":41}, 
{"arrivalDate": '03/05/2020', "totalGood": 4, "totalBad":43}, 
{"arrivalDate": '04/05/2020', "totalGood": 23, "totalBad":28}, 
{"arrivalDate": '05/05/2020', "totalGood": 41, "totalBad":3}, 
{"arrivalDate": '06/05/2020', "totalGood": 47, "totalBad":31}, 
{"arrivalDate": '07/05/2020', "totalGood": 1, "totalBad":45}, 
{"arrivalDate": '08/05/2020', "totalGood": 29, "totalBad":11}, 
{"arrivalDate": '09/05/2020', "totalGood": 41, "totalBad":30}, 
{"arrivalDate": '10/05/2020', "totalGood": 37, "totalBad":49}, 
{"arrivalDate": '11/05/2020', "totalGood": 17, "totalBad":43}, 
{"arrivalDate": '12/05/2020', "totalGood": 25, "totalBad":5}, 
{"arrivalDate": '13/05/2020', "totalGood": 46, "totalBad":15}, 
{"arrivalDate": '14/05/2020', "totalGood": 44, "totalBad":20}, 
{"arrivalDate": '15/05/2020', "totalGood": 1, "totalBad":47}, 
{"arrivalDate": '16/05/2020', "totalGood": 40, "totalBad":36}, 
{"arrivalDate": '17/05/2020', "totalGood": 12, "totalBad":6}, 
{"arrivalDate": '18/05/2020', "totalGood": 40, "totalBad":48}, 
{"arrivalDate": '19/05/2020', "totalGood": 12, "totalBad":29}, 
{"arrivalDate": '20/05/2020', "totalGood": 31, "totalBad":48}, 
{"arrivalDate": '21/05/2020', "totalGood": 3, "totalBad":43}, 
{"arrivalDate": '22/05/2020', "totalGood": 48, "totalBad":47}, 
{"arrivalDate": '23/05/2020', "totalGood": 47, "totalBad":20}, 
{"arrivalDate": '24/05/2020', "totalGood": 27, "totalBad":8}, 
{"arrivalDate": '25/05/2020', "totalGood": 33, "totalBad":7}, 
{"arrivalDate": '26/05/2020', "totalGood": 29, "totalBad":7}, 
{"arrivalDate": '27/05/2020', "totalGood": 5, "totalBad":3}, 
{"arrivalDate": '28/05/2020', "totalGood": 7, "totalBad":11}, 
{"arrivalDate": '29/05/2020', "totalGood": 10, "totalBad":39}, 
{"arrivalDate": '30/05/2020', "totalGood": 36, "totalBad":43}, 
{"arrivalDate": '31/05/2020', "totalGood": 12, "totalBad":23}, 
{"arrivalDate": '01/06/2020', "totalGood": 23, "totalBad":17}, 
{"arrivalDate": '02/06/2020', "totalGood": 35, "totalBad":14}, 
{"arrivalDate": '03/06/2020', "totalGood": 17, "totalBad":30}, 
{"arrivalDate": '04/06/2020', "totalGood": 2, "totalBad":29}, 
{"arrivalDate": '05/06/2020', "totalGood": 25, "totalBad":33}, 
{"arrivalDate": '06/06/2020', "totalGood": 30, "totalBad":17}, 
{"arrivalDate": '07/06/2020', "totalGood": 49, "totalBad":2}, 
{"arrivalDate": '08/06/2020', "totalGood": 42, "totalBad":17}, 
{"arrivalDate": '09/06/2020', "totalGood": 13, "totalBad":9}, 
{"arrivalDate": '10/06/2020', "totalGood": 35, "totalBad":35}, 
{"arrivalDate": '11/06/2020', "totalGood": 17, "totalBad":20}, 
{"arrivalDate": '12/06/2020', "totalGood": 19, "totalBad":30}, 
{"arrivalDate": '13/06/2020', "totalGood": 14, "totalBad":26}, 
{"arrivalDate": '14/06/2020', "totalGood": 4, "totalBad":47}, 
{"arrivalDate": '15/06/2020', "totalGood": 30, "totalBad":4}, 
{"arrivalDate": '16/06/2020', "totalGood": 12, "totalBad":34}, 
{"arrivalDate": '17/06/2020', "totalGood": 6, "totalBad":26}, 
{"arrivalDate": '18/06/2020', "totalGood": 41, "totalBad":1}, 
{"arrivalDate": '19/06/2020', "totalGood": 5, "totalBad":13}, 
{"arrivalDate": '20/06/2020', "totalGood": 47, "totalBad":46}, 
{"arrivalDate": '21/06/2020', "totalGood": 17, "totalBad":28}, 
{"arrivalDate": '22/06/2020', "totalGood": 8, "totalBad":2}, 
{"arrivalDate": '23/06/2020', "totalGood": 42, "totalBad":22}, 
{"arrivalDate": '24/06/2020', "totalGood": 18, "totalBad":30}, 
{"arrivalDate": '25/06/2020', "totalGood": 29, "totalBad":21}, 
{"arrivalDate": '26/06/2020', "totalGood": 37, "totalBad":45}, 
{"arrivalDate": '27/06/2020', "totalGood": 36, "totalBad":5}, 
{"arrivalDate": '28/06/2020', "totalGood": 14, "totalBad":14}, 
{"arrivalDate": '29/06/2020', "totalGood": 18, "totalBad":20}, 
{"arrivalDate": '30/06/2020', "totalGood": 44, "totalBad":41}, 
{"arrivalDate": '01/07/2020', "totalGood": 19, "totalBad":1}, 
{"arrivalDate": '02/07/2020', "totalGood": 37, "totalBad":43}, 
{"arrivalDate": '03/07/2020', "totalGood": 17, "totalBad":50}, 
{"arrivalDate": '04/07/2020', "totalGood": 29, "totalBad":49}, 
{"arrivalDate": '05/07/2020', "totalGood": 1, "totalBad":19}, 
{"arrivalDate": '06/07/2020', "totalGood": 15, "totalBad":43}, 
{"arrivalDate": '07/07/2020', "totalGood": 30, "totalBad":34}, 
{"arrivalDate": '08/07/2020', "totalGood": 16, "totalBad":44}, 
{"arrivalDate": '09/07/2020', "totalGood": 42, "totalBad":40}, 
{"arrivalDate": '10/07/2020', "totalGood": 24, "totalBad":38}, 
{"arrivalDate": '11/07/2020', "totalGood": 4, "totalBad":3}, 
{"arrivalDate": '12/07/2020', "totalGood": 42, "totalBad":32}, 
{"arrivalDate": '13/07/2020', "totalGood": 4, "totalBad":42}, 
{"arrivalDate": '14/07/2020', "totalGood": 17, "totalBad":3}, 
{"arrivalDate": '15/07/2020', "totalGood": 2, "totalBad":17}, 
{"arrivalDate": '16/07/2020', "totalGood": 4, "totalBad":10}, 
{"arrivalDate": '17/07/2020', "totalGood": 19, "totalBad":22}, 
{"arrivalDate": '18/07/2020', "totalGood": 30, "totalBad":24}, 
{"arrivalDate": '19/07/2020', "totalGood": 17, "totalBad":12}, 
{"arrivalDate": '20/07/2020', "totalGood": 6, "totalBad":28}], 
"LFPG":[{"arrivalDate": '22/04/2020', "totalGood": 25, "totalBad":16}, 
{"arrivalDate": '23/04/2020', "totalGood": 30, "totalBad":21}, 
{"arrivalDate": '24/04/2020', "totalGood": 2, "totalBad":32}, 
{"arrivalDate": '25/04/2020', "totalGood": 16, "totalBad":9}, 
{"arrivalDate": '26/04/2020', "totalGood": 40, "totalBad":20}, 
{"arrivalDate": '27/04/2020', "totalGood": 10, "totalBad":23}, 
{"arrivalDate": '28/04/2020', "totalGood": 43, "totalBad":47}, 
{"arrivalDate": '29/04/2020', "totalGood": 8, "totalBad":3}, 
{"arrivalDate": '30/04/2020', "totalGood": 27, "totalBad":6}, 
{"arrivalDate": '01/05/2020', "totalGood": 24, "totalBad":15}, 
{"arrivalDate": '02/05/2020', "totalGood": 19, "totalBad":11}, 
{"arrivalDate": '03/05/2020', "totalGood": 21, "totalBad":10}, 
{"arrivalDate": '04/05/2020', "totalGood": 49, "totalBad":39}, 
{"arrivalDate": '05/05/2020', "totalGood": 27, "totalBad":24}, 
{"arrivalDate": '06/05/2020', "totalGood": 33, "totalBad":40}, 
{"arrivalDate": '07/05/2020', "totalGood": 31, "totalBad":2}, 
{"arrivalDate": '08/05/2020', "totalGood": 25, "totalBad":14}, 
{"arrivalDate": '09/05/2020', "totalGood": 22, "totalBad":20}, 
{"arrivalDate": '10/05/2020', "totalGood": 44, "totalBad":39}, 
{"arrivalDate": '11/05/2020', "totalGood": 9, "totalBad":4}, 
{"arrivalDate": '12/05/2020', "totalGood": 42, "totalBad":46}, 
{"arrivalDate": '13/05/2020', "totalGood": 22, "totalBad":25}, 
{"arrivalDate": '14/05/2020', "totalGood": 16, "totalBad":13}, 
{"arrivalDate": '15/05/2020', "totalGood": 23, "totalBad":29}, 
{"arrivalDate": '16/05/2020', "totalGood": 35, "totalBad":43}, 
{"arrivalDate": '17/05/2020', "totalGood": 29, "totalBad":3}, 
{"arrivalDate": '18/05/2020', "totalGood": 39, "totalBad":13}, 
{"arrivalDate": '19/05/2020', "totalGood": 45, "totalBad":25}, 
{"arrivalDate": '20/05/2020', "totalGood": 42, "totalBad":40}, 
{"arrivalDate": '21/05/2020', "totalGood": 9, "totalBad":3}, 
{"arrivalDate": '22/05/2020', "totalGood": 37, "totalBad":18}, 
{"arrivalDate": '23/05/2020', "totalGood": 38, "totalBad":22}, 
{"arrivalDate": '24/05/2020', "totalGood": 12, "totalBad":22}, 
{"arrivalDate": '25/05/2020', "totalGood": 33, "totalBad":35}, 
{"arrivalDate": '26/05/2020', "totalGood": 42, "totalBad":10}, 
{"arrivalDate": '27/05/2020', "totalGood": 32, "totalBad":18}, 
{"arrivalDate": '28/05/2020', "totalGood": 40, "totalBad":33}, 
{"arrivalDate": '29/05/2020', "totalGood": 3, "totalBad":10}, 
{"arrivalDate": '30/05/2020', "totalGood": 45, "totalBad":49}, 
{"arrivalDate": '31/05/2020', "totalGood": 39, "totalBad":10}, 
{"arrivalDate": '01/06/2020', "totalGood": 3, "totalBad":5}, 
{"arrivalDate": '02/06/2020', "totalGood": 4, "totalBad":26}, 
{"arrivalDate": '03/06/2020', "totalGood": 9, "totalBad":32}, 
{"arrivalDate": '04/06/2020', "totalGood": 7, "totalBad":28}, 
{"arrivalDate": '05/06/2020', "totalGood": 45, "totalBad":16}, 
{"arrivalDate": '06/06/2020', "totalGood": 23, "totalBad":31}, 
{"arrivalDate": '07/06/2020', "totalGood": 19, "totalBad":20}, 
{"arrivalDate": '08/06/2020', "totalGood": 32, "totalBad":4}, 
{"arrivalDate": '09/06/2020', "totalGood": 23, "totalBad":21}, 
{"arrivalDate": '10/06/2020', "totalGood": 45, "totalBad":25}, 
{"arrivalDate": '11/06/2020', "totalGood": 6, "totalBad":42}, 
{"arrivalDate": '12/06/2020', "totalGood": 48, "totalBad":20}, 
{"arrivalDate": '13/06/2020', "totalGood": 2, "totalBad":3}, 
{"arrivalDate": '14/06/2020', "totalGood": 47, "totalBad":43}, 
{"arrivalDate": '15/06/2020', "totalGood": 36, "totalBad":25}, 
{"arrivalDate": '16/06/2020', "totalGood": 20, "totalBad":10}, 
{"arrivalDate": '17/06/2020', "totalGood": 30, "totalBad":37}, 
{"arrivalDate": '18/06/2020', "totalGood": 49, "totalBad":23}, 
{"arrivalDate": '19/06/2020', "totalGood": 14, "totalBad":10}, 
{"arrivalDate": '20/06/2020', "totalGood": 9, "totalBad":21}, 
{"arrivalDate": '21/06/2020', "totalGood": 10, "totalBad":13}, 
{"arrivalDate": '22/06/2020', "totalGood": 14, "totalBad":23}, 
{"arrivalDate": '23/06/2020', "totalGood": 31, "totalBad":32}, 
{"arrivalDate": '24/06/2020', "totalGood": 38, "totalBad":34}, 
{"arrivalDate": '25/06/2020', "totalGood": 19, "totalBad":49}, 
{"arrivalDate": '26/06/2020', "totalGood": 38, "totalBad":18}, 
{"arrivalDate": '27/06/2020', "totalGood": 43, "totalBad":43}, 
{"arrivalDate": '28/06/2020', "totalGood": 26, "totalBad":22}, 
{"arrivalDate": '29/06/2020', "totalGood": 38, "totalBad":9}, 
{"arrivalDate": '30/06/2020', "totalGood": 2, "totalBad":28}, 
{"arrivalDate": '01/07/2020', "totalGood": 1, "totalBad":9}, 
{"arrivalDate": '02/07/2020', "totalGood": 39, "totalBad":21}, 
{"arrivalDate": '03/07/2020', "totalGood": 34, "totalBad":40}, 
{"arrivalDate": '04/07/2020', "totalGood": 33, "totalBad":12}, 
{"arrivalDate": '05/07/2020', "totalGood": 14, "totalBad":18}, 
{"arrivalDate": '06/07/2020', "totalGood": 21, "totalBad":40}, 
{"arrivalDate": '07/07/2020', "totalGood": 43, "totalBad":48}, 
{"arrivalDate": '08/07/2020', "totalGood": 50, "totalBad":33}, 
{"arrivalDate": '09/07/2020', "totalGood": 43, "totalBad":35}, 
{"arrivalDate": '10/07/2020', "totalGood": 24, "totalBad":15}, 
{"arrivalDate": '11/07/2020', "totalGood": 25, "totalBad":31}, 
{"arrivalDate": '12/07/2020', "totalGood": 29, "totalBad":41}, 
{"arrivalDate": '13/07/2020', "totalGood": 15, "totalBad":22}, 
{"arrivalDate": '14/07/2020', "totalGood": 34, "totalBad":23}, 
{"arrivalDate": '15/07/2020', "totalGood": 3, "totalBad":10}, 
{"arrivalDate": '16/07/2020', "totalGood": 10, "totalBad":22}, 
{"arrivalDate": '17/07/2020', "totalGood": 22, "totalBad":4}, 
{"arrivalDate": '18/07/2020', "totalGood": 33, "totalBad":48}, 
{"arrivalDate": '19/07/2020', "totalGood": 4, "totalBad":34}, 
{"arrivalDate": '20/07/2020', "totalGood": 22, "totalBad":32}]} 


class ShippingUpload extends React.Component {
  constructor(props) {
    super(props);
    this.loadShippingsFile = this.loadShippingsFile.bind(this);
    this.clearFile = this.clearFile.bind(this);
    this.loadFile = this.loadFile.bind(this);
    this.cancelLoad = this.cancelLoad.bind(this);
    this.openModal = this.openModal.bind(this);
    this.handlerCloseModal = this.handlerCloseModal.bind(this);
    this.handleWarehouseChange = this.handleWarehouseChange.bind(this);
    this.filterBar = this.filterBar.bind(this); 
    this.handlerDate = this.handlerDate.bind(this)
    this.showDateTime = this.showDateTime(this);
    this.state = {
      selectedFile: null,
      fileName:'Seleccionar archivo',
      fileError:false,
      modalOpen: false,
      warehouses:[],
      response:null,
      loadSuccess:false,
      warehouseSelected:'',
      isLoading:false,
      isLoading2:false,
      loadingFile:false,
      goodView:true,
      badView:true,
      selectedDate:'',
      secondGraph:null,
      datetimeOpen:null
    }
  }
  showDateTime(){
    this.setState({
      datetimeOpen:true
    })
  }
  handlerDate(event){
    console.log(event.format("YYYYMMDD").toString())
    this.setState({
      selectedDate:event.format("YYYYMMDD").toString(),
      isLoading2:true,
      datetimeOpen:false
    }, ()=>{
      if(event.format("YYYYMMDD").toString()!=''){
        var aux = {icaoCode:this.state.warehouseSelected, date:event.format("YYYYMMDD").toString()}
        axios.post(apiURL.value + 'warehouse/massiveHour', aux).then((res)=>{
          if(res.status==200){
            console.log(res.data)
            this.setState({
              secondGraph:res.data,
              isLoading2:false
            })
          }
          else{
            toast.error('Ocurrió un error en la consulta', { type: toast.TYPE.ERROR, autoClose: 5000 })
          }
        })
        .catch(()=>{
          toast.error('Ocurrió un error en el servidor', { type: toast.TYPE.ERROR, autoClose: 5000 })
        })
      }
    })
  }
  filterBar(event){
    console.log(event.dataKey)
    if(event.dataKey=='totalGood' || event.dataKey=='totalGoodHidden'){
      this.setState({
        goodView:!this.state.goodView
      })
    }
    else if(event.dataKey=='totalBad' || event.dataKey=='totalBadHidden'){
      this.setState({
        badView:!this.state.badView
      })
    }
  }
  handleWarehouseChange(event){
    this.setState({
      warehouseSelected: event.target.value,
      selectedDate:'',
      isLoading:true,
      response:null,
      secondGraph:null
    }, ()=>{
      var body ={"icaoCode" : this.state.warehouseSelected.toString()}
      axios.post(apiURL.value + 'warehouse/massiveTest', body).then((res)=>{
        if(res.status==200){
          this.setState({
            response:res.data,
            isLoading:false
          }, ()=>{
            console.log('DATA', this.state.response)
          })
        }
        else{
          toast.error('Ocurrió un error en la consulta', { type: toast.TYPE.ERROR, autoClose: 5000 })
          this.setState({
            isLoading:false,
            response:null
          })
        }
      })
      .catch(()=>{
        toast.error('Ocurrió un error en el servidor', { type: toast.TYPE.ERROR, autoClose: 5000 })
        this.setState({
          isLoading:false,
          response:null
        })
      })
    })
  }
  handlerCloseModal(){
    this.setState({
      modalOpen:false
    })
  }
  openModal(){
    if(!this.state.selectedFile){
      this.setState({
        fileError:true
      })
      toast.error('No ha seleccionado un archivo para la carga', { type: toast.TYPE.ERROR, autoClose: 5000 })
    }
    else{
      this.setState({
        fileError:false,
        modalOpen:true
      })
    }
  }
  cancelLoad(){
    this.props.history.push('/'+ shippingList.url)
  }
  async loadFile(){
    if(!this.state.selectedFile){
      this.setState({
        fileError:true
      })
      toast.error('No ha seleccionado un archivo para la carga', { type: toast.TYPE.ERROR, autoClose: 5000 })
    }
    else{
      
      const data = new FormData()
      console.log(this.state.selectedFile);
      data.append('file', this.state.selectedFile)
      console.log(data);
      console.log(this.state.selectedFile);
      await this.setState({
        modalOpen:false,
        loadingFile:true,
      })
      axios.post(apiURL.value + 'warehouse/massive', data).then((res)=>{
        if(res.status==200){
          toast.success('Se cargó el archivo correctamente', { type: toast.TYPE.SUCCESS, autoClose: 5000 })
          this.setState({
            fileError:false,
            selectedFile:null,
            fileName:'Seleccionar archivo',
            loadingFile:false,
            loadSuccess:true
          }, ()=>{
            axios.post(apiURL.value + 'city/list').then((res)=>{
              if(res.status==200){
                var cities = res.data.map((city, index)=>
                  <option value={city.airportCode}>{city.name}</option>
                )
                this.setState({
                  warehouses:cities
                })
              }
            })
          });
          
        }
        else{
          toast.error('Ocurrió un error en la carga del archivo', { type: toast.TYPE.ERROR, autoClose: 5000 })
          this.setState({
            loadingFile:false
          })
        }
      })
      .catch((resp)=>{
        toast.error('Ocurrió un error en el servidor', { type: toast.TYPE.ERROR, autoClose: 5000 })
        this.setState({
          loadingFile:false
        })
      })
      
    }
  }
  async clearFile(){
    await this.setState({
      selectedFile: null
    });
    this.setState({
      fileName: this.state.selectedFile?this.state.selectedFile.name:'Seleccionar archivo'
    })
  }
  async loadShippingsFile(event){
    var file = event.target.files[0];
    console.log(file);
    // if return true allow to setState
    await this.setState({
      selectedFile: file
    });
    await this.setState({
      fileName: file?file.name:'Seleccionar archivo',
      fileError: file?false:true
    });
    console.log(this.state.selectedFile)
  }
  render(){
    var yesterday = Datetime.moment().subtract( 1, 'day' );
    var valid = function( current ){
        return !current.isAfter( yesterday );
    };
  return (
    <>
    
    <div className="App">
      
      <SideBar fluid toggle={this.sidebarIsClose} isOpen={true}/>
      
      <Container fluid className={classNames('content', {'is-open': true})}>
        <NavbarRedexAdmin style={{position:'fixed', marginTop:'0'}}/>
        <Container style={{marginTop:'10%', backgroundColor:'#FAFAFA'}}>
          <ToastContainer />
          <Card>
            <Col md={{size:6, offset:3}}>
              <div className='text-center'><h4>Simulación</h4></div>
              <Form>
                {this.state.loadingFile?
                <div className='text-center'  style={{width:'100%', weight:'100%', backgroundColor:'#FFFFFF'
                }}>
                  <Loader
                    type="ThreeDots"
                    color="#03256C"
                    height={100}
                    width={100}
                    timeout={3000000}
                  />
                </div> :PassThrough}
                <FormGroup>
                  <Label for="exampleCustomFileBrowser">Plantilla</Label>
                  <a href='https://drive.google.com/drive/u/1/folders/1TlWop8x6q2K-FpHdI6lsD0QuoCjmqkCH' target='_blank'>
                  <u><p style={{fontSize:'14px', color:'#03256C'}}>Link de referencia</p></u>
                  </a>                
                </FormGroup>
               
                <FormGroup>
                  <Label for="exampleCustomFileBrowser">Cargar archivo</Label>
                  <InputGroup>
                    {(this.state.selectedFile)?
                      <InputGroupAddon id="exampleCustomFileBrowser"  addonType="prepend">
                        <InputGroupText  id="exampleCustomFileBrowser"style={this.state.fileError?{padding:'0%', borderColor:'#dc3545'}:{padding:'0%'}} >
                          <Tooltip2 title='Borrar archivo' onClick={this.clearFile}>
                            <Delete style={{color:'#FF4136'}}/>
                          </Tooltip2>
                        </InputGroupText>
                      </InputGroupAddon>:PassThrough
                    } 
                  <CustomInput accept='.zip,.rar,.7zip' style={{backgroundColor: '#000000'}} invalid = {this.state.fileError} 
                    type="file" id="exampleCustomFileBrowser" name="customFile" label={this.state.fileName}
                    value={!this.state.selectedFile?null:PassThrough} 
                    onChange={this.loadShippingsFile}/>
                  </InputGroup>
                </FormGroup>
                <Row style={{marginBottom:'5%', marginTop:'5%'}}>
                  <Button 
                    size="lg"
                    color="warning"
                    outline="warning"
                    style={{marginLeft:'20%'}}
                    onClick={this.cancelLoad}>
                    Cancelar
                  </Button>
                  <Button
                      size="lg"
                      variant="contained"
                      color="primary"
                      style={{marginLeft:'5%'}}
                      onClick={this.openModal}>
                    Cargar
                  </Button>
                </Row>
                {this.state.loadSuccess?<FormGroup style={{marginBottom:'7%'}}>
                  <label htmlFor="exampleFormControlSelect1">Almacén</label>
                  <Input id="countrySelect" type="select" onChange={this.handleWarehouseChange}>
                      <option disabled value={0} selected>Seleccionar</option>
                      {this.state.warehouses}
                  </Input>
                </FormGroup>:PassThrough}
                {(this.state.isLoading && !this.state.response)?
                <div className='text-center'  style={{width:'100%'}}>
                  <Loader
                    type="ThreeDots"
                    color="#03256C"
                    height={100}
                    width={100}
                    timeout={3000000}
                  />
                </div> :PassThrough}
              </Form> 
            </Col>
           {(!this.state.isLoading && this.state.response)? 
              <BarChart
                width= {window.innerWidth-250-window.innerWidth*0.07}
                height={300}
                data={this.state.response[this.state.warehouseSelected]}
                margin={{
                top: 20, bottom: 80,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="arrivalDate" interval={2} angle={-45} textAnchor='end' />
                <YAxis yAxisId="left" orientation="left" stroke="#8884d8" />
                
                <Tooltip />
                <Legend wrapperStyle={{top: 250}} onClick={this.filterBar}/>
                <Bar yAxisId="left" name='Recepcionados' dataKey={this.state.goodView?"totalGood":"totalGoodHidden"} fill="#4c9141" />
                <Bar yAxisId="left" name='No Recepcionados' dataKey={this.state.badView?"totalBad":"totalBadHidden"} fill="#B20004" />
            </BarChart>:PassThrough}
            {(!this.state.isLoading && this.state.response)? 
            <Col md={{size:6, offset:3}}>
              <FormGroup>
                <label >Día de consulta</label>
                <Datetime
                  isValidDate={ valid }
                  dateFormat="DD/MM/YYYY"
                  timeFormat={false}
                  value={this.state.selectedDate !== "" ? (Datetime.moment(this.state.selectedDate)) : ""}
                  inputProps={{ placeholder: "Seleccione fecha de consulta" }}
                  onChange={this.handlerDate}
                  closeOnSelect
                />
              </FormGroup>
            </Col>:PassThrough}
            {this.state.isLoading2?
            <div className='text-center'  style={{width:'100%'}}>
              <Loader
                type="ThreeDots"
                color="#03256C"
                height={100}
                width={100}
                timeout={3000000}
              />
            </div>
            :PassThrough}
            {(!this.state.isLoading2 && this.state.secondGraph)?
            <LineChart
              width={window.innerWidth-250-window.innerWidth*0.07}
              height={400}
              data={this.state.secondGraph[this.state.warehouseSelected]}
              margin={{
                top: 20, bottom: 80,
              }}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="hour" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Line type="monotone" name='Recepcionados' dataKey="totalGood" stroke="#4c9141" activeDot={{ r: 8 }} />
              <Line type="monotone" name='Flujo Total' dataKey="total" stroke="#B20004" />
            </LineChart>:PassThrough}
            
            <Modal toggle={this.handlerCloseModal} isOpen={this.state.modalOpen}>
              <Col  sm="12" md={{ size: 5, offset: 4 }}>
                <HelpOutline style={{ fontSize: 100, color:'#1768AC' }}/>
              </Col>
              <div className="modal-header">
              <div className='text-center'  style={{width:'100%'}}><h5 /* style={{textAlign:'center'}} */  id="exampleModalLiveLabel">
                  ¿Desea confirmar la carga masiva de envíos?
                </h5>
              </div>
                <button aria-label="Close" className="close" type="button" 
                        onClick={this.handlerCloseModal}>
                  <span aria-hidden={true}>×</span>
                </button>
              </div>
              <Col  sm="12" md={{ size: 8, offset: 2 }}>
                <div className="modal-footer">
                  <Button
                    color="danger"
                    type="button"
                    size="lg"
                    onClick={this.handlerCloseModal}>
                    Cancelar
                  </Button>
                  <Button
                    color="primary"
                    type="button"
                    size="lg"
                    onClick={this.loadFile}>
                    Aceptar
                  </Button>
                </div>
              </Col>
            </Modal>
          </Card>   
        </Container>
      </Container>
  
    </div>       
    </>
  );
  }
}

export default ShippingUpload;
