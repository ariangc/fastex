import React from "react";

// reactstrap components
import {
  Button,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  FormGroup,
  Card,
  Row,
  Col
} from "reactstrap";
import {login,apiURL,shipping} from "variables/Variables.js"
import { Link } from "react-router-dom";
// core components
import ExamplesNavbar from "components/Navbars/ExamplesNavbar.js";
import HeaderConstrunet from "components/Headers/HeaderConstrunet.js";
import FooterConstrunet from "components/Footers/FooterConstrunet.js";
import IconButton from '@material-ui/core/IconButton';
import Search from '@material-ui/icons/Search';
import AccessTime from '@material-ui/icons/AccessTime';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import axios from "axios";
import history from "../clients/history";

class QuoteShipping extends React.Component {
    constructor(props) {
        super(props);
        this.homepage = this.homepage.bind(this);
        this.handleOriginCityChange = this.handleOriginCityChange.bind(this);
        this.handleAddresseeCityChange = this.handleAddresseeCityChange.bind(this);

        this.state={
            originCities:[],
            addresseeCities:[],
            originCitySelected:0,
            addresseeCitySelected:0,
            price:0
        }
    }

    homepage(){
        this.props.history.push({pathname: '/landing'})
    }

    handleOriginCityChange(event){
        this.setState({originCitySelected:event.target.value},()=>{
          if(this.state.addresseeCitySelected != 0){
            console.log(this.state.originCitySelected)
            console.log(this.state.addresseeCitySelected)
            var body = {idStartCity:parseInt(this.state.originCitySelected),idEndCity:parseInt(this.state.addresseeCitySelected)}
            axios.post(apiURL.value + 'shipping/quote',body).then((res)=>{
              console.log(res.data);
              if(res.status === 200){
                this.setState({price:(res.data.price/100).toFixed(2)})
              }
            })
          }
        })
    }

    handleAddresseeCityChange(event){
        this.setState({addresseeCitySelected:event.target.value},() => {
          console.log(this.state.originCitySelected)
          console.log(this.state.addresseeCitySelected)
          var body = {idStartCity:parseInt(this.state.originCitySelected),idEndCity:parseInt(this.state.addresseeCitySelected)}
          axios.post(apiURL.value + 'shipping/quote',body).then((res)=>{
            console.log(res.data);
            if(res.status === 200){
              this.setState({price:(res.data.price/100).toFixed(2)})
            }
          })
        })
    }

    componentWillMount(){
        axios.post(apiURL.value + 'city/list').then((res)=>{
            console.log(res);
            if(res.status === 200){
              const citiesRes = res.data.map((city) =>
                          <option value={city.id}>{city.name} - {city.country.name}</option>
                      );
              this.setState({originCities:citiesRes,addresseeCities:citiesRes})
            }
          })
    }

  render(){
  return (
    <>
      <main ref="main">
      <div className="wrapper" >
        <HeaderConstrunet/>
        <div className="container-fluid text-center">
            <Row>
                <Col sm={{ size:8, offset:2}}>
                  <br></br>
                  <h2 style={{marginBottom:"1vh",fontWeight:"bold"}}>Cotiza tu envío</h2>
                  <h4 style={{marginTop:"1vh",marginBottom:"5vh"}}>Para conocer el costo de tu envío, ingresa los siguientes datos</h4>
                </Col>
            </Row>
            <Row>
                <Col sm={{ size:6,offset:3}}>
                    <FormGroup>
                        <label htmlFor="exampleFormControlSelect1" style={{fontSize:"16px", fontWeight:"bold"}}>Ciudad de origen:&nbsp;&nbsp;</label>
                        <Input id="countrySelect" type="select" style={{width:"40%",display:"inline-block"}} value={this.state.originCitySelected} onChange={this.handleOriginCityChange}>
                          <option disabled value={0} selected>Seleccione una ciudad de origen</option>
                          {this.state.originCities}
                        </Input>
                    </FormGroup>
                    <FormGroup style={{marginTop:"5vh"}}>
                        <label htmlFor="exampleFormControlSelect2" style={{fontSize:"16px", fontWeight:"bold"}}>Ciudad de destino:&nbsp;&nbsp;</label>
                        <Input id="countrySelect" type="select" style={{width:"40%",display:"inline-block"}} value={this.state.addresseeCitySelected} onChange={this.handleAddresseeCityChange}>
                          <option disabled value={0} selected>Seleccione una ciudad de destino</option>
                          {this.state.addresseeCities}
                        </Input>
                    </FormGroup>
                    <FormGroup style={{marginTop:"5vh"}}>
                        <label htmlFor="exampleFormControlText" style={{fontSize:"16px", fontWeight:"bold"}}>Tarifa:&nbsp;&nbsp;</label>
                        <Input id="countrySelect" type="text"disabled style={{width:"20%",display:"inline-block"}} value={"$ " + this.state.price}>
                          
                        </Input>
                    </FormGroup>   
                    <label style={{marginTop:"5vh"}}>*La tarifa se calcula en dólares. La tarifa incluye IGV.</label>               
                </Col>

            </Row>
            <Row>
                <Col>
                  <Button
                    size="lg"
                    variant="contained"
                    color="primary"
                    onClick={this.homepage}
                    style={{fontSize:"18px"}}
                    >Inicio
                  </Button>
                </Col>
            </Row>
        </div>
        <FooterConstrunet/>
      </div>
    </main>
    </>
  );
  }
}

export default QuoteShipping;
