import React from "react";

// reactstrap components
import { Grid } from "react-bootstrap";
import {
  Button,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormText,
  Label,
  Card,
  CardTitle,
  Modal
} from "reactstrap";
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import HelpOutline from '@material-ui/icons/HelpOutline';
import classNames from 'classnames';
import {apiURL, warehouse, editWarehouse, editedWarehouse} from "variables/Variables.js"
// core components
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import SideBar from 'components/sidebar/SideBar.js';
import MaterialTable from "material-table";
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const columns = [
{
  title: 'N°',
  field: 'id'
},
{
   title: 'Cod.ICAO',
   field: 'icaoCode'
}, 
{
  title: 'País',
  field: 'city.country.name'
}, 
{
  title: 'Ciudad',
  field: 'city.name'
}, 
{
  title: 'Cap.Ocupada',
  field: 'filledCapacity',
  filtering:false
}, 
{
  title: 'Cap.Total',
  field: 'totalCapacity',
  filtering:false
},
{
  title: '%Límite',
  field: 'capacityLimitPercentage',
  filtering:false
},
{
  title: 'Estado',
  field: 'warehouseStatus.description'
}
];

class WarehouseList extends React.Component {
  constructor(props) {
    super(props);
    this.handlerCloseEditModal = this.handlerCloseEditModal.bind(this);
    this.sidebarIsOpen = this.sidebarIsOpen.bind(this);
    this.sidebarIsClose = this.sidebarIsClose.bind(this);
    this.handlerWarehouse = this.handlerWarehouse.bind(this);
    this.state = {
      selectedRow: null,
      sidebarOpen:true,
      modalOpen:false,
      warehouseToDelete:'',
      warehouses:[]
    }
}
handlerWarehouse(){
  console.log(this.state.selectedRow)
  toast.error('Ha seleccionado esta fila', { type: toast.TYPE.ERROR, autoClose: 5000 })
  warehouse.value = this.state.selectedRow;
  this.props.history.push("/" + editWarehouse.url)
}
handlerCloseEditModal(){
  this.setState({
    modalOpen:false
  })
}

sidebarIsClose(){
    this.setState({
      sidebarOpen:!this.state.sidebarOpen
    })
}

sidebarIsOpen(){
    this.setState({
      sidebarOpen:true
    })
}

componentWillMount(){
  axios.post(apiURL.value + 'warehouse/list').then((res)=>{
    if(res.status === 200){
        console.log(res)
        this.setState({
        warehouses:res.data
        })
    }
  })
  
}
componentDidMount(){
  if(editedWarehouse.value==1){
    toast.success('Se actualizó el almacén correctamente', { type: toast.TYPE.SUCCESS, autoClose: 5000 })
  }
}

render(){
  return (
    <>
    <div className="App">
      
      <SideBar fluid toggle={this.sidebarIsClose} isOpen={this.state.sidebarOpen}/>
       
      <Container
      fluid className={classNames('content', {'is-open': this.state.sidebarOpen})}>
      <NavbarRedexAdmin style={{position:'fixed', marginTop:'0'}}/>
        <Container style={{marginTop:'10%', padding:'0%', backgroundColor:'#FAFAFA'}}>
        <ToastContainer /> 
                <Col> 
                    <MaterialTable
                      title="Almacenes"
                      data={this.state.warehouses}
                      columns={columns}
                      actions={[
                        {
                          icon:'edit',
                          iconButton:{color:'red'},
                          color:'red',
                          tooltip: 'Save User'/* ,
                          onClick: (event, rowData) => alert("You saved " + rowData.name) */
                        },
                        {
                          icon: 'delete',
                          root: 'blue',
                          tooltip: 'Delete User'/* ,
                          onClick: (event, rowData) => confirm("You want to delete " + rowData.name),
                          disabled: rowData.birthYear < 2000 */
                        }
                      ]}
                      components={{
                        Action: 
                          props => {
                            if(props.action.icon === 'edit'){
                              return(
                                <Tooltip title='Editar Almacén'>
                                  <IconButton 
                                  style={{color:'#0074D9'}} 
                                  component="span"
                                  size = 'small'
                                  onClick= {(event, rowData) => {this.setState({selectedRow:props.data}, ()=>{this.handlerWarehouse()})}}
                                  >
                                    <Edit fontSize="large"/>
                                  </IconButton >
                                </Tooltip>
                              )
                            }
                            if(props.action.icon === 'delete'){
                              return(
                                <Tooltip title='Desactivar Almacén'>
                                  <IconButton 
                                  style={{color:'#FF4136'}}
                                  component="span"
                                  onClick = {(event, rowData) => {this.setState({
                                    warehouseToDelete: ' ',
                                    modalOpen:true
                                  })}}
                                  >
                                    <Delete fontSize="large"/>
                                  </IconButton >
                                </Tooltip>
                              )
                            }
                          }
                      }}
                      /* onRowClick={(evt, selectedRow) => {
                        (evt.target).ondblclick = () =>{this.setState({selectedRow}, ()=>{this.handlerWarehouse()})}
                        }} */
                      options={{actionsColumnIndex: -1, filtering:true,
                        rowStyle: rowData => ({
                          backgroundColor: (this.state.selectedRow && this.state.selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
                        })
                      }}
                      localization={{
                        toolbar: {
                          searchPlaceholder: "Buscar"
                        },
                        header:{
                          actions:'Acciones'
                        },
                        body: {
                          emptyDataSourceMessage: 'No hay datos'
                        }
                      }}
                    />
                    <Modal toggle={this.handlerCloseEditModal} isOpen={this.state.modalOpen}>
                        <Col  sm="12" md={{ size: 5, offset: 4 }}><HelpOutline style={{ fontSize: 100, color:'#1768AC' }}/></Col>
                        <div className="modal-header">
                            <h5 style={{textAlign:'center'}} className="modal-title" id="exampleModalLiveLabel">
                                ¿Desea confirmar la desactivación del almacén {this.state.warehouseToDelete}?
                            </h5>
                            <button
                                aria-label="Close"
                                className="close"
                                type="button"
                                onClick={this.handlerCloseEditModal}
                            >
                                <span aria-hidden={true}>×</span>
                            </button>
                        </div>
                        <Col  sm="12" md={{ size: 8, offset: 2 }}>
                            <div className="modal-footer">
                            <Button
                                color="danger"
                                type="button"
                                size="lg"
                                onClick={this.handlerCloseEditModal}
                            >
                                Cancelar
                            </Button>
                            <Button
                                color="primary"
                                type="button"
                                size="lg"
                                onClick={this.handlerCloseEditModal}
                            >
                                Aceptar
                            </Button>
                            </div>
                        </Col>
                    </Modal>
                </Col>
          </Container>
      </Container>
  
    </div>       
    </>
  );
  }
}

export default WarehouseList;
