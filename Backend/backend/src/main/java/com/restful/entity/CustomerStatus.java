package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "CustomerStatus")
@Entity
public class CustomerStatus {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idCustomerStatus")
    private Long id;
    private String description;

    @OneToMany(mappedBy = "customerStatus", cascade = CascadeType.ALL)
    private List<Customer> customers = new ArrayList<>();

    protected CustomerStatus() {}

    public CustomerStatus(Long id, String description, List<Customer> customers) {
        this.id = id;
        this.description = description;
        this.customers = customers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }
}
