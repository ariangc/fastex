package com.restful.model;

public class HourCharge {
    String hour;
    int totalGood;
    int total;

    public HourCharge(String hour, int totalGood, int total) {
        this.hour = hour;
        this.totalGood = totalGood;
        this.total = total;
    }

    public HourCharge() {
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public int getTotalGood() {
        return totalGood;
    }

    public void setTotalGood(int totalGood) {
        this.totalGood = totalGood;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
