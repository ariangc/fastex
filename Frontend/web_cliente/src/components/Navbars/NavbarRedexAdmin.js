import React from "react";
// reactstrap components
import {
  Navbar,
  Container,
  Collapse,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown
} from "reactstrap";
import {currentUser, login} from "variables/Variables.js"
import ExampleComponent from "react-rounded-image";
import photo from "assets/img/user.png";

function NavbarRedexAdmin(props) {
  const [collapseOpen, setCollapseOpen] = React.useState(false);
  return (
    <>
      
      <Navbar className={"top"} expand="lg" style={{backgroundColor:'#1768AC', position:'fixed', width:'82%', 
                                                    zIndex:1000, height:'12vh'}}>
        <Container>
          <h5 style={{fontFamily:'Lato', marginTop:'1%', marginBottom:'1%'}}><b>{props.title}</b></h5>
          
          <Collapse
            className="justify-content-end"
            isOpen={collapseOpen}
            navbar
          >
            <p>Lima - Perú&nbsp;&nbsp;&nbsp;&nbsp;</p>
            <UncontrolledDropdown className="button-dropdown">
              <DropdownToggle
                caret
                data-toggle="dropdown"
                id="navbarDropdown"
                tag="a"
                /* onClick={e => e.preventDefault()} */
              >
                <ExampleComponent
                  image={currentUser.img?currentUser.img:photo}
                  roundedSize="0"
                  imageWidth="30"
                  imageHeight="30"
                />
              </DropdownToggle>
              <DropdownMenu aria-labelledby="navbarDropdown" right>
                <DropdownItem header tag="a">
                  {currentUser.names + ' ' + currentUser.surname}
                </DropdownItem>
                <DropdownItem>
                  Mis Datos
                </DropdownItem>
                <DropdownItem>
                  Ayuda
                </DropdownItem>
                <DropdownItem divider></DropdownItem>
                <DropdownItem href='/login'/* onClick={(event) => {props.history.push("/login")}} */>
                  Cerrar Sesión
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Collapse>
        </Container>
      </Navbar>
    </>
  );
}

export default NavbarRedexAdmin;