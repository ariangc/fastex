/*

=========================================================
* Now UI Kit React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/now-ui-kit-react
* Copyright 2019 Creative Tim (http://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/now-ui-kit-react/blob/master/LICENSE.md)

* Designed by www.invisionapp.com Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import {login, createClient, clientList, createShipping, shippingList, shippingUpload, warehouseList,
        createWarehouse, editWarehouse, employeeList, createEmployee, systemVariables, systemGraphics,
        shippingListAdmin,shippingDetail,deliverShipping, flights, warehouseMap,dashboard} from "variables/Variables.js"
// styles for this kit
import "assets/css/bootstrap.min.css";
import "assets/scss/now-ui-kit.scss";
import "assets/demo/demo.css";
import "assets/demo/nucleo-icons-page-styles.css";
import Login from "views/login/Login.js";
//Clients
import ClientList from "views/clients/ClientList.js"
import CreateClient from "views/clients/CreateClient";
//Shippings
import CreateShipping from "views/shippings/CreateShipping";
import ShippingList from "views/shippings/ShippingList";
import ShippingUpload from "views/shippings/ShippingUpload"
import ShippingListAdmin from "views/shippings/ShippingListAdmin"
import ShippingDetail from "views/shippings/ShippingDetail"
import DeliverShipping from "views/shippings/DeliverShipping"
//WareHouse
import WarehouseList from "views/warehouses/WarehouseList";
import CreateWarehouse from "views/warehouses/CreateWarehouses.js";
import EditWarehouse from "views/warehouses/EditWarehouse.js";
import WarehouseMap from "views/warehouses/WarehouseMap.js";
//Simulation
import Graphics from "views/simulation/Graphics"
//Flights
import FlightList from "views/flights/Flights"

import history from "views/clients/history";
import Settings from "views/simulation/Settings";
import Dashboard from "views/dashboard/Dashboard";

ReactDOM.render(
  <Router history={history}>
    <Switch>
      <Switch>
        <Route path={"/"+ systemGraphics.url} render={props => <Graphics {...props} />} />
        <Route path={"/"+ clientList.url} render={props => <ClientList {...props} />} />
        <Route path={"/"+ createClient.url} render={props => <CreateClient {...props} />} />
        <Route path={"/"+ createShipping.url} render={props => <CreateShipping {...props} />} />
        <Route path={"/"+ warehouseList.url} render={props => <WarehouseList {...props} />} />
        <Route path={"/"+ shippingList.url} render={props => <ShippingList {...props}/>} />
        <Route path={"/"+ shippingListAdmin.url} render={props => <ShippingListAdmin {...props}/>} />
        <Route path={"/"+ shippingUpload.url} render={props => <ShippingUpload {...props}/>} />
        <Route path={"/"+ shippingDetail.url} render={props => <ShippingDetail {...props}/>} />
        <Route path={"/"+ deliverShipping.url} render={props => <DeliverShipping {...props}/>} />
        <Route path={"/"+ systemVariables.url} render={props => <Settings {...props}/>} />
        <Route path={"/"+ dashboard.url} render={props => <Dashboard {...props}/>} />
        <Route path={"/"+ flights.url} render={props => <FlightList {...props}/>} />
        <Route path={"/"+ warehouseMap.url} render={props => <WarehouseMap {...props}/>} />
        <Route
          path={'/' + login.url}
          render={props => <Login {...props} />}
        />
        <Route path={"/"+ createWarehouse.url} render={props => <CreateWarehouse {...props} />} />
        <Route path={"/"+ editWarehouse.url} render={props => <EditWarehouse {...props} />} />
        <Redirect to={'/' + login.url} />
        <Redirect from="/" to={'/' + login.url} />
      </Switch>
    </Switch>
  </Router>,
  document.getElementById("root")
);
