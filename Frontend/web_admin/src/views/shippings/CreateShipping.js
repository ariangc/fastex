import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import Typography from '@material-ui/core/Typography';
import SideBar from 'components/sidebar/SideBar.js';
import classNames from 'classnames';
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import FirstStepShipping from './FirstStepShipping.js';
import SecondStepShipping from './SecondStepShipping';
import ThirdStepShipping from './ThirdStepShipping';
import FourthStepShipping from './FourthStepShipping';
import FifthStepShipping from './FifthStepShipping';
import {PassThrough} from 'stream';
import ThirdStepClient from 'views/clients/ThirdStepClient.js';
import HelpOutline from '@material-ui/icons/HelpOutline';
import { ToastContainer, toast } from 'react-toastify';
import axios from "axios";
import {apiURL} from "variables/Variables.js"
import 'react-toastify/dist/ReactToastify.css';
import './Shipping.css';
import history from "../clients/history";

import {
    Modal,
    Container,
    Row,
    Col,
    Card,
    Button
  } from "reactstrap";

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    //marginRight: theme.spacing(1),
    marginLeft: '10%'
  },
  backButton: {
    //marginRight: theme.spacing(2),
    marginLeft: '35%'
  },
  buttonFirstStep:{
    marginLeft: '42%'
  },
  completed: {
    display: 'inline-block',
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return ['Registrar Remitente', 'Registrar Destinatario', 'Registrar Paquetes', 'Ruta del Envío', 'Resumen del Registro'];
}

export default function CreateShipping(props) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [completed, setCompleted] = React.useState(new Set());
  const [documentNumber, setDocumentNumber] = React.useState("");
  const [names, setNames] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [telephone, setTelephone] = React.useState("");
  const [isOpen, setIsOpen] = React.useState(false);
  const [documentNumberHasError, setDocumentNumberHasError] = React.useState(false);
  const [personFound, setPersonFound] = React.useState(false);
  const [personID, setPersonID] = React.useState(0);
  const [person, setPerson] = React.useState({});
  const [customer, setCustomer] = React.useState({});
  const [customerFound, setCustomerFound] = React.useState(false);
  const [originCity, setOriginCity] = React.useState("");
  //Addressee
  const [documentNumberAddressee, setDocumentNumberAddressee] = React.useState("");
  const [nameAddressee, setNameAddressee] = React.useState("");
  const [personAddresseeFound, setPersonAddresseeFound] = React.useState(false);
  const [documentNumberAddresseeHasError, setDocumentNumberAddresseeHasError] = React.useState(false);
  const [personIDAddressee, setPersonIDAddressee] = React.useState(0);
  const [personAddressee, setPersonAddressee] = React.useState({});  
  const [emailAddressee, setEmailAddressee] = React.useState("");
  const [telephoneAddressee, setTelephoneAddressee] = React.useState("");
  const [emailAddresseeHasError, setEmailAddresseeHasError] = React.useState(false);
  const [cityAddressee, setCityAddressee] = React.useState({});
  const [customerAddressee, setCustomerAddressee] = React.useState({});
  const [customerAddresseeFound, setCustomerAddresseeFound] = React.useState(false);
  const [packageShipping, setPackageShipping] = React.useState([]);
  const [route,setRoute] = React.useState([])
  const [receiver,setReceiver] = React.useState({})
  const [price, setPrice] = React.useState(0)

  const steps = getSteps();

  const totalSteps = () => {
    return getSteps().length;
  };

  const completedSteps = () => {
    return completed.size;
  };

  const allStepsCompleted = () => {
    return completedSteps() === totalSteps() 
  };

  const isLastStep = () => {
    return activeStep === totalSteps() - 1;
  };

  const handleLast = () => {
    console.log("handleLast")
    setIsOpen(true)
  }

  const createShipping = () => {
    console.log("entroooo")
    var receiverDict={id:null,mail:emailAddressee,phone:telephoneAddressee,phonePrefix:null,person:personAddressee}
    var body = {receiver:receiverDict};
      axios.post(apiURL.value + 'receiver/create', body).then((resp)=>{
      if(resp.status === 200){
          console.log("receiver registered")
          console.log(resp.data.receiver)
          setReceiver(resp.data.receiver)
          //register shipping
          console.log(parseFloat(packageShipping.length*50))
          console.log(customer)
          console.log(resp.data.receiver)
          var shipping ={id:null,
            trackingCode:null,
            description:null,
            shippingDate:null,
            receiptDate:null,
            registrationDate:null,
            price:packageShipping.length*50,
            customer: customer,
            receiver: resp.data.receiver,
            shippingStatus: {id:2, description:"ACTIVO"}}
          
          var bodyShipping = {shipping:shipping}
          
          axios.post(apiURL.value + 'shipping/create', bodyShipping).then((res)=>{
            if(res.status === 200){
                console.log("shipping created")
                setIsOpen(false)
                //update capacities
                var bodyRoute ={route:route}
                axios.post(apiURL.value + 'algorithm/updateRoute', bodyRoute).then((resRoute)=>{
                  if(resRoute.status === 200){
                    console.log("capacities updated")
                  }else{
                    toast.error('Ocurrió un problema con la actualización de la ruta', { type: toast.TYPE.ERROR, autoClose: 5000 })
                  }
                }).catch(error => {
                  console.log(error.response.data.error)
                 })
                //register packages
                var idShipping = res.data.shipping.id
                var packageList = []
                packageShipping.forEach((packageItem)=>{
                  packageItem.shipping.id=idShipping
                  packageList.push(packageItem)
                })
                var bodyPackage={packages:packageList}
                axios.post(apiURL.value + 'package/registerPackages', bodyPackage).then((resPackages)=>{
                  if(resPackages.status === 200){
                      console.log("packages created")         
                  }
                  else{
                    toast.error('Ocurrió un problema con el registro de los paquetes', { type: toast.TYPE.ERROR, autoClose: 5000 })
                  }
                }).catch(error => {
                  console.log(error.response.data.error)
                 })
                //register programmed flights
                var integerList = []
                integerList.push(idShipping)
                route.forEach((programmedFlight)=>{
                  integerList.push(programmedFlight.id)
                })
                var bodyFlights = {integerList:integerList}
                axios.post(apiURL.value + 'algorithm/registerRoute',bodyFlights).then((resFlights)=>{
                  if(resFlights.status === 200){
                    console.log("programmed flights created")
                    history.push({pathname:"/shippings",state:{activeToast:true}})
                  }
                  else{
                    toast.error('Ocurrió un problema con el registro de la ruta', { type: toast.TYPE.ERROR, autoClose: 5000 })
                  }
                }).catch(error => {
                  console.log(error.response.data.error)
                 })
            }
            else{
              toast.error('Ocurrió un problema con el registro del envío', { type: toast.TYPE.ERROR, autoClose: 5000 })
            }
          }).catch(error => {
            console.log(error.response.data.error)
           })
        }else{
          toast.error('Ocurrió un problema con el registro del destinatario', { type: toast.TYPE.ERROR, autoClose: 5000 })
        }

    }).catch(error => {
      console.log(error.response.data.error)
     })
    
    
  }

  const openFalse = () => {
    setIsOpen(false);
  }

  const handleNext = () => {
    const newActiveStep =
      isLastStep() && !allStepsCompleted()
        ? // It's the last step, but not all steps have been completed
          // find the first step that has been completed
          steps.findIndex((step, i) => !completed.has(i))
        : activeStep + 1;

    setActiveStep(newActiveStep); 
   
  };

  const handleCustomerNotFound = () => {
    toast.error('No ha buscado un remitente', { type: toast.TYPE.ERROR, autoClose: 5000 })
  }

  const handleErrors = () => {
    if(activeStep==1){
      if(emailAddresseeHasError){toast.error('Complete los campos obligatorios (*)', { type: toast.TYPE.ERROR, autoClose: 5000 })}
      if(!personAddresseeFound){toast.error('Persona no encontrada', { type: toast.TYPE.ERROR, autoClose: 5000 })}
      if(cityAddressee.id == 0){toast.error('Ciudad de destino no seleccionada', { type: toast.TYPE.ERROR, autoClose: 5000 })}
    }else if(activeStep==2){
      if(packageShipping.length==0){toast.error('No ha registrado paquetes', { type: toast.TYPE.ERROR, autoClose: 5000 })}
    }else if(activeStep==3){
      if(route.length==0){toast.error('No se pudo obtener una ruta para este envío', { type: toast.TYPE.ERROR, autoClose: 5000 })}
    }
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const reviewErrors = () => {
    console.log("revisar errores")
    if(activeStep==1){
      console.log(personAddresseeFound)
      console.log(emailAddresseeHasError)
      console.log(cityAddressee)
      if(!personAddresseeFound || emailAddresseeHasError || cityAddressee.id == 0){
        return true
      }
      else{
        
        return false
      }
    }else if(activeStep==2){
      console.log(packageShipping.length)
      if(packageShipping.length==0){
        return true
      }
      else{
        return false
      }
    }else if(activeStep==3){
      if(route.length==0){
        return true
      }
      else{
        return false
      }
    }
  }
  


  const handleComplete = () => {
    const newCompleted = new Set(completed);
    newCompleted.add(activeStep);
    setCompleted(newCompleted);

    /**
     * Sigh... it would be much nicer to replace the following if conditional with
     * `if (!this.allStepsComplete())` however state is not set when we do this,
     * thus we have to resort to not being very DRY.
     */
    if (completed.size !== totalSteps()) {
      handleNext();
    }
  };

  return (
    <div className="App">
    <SideBar fluid toggle={false} isOpen={true}/>
    <Container fluid className={classNames('content', {'is-open': true})} style={{backgroundColor:'#FAFAFA'}}>
        <NavbarRedexAdmin />
        <Container style={{padding:'0%', margin:'0%', backgroundColor:'#FAFAFA'}}>
        <Stepper alternativeLabel nonLinear activeStep={activeStep} style={{position:'fixed', width:'85%', marginTop:'5.5%',
                                                                            zIndex:800, backgroundColor:'#FAFAFA'}}>
            {steps.map((label, index) => {
            const stepProps = {};
            const buttonProps = {};
            return (
                <Step key={label} {...stepProps}>
                <StepButton disabled className="svg.MuiStepIcon-root.MuiStepIcon-active"
                >
                    {label}
                </StepButton>
                </Step>
            );
            })}
        </Stepper>
        </Container>

        <Container style={{marginTop:'18%'}}>
            <Card > 
              <Col> 
              <div>
                      <Typography className={classes.instructions}>{activeStep==0?<FirstStepShipping 
                      documentNumberFunction = {setDocumentNumber} 
                      documentNumber = {documentNumber}
                      namesFunction = {setNames}
                      names = {names}
                      documentNumberHasErrorFunction = {setDocumentNumberHasError}
                      documentNumberHasError = {documentNumberHasError}
                      customerFunction = {setCustomer}
                      customer = {customer}
                      customerFoundFunction = {setCustomerFound}
                      />:
                      activeStep==1?<SecondStepShipping 
                      documentNumberAddresseeFunction = {setDocumentNumberAddressee} 
                      documentNumberAddressee = {documentNumberAddressee}
                      nameAddresseeFunction = {setNameAddressee}
                      nameAddressee = {nameAddressee}
                      personAddresseeFoundFunction = {setPersonAddresseeFound}
                      documentNumberAddresseeHasErrorFunction = {setDocumentNumberAddresseeHasError}
                      documentNumberAddresseeHasError = {documentNumberAddresseeHasError}
                      personIDAddresseeFunction = {setPersonIDAddressee}
                      personAddresseeFunction = {setPersonAddressee}
                      emailAddresseeFunction = {setEmailAddressee}
                      emailAddressee = {emailAddressee}
                      telephoneAddresseeFunction = {setTelephoneAddressee}
                      telephoneAddressee = {telephoneAddressee}
                      emailAddresseeHasErrorFunction = {setEmailAddresseeHasError}
                      emailAddresseeHasError = {emailAddresseeHasError}
                      cityAddresseeFunction = {setCityAddressee}
                      cityAddressee = {cityAddressee}
                      customerAddresseeFunction = {setCustomerAddressee}
                      customerAddressee = {customerAddressee}
                      customerAddresseeFoundFunction = {setCustomerAddresseeFound}
                      customerAddresseeFound = {customerAddresseeFound}
                      />:activeStep==2?<ThirdStepShipping
                      packageShippingFunction = {setPackageShipping}
                      packageShipping = {packageShipping}/>:
                      activeStep==3?<FourthStepShipping
                      customer = {customer}
                      cityAddressee = {cityAddressee}
                      customerAddressee = {customerAddressee}
                      packageShipping = {packageShipping}
                      priceFunction = {setPrice}
                      routeFunction = {setRoute}/>:
                      activeStep==4?<FifthStepShipping
                      customer = {customer}
                      names = {names}
                      nameAddressee = {nameAddressee}
                      documentNumber = {documentNumber}
                      documentNumberAddressee = {documentNumberAddressee}
                      customerAddressee = {customerAddressee}
                      packageShipping = {packageShipping}
                      cityAddressee = {cityAddressee}
                      price = {price}
                      />:
                      PassThrough}</Typography>
                      <Row>
                        <Col>
                      {activeStep === 0 ?PassThrough:
                      <Button onClick={handleBack} 
                          size="lg"
                          color="warning"
                          outline="warning"
                          className={classes.backButton}
                          >
                          Volver
                      </Button>}
                      {activeStep === 0 ?
                      <Button
                          size="lg"
                          variant="contained"
                          color="primary"
                          onClick={customerFound?handleNext:handleCustomerNotFound}
                          className={classes.buttonFirstStep}
                      >{isLastStep()? 'Terminar' : 'Siguiente'}
                      </Button>:
                      <Button
                          size="lg"
                          variant="contained"
                          color="primary"
                          onClick={isLastStep()?handleLast:reviewErrors()?handleErrors:handleNext}
                          className={classes.button}
                      >{isLastStep()? 'Terminar' : 'Siguiente'}
                      </Button>}
                          
                      </Col>
                      </Row>  
                      <Modal toggle={openFalse} isOpen={isOpen}>
                        <Col  sm="12" md={{ size: 5, offset: 4 }}><HelpOutline style={{ fontSize: 100, color:'#1768AC' }}/></Col>
                          <div className="modal-header text-center">
                          
                            <h5 style={{marginLeft:'7vh'}} className="modal-title" id="exampleModalLiveLabel">
                              ¿Desea confirmar el registro del envío?
                            </h5>
                            <button
                              aria-label="Close"
                              className="close"
                              type="button"
                              onClick={openFalse}
                            >
                              <span aria-hidden={true}>×</span>
                            </button>
                          </div>
                          <Col  sm="12" md={{ size: 8, offset: 2 }}>
                          <div className="modal-footer">
                            <Button
                              color="danger"
                              type="button"
                              size="lg"
                              onClick={openFalse}
                            >
                              Cancelar
                            </Button>
                            <Button
                              color="primary"
                              type="button"
                              size="lg"
                              onClick={createShipping}
                            >
                              Aceptar
                            </Button>
                          </div>
                        </Col>
                      </Modal>                
              </div>
              </Col>
            </Card>  
        </Container>
        
        </Container>
    </div>
  );
}
