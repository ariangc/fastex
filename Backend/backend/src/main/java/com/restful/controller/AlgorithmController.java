package com.restful.controller;

import com.restful.entity.ProgrammedFlight;
import com.restful.model.ProgrammedFlightModel;
import com.restful.service.AlgorithmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/algorithm")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
public class AlgorithmController {
    @Autowired
    @Qualifier("algorithmService")
    private AlgorithmService algorithmService;

    @CrossOrigin
    @PostMapping("/run")
    public @ResponseBody Map<String, List<ProgrammedFlightModel>> runAlgorithm(@RequestBody Map<String, Long> data){
        Map<String, List<ProgrammedFlightModel>> response = new HashMap<>();
        try {
            Map<String, List<ProgrammedFlightModel>> route = algorithmService.runAlgorithm(data.get("idStartCity"), data.get("idEndCity"));
            return route;
        }catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }

    @CrossOrigin
    @PostMapping("/updateRoute")
    public @ResponseBody boolean updateRoute(@RequestBody Map<String, List<ProgrammedFlight>> data){
        return algorithmService.updateRoute(data.get("route"));
    }

    @CrossOrigin
    @PostMapping("/registerRoute")
    public @ResponseBody boolean registerRoute(@RequestBody Map<String, List<Long>> data){
        try{
            return algorithmService.registerRoute(data.get("integerList"));
        }catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }
}
