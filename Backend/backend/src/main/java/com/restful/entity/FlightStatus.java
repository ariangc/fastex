package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "flightStatus")
@Entity
public class FlightStatus {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idFlightStatus")
    private Long id;

    private String description;

    @OneToMany(mappedBy = "flightStatus", cascade = CascadeType.ALL)
    private List<Flight> flights = new ArrayList<>();

    protected FlightStatus(){}

    public FlightStatus(Long id, String description, List<Flight> flights) {
        this.id = id;
        this.description = description;
        this.flights = flights;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }
}
