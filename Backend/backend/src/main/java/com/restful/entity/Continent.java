package com.restful.entity;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.*;

@Table(name = "Continent")
@Entity
public class Continent{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idContinent")
    private Long id;

    private String name;

    @OneToMany(mappedBy = "continent", cascade = CascadeType.ALL)
    private List<Country> countries = new ArrayList<>();

    protected Continent(){}

    @Override
    public String toString() {
        return String.format("Continent=[id=%d, name='%s']", id, name);
    }

    public Continent(Long id, String name, List<Country> countries) {
        this.id = id;
        this.name = name;
        this.countries = countries;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }
}
