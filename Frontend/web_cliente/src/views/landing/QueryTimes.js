import React from "react";

// reactstrap components
import {
  Button,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  FormGroup,
  Card,
  Row,
  Col
} from "reactstrap";
import {login,apiURL,shipping} from "variables/Variables.js"
import { Link } from "react-router-dom";
// core components
import ExamplesNavbar from "components/Navbars/ExamplesNavbar.js";
import HeaderConstrunet from "components/Headers/HeaderConstrunet.js";
import FooterConstrunet from "components/Footers/FooterConstrunet.js";
import IconButton from '@material-ui/core/IconButton';
import Search from '@material-ui/icons/Search';
import AccessTime from '@material-ui/icons/AccessTime';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import axios from "axios";
import history from "../clients/history";

class QueryTimes extends React.Component {
    constructor(props) {
        super(props);
        this.homepage = this.homepage.bind(this);
    }

    homepage(){
        this.props.history.push({pathname: '/landing'})
    }


    componentWillMount(){
       
    }

  render(){
  return (
    <>
      <main ref="main">
      <div className="wrapper" >
        <HeaderConstrunet/>
        <div className="container-fluid text-center">
            <Row>
                <Col sm={{ size:8, offset:2}}>
                  <br></br>
                  <h2 style={{marginBottom:"1vh",fontWeight:"bold"}}>Tiempos de envío</h2>
                  <h4 style={{marginTop:"1vh",marginBottom:"5vh"}}>Conoce el tiempo que puede demorar tu envío</h4>
                </Col>
            </Row>
            <Row>
                <Col sm={{ size:6,offset:3}}>
                    <p><label style={{marginTop:"5vh",color:"#606366",fontWeight:"bold"}}>Envío Intracontinental:&nbsp;</label><label> 1 día</label></p>   
                    <p><label style={{marginTop:"5vh",color:"#606366",fontWeight:"bold"}}>Envío Intercontinental:&nbsp;</label><label>2 días</label></p>   
                    <label style={{marginTop:"5vh"}}>*Los tiempos mostrados son válidos en condiciones regulares.</label>   
                </Col>
            </Row>
            <Row>
                <Col>
                  <Button
                    size="lg"
                    variant="contained"
                    color="primary"
                    onClick={this.homepage}
                    style={{fontSize:"18px",marginTop:"7vh"}}
                    >Inicio
                  </Button>
                </Col>
            </Row>
        </div>
        <FooterConstrunet/>
      </div>
    </main>
    </>
  );
  }
}

export default QueryTimes;
