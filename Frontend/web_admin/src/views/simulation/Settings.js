import React from "react";

// reactstrap components
import {
  Row,
  Col,
  Form,
  FormGroup,
  Container,
  Input,
} from "reactstrap";
import IconButton from '@material-ui/core/IconButton';
import Search from '@material-ui/icons/Search';
import {apiURL,originWarehouse} from "variables/Variables.js"
// core components
import SideBar from 'components/sidebar/SideBar.js';
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import classNames from 'classnames';
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';


class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.handleWarehouseChange = this.handleWarehouseChange.bind(this)

    this.state = {
        warehouses:[],
        warehousesData:[],
        warehouseSelected:'',
        test:""
    }
}


handleWarehouseChange(event){
    this.setState({
      warehouseSelected: event.target.value,
    },()=>{originWarehouse.id=this.state.warehouseSelected
           console.log(this.state.warehouseSelected)
        this.state.warehousesData.map((warehouse)=>{
            if(parseInt(warehouse.id) == parseInt(this.state.warehouseSelected)){
                originWarehouse.name = warehouse.name + ' - ' + warehouse.country.name
                originWarehouse.icao = warehouse.airportCode
            }
        })
        this.setState({test:"test"})
    })
   
  }

componentWillMount(){
    axios.post(apiURL.value + 'city/list').then((res)=>{
        if(res.status==200){
          var cities = res.data.map((city, index)=>
        <option value={city.id}>{city.name} - {city.country.name}</option>
          )
          this.setState({
            warehouses:cities,
            warehousesData:res.data

          })
        }
    })
}

render(){
  return (
    <>
     <div className="App">
        <SideBar fluid toggle={false} isOpen={true}/>
        <Container fluid className={classNames('content', {'is-open': true})} style={{backgroundColor:'#FAFAFA'}}>
            <NavbarRedexAdmin />
            <Container style={{padding:'0%', marginTop:'10%', backgroundColor:'#FAFAFA'}}>
          <Col>
            <ToastContainer /> 
            <Form>
              <Row>
                <h3 className="stepTitle">Configuración de variables</h3>
              </Row>
              <Row>
                <Col sm={{ size: 6, offset: 3 }}>
                <FormGroup style={{marginBottom:'7%'}}>
                  <label htmlFor="exampleFormControlSelect1">Almacén</label>
                  <Input id="countrySelect" type="select" onChange={this.handleWarehouseChange}>
                      <option disabled value={0} selected>Seleccionar</option>
                      {this.state.warehouses}
                  </Input>
                </FormGroup>
                </Col>
              </Row>  
            </Form>
          </Col>
          </Container>
    </Container>
                              
  
    </div>       
    </>
  );
  }
}

export default Settings;
