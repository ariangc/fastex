package com.restful.service;

import com.restful.entity.Country;
import com.restful.entity.Customer;
import com.restful.entity.DocumentType;
import com.restful.entity.Person;
import com.restful.model.CustomerModel;
import com.restful.model.PersonModel;
import com.restful.repository.CountryRepository;
import com.restful.repository.CustomerRepository;
import com.restful.repository.DocumentTypeRepository;
import com.restful.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("personService")
public class PersonService {
    @Autowired
    @Qualifier("personRepository")
    private PersonRepository personRepository;

    @Autowired
    @Qualifier("customerRepository")
    private CustomerRepository customerRepository;

    @Autowired
    @Qualifier("countryRepository")
    private CountryRepository countryRepository;

    @Autowired
    @Qualifier("documentTypeRepository")
    private DocumentTypeRepository documentTypeRepository;

    public PersonService(){}

    public List<PersonModel> getPersons(){
        List<Person> l = personRepository.findAll();
        List<PersonModel> lModel = new ArrayList<PersonModel>();
        for(Person person: l){
            PersonModel x = new PersonModel(person);
            lModel.add(x);
        }
        return lModel;
    }

    public List<PersonModel> getPersonByDocument(String document){
        String parsedDocument = "";
        for(int i = 0; i < document.length(); ++ i){
            char c = document.charAt(i);
            if(c != '.' && c != '-') parsedDocument += c;
        }
        List<Person> l = personRepository.findByDocument(parsedDocument);
        List<PersonModel> lModel = new ArrayList<PersonModel>();
        for(Person person: l) {
            PersonModel x = new PersonModel(person);
            lModel.add(x);
        }
        return lModel;
    }

    public List<PersonModel> getPersonByData(Long idCountry, Long idDocumentType, String document){
        List<Country> country = countryRepository.findById(idCountry);
        Optional<DocumentType> documentType = documentTypeRepository.findById(idDocumentType);
        List<Person> person = personRepository.findByDocumentAndCountryAndDocumentType(document, country.get(0), documentType.get());

        List<PersonModel> lModel= new ArrayList<>();
        for(Person person1: person){
            PersonModel x = new PersonModel(person1);
            lModel.add(x);
        }
        return lModel;
    }

    public List<PersonModel> getPersonById(Long idPerson){
        List<Person> l = personRepository.findById(idPerson);
        List<PersonModel> lModel = new ArrayList<PersonModel>();
        for(Person person: l) {
            PersonModel x = new PersonModel(person);
            lModel.add(x);
        }
        return lModel;
    }

    public List<String> getUsernames(Long idPerson){
        Person person = personRepository.findById(idPerson).get(0);
        PersonModel x = new PersonModel(person);
        String name = x.getNames(), surname = x.getSurnames();
        String[] names = name.split(" ");
        String[] surnames = surname.split(" ");

        List<String> possibles = new ArrayList<>();

        possibles.add(names[0].charAt(0) + surnames[0]);
        possibles.add(surnames[0] + names[0].charAt(0));
        possibles.add(surnames[0].charAt(0) + names[0]);
        if(names.length > 1) possibles.add(names[1] + surnames[0].charAt(0));

        List<String> usernames = new ArrayList<>();

        for(String possible: possibles){
            List<Customer> customers = customerRepository.findByUser(possible);
            if(!customers.isEmpty()) continue;
            usernames.add(possible);
        }

        return usernames;
    }
}
