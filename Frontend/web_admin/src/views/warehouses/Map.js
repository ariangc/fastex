import React from "react";

// reactstrap components
import { Grid } from "react-bootstrap";
import {
  Button,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormText,
  Label,
  Card,
  CardTitle,
  Modal
} from "reactstrap";
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import HelpOutline from '@material-ui/icons/HelpOutline';
import classNames from 'classnames';
import {apiURL, warehouse, editWarehouse, editedWarehouse} from "variables/Variables.js"
// core components
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import SideBar from 'components/sidebar/SideBar.js';
import MaterialTable from "material-table";
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
    ZoomableGroup,
    ComposableMap,
    Geographies,
    Geography
  } from "react-simple-maps";
import * as data1 from './Mapa.json'
import {PassThrough} from 'stream';


  const geoUrl = data1;
  

class Map extends React.Component {
  constructor(props) {
    super(props);
    this.sidebarIsOpen = this.sidebarIsOpen.bind(this);
    this.sidebarIsClose = this.sidebarIsClose.bind(this);
    this.segmentation = this.segmentation.bind(this); 
    this.state = {
      sidebarOpen:true,
      flights:[],
      loading:false,
      warehouses:[],
      data:[]
    }
}
segmentation(n1, n2){
    if(n1/n2<=0.5){
        return 1;
    }
    else if(n1/n2>0.5 && n1/n2<=0.8){
        return 2;
    }
    else if(n1/n2>0.8){
        return 3;
    }
}
sidebarIsClose(){
    this.setState({
      sidebarOpen:!this.state.sidebarOpen
    })
  }
  sidebarIsOpen(){
    this.setState({
      sidebarOpen:true
    })
  }
componentDidMount(){
    console.log('lista personalizada. ', this.props.data)
}
render(){
  return (
    <>
    <div className="App">
      
     
      <Container
      fluid>
        <Container >
            <Col> 
                <ComposableMap data-tip="" projectionConfig={{ scale: 200 }}>
                    <Geographies geography='/mapa.json'>
                        {({ geographies }) =>
                        geographies.map((geo, index) => (
                            <Geography
                            key={geo.rsmKey}
                            geography={geo}
                            onMouseEnter={() => {
                                    console.log(geo.properties['NAME'])
                                    if(this.props.data[index].active){
                                        this.props.setTooltipContent('Almacén: ' + this.props.data[index].name + '  '
                                         + 'C. Llena: ' + this.props.data[index].filled + '  ' +
                                        'C. Total: ' + this.props.data[index].total + '  ' + 'Estado: ' + this.props.data[index].status);
                                    }

                                }    
                                
                            }
                            onMouseLeave={() => {
                                this.props.setTooltipContent("");
                            }}
                            style={this.props.data[index].statusId==1?{overflow:'hidden',
                                default: {
                                fill: "#F53",
                                outline: "none"
                                },
                                hover: {
                                fill: "#F53",
                                outline: "none"
                                },
                                pressed: {
                                fill: "#F53",
                                outline: "none"
                                }
                            }:this.segmentation(this.props.data[index].filled, this.props.data[index].total)==1?
                            {overflow:'hidden',
                            default: {
                                fill: "#4c9141",
                                outline: "none"
                            },
                            hover: {
                                fill: "#4c9141",
                                outline: "none"
                            },
                            pressed: {
                                fill: "#4c9141",
                                outline: "none"
                            }
                            }:this.segmentation(this.props.data[index].filled, this.props.data[index].total)==2?
                            {overflow:'hidden',
                            default: {
                                fill: "#FDFD96",
                                outline: "none"
                            },
                            hover: {
                                fill: "#FDFD96",
                                outline: "none"
                            },
                            pressed: {
                                fill: "#FDFD96",
                                outline: "none"
                            }
                            }:this.segmentation(this.props.data[index].filled, this.props.data[index].total)==3?
                            {overflow:'hidden',
                            default: {
                                fill: "#FF8000",
                                outline: "none"
                            },
                            hover: {
                                fill: "#FF8000",
                                outline: "none"
                            },
                            pressed: {
                                fill: "#FF8000",
                                outline: "none"
                            }
                            }:
                            {overflow:'hidden',
                            default: {
                                fill: "#D6D6DA",
                                outline: "none"
                            },
                            hover: {
                                fill: "#D6D6DA",
                                outline: "none"
                            },
                            pressed: {
                                fill: "#D6D6DA",
                                outline: "none"
                            }
                            }
                        }
                            />
                        ))
                        }
                    </Geographies>
                    
                    </ComposableMap>   
                </Col>
          </Container>
      </Container>
                  
    </div>       
    </>
  );
  }
}

export default Map;
