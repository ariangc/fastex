package com.restful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastExApplication {

    public static void main(String[] args) {
        SpringApplication.run(com.restful.FastExApplication.class, args);
    }

}
