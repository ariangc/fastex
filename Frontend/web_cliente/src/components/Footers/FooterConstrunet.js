/*eslint-disable*/
import React from "react";

// reactstrap components
import { Container } from "reactstrap";

// core components

function FooterConstrunet() {
  return (
    <>
      <footer className="footer footer-default">
        <Container>
          <nav>
            <ul>
              <li>
                <a
                  href=""
                  target="_blank"
                >
                  Sobre nosotros
                </a>
              </li>
              <li>
                <a
                  href=""
                  target="_blank"
                >
                  Servicios
                </a>
              </li>
              <li>
                <a
                  href=""
                  target="_blank"
                >
                  Oficinas
                </a>
              </li>
            </ul>
          </nav>
          <div className="copyright" id="copyright">
            © {new Date().getFullYear()}, Todos los derechos reservados.
            
          </div>
        </Container>
      </footer>
    </>
  );
}

export default FooterConstrunet;
