package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Table(name = "ProgrammedFlight")
@Entity
public class ProgrammedFlight {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idProgrammedFlight")
    private Long id;

    private Date startDate;
    private int filledCapacity;
    private int totalCapacity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idProgrammedFlightStatus")
    private ProgrammedFlightStatus programmedFlightStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idFlight")
    private Flight flight;

    @OneToMany(mappedBy = "programmedFlight", cascade = CascadeType.ALL)
    private List<ShippingXProgrammedFlight> shippingXProgrammedFlights = new ArrayList<>();

    public ProgrammedFlight() {
    }

    public ProgrammedFlight(Long id, Date startDate, int filledCapacity, int totalCapacity, ProgrammedFlightStatus programmedFlightStatus, Flight flight, List<ShippingXProgrammedFlight> shippingXProgrammedFlights) {
        this.id = id;
        this.startDate = startDate;
        this.filledCapacity = filledCapacity;
        this.totalCapacity = totalCapacity;
        this.programmedFlightStatus = programmedFlightStatus;
        this.flight = flight;
        this.shippingXProgrammedFlights = shippingXProgrammedFlights;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getFilledCapacity() {
        return filledCapacity;
    }

    public void setFilledCapacity(int filledCapacity) {
        this.filledCapacity = filledCapacity;
    }

    public int getTotalCapacity() {
        return totalCapacity;
    }

    public void setTotalCapacity(int totalCapacity) {
        this.totalCapacity = totalCapacity;
    }

    public ProgrammedFlightStatus getProgrammedFlightStatus() {
        return programmedFlightStatus;
    }

    public void setProgrammedFlightStatus(ProgrammedFlightStatus programmedFlightStatus) {
        this.programmedFlightStatus = programmedFlightStatus;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public List<ShippingXProgrammedFlight> getShippingXProgrammedFlights() {
        return shippingXProgrammedFlights;
    }

    public void setShippingXProgrammedFlights(List<ShippingXProgrammedFlight> shippingXProgrammedFlights) {
        this.shippingXProgrammedFlights = shippingXProgrammedFlights;
    }
}
