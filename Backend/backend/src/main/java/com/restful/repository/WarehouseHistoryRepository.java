package com.restful.repository;

import com.restful.entity.Warehouse;
import com.restful.entity.WarehouseHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WarehouseHistoryRepository extends JpaRepository<WarehouseHistory, Long> {
    public abstract List<WarehouseHistory> findByWarehouse(Warehouse warehouse);
}
