import React from 'react';
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//import { faHome, faBriefcase, faPaperPlane, faQuestion, faImage, faCopy } from '@fortawesome/free-solid-svg-icons';
import SubMenu from './SubMenu';
import { NavItem, NavLink, Nav } from 'reactstrap';
import classNames from 'classnames';
import {Link} from 'react-router-dom';
import {createClient, clientList, createShipping, shippingList, warehouseList,
        createWarehouse, employeeList, createEmployee,systemVariables, systemGraphics,
        shippingUpload,currentUser, shippingListAdmin} from 'variables/Variables.js';
import {PassThrough} from 'stream';
import './App.css'

const SideBar = props => (
    <div className={classNames('sidebar', {'is-open': props.isOpen})}>
      <div className="sidebar-header">
        <span color="info" onClick={props.toggle} style={{color: '#fff'}}>&times;</span>
        <h3>FastEx</h3>
      </div>
      <div className="side-menu">
        <Nav vertical className="list-unstyled pb-3">
          <NavItem>
            <NavLink tag={Link} to={'/pages'}>
              Dashboard
            </NavLink>
          </NavItem>
          <SubMenu title="Clientes" items={submenus[0]}/>
          {currentUser.role == 'Administrador'?<SubMenu title="Envíos" items={submenus[1]}/>:<SubMenu title="Envíos" items={submenus[5]}/>}
          {currentUser.role == 'Administrador'?<SubMenu title="Almacenes" items={submenus[2]}/>:PassThrough}
          {currentUser.role == 'Administrador'?<SubMenu title="Empleados" items={submenus[3]}/>:PassThrough}
          {currentUser.role == 'Administrador'?<SubMenu title="Simulación" items={submenus[4]}/>:PassThrough}
        </Nav>        
      </div>
    </div>
  );

  const submenus = [
    [
      {
        title: "Listado",
        target: clientList.url        
      },
      {
        title: "Registrar",
        target: createClient.url        
      },
      {
        title: "Activar",
        target: createClient.url        
      }
    ],
    [ {
      title: "Listado",
        target: shippingListAdmin.url     
      },
      {
        title: "Registrar",
        target: createShipping.url        
      }
    ],
    [ {
      title: "Listado",
        target: warehouseList.url        
      },
      {
        title: "Registrar",
        target: createWarehouse.url        
      }
    ],
    [ {
      title: "Listado",
        target: employeeList.url        
      },
      {
        title: "Registrar",
        target: createEmployee.url        
      }
    ],
    [ {
        title: "Envíos Históricos",
        target: shippingUpload.url
      },
      {
        title: "Variables",
        target: systemVariables.url
      }
    ],
    [ {
      title: "Listado",
        target:shippingList.url        
      },
      {
        title: "Registrar",
        target: createShipping.url        
      }
    ]

  ]
  

export default SideBar;
