package com.restful.model;

import com.restful.entity.ProgrammedFlightStatus;

public class ProgrammedFlightStatusModel {
    private Long id;
    private String description;

    public ProgrammedFlightStatusModel() {
    }

    public ProgrammedFlightStatusModel(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    public ProgrammedFlightStatusModel(ProgrammedFlightStatus a){
        this.id = a.getId();
        this.description = a.getDescription();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
