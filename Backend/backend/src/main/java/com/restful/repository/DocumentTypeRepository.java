package com.restful.repository;

import com.restful.entity.Country;
import com.restful.entity.DocumentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("documentTypeRepository")
public interface DocumentTypeRepository extends JpaRepository<DocumentType, Long> {
    public abstract List<DocumentType> findAll();
    public abstract List<DocumentType> findByCountry(Country country);
}
