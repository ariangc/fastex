package com.restful.model;

import com.restful.entity.SettingParameters;

public class SettingParametersModel {
    private Long id;
    private String name;
    private int value;
    private boolean active = true;

    public SettingParametersModel(Long id, String name, int value, boolean active) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.active = active;
    }

    public SettingParametersModel(SettingParameters a){
        this.id = a.getId();
        this.name = a.getName();
        this.value = a.getValue();
        this.active = a.isActive();
    }

    public SettingParametersModel(){
        super();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public boolean isActive() {
        return active;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
