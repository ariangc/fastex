package com.restful.controller;

import com.restful.entity.Country;
import com.restful.model.CityModel;
import com.restful.model.CountryModel;
import com.restful.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/city")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
public class CityController {
    @Autowired
    @Qualifier("cityService")
    private CityService cityService;

    @CrossOrigin
    @PostMapping("/list")
    public @ResponseBody List<CityModel> getCountries(){
        return cityService.listCities();
    }

    @CrossOrigin
    @PostMapping("/listByCountry")
    public @ResponseBody
    List<CityModel> listByCountry(@RequestBody Map<String, Long> data){
        return cityService.getCitiesByCountry(data.get("idCountry"));
    }
}
