import React from "react";

// reactstrap components
import {
  Button,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  FormGroup,
  Card,
  Row,
  Col
} from "reactstrap";
import {login,apiURL,shipping} from "variables/Variables.js"
import { Link } from "react-router-dom";
// core components
import ExamplesNavbar from "components/Navbars/ExamplesNavbar.js";
import HeaderConstrunet from "components/Headers/HeaderConstrunet.js";
import FooterConstrunet from "components/Footers/FooterConstrunet.js";
import MaterialTable from "material-table";
import IconButton from '@material-ui/core/IconButton';
import Search from '@material-ui/icons/Search';
import AccessTime from '@material-ui/icons/AccessTime';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import Datetime from 'react-datetime';
import axios from "axios";

const columns = [
  {
    title: 'Fecha / Hora de partida',
    field: 'startDate'
  },
  {
    title: 'Fecha / Hora de llegada',
    field: 'addresseeDate'
  },
  {
    title: 'Ciudad de origen',
    field: 'originCity'
  }, 
  {
    title: 'Ciudad de destino',
    field: 'addresseeCity'
  }, 
  {
    title: 'Estado',
    field: 'state'
  }
  ];

class ShippingDetail extends React.Component {
  constructor(props) {
    super(props);
    this.homePage = this.homePage.bind(this)
    this.appendLeadingZeroes = this.appendLeadingZeroes.bind(this)
    this.substractDates = this.substractDates.bind(this)

    this.state = {
      shipping:null,
      originCity:'',
      addresseeCity:'',
      routeFlights:[],
      registrationDate:'',
      arrivalDate:''
    }
  }

  homePage(){
    this.props.history.push({pathname: '/landing'})
  }

  substractDates(hourstart, minstart, hourend, minend){
    console.log('funcion', hourstart, minstart, hourend, minend)
    var startMin = parseInt(hourstart)*60+parseInt(minstart)
    var endMin = parseInt(hourend)*60+parseInt(minend)
    if(endMin==startMin){
      return 0;
    }
    else if(endMin<startMin){
      //es el dia siguiente
      return (24*60-startMin)+endMin
    }
    else if(endMin>startMin){
      //es el mismo dia
      return endMin-startMin
    }
  }
  appendLeadingZeroes(n){
    if(n <= 9){
      return "0" + n;
    }
    return n
  }

  componentWillMount(){
    console.log(this.props.location.state.shipping)
    this.setState({shipping:this.props.location.state.shipping},()=>{
      var body ={idShipping: parseInt(this.state.shipping.id)}
      axios.post(apiURL.value + 'shipping/getRoute', body).then((res)=>{
        if(res.status === 200){
          console.log(res.data.route)
          console.log(res.data.route[0])
          console.log(res.data.route[res.data.route.length-1])
          var flights=[]
          var endDate=""
          var startDate=""
          res.data.route.forEach((flight,index) => {
            var date = new Date(flight.startDate)
            var minutes = this.substractDates(flight.flight.departureTime.slice(0,2), flight.flight.departureTime.slice(3,5), flight.flight.landingTime.slice(0,2), flight.flight.landingTime.slice(3,5))
            console.log('minutes', minutes)
            var end = Datetime.moment(date).add(minutes, 'm').toDate()
            console.log('end', end)
            var flight={'startDate':this.appendLeadingZeroes(this.appendLeadingZeroes(date.getUTCDate()) + '/' + this.appendLeadingZeroes(date.getUTCMonth() + 1) + "/" + date.getUTCFullYear() + " " + date.getUTCHours()) + ":" + this.appendLeadingZeroes(date.getUTCMinutes()),
                        'addresseeDate':this.appendLeadingZeroes(this.appendLeadingZeroes(end.getUTCDate()) + '/' + this.appendLeadingZeroes(end.getUTCMonth() + 1) + "/" + end.getUTCFullYear() + " / " + end.getUTCHours()) + ":" + this.appendLeadingZeroes(end.getUTCMinutes()),
                        'originCity':flight.flight.startCity.name+' - '+flight.flight.startCity.country.name,
                        'addresseeCity':flight.flight.endCity.name+' - '+flight.flight.endCity.country.name,
                        'state':flight.programmedFlightStatus.description}
            endDate=this.appendLeadingZeroes(this.appendLeadingZeroes(end.getUTCDate()) + '/' + this.appendLeadingZeroes(end.getUTCMonth() + 1) + "/" + end.getUTCFullYear() + " / " + end.getUTCHours()) + ":" + this.appendLeadingZeroes(end.getUTCMinutes())
            if(index==0){
              startDate=this.appendLeadingZeroes(this.appendLeadingZeroes(date.getUTCDate()) + '/' + this.appendLeadingZeroes(date.getUTCMonth() + 1) + "/" + date.getUTCFullYear() + " " + date.getUTCHours()) + ":" + this.appendLeadingZeroes(date.getUTCMinutes())
            }
            flights.push(flight)
          });
          this.setState({originCity:res.data.route[0],addresseeCity:res.data.route[res.data.route.length-1],
                        routeFlights:flights,arrivalDate:endDate,registrationDate:startDate})
        }
      })
    })
}

  render(){
  return (
    <>
      <main ref="main">
      <div className="wrapper" >
        <HeaderConstrunet/>
        <div className="container-fluid text-center">
            <br></br>
            <h2 style={{marginBottom:"5vh"}}>Seguimiento del envío</h2>
            <Row>
                <Col sm={{ size:8, offset:2}}>
                  <Card style={{backgroundColor:"#1768AC"}}>
                    <Row>
                        <Col>
                            <label style={{color:"#ffffff",fontWeight:"bold",fontSize:"16px"}}>Fecha de registro</label>  
                        </Col>
                        <Col>
                            <label style={{color:"#ffffff",fontWeight:"bold",fontSize:"16px"}}>Fecha de entrega</label>  
                        </Col>
                        <Col>
                            <label style={{color:"#ffffff",fontWeight:"bold",fontSize:"16px"}}>Ciudad de origen</label> 
                        </Col>
                        <Col>
                            <label style={{color:"#ffffff",fontWeight:"bold",fontSize:"16px"}}>Ciudad de destino</label> 
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                          <label style={{color:"#ffffff"}}>{this.state.registrationDate != ""?this.state.registrationDate.slice(0,10):"-"}</label>  
                        </Col>
                        <Col>
                          <label style={{color:"#ffffff"}}>{this.state.arrivalDate != ""?this.state.arrivalDate.slice(0,10):"-"}</label> 
                        </Col>
                        <Col>
                            <label style={{color:"#ffffff"}}>{this.state.originCity?this.state.originCity.flight.startCity.name+'-'+this.state.originCity.flight.startCity.country.name:"-"}</label> 
                        </Col>
                        <Col>
                            <label style={{color:"#ffffff"}}>{this.state.addresseeCity?this.state.addresseeCity.flight.endCity.name+'-'+this.state.addresseeCity.flight.endCity.country.name:"-"}</label> 
                        </Col>
                    </Row>

                  </Card>

              </Col>
              </Row>
              <Row>
                <Col sm={{ size: 8, offset: 2 }}>
                <MaterialTable
                    title="Hoja de Ruta"
                    data={this.state.routeFlights}
                    columns={columns}
                    options={{search:false,headerStyle:{fontWeight:'bold'}}}
                    localization={{
                      toolbar: {
                        searchPlaceholder: "Buscar"
                      },
                      body: {
                        emptyDataSourceMessage: 'No hay datos'
                      }
                    }}
                  />
                  <Button
                    size="lg"
                    variant="contained"
                    color="primary"
                    onClick={this.homePage}
                    style={{fontSize:"18px"}}
                    >Inicio
                  </Button>
                </Col>
              </Row>
        </div>
        <FooterConstrunet/>
      </div>
    </main>
    </>
  );
  }
}

export default ShippingDetail;
