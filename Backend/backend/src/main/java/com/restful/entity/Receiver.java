package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "Receiver")
@Entity
public class Receiver {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idReceiver")
    private Long id;

    private String mail;
    private String phone;
    private String phonePrefix; //To add in dbd

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPerson")
    private Person person;

    @OneToMany(mappedBy = "receiver", cascade = CascadeType.ALL)
    private List<Shipping> shippings = new ArrayList<>();

    protected Receiver(){}

    public Receiver(Long id, String mail, String phone, String phonePrefix, Person person, List<Shipping> shippings) {
        this.id = id;
        this.mail = mail;
        this.phone = phone;
        this.phonePrefix = phonePrefix;
        this.person = person;
        this.shippings = shippings;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public List<Shipping> getShippings() {
        return shippings;
    }

    public void setShippings(List<Shipping> shippings) {
        this.shippings = shippings;
    }
}
