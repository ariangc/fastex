import React from "react";

// reactstrap components
import { Grid } from "react-bootstrap";
import {
  Button,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  FormText,
  Label,
  Card,
  CardTitle,
  Modal
} from "reactstrap";
import {Tabs} from 'react-bootstrap';
import {Tab} from 'react-bootstrap';
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import HelpOutline from '@material-ui/icons/HelpOutline';
import PublishIcon from '@material-ui/icons/Publish';
import classNames from 'classnames';
import {currentUser,shippingUpload, shipping,
  apiURL} from "variables/Variables.js"
// core components
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import SideBar from 'components/sidebar/SideBar.js';
import MaterialTable from "material-table";
import axios from "axios";
import './Shipping.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Datetime from 'react-datetime';
import ExampleComponent from "react-rounded-image";

const columns = [
  {
    title: 'Origen',
    field: 'startCity'
  },
  {
    title: 'Destino',
    field: 'endCity'
  },
  {
    title: 'F. Partida',
    field: 'startDate'
  },
  {
    title: 'F. Llegada',
    field: 'endDate'
  },
  {
    title: 'Estado',
    field: 'state'
  }
  ];

class ShippingDetail extends React.Component {
  constructor(props) {
    super(props);
    this.appendLeadingZeroes = this.appendLeadingZeroes.bind(this)
    this.substractDates = this.substractDates.bind(this)
    
    this.state = {
      startCity:null,
      endCity:null,
      routes:null,
      fixedRoutes:null
    }
  }
  
  substractDates(hourstart, minstart, hourend, minend){
    console.log('funcion', hourstart, minstart, hourend, minend)
    var startMin = parseInt(hourstart)*60+parseInt(minstart)
    var endMin = parseInt(hourend)*60+parseInt(minend)
    if(endMin==startMin){
      return 0;
    }
    else if(endMin<startMin){
      //es el dia siguiente
      return (24*60-startMin)+endMin
    }
    else if(endMin>startMin){
      //es el mismo dia
      return endMin-startMin
    }
  }
  appendLeadingZeroes(n){
    if(n <= 9){
      return "0" + n;
    }
    return n
  }
  
  componentWillMount(){
    if(shipping.value){
    var aux ={idShipping: parseInt(shipping.value.id)}
    axios.post(apiURL.value + 'shipping/getRoute', aux).then((res)=>{
      console.log(res.data['route'][0].flight.startCity.name)
      if(res.status==200){
        axios.get('https://restcountries.eu/rest/v2/alpha/' + res.data['route'][0].flight.startCity.country.code).then((resp1)=>{
          console.log(resp1) 
          if(resp1.status==200){
            this.setState({
              startCity:resp1.data.flag
            })
          }
          
        })
        axios.get('https://restcountries.eu/rest/v2/alpha/' + res.data['route'][res.data['route'].length-1].flight.endCity.country.code).then((resp2)=>{
          if(resp2.status==200){
            this.setState({
              endCity:resp2.data.flag
            })
          }
          
        })
        this.setState({
          routes:res.data['route'],

        }
        , ()=>{
          console.log(this.state.routes)
          var routesAux = []
          this.state.routes.map((route, index)=>{
            var date = new Date(route.startDate)
            var minutes = this.substractDates(route.flight.departureTime.slice(0,2), route.flight.departureTime.slice(3,5), route.flight.landingTime.slice(0,2), route.flight.landingTime.slice(3,5))
            console.log('minutes', minutes)
            var end = Datetime.moment(date).add(minutes, 'm').toDate()
            console.log('end', end)
            var aux = {startCity:route.flight.startCity.name + ' - ' + route.flight.startCity.country.name,
                      endCity:route.flight.endCity.name + ' - ' + route.flight.endCity.country.name,
                      startDate:this.appendLeadingZeroes(this.appendLeadingZeroes(date.getUTCDate()) + '/' + this.appendLeadingZeroes(date.getUTCMonth() + 1) + "/" + date.getUTCFullYear() + " " + date.getUTCHours()) + ":" + this.appendLeadingZeroes(date.getUTCMinutes()),
                      endDate: this.appendLeadingZeroes(this.appendLeadingZeroes(end.getUTCDate()) + '/' + this.appendLeadingZeroes(end.getUTCMonth() + 1) + "/" + end.getUTCFullYear() + " " + end.getUTCHours()) + ":" + this.appendLeadingZeroes(end.getUTCMinutes()),
                      state: route.programmedFlightStatus.description
                    }
            routesAux.push(aux)
          })
          this.setState({
            fixedRoutes:routesAux
          })
          console.log('rutas', routesAux)
        })
      }
    })
  }
  }
  render(){
  return (
    <>
    <div className="App">
      
      <SideBar fluid toggle={this.sidebarIsClose} isOpen={true}/>
      
      <Container fluid className={classNames('content', {'is-open': true})}>
        <NavbarRedexAdmin style={{position:'fixed', marginTop:'0'}}/>
        <Container style={{marginTop:'10%', backgroundColor:'#FAFAFA'}}>
          <ToastContainer />
          <Card>
            <div className='text-center'><h4>Envío {shipping.value?shipping.value.trackingCode:''}</h4></div>
            <Col md={{size:10, offset:1}}>
              
              <Row>
                <Col md={{size:5}}>
                  <div className='text-center'><h5 style={{fontSize:"bold"}}>Remitente</h5></div>
                  <FormGroup>
                    <label htmlFor="exampleFormControlInput1">Nombres</label>
                    <Input
                      id="telephoneInput"
                      type="text"
                      value = {shipping.value?shipping.value.customer.person.names + ' ' + shipping.value.customer.person.surnames:''}
                      disabled
                    ></Input>
                  </FormGroup>
                  <FormGroup>
                    <label htmlFor="exampleFormControlInput1">Documento</label>
                    <Input
                      id="telephoneInput"
                      type="text"
                      value = {shipping.value?shipping.value.customer.person.document:''}
                      disabled
                    ></Input>
                  </FormGroup>
                  <Col md={{size:7}} style={{padding:'0', margin:'0'}}>
                    <FormGroup className="mb-3">
                      <label htmlFor="exampleFormControlInput1">Origen</label>
                      <InputGroup style={{minWidth:'40vh'}}>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText style={{padding:'0', margin:'0'}}>
                            <img
                              src={this.state.startCity?this.state.startCity:null}
                              style={{height:'34.55px', width:'50px', borderRadius:'1.5px'}}
                            />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input style={{minWidth:'30vh'}} type="text" value = {this.state.routes?this.state.routes[0].flight.startCity.name + ' - ' + this.state.routes[0].flight.startCity.country.name:''} disabled/>
                      </InputGroup>
                    </FormGroup>
                  </Col>
                </Col>
                <Col md={{size:5, offset:2}}>
                  <div className='text-center'><h5 style={{fontSize:"bold"}}>Destinatario</h5></div>
                  <FormGroup>
                    <label htmlFor="exampleFormControlInput1">Nombres</label>
                    <Input
                      id="telephoneInput"
                      type="text"
                      value = {shipping.value?shipping.value.receiver.person.names + ' ' + shipping.value.receiver.person.surnames:''}
                      disabled
                    ></Input>
                  </FormGroup>
                  <FormGroup>
                    <label htmlFor="exampleFormControlInput1">Documento</label>
                    <Input
                      id="telephoneInput"
                      type="text"
                      value = {shipping.value?shipping.value.receiver.person.document:''}
                      disabled
                    ></Input>
                  </FormGroup>
                  <Col md={{size:7}} style={{padding:'0', margin:'0'}}>
                    <FormGroup className="mb-3">
                      <label htmlFor="exampleFormControlInput1">Destino</label>
                      <InputGroup style={{minWidth:'40vh'}}>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText style={{padding:'0', margin:'0'}}>
                            <img
                              src={this.state.endCity?this.state.endCity:null}
                              style={{height:'34.55px', width:'50px', borderRadius:'1.5px'}}
                            />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input style={{minWidth:'30vh'}} type="text" value = {this.state.routes?this.state.routes[this.state.routes.length-1].flight.endCity.name + ' - ' + this.state.routes[this.state.routes.length-1].flight.endCity.country.name:''} disabled/>
                      </InputGroup>
                    </FormGroup>
                  </Col>
                </Col>
              </Row>
              <MaterialTable
                  style={{marginBottom:'5%'}}   
                  title="Ruta"
                  data={this.state.fixedRoutes?this.state.fixedRoutes:[]}
                  columns={columns}
                  localization={{
                    toolbar: {
                      searchPlaceholder: "Buscar"
                    },
                    body: {
                      emptyDataSourceMessage: 'No hay datos'
                    }
                  }}
                  options={{
                    paging: false
                  }}
                />
            </Col>            
          </Card>   
        </Container>
      </Container>
  
    </div>     
    </>
  );
  }
}

export default ShippingDetail;
