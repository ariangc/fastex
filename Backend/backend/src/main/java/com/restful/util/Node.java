package com.restful.util;

import java.util.Comparator;
import java.util.Date;

public class Node implements Comparable<Node> {
    public Long idCity;
    public Date currentDatetime;
    public int heuristicCost;

    public Node() {
    }

    public Node(Node other){
        this.idCity = other.idCity;
        this.currentDatetime = other.currentDatetime;
        this.heuristicCost = other.heuristicCost;
    }

    public Node(Long idCity, Date currentDatetime, int heuristicCost) {
        this.idCity = idCity;
        this.currentDatetime = currentDatetime;
        this.heuristicCost = heuristicCost;
    }

    @Override
    public int compareTo(Node o) {
        if(this.currentDatetime.before(o.currentDatetime)){
            return -1;
        }
        else if(this.currentDatetime.after(o.currentDatetime)){
            return 1;
        }
        else return 0;
    }
}
