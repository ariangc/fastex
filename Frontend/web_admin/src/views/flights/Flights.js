import React from "react";

// reactstrap components
import { Grid } from "react-bootstrap";
import {
  Button,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormText,
  Label,
  Card,
  CardTitle,
  Modal
} from "reactstrap";
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import HelpOutline from '@material-ui/icons/HelpOutline';
import classNames from 'classnames';
import {apiURL, warehouse, editWarehouse, editedWarehouse} from "variables/Variables.js"
// core components
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import SideBar from 'components/sidebar/SideBar.js';
import MaterialTable from "material-table";
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'
import {PassThrough} from 'stream';
import Datetime from 'react-datetime';

const data = [
{
    filledCapacity: 100,
    flight:{
        actualLandingTime:'04:05',
        departureTime:'05:44',
        description:null,
        endCity:{airportCode:'LPPT', cityCode:'lisb',
                country:{code:'prt', continent:{id:2, name:'Europa'}, id:81, name:'Portugal'}, id:76, name:'Lisboa'},
        flightSatus:{description:'activo', id:2},
        id:818,
        landingTime:'03:57',
        startCity:{airportCode:'SPIM', cityCode:'lima',
        country:{code:'per', continent:{id:1, name:'America del Sur'}, id:49, name:'Peru'}, id:44, name:'Lima'}
    },
    full:false,
    id: 93032,
    programmedFlightStatus:{description:'aterrizó', id:5},
    startDate:'16/07/2020',
    totalCapacity:260
}
]
const columns = [
{
   title: 'Ciudad Origen',
   field: 'startCity'
}, 
{
  title: 'Ciudad Destino',
  field: 'endCity'
}, 
{
  title: 'Hora de Partida',
  field: 'departureTime'
}, 
{
 title: 'Hora de Llegada',
 field: 'landingTime'
}, 
{
  title: 'Cantidad Paquetes',
  field: 'filled'
},
{
  title: 'Estado',
  field: 'status'
}
];

class FlightList extends React.Component {
  constructor(props) {
    super(props);
    this.sidebarIsOpen = this.sidebarIsOpen.bind(this);
    this.sidebarIsClose = this.sidebarIsClose.bind(this);
    this.appendLeadingZeroes = this.appendLeadingZeroes.bind(this);
    this.substractDates = this.substractDates.bind(this);
    this.state = {
      sidebarOpen:true,
      flights:[],
      loading:true
    }
}
substractDates(hourstart, minstart, hourend, minend){
  console.log('funcion', hourstart, minstart, hourend, minend)
  var startMin = parseInt(hourstart)*60+parseInt(minstart)
  var endMin = parseInt(hourend)*60+parseInt(minend)
  if(endMin==startMin){
    return 0;
  }
  else if(endMin<startMin){
    //es el dia siguiente
    return (24*60-startMin)+endMin
  }
  else if(endMin>startMin){
    //es el mismo dia
    return endMin-startMin
  }
}
appendLeadingZeroes(n){
  if(n <= 9){
    return "0" + n;
  }
  return n
}
sidebarIsClose(){
    this.setState({
      sidebarOpen:!this.state.sidebarOpen
    })
  }
  sidebarIsOpen(){
    this.setState({
      sidebarOpen:true
    })
  }
componentDidMount(){
    axios.post(apiURL.value + 'flight/getAll').then((res)=>{
        //to do
        console.log(res.data)
        if(res.status==200){
          var list = []
          res.data['programmedFlights'].map((flight, index)=>{
            console.log('entra a map')
            var date = new Date(flight.startDate)
            console.log('setea dia inciio')
            var minutes = this.substractDates(flight.flight.departureTime.slice(0,2), flight.flight.departureTime.slice(3,5), flight.flight.landingTime.slice(0,2), flight.flight.landingTime.slice(3,5))
            console.log('regresa de función minutos')
            var end = Datetime.moment(date).add(minutes, 'm').toDate()
            console.log('calcula fecha fin')
            var aux = {startCity:flight.flight.startCity.name + ' - ' + flight.flight.startCity.country.name, 
                      endCity:flight.flight.endCity.name + ' - ' + flight.flight.endCity.country.name, 
                      departureTime:this.appendLeadingZeroes(this.appendLeadingZeroes(date.getUTCDate()) + '/' + this.appendLeadingZeroes(date.getUTCMonth() + 1) + "/" + date.getUTCFullYear() + " " + date.getUTCHours()) + ":" + this.appendLeadingZeroes(date.getUTCMinutes()),
                      landingTime:this.appendLeadingZeroes(this.appendLeadingZeroes(end.getUTCDate()) + '/' + this.appendLeadingZeroes(end.getUTCMonth() + 1) + "/" + end.getUTCFullYear() + " " + end.getUTCHours()) + ":" + this.appendLeadingZeroes(end.getUTCMinutes()),
                      filled:flight.filledCapacity, status:flight.programmedFlightStatus.description}
            console.log('setea auxialiar')
            list.push(aux)
            console.log('agrega a lista')
          })
            this.setState({
                flights:list,
                loading:false
            })
        }
    })
    .catch(()=>{
        toast.error('Ha ocurrido un error con el servidor', { type: toast.TYPE.ERROR, autoClose: 5000 })
    })
}
render(){
  return (
    <>
    <div className="App">
      
      <SideBar fluid toggle={this.sidebarIsClose} isOpen={this.state.sidebarOpen}/>
       
      <Container
      fluid className={classNames('content', {'is-open': this.state.sidebarOpen})}>
      <NavbarRedexAdmin style={{position:'fixed', marginTop:'0'}}/>
        <Container style={{marginTop:'10%', padding:'0%', backgroundColor:'#FAFAFA'}}>
        <ToastContainer /> 
                <Col> 
                    {this.state.loading?
                      <div className='text-center'  style={{width:'100%', weight:'100%', backgroundColor:'#FFFFFF'
                    }}>
                      <Loader
                        type="ThreeDots"
                        color="#03256C"
                        height={100}
                        width={100}
                        timeout={3000000}
                      />
                    </div> :
                    <MaterialTable
                      title="Vuelos"
                      data={this.state.flights}
                      columns={columns}
                      
                      options={{filtering:true,exportButton: true,
                        rowStyle: rowData => ({
                          backgroundColor: (this.state.selectedRow && this.state.selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
                        })
                      }}
                      localization={{
                        toolbar: {
                          searchPlaceholder: "Buscar"
                        },
                        header:{
                          actions:'Acciones'
                        },
                        body: {
                          emptyDataSourceMessage: 'No hay datos'
                        }
                      }}
                    />}  
                </Col>
          </Container>
      </Container>
                  
    </div>       
    </>
  );
  }
}

export default FlightList;
