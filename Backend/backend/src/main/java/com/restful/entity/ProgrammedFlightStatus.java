package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "ProgrammedFlightStatus")
@Entity
public class ProgrammedFlightStatus {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idProgrammedFlightStatus")
    private Long id;
    private String description;

    @OneToMany(mappedBy = "programmedFlightStatus", cascade = CascadeType.ALL)
    private List<ProgrammedFlight> programmedFlights = new ArrayList<>();

    public ProgrammedFlightStatus() {
    }

    public ProgrammedFlightStatus(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
