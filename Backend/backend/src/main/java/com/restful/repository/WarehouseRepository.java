package com.restful.repository;

import com.restful.entity.City;
import com.restful.entity.Warehouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("warehouseRepository")

public interface WarehouseRepository extends JpaRepository<Warehouse, Long> {
    public abstract List<Warehouse> findAll();
    public abstract List<Warehouse> findByCity(City city);
}
