package com.restful.service;

import com.restful.entity.*;
import com.restful.model.FlightModel;
import com.restful.model.ProgrammedFlightModel;
import com.restful.repository.*;
import com.restful.util.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("algorithmService")
public class AlgorithmService {

    @Autowired
    @Qualifier("warehouseRepository")
    private WarehouseRepository warehouseRepository;

    @Autowired
    @Qualifier("cityRepository")
    private CityRepository cityRepository;

    @Autowired
    @Qualifier("programmedFlightRepository")
    private ProgrammedFlightRepository programmedFlightRepository;

    @Autowired
    @Qualifier("shippingXProgrammedFlightRepository")
    private ShippingXProgrammedFlightRepository shippingXProgrammedFlightRepository;

    @Autowired
    @Qualifier("shippingRepository")
    private ShippingRepository shippingRepository;

    @Autowired
    @Qualifier("customerRepository")
    private CustomerRepository customerRepository;

    @Autowired
    @Qualifier("shippingStatusRepository")
    private ShippingStatusRepository shippingStatusRepository;

    @Autowired
    @Qualifier("receiverRepository")
    private ReceiverRepository receiverRepository;

    @Autowired
    @Qualifier("warehouseHistoryRepository")
    private WarehouseHistoryRepository warehouseHistoryRepository;

    public Map<String, List<ProgrammedFlightModel>> runAlgorithm(Long idStartCity, Long idEndCity){
        Map<String, List<ProgrammedFlightModel>> response = new HashMap<>();
        /*
        Primer paso: revisar si el almacen del inicio está lleno
         */
        Optional<City> city = cityRepository.findById(idStartCity);
        Warehouse warehouse = warehouseRepository.findByCity(city.get()).get(0);

        if(warehouse.isFull() || warehouse.getWarehouseStatus().getId() != 2) {
            //En este caso, el almacen no tiene espacio o esta inactivo. Retornar lista vacia.
            response.put("firstfull", new ArrayList<>());
            warehouse.setTotalBad(warehouse.getTotalBad() + 1);
            warehouseRepository.save(warehouse);
            WarehouseHistory warehouseHistory = new WarehouseHistory(null, warehouse.getFilledCapacity(), warehouse.getFilledCapacity(), new Date(), false, warehouse);
            warehouseHistoryRepository.save(warehouseHistory);
            return response;
        }

        /*
        Segundo paso: Algoritmo
         */

        Date now = new Date();
        PriorityQueue<Node> pq = new PriorityQueue<Node>();
        Map<Long, Date> dist = new HashMap<>();
        Map<Long, ProgrammedFlightModel> parent = new HashMap<>();
        pq.add(new Node(idStartCity, now, 0));

        while(!pq.isEmpty()){
            Node front = pq.remove();
            Long orig = front.idCity;
            Date currentDatetime = front.currentDatetime;
            System.out.println(orig.toString() + " " + currentDatetime.toString());
            //Obtener los siguientes vuelos a partir de fecha inicial y ciudad
            List<ProgrammedFlight> byDate = programmedFlightRepository.findByStartDateGreaterThanEqual(currentDatetime);
            List<ProgrammedFlightModel> programmedFlights = new ArrayList<>();

            //Filtrar los vuelos programados por ciudad de origen
            for(ProgrammedFlight programmedFlight: byDate){
                if(programmedFlight.getFlight().getStartCity().getId().equals(orig)){
                    programmedFlights.add(new ProgrammedFlightModel(programmedFlight));
                }
            }

            for(ProgrammedFlightModel programmedFlight: programmedFlights){
                if(programmedFlight.isFull()) continue;
                FlightModel flight = programmedFlight.getFlight();
                Long dest = flight.getEndCity().getId();
                Date startDate = programmedFlight.getStartDate();

                City destCity = cityRepository.findById(dest).get();
                Warehouse destWarehouse = warehouseRepository.findByCity(destCity).get(0);
                if(destWarehouse.isFull() || destWarehouse.getWarehouseStatus().getId() != 2) continue;

                int departureHour = Integer.parseInt(flight.getDepartureTime().substring(0,2));
                int departureMinute = Integer.parseInt(flight.getDepartureTime().substring(3,5));

                Calendar landingDateCal = Calendar.getInstance();

                landingDateCal.setTime(startDate);
                landingDateCal.set(Calendar.HOUR_OF_DAY, departureHour);
                landingDateCal.set(Calendar.MINUTE, departureMinute);

                //Aumentarle a arrivalDateCal la duración del vuelo
                int landingHour = Integer.parseInt(flight.getLandingTime().substring(0,2));
                int landingMinute = Integer.parseInt(flight.getLandingTime().substring(3,5));

                int depStamp = 60 * departureHour + departureMinute;
                int landStamp = 60 * landingHour + landingMinute;

                int totalDuration = 0;

                if(depStamp > landStamp) totalDuration = 1440 - (depStamp - landStamp);
                else totalDuration = landStamp - depStamp;

                landingDateCal.add(Calendar.MINUTE, totalDuration);

                Date arrivalDate = landingDateCal.getTime();

                if(!dist.containsKey(dest) || dist.get(dest).after(arrivalDate)){
                    dist.put(dest, arrivalDate);
                    parent.put(dest, programmedFlight);
                    pq.add(new Node(dest, arrivalDate, 0));
                    System.out.println(pq.size());
                }
            }
        }


        if(!dist.containsKey(idEndCity)){
            response.put("enderror", new ArrayList<>());
            return response;
        }

        long diff = (dist.get(idEndCity).getTime() - now.getTime()) / (60 * 1000);

        Long continent1 = cityRepository.findById(idStartCity).get().getCountry().getContinent().getId();
        Long continent2 = cityRepository.findById(idEndCity).get().getCountry().getContinent().getId();
        if(continent1.equals(continent2)){
            if(diff > 1440){
                response.put("timeerror1", new ArrayList<>());
            }
        }
        else{
            if(diff > 2880){
                response.put("timeerror2", new ArrayList<>());
            }
        }
        Long cur = idEndCity;
        List<ProgrammedFlightModel> route = new ArrayList<>();
        while(!cur.equals(idStartCity)){
            ProgrammedFlightModel edge = parent.get(cur);
            route.add(edge);
            cur = edge.getFlight().getStartCity().getId();
        }
        Collections.reverse(route);
        response.put("route", route);
        return response;
    }

    public boolean updateRoute(List<ProgrammedFlight> route){
        Long first = route.get(0).getFlight().getStartCity().getId();
        City firsty = cityRepository.findById(first).get();
        Warehouse firstx = warehouseRepository.findByCity(firsty).get(0);
        WarehouseHistory warehouseHistory = new WarehouseHistory(null, firstx.getFilledCapacity(), firstx.getFilledCapacity() + 1, new Date(), true, firstx);
        warehouseHistoryRepository.save(warehouseHistory);
        for(ProgrammedFlight programmedFlight: route){
            Long from = programmedFlight.getFlight().getStartCity().getId();
            City y = cityRepository.findById(from).get();
            Warehouse x = warehouseRepository.findByCity(y).get(0);
            x.setFilledCapacity(x.getFilledCapacity() + 1);
            warehouseRepository.save(x);
            programmedFlight.setFilledCapacity(programmedFlight.getFilledCapacity() + 1);
            programmedFlightRepository.save(programmedFlight);
        }
        Long to = route.get(route.size() - 1).getFlight().getEndCity().getId();
        City y = cityRepository.findById(to).get();
        Warehouse x = warehouseRepository.findByCity(y).get(0);
        x.setFilledCapacity(x.getFilledCapacity() + 1);
        warehouseRepository.save(x);

        return true;
    }

    public boolean registerRoute(List<Long> ids){
        Long idShipping = ids.get(0);
        Optional<Shipping> shipping = shippingRepository.findById(idShipping);
        for(int i = 1; i < ids.size(); i ++){
            Long idProgrammedFlight = ids.get(i);
            Optional<ProgrammedFlight> programmedFlight = programmedFlightRepository.findById(idProgrammedFlight);
            ShippingXProgrammedFlight x = new ShippingXProgrammedFlight(Long.parseLong("0"), i, true, shipping.get(), programmedFlight.get());
            shippingXProgrammedFlightRepository.save(x);
        }
        return true;
    }
}
