package com.restful.model;

import com.restful.entity.City;

public class CityModel {
    private Long id;
    private String name;
    private String cityCode;
    private String airportCode;
    private Float latitude;
    private Float longitude;
    private Float exponent;
    private CountryModel country;

    public CityModel(Long id, String name, String cityCode, String airportCode, Float latitude, Float longitude, Float exponent) {
        this.id = id;
        this.name = name;
        this.cityCode = cityCode;
        this.airportCode = airportCode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.exponent = exponent;
    }

    public CityModel(City a){
        this.id = a.getId();
        this.name = a.getName();
        this.cityCode = a.getCityCode();
        this.airportCode = a.getAirportCode();
        this.latitude = a.getLatitude();
        this.longitude = a.getLongitude();
        this.exponent = a.getExponent();
        this.country = new CountryModel(a.getCountry());
    }

    public Float getExponent() {
        return exponent;
    }

    public void setExponent(Float exponent) {
        this.exponent = exponent;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public CityModel(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public CountryModel getCountry() {
        return country;
    }

    public void setCountry(CountryModel country) {
        this.country = country;
    }
}
