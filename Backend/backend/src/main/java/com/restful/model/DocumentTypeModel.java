package com.restful.model;

import com.restful.entity.DocumentType;

public class DocumentTypeModel {
    private Long id;
    private String name;
    private String regex;
    private CountryModel country;

    public DocumentTypeModel(Long id, String name, String regex) {
        this.id = id;
        this.name = name;
        this.regex = regex;
    }

    public DocumentTypeModel(DocumentType a){
        this.id = a.getId();
        this.name = a.getName();
        this.regex = a.getRegex();
        this.country = new CountryModel(a.getCountry());
    }

    public DocumentTypeModel() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public CountryModel getCountry() {
        return country;
    }

    public void setCountry(CountryModel country) {
        this.country = country;
    }
}
