package com.restful.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Table(name = "Shipping")
@Entity
public class Shipping {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idShipping")
    private Long id;

    private String trackingCode;
    private String description;
    private Float price;
    private byte[] receiptDocument;

    @Column(name = "shippingDate", columnDefinition="DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date shippingDate;

    @Column(name = "receiptDate", columnDefinition="DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date receiptDate;

    @Column(name = "registrationDate", columnDefinition="DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idCustomer")
    private Customer customer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idReceiver")
    private Receiver receiver;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idShippingStatus")
    private ShippingStatus shippingStatus;

    @OneToMany(mappedBy = "shipping", cascade = CascadeType.ALL)
    private List<Package> packages = new ArrayList<>();

    @OneToMany(mappedBy = "shipping", cascade = CascadeType.ALL)
    private List<ShippingXProgrammedFlight> shippingXProgrammedFlights = new ArrayList<>();

    public Shipping(){}

    public Shipping(Long id, String trackingCode, String description, Float price, byte[] receiptDocument, Date shippingDate, Date receiptDate, Date registrationDate, Customer customer, Receiver receiver, ShippingStatus shippingStatus, List<Package> packages, List<ShippingXProgrammedFlight> shippingXProgrammedFlights) {
        this.id = id;
        this.trackingCode = trackingCode;
        this.description = description;
        this.price = price;
        this.receiptDocument = receiptDocument;
        this.shippingDate = shippingDate;
        this.receiptDate = receiptDate;
        this.registrationDate = registrationDate;
        this.customer = customer;
        this.receiver = receiver;
        this.shippingStatus = shippingStatus;
        this.packages = packages;
        this.shippingXProgrammedFlights = shippingXProgrammedFlights;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTrackingCode() {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        this.trackingCode = trackingCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public byte[] getReceiptDocument() {
        return receiptDocument;
    }

    public void setReceiptDocument(byte[] receiptDocument) {
        this.receiptDocument = receiptDocument;
    }

    public Date getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(Date shippingDate) {
        this.shippingDate = shippingDate;
    }

    public Date getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(Date receiptDate) {
        this.receiptDate = receiptDate;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public ShippingStatus getShippingStatus() {
        return shippingStatus;
    }

    public void setShippingStatus(ShippingStatus shippingStatus) {
        this.shippingStatus = shippingStatus;
    }

    public List<Package> getPackages() {
        return packages;
    }

    public void setPackages(List<Package> packages) {
        this.packages = packages;
    }

    public List<ShippingXProgrammedFlight> getShippingXProgrammedFlights() {
        return shippingXProgrammedFlights;
    }

    public void setShippingXProgrammedFlights(List<ShippingXProgrammedFlight> shippingXProgrammedFlights) {
        this.shippingXProgrammedFlights = shippingXProgrammedFlights;
    }
}
