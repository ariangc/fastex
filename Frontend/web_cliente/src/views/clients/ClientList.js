import React from "react";

// reactstrap components
import { Grid } from "react-bootstrap";
import {
  Button,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormText,
  Label,
  Card,
  CardTitle,
  Modal
} from "reactstrap";
import {Tabs} from 'react-bootstrap';
import {Tab} from 'react-bootstrap';
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import HelpOutline from '@material-ui/icons/HelpOutline';
import classNames from 'classnames';
import {currentUser, loginConstrunetAdmision, landingConstrunetAdmision, table,
  apiURL} from "variables/Variables.js"
// core components
import NavbarRedexAdmin from 'components/Navbars/NavbarRedexAdmin.js';
import SideBar from 'components/sidebar/SideBar.js';
import MaterialTable from "material-table";
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const data = [];
const columns = [
{
  title: 'N°',
  field: 'id'
},
{
  title: 'Nombres',
  field: 'person.names'
}, 
{
  title: 'Apellidos',
  field: 'person.surnames'
},
{
  title: 'Nacionalidad',
  field: 'person.country.name'
},
{
  title: 'N° Documento',
  field: 'person.document'
},
{
  title: 'Estado',
  field: 'customerStatus.description'
}
];
class ClientList extends React.Component {
  constructor(props) {
    super(props);
    this.sidebarIsOpen = this.sidebarIsOpen.bind(this);
    this.sidebarIsClose = this.sidebarIsClose.bind(this);
    this.togglerADName = this.togglerADName.bind(this);
    this.handlerCloseEditModal = this.handlerCloseEditModal.bind(this);
    this.state = {
      selectedRow: null,
      data:[],
      sidebarOpen:true,
      modalOpen:false,
      clientDeleteNames:'',
      customers:[]
    }
}
handlerCloseEditModal(){
  this.setState({
    modalOpen:false
  })
}
togglerADName(event){
  this.setState({
    ADName:event.target.value
  })
}
sidebarIsClose(){
  this.setState({
    sidebarOpen:!this.state.sidebarOpen
  })
}
sidebarIsOpen(){
  this.setState({
    sidebarOpen:true
  })
}
componentWillMount(){
  /* if(!currentUser.logged){
    this.props.history.push("");
  } */
  axios.post(apiURL.value + 'customer/list').then((res)=>{
    console.log(res)
    this.setState({
      customers:res.data
    })
    console.log("data")
    console.log(data)
  })
}


  render(){
  return (
    <>
    
    <div className="App">
      
      <SideBar fluid toggle={this.sidebarIsClose} isOpen={this.state.sidebarOpen}/>
      
      <Container
      fluid className={classNames('content', {'is-open': this.state.sidebarOpen})}>
      <NavbarRedexAdmin style={{position:'fixed', marginTop:'0'}}/>
        <Container style={{marginTop:'10%', padding:'0%', backgroundColor:'#FAFAFA'}}>
        <ToastContainer />
                 
                  <Col> 
                    <MaterialTable
                    
                      title="Clientes"
                      data={this.state.customers}
                      columns={columns}
                      actions={[
                        {
                          icon:'edit',
                          iconButton:{color:'red'},
                          color:'red',
                          tooltip: 'Save User'/* ,
                          onClick: (event, rowData) => alert("You saved " + rowData.name) */
                        },
                        {
                          icon: 'delete',
                          root: 'blue',
                          tooltip: 'Delete User'/* ,
                          onClick: (event, rowData) => confirm("You want to delete " + rowData.name),
                          disabled: rowData.birthYear < 2000 */
                        }
                      ]}
                      components={{
                        Action: 
                          props => {
                            if(props.action.icon === 'edit'){
                              return(
                                <Tooltip title='Editar Cliente'>
                                  <IconButton 
                                  style={{color:'#0074D9'}} 
                                  component="span"
                                  size = 'small'
                                  onClick= {(event, rowData) => alert("You selected " + props.data.person.names)}
                                  >
                                    <Edit fontSize="large"/>
                                  </IconButton >
                                </Tooltip>
                              )
                            }
                            if(props.action.icon === 'delete'){
                              return(
                                <Tooltip title='Desactivar Cliente'>
                                  <IconButton 
                                  style={{color:'#FF4136'}}
                                  component="span"
                                  onClick = {(event, rowData) => {this.setState({
                                    clientDeleteNames:props.data.person.names + ' ' + props.data.person.surnames,
                                    modalOpen:true
                                  })}}
                                  >
                                    <Delete fontSize="large"/>
                                  </IconButton >
                                </Tooltip>
                              )
                            }
                          }
                      }}
                      onRowClick={(evt, selectedRow) => {
                        (evt.target).ondblclick = () =>{this.setState({selectedRow}, ()=>{console.log(this.state.selectedRow)})
                        toast.error('Ha seleccionado a ' + selectedRow.person.names, { type: toast.TYPE.ERROR, autoClose: 5000 });}
                        }}
                      options={{actionsColumnIndex: -1,filtering:true,
                        rowStyle: rowData => ({
                          backgroundColor: (this.state.selectedRow && this.state.selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
                        })
                      }}
                      localization={{
                        toolbar: {
                          searchPlaceholder: "Buscar"
                        },
                        header:{
                          actions:'Acciones'
                        },
                        body: {
                          emptyDataSourceMessage: 'No hay datos'
                        }
                      }}
                    />
                    <Modal toggle={this.handlerCloseEditModal} isOpen={this.state.modalOpen}>
                    <Col  sm="12" md={{ size: 5, offset: 4 }}><HelpOutline style={{ fontSize: 100, color:'#1768AC' }}/></Col>
                <div className="modal-header">
                
                  <h5 style={{textAlign:'center'}} className="modal-title" id="exampleModalLiveLabel">
                    ¿Desea confirmar la desactivación del cliente {this.state.clientDeleteNames}?
                  </h5>
                  <button
                    aria-label="Close"
                    className="close"
                    type="button"
                    onClick={this.handlerCloseEditModal}
                  >
                    <span aria-hidden={true}>×</span>
                  </button>
                </div>
                <Col  sm="12" md={{ size: 8, offset: 2 }}>
                <div className="modal-footer">
                  <Button
                    color="danger"
                    type="button"
                    size="lg"
                    onClick={this.handlerCloseEditModal}
                  >
                    Cancelar
                  </Button>
                  <Button
                    color="primary"
                    type="button"
                    size="lg"
                    onClick={this.handlerCloseEditModal}
                  >
                    Aceptar
                  </Button>
                </div>
                </Col>
              </Modal>
                  </Col>
       
          </Container>
      </Container>
  
    </div>       
    </>
  );
  }
}

export default ClientList;
