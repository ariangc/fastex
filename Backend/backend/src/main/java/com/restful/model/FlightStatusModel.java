package com.restful.model;

import com.restful.entity.FlightStatus;

public class FlightStatusModel {
    private Long id;
    private String description;

    public FlightStatusModel(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    public FlightStatusModel(FlightStatus a){
        this.id = a.getId();
        this.description = a.getDescription();
    }

    public FlightStatusModel(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
