package com.restful.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormat {
    public static Date parseDate(String date) throws ParseException {
        try{
            return new SimpleDateFormat("yyyyMMdd").parse(date);
        }
        catch (ParseException e){
            e.printStackTrace();
            return null;
        }
    }
}
