package com.restful.service;

import com.restful.entity.Country;
import com.restful.entity.Customer;
import com.restful.entity.DocumentType;
import com.restful.entity.Person;
import com.restful.model.CustomerModel;
import com.restful.repository.CountryRepository;
import com.restful.repository.CustomerRepository;
import com.restful.repository.DocumentTypeRepository;
import com.restful.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("customerService")
public class CustomerService {
    @Autowired
    @Qualifier("customerRepository")
    private CustomerRepository customerRepository;

    @Autowired
    @Qualifier("countryRepository")
    private CountryRepository countryRepository;

    @Autowired
    @Qualifier("documentTypeRepository")
    private DocumentTypeRepository documentTypeRepository;

    @Autowired
    @Qualifier("personRepository")
    private PersonRepository personRepository;

    public CustomerService(){}

    public List<CustomerModel> getCustomers(){
        List<Customer> l = customerRepository.findAll();
        List<CustomerModel> lModel = new ArrayList<CustomerModel>();
        for(Customer customer: l){
            CustomerModel x = new CustomerModel(customer);
            if(x.getCustomerStatus().getId() != 1) lModel.add(x);
        }
        return lModel;
    }

    public List<CustomerModel> getCustomerByPersonData(Long idCountry, Long idDocumentType, String document){
        List<Country> country = countryRepository.findById(idCountry);
        Optional<DocumentType> documentType = documentTypeRepository.findById(idDocumentType);
        List<Person> person = personRepository.findByDocumentAndCountryAndDocumentType(document, country.get(0), documentType.get());

        List<Customer> customer = customerRepository.findByPerson(person.get(0));
        List<CustomerModel> lModel= new ArrayList<>();
        for(Customer customer1: customer){
            CustomerModel x = new CustomerModel(customer1);
            if(x.getCustomerStatus().getId() != 1) lModel.add(x);
        }
        return lModel;
    }

    public boolean createCustomer(Customer customer){
        customerRepository.save(customer);
        return true;
    }


}
