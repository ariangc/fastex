import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Form,
  FormGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Col,
  Row,
  Modal
} from "reactstrap";

// core components
import ExamplesNavbar from "components/Navbars/ExamplesNavbar.js";
import TransparentFooterConstrunet from "components/Footers/TransparentFooterConstrunet.js";
import {apiURL} from "variables/Variables.js";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from "axios";

class LoginConstrunet extends React.Component {
  constructor(props) {
    super(props);
    this.handlerUsername = this.handlerUsername.bind(this);
    this.handlerPassword = this.handlerPassword.bind(this);
    this.onClickLogin = this.onClickLogin.bind(this);
    this.handlerForgotPasswordOpen = this.handlerForgotPasswordOpen.bind(this);
    this.handlerForgotPasswordClose = this.handlerForgotPasswordClose.bind(this);
    this.state = {
        user:'',
        password:'',
        modalIsOpen:false
    };
    
}
componentWillUnmount(){
  console.log("login")
  console.log(this.props)
}
handlerForgotPasswordClose(){
  this.setState({
    modalIsOpen:false
  })
}
handlerForgotPasswordOpen(){
  this.setState({
    modalIsOpen:true
  })
}
handlerPassword(e){
  this.setState({
    password:e.target.value
  })
}
handlerUsername(e){
  this.setState({
    user:e.target.value
  })
}
onClickLogin(){
  console.log(this.props.location.pathname)
  if(this.state.user=='' && this.state.password==''){
    toast.error('Debe ingresar sus credenciales', { type: toast.TYPE.ERROR, autoClose: 5000 });
  }
  else{
    if(this.state.user==''){
      toast.error('Debe ingresar su usuario', { type: toast.TYPE.ERROR, autoClose: 5000 });
    }
    else{
      if(this.state.password==''){
        toast.error('Debe ingresar su constraseña', { type: toast.TYPE.ERROR, autoClose: 5000 });
      }
      else{
        var aux={
          "usuarioNT": this.state.user,
          "password": this.state.password
        }
        axios.post(apiURL.value + 'login', aux).then (res => {
          if(res.data==0){
            //usuario no encontrado
            toast.error('Ingrese su usuario correctamente', { type: toast.TYPE.ERROR, autoClose: 5000 });
          }
          else if(res.data==2){
            //contraseña incorrecta
            toast.error('Ingrese su contraseña correctamente', { type: toast.TYPE.ERROR, autoClose: 5000 });
          }
          else if(res.data==1){
            //creddenciales correctos
            this.props.history.push("/profile-page")
          }
        })
      }
    }
  }
  
  
  

}
render(){
    
  return (
    <>
      <div className="page-header clear-filter" filter-color="blue">
      <ToastContainer />
        <div
          className="page-header-image"
          style={{
            backgroundImage: "url(" + require("assets/img/banner.jpg") + ")"
          }}
        ></div>
        <div className="content" style={{marginTop:'-2%'}}>
          <Container>
            <Col className="ml-auto mr-auto" md="4">
              <Card className="card-login card-plain">
                <Form action="" className="form" method="">
                  <CardHeader className="text-center">
                    <div className="logo-container">
                      <img className="rounded-circle"
                        alt="..."
                        src={require("assets/img/nomina2.png")}
                      ></img>
                    </div>
                  </CardHeader>
                  <Form role="form">
                    <FormGroup className="mb-3">
                      <InputGroup className="no-border input-lg">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="now-ui-icons users_circle-08"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input placeholder="Usuario" /* type="email" */ onChange={this.handlerUsername} />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <InputGroup className="no-border input-lg">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="now-ui-icons ui-1_lock-circle-open"
                            style={{marginLeft:0}}></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          placeholder="Password"
                          type="password"
                          autoComplete="off"
                          onChange={this.handlerPassword}
                        />
                      </InputGroup>
                    </FormGroup>
                    {/* <div className="custom-control custom-control-alternative custom-checkbox">
                      <input
                        className="custom-control-input"
                        id=" customCheckLogin"
                        type="checkbox"
                      />
                      <label
                        className="custom-control-label"
                        htmlFor=" customCheckLogin"
                      >
                        <span>Remember me</span>
                      </label>
                    </div> */}
                    <Button
                      
                      className="btn-round"
                      color="info"                
                      onClick={this.onClickLogin}
                      size="lg"
                    >
                      Iniciar Sesión
                    </Button>
                    <div className="pull-left">
                     <u> <h6>
                        <a
                          className="link"
                          href="#pablo"
                          onClick={this.handlerForgotPasswordOpen}
                        >
                          ¿Olvidó su contraseña?
                        </a>
                      </h6></u>
                    </div>
                  </Form>
                </Form>
              </Card>
              <Modal toggle={this.handlerForgotPasswordClose} isOpen={this.state.modalIsOpen}>
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLiveLabel">
                    Recuperar Contraseña
                  </h5>
                  <button
                    aria-label="Close"
                    className="close"
                    type="button"
                    onClick={this.handlerForgotPasswordClose}
                  >
                    <span aria-hidden={true}>×</span>
                  </button>
                </div>
                <div className="modal-body">
                  <p>Ingrese su usuario</p>
                </div>
                <div className="modal-footer">
                  <Button
                    color="secondary"
                    type="button"
                    onClick={this.handlerForgotPasswordClose}
                  >
                    Close
                  </Button>
                  <Button
                    color="primary"
                    type="button"
                    onClick={this.handlerForgotPasswordClose}
                  >
                    Save changes
                  </Button>
                </div>
              </Modal>
            </Col>
          </Container>
        </div>
        <TransparentFooterConstrunet />
      </div>
    </>
  );
  }
}


export default LoginConstrunet;
