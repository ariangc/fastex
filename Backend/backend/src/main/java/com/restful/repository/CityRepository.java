package com.restful.repository;

import com.restful.entity.City;
import com.restful.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository("cityRepository")
public interface CityRepository extends JpaRepository<City, Long> {
    public abstract List<City> findByCountry(Country country);
    public abstract Optional<City> findByAirportCode(String airport_code);
}
