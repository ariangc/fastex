package com.restful.model;

import com.restful.entity.City;
import com.restful.entity.Flight;
import com.restful.entity.FlightStatus;

import javax.persistence.*;

public class FlightModel {
    private Long id;
    String departureTime; //Change both in DBD
    String landingTime;
    String actualLandingTime;
    String description;
    private CityModel startCity;
    private CityModel endCity;
    private FlightStatusModel flightStatus;


    public FlightModel(Long id, String departureTime, String landingTime, String actualLandingTime, String description) {
        this.id = id;
        this.departureTime = departureTime;
        this.landingTime = landingTime;
        this.actualLandingTime = actualLandingTime;
        this.description = description;
    }

    public FlightModel(Flight a){
        this.id = a.getId();
        this.departureTime = a.getDepartureTime();
        this.landingTime = a.getLandingTime();
        this.actualLandingTime = a.getActualLandingTime();
        this.description = a.getDescription();
        this.startCity = new CityModel(a.getStartCity());
        this.endCity = new CityModel(a.getEndCity());
        this.flightStatus = new FlightStatusModel(a.getFlightStatus());
    }

    public FlightModel(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getLandingTime() {
        return landingTime;
    }

    public String getActualLandingTime() {
        return actualLandingTime;
    }

    public void setActualLandingTime(String actualLandingTime) {
        this.actualLandingTime = actualLandingTime;
    }

    public void setLandingTime(String landingTime) {
        this.landingTime = landingTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CityModel getStartCity() {
        return startCity;
    }

    public void setStartCity(CityModel startCity) {
        this.startCity = startCity;
    }

    public CityModel getEndCity() {
        return endCity;
    }

    public void setEndCity(CityModel endCity) {
        this.endCity = endCity;
    }

    public FlightStatusModel getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(FlightStatusModel flightStatus) {
        this.flightStatus = flightStatus;
    }
}
