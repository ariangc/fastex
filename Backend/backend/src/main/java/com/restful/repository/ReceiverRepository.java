package com.restful.repository;

import com.restful.entity.Country;
import com.restful.entity.DocumentType;
import com.restful.entity.Person;
import com.restful.entity.Receiver;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ReceiverRepository extends JpaRepository<Receiver, Long> {
    public List<Receiver> findByPerson(Person person);
}
