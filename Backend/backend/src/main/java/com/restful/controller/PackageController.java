package com.restful.controller;

import com.restful.entity.Package;
import com.restful.service.PackageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/package")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
public class PackageController {
    @Autowired
    @Qualifier("packageService")
    private PackageService packageService;

    @CrossOrigin
    @PostMapping("/registerPackages")
    public boolean registerPackages(@RequestBody Map<String, List<Package>> data){
        return packageService.registerPackages(data.get("packages"));
    }
}
