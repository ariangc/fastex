/*

=========================================================
* Now UI Kit React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/now-ui-kit-react
* Copyright 2019 Creative Tim (http://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/now-ui-kit-react/blob/master/LICENSE.md)

* Designed by www.invisionapp.com Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import {login, createClient, clientList, createShipping, shippingList, shippingUpload, warehouseList,
        createWarehouse, editWarehouse, employeeList, createEmployee, systemVariables, systemGraphics,
        shippingListAdmin, landing, shippingDetail, shippingNotFound, quoteShipping, queryTimes} from "variables/Variables.js"
// styles for this kit
import "assets/css/bootstrap.min.css";
import "assets/scss/now-ui-kit.scss";
import "assets/demo/demo.css";
import "assets/demo/nucleo-icons-page-styles.css";
import Login from "views/login/Login.js";
//Clients
import ClientList from "views/clients/ClientList.js"
import CreateClient from "views/clients/CreateClient";
//Shippings
import CreateShipping from "views/shippings/CreateShipping";
import ShippingList from "views/shippings/ShippingList";
import ShippingUpload from "views/shippings/ShippingUpload"
import ShippingListAdmin from "views/shippings/ShippingListAdmin"
//WareHouse
import WarehouseList from "views/warehouses/WarehouseList";
import CreateWarehouse from "views/warehouses/CreateWarehouses.js";
import EditWarehouse from "views/warehouses/EditWarehouse.js";
//Simulation
import Graphics from "views/simulation/Graphics"
//Landing
import LandingPage from "views/landing/LandingPage"
import ShippingDetail from "views/landing/ShippingDetail"
import ShippingNotFound from "views/landing/ShippingNotFound";
import QuoteShipping from "views/landing/QuoteShipping"
import QueryTimes from "views/landing/QueryTimes"

import history from "views/clients/history";

ReactDOM.render(
  <BrowserRouter history={history}>
    <Switch>
      <Switch>
        <Route path={"/"+ shippingDetail.url} render={props => <ShippingDetail {...props}/>} />
        <Route path={"/"+ shippingNotFound.url} render={props => <ShippingNotFound {...props}/>} />
        <Route path={"/"+ quoteShipping.url} render={props => <QuoteShipping {...props}/>} />
        <Route path={"/"+ queryTimes.url} render={props => <QueryTimes {...props}/>} />
        <Route
          path={'/' + landing.url}
          render={props => <LandingPage {...props} />}
        />
        <Route path={"/"+ createWarehouse.url} render={props => <CreateWarehouse {...props} />} />
        <Redirect to={'/' + landing.url} />
        <Redirect from="/" to={'/' + landing.url} />
      </Switch>
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);
