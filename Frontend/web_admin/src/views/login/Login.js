import React from "react";

// reactstrap components
import {
  Button,
  Card,
  Form,
  FormGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Col,
  Row,
  Modal
} from "reactstrap";

// core components
import NavbarRedex from "components/Navbars/NavbarRedex.js";
import {apiURL, currentUser, landingConstrunetAdmision} from "variables/Variables.js";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from "axios";
//google login api
import { GoogleLogin } from 'react-google-login';



class Login extends React.Component {
  constructor(props) {
    super(props);
    this.handlerUsername = this.handlerUsername.bind(this);
    this.handlerPassword = this.handlerPassword.bind(this);
    this.onClickLogin = this.onClickLogin.bind(this);
    this.handlerForgotPasswordOpen = this.handlerForgotPasswordOpen.bind(this);
    this.handlerForgotPasswordClose = this.handlerForgotPasswordClose.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.changeInputTypeText = this.changeInputTypeText.bind(this);
    this.changeInputTypePassword = this.changeInputTypePassword.bind(this);
    this.handlerIsLoggedOpen = this.handlerIsLoggedOpen.bind(this);
    this.handlerIsLoggedClose = this.handlerIsLoggedClose.bind(this);
    this.state = {
        user:'',
        password:'',
        modalIsOpen:false,
        modalSessionIsOpen:false,
        inputType:'password'
    };
    
}
handlerIsLoggedClose(){
  this.setState({
    modalSessionIsOpen:false
  })
}
handlerIsLoggedOpen(){
  this.setState({
    modalSessionIsOpen:true
  })
}
changeInputTypePassword(){
  this.setState({
    inputType:'password'
  })
}
changeInputTypeText(){
  this.setState({
    inputType:'text'
  })
}
componentWillUnmount(){
  console.log("login")
  console.log(this.props)
}
handlerForgotPasswordClose(){
  this.setState({
    modalIsOpen:false
  })
}
handlerForgotPasswordOpen(){
  this.setState({
    modalIsOpen:true
  })
}
handlerPassword(e){
  this.setState({
    password:e.target.value
  })
}
handlerUsername(e){
  this.setState({
    user:e.target.value
  })
}
onClickLogin(){
  this.props.history.push("/client-list")
  /* console.log(this.props.location.pathname)
  if(this.state.user=='' && this.state.password==''){
    toast.error('Debe ingresar sus credenciales', { type: toast.TYPE.ERROR, autoClose: 5000 });
  }
  else{
    if(this.state.user==''){
      toast.error('Debe ingresar su usuario', { type: toast.TYPE.ERROR, autoClose: 5000 });
    }
    else{
      if(this.state.password==''){
        toast.error('Debe ingresar su constraseña', { type: toast.TYPE.ERROR, autoClose: 5000 });
      }
      else{
        var aux={
          "usuarioNT": this.state.user,
          "password": this.state.password
        }
        axios.post(apiURL.value + 'login', aux).then (res => {
          if(res.data==0){
            //usuario no encontrado
            toast.error('Ingrese su usuario correctamente', { type: toast.TYPE.ERROR, autoClose: 5000 });
          }
          else if(res.data==2){
            //contraseña incorrecta
            toast.error('Ingrese su contraseña correctamente', { type: toast.TYPE.ERROR, autoClose: 5000 });
          }
          else if(res.data==1){
            //creddenciales correctos
            currentUser.logged=true;
            this.props.history.push("/admision")
          }
        })
      }
    }
  } */
}
  handleKeyPress(event){
    if(event.key === 'Enter'){
      if(this.state.user==='' && this.state.password===''){
        toast.error('Debe ingresar sus credenciales', { type: toast.TYPE.ERROR, autoClose: 5000 });
      }
      else{
        if(this.state.user===''){
          toast.error('Debe ingresar su usuario', { type: toast.TYPE.ERROR, autoClose: 5000 });
        }
        else{
          if(this.state.password===''){
            toast.error('Debe ingresar su constraseña', { type: toast.TYPE.ERROR, autoClose: 5000 });
          }
          else{
            var aux={
              "usuarioNT": this.state.user,
              "password": this.state.password
            }
            axios.post(apiURL.value + 'login', aux).then (res => {
              if(res.data===0){
                //usuario no encontrado
                toast.error('Ingrese su usuario correctamente', { type: toast.TYPE.ERROR, autoClose: 5000 });
              }
              else if(res.data===2){
                //contraseña incorrecta
                toast.error('Ingrese su contraseña correctamente', { type: toast.TYPE.ERROR, autoClose: 5000 });
              }
              else if(res.data===1){
                //credenciales correctos
                axios.get(apiURL.value + 'user/' + this.state.user).then (resp => {
                  console.log("usuario:")
                  console.log(resp)
                  currentUser.logged = true;
                  currentUser.names = resp.nombres
                  currentUser.surname = resp.apellidoPaterno
                  currentUser.grade = resp.grade    
                },() =>{
                  this.props.history.push("/"+landingConstrunetAdmision.url)
                })

                
                
              }
              else if(res.data===3){
                //ya se cuenta con una sesión abierta para ese usuario
                this.setState({
                  modalSessionIsOpen:true
                })
              }
            })
          }
        }
      }
    }
  }
  
render(){
  const responseGoogle = (response) => {
    console.log(response);
    if(response){
      console.log("entro al if")
      currentUser.names = response.profileObj.givenName;
      currentUser.surname = response.profileObj.familyName;
      currentUser.img = response.profileObj.imageUrl;
      if(response.profileObj.email.indexOf('pucp')>=0){
        currentUser.role = 'Administrador'//es administrador
      }
      else{
        currentUser.role = 'Recepcionista'//es recepcionista
      }
      this.props.history.push("/dashboard")
    }
  }
  return (
    <>
    
      <div className="page-header clear-filter" filter-color="blue">
      <ToastContainer />
      
        <div
          className="page-header-image"
          style={{
            backgroundImage: "url(" + require("assets/img/login2.jpg") + ")"
          }}
        ></div>
        <NavbarRedex title={'FastEx'}/>
        <div className="content" style={{marginTop:'-2%'}}>
          <Container>
            
            <Col className="ml-auto mr-auto" md="4">
              
                <Form action="" className="form" method="">
                  <Card style={{backgroundColor:'#C4CBD1', marginTop:'20%'}}>
                    <h3 className="center" style={{fontFamily:'Lato', color:'#000000',
                      marginTop:'10%'}}><b>Login</b></h3>
                  <Form role="form" onKeyPress={this.handleKeyPress}>
                    <Container>
                    <FormGroup className="mb-3">
                      <InputGroup className="no-border input-lg">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText /* style={{padding:'0% 75% 0% 35%'}} */>
                            <i className="now-ui-icons users_circle-08"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input /* style={{backgroundColor:'#FFFFFF'}} */
                        placeholder="Usuario" /* type="email" */ onChange={this.handlerUsername} />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <InputGroup className="no-border input-lg">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="now-ui-icons ui-1_lock-circle-open"
                            style={{marginLeft:0}}></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          placeholder="Constraseña"
                          type={this.state.inputType}
                          autoComplete="off"
                          onChange={this.handlerPassword}
                        />
                        <InputGroupAddon addonType="append" onMouseUp={this.changeInputTypePassword}
                        onMouseOut={this.changeInputTypePassword}>
                          <InputGroupText style={{padding:'0% 25% 0% 25%'}} 
                          onMouseUp={this.changeInputTypePassword}>
                            <i class="fas fa-eye" onMouseDown={this.changeInputTypeText}
                            onMouseUp={this.changeInputTypePassword}></i>
                          </InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </FormGroup>
                    <Container>
                    <Row>
                    <Col sm="12" md={{ size: 10, offset: 1 }}>
                    <Button
                      
                      className="btn-fill"
                      style={{backgroundColor:'#1768AC'}}                
                      onClick={this.onClickLogin}
                      size="lg"
                    >
                      Iniciar Sesión
                    </Button>
                    </Col>
                    </Row>
                    <Row>
                    <Col sm="12" md={{ size: 10, offset: 1 }}>
                    <GoogleLogin
                      clientId="555423287433-a21f4roj3rpa20u43rdltvac7sp3uj5u.apps.googleusercontent.com"
                      buttonText="Google"
                      onSuccess={responseGoogle}
                      onFailure={responseGoogle}
                      cookiePolicy={'single_host_origin'}
                    />
                    </Col>
                    </Row>
                    </Container>
                    <div className="center">
                     <u> 
                        <a
                          className="link"
                          href="#pablo"
                          onClick={this.handlerForgotPasswordOpen}
                        >
                          ¿Olvidó su contraseña?
                        </a>
                      </u>
                    </div>
                    </Container>
                  </Form>
                  </Card>
                </Form>
                
             
              <Modal toggle={this.handlerForgotPasswordClose} isOpen={this.state.modalIsOpen}>
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLiveLabel">
                    Recuperar Contraseña
                  </h5>
                  <button
                    aria-label="Close"
                    className="close"
                    type="button"
                    onClick={this.handlerForgotPasswordClose}
                  >
                    <span aria-hidden={true}>×</span>
                  </button>
                </div>
                <div className="modal-body">
                  <p>Ingrese su usuario</p>
                </div>
                <div className="modal-footer">
                  <Button
                    color="secondary"
                    type="button"
                    onClick={this.handlerForgotPasswordClose}
                  >
                    Close
                  </Button>
                  <Button
                    color="primary"
                    type="button"
                    onClick={this.handlerForgotPasswordClose}
                  >
                    Save changes
                  </Button>
                </div>
              </Modal>
              <Modal toggle={this.handlerIsLoggedClose} isOpen={this.state.modalSessionIsOpen}>
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLiveLabel">
                    Error de inicio de sesión
                  </h5>
                  <button
                    aria-label="Close"
                    className="close"
                    type="button"
                    onClick={this.handlerIsLoggedClose}
                  >
                    <span aria-hidden={true}>×</span>
                  </button>
                </div>
                <div className="modal-body">
                  <p>El usuario {} ya cuenta con una sesión iniciada. Comuníquese con el administrador para desbloquear su usuario.</p>
                </div>
                <div className="modal-footer">
                  <Button
                    color="secondary"
                    type="button"
                    onClick={this.handlerForgotPasswordClose}
                  >
                    Cancelar
                  </Button>
                  <Button
                    color="primary"
                    type="button"
                    onClick={this.handlerForgotPasswordClose}
                  >
                    Aceptar
                  </Button>
                </div>
              </Modal>
            </Col>
          </Container>
        </div>
      </div>
    </>
  );
  }
}


export default Login;
