package com.restful.service;

import com.restful.entity.ProgrammedFlight;
import com.restful.model.ProgrammedFlightModel;
import com.restful.repository.ProgrammedFlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("programmedFlightService")
public class ProgrammedFlightService {
    @Autowired
    @Qualifier("programmedFlightRepository")
    private ProgrammedFlightRepository programmedFlightRepository;

    public Map<String, List<ProgrammedFlightModel>> getProgrammedFlights(){
        Date now = new Date();
        List<ProgrammedFlight> l = programmedFlightRepository.findByStartDateGreaterThanEqual(now);
        List<ProgrammedFlightModel> lModel = new ArrayList<>();
        for(ProgrammedFlight programmedFlight: l){
            ProgrammedFlightModel x = new ProgrammedFlightModel(programmedFlight);
            lModel.add(x);
        }
        Map<String, List<ProgrammedFlightModel>> response = new HashMap<>();
        response.put("programmedFlights", lModel);
        return response;
    }
}
