package com.restful.service;

import com.restful.entity.Flight;
import com.restful.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("flightService")
public class FlightService {
    @Autowired
    @Qualifier("flightRepository")
    private FlightRepository flightRepository;

    public Map<String, Integer> adjustFlights(){
        Map<String, Integer> response = new HashMap<>();
        List<Flight> l = flightRepository.findAll();
        int high_time = 0;
        int low_time = 2880;
        for(Flight flight: l){
            String departure_time = flight.getDepartureTime();
            String landing_time = flight.getActualLandingTime();
            int start_time = Integer.parseInt(departure_time.substring(0,2)) * 60 + Integer.parseInt(departure_time.substring(3, 5));
            int end_time = Integer.parseInt(landing_time.substring(0,2)) * 60 + Integer.parseInt(landing_time.substring(3, 5));
            int total_time = 0;
            if(start_time > end_time) total_time = 1440 - start_time + end_time;
            else total_time = end_time - total_time;

            total_time = (9 * total_time - (482*10 - 1*1439)) / (1439 - 482);
            if(total_time == 0) total_time ++;

            end_time = (start_time + total_time) % 1440;
            Integer h = end_time / 60;
            Integer m = end_time % 60;
            String hh = "", mm = "";
            if(h < 10) hh += "0";
            hh += h.toString();
            if(m < 10) mm += "0";
            mm += m.toString();
            String new_end_time = hh + ":" + mm;
            flight.setLandingTime(new_end_time);
            flightRepository.save(flight);
            high_time = Integer.max(total_time, high_time);
            low_time = Integer.min(total_time, low_time);
        }

        response.put("max", high_time);
        response.put("min", low_time);
        return response;
    }
}
