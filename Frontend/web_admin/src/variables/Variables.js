//URL de servicios
const apiURL = {value:'http://54.146.134.16:9993/'};
//URL de cada componente
const login = {url:'login'}
const clientList = {url: 'client-list'}
const createClient = {url:'create-client'}
const shippingList = {url: 'shipping-list'}
const shippingListAdmin = {url: 'shippings'}
const createShipping = {url:'create-shipping'}
const shippingUpload = {url:'shipping-upload'}
const warehouseList = {url: 'warehouse-list'}
const createWarehouse = {url:'create-warehouse'}
const editWarehouse = {url:'edit-warehouse'}
const employeeList = {url: 'employee-list'}
const warehouse = {value: null}
const createEmployee = {url:'create-employee'}
const systemGraphics = {url: 'simulation'}
const systemVariables = {url: 'system-variables'}
const editedWarehouse = {value:0}
const deliverShipping = {url: 'deliver-shipping'}
const shippingDetail = {url: 'shipping-detail'}
const dashboard = {url: 'dashboard'}
const flights = {url:'flights-list'}
const warehouseMap = {url:'warehouse-map'}

const currentUser = {names:'', surname:'', grade:'', img: null, logged:false, role:''}
const shipping = {value:null}
const originWarehouse = {id:44,name:'Lima - Perú',icao:'SPIM'}

module.exports ={apiURL, currentUser, login, createClient, clientList, createShipping, 
                 shippingList, shippingUpload, warehouseList, createWarehouse, editWarehouse,
                 employeeList, createEmployee,systemVariables, systemGraphics,
                 warehouse, editedWarehouse, shippingListAdmin,shipping, deliverShipping,
                 shippingDetail,originWarehouse, flights, warehouseMap,dashboard
}