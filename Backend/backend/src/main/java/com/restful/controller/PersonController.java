package com.restful.controller;

import com.restful.model.CustomerModel;
import com.restful.model.PersonModel;
import com.restful.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/person")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
public class PersonController {
    @Autowired
    @Qualifier("personService")
    private PersonService personService;

    @CrossOrigin
    @PostMapping("/getByDocument")
    public @ResponseBody List<PersonModel> getPersonByDocument(@RequestBody Map<String, String> data){
        try {
            return personService.getPersonByData(Long.parseLong(data.get("idCountry")), Long.parseLong(data.get("idDocumentType")), data.get("document"));
        }catch(Exception e){
            e.printStackTrace();
        }
        return new ArrayList<PersonModel>();
    }
}
