package com.restful.repository;

import com.restful.entity.CustomerStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("customerStatusRepository")
public interface CustomerStatusRepository extends JpaRepository<CustomerStatus, Integer> {
    public abstract List<CustomerStatus> findAll();
    public abstract List<CustomerStatus> findById(Long id);
}
