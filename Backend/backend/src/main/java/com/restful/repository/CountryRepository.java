package com.restful.repository;

import com.restful.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("countryRepository")
public interface CountryRepository extends JpaRepository<Country, Integer> {
    public abstract List<Country> findAll();
    public abstract List<Country> findById(Long id);
}
