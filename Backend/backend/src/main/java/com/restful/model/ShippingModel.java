package com.restful.model;

import com.restful.entity.Customer;
import com.restful.entity.Receiver;
import com.restful.entity.Shipping;
import com.restful.entity.ShippingStatus;

import javax.persistence.*;
import java.util.Date;

public class ShippingModel {
    private Long id;
    private String trackingCode;
    private String description;
    private Float price;
    private byte[] receiptDocument;
    private Date shippingDate;
    private Date receiptDate;
    private Date registrationDate;
    private CustomerModel customer;
    private ReceiverModel receiver;
    private ShippingStatusModel shippingStatus;

    public ShippingModel(Long id, String trackingCode, String description, Float price, byte[] receiptDocument, Date shippingDate, Date receiptDate, Date registrationDate) {
        this.id = id;
        this.trackingCode = trackingCode;
        this.description = description;
        this.price = price;
        this.receiptDocument = receiptDocument;
        this.shippingDate = shippingDate;
        this.receiptDate = receiptDate;
        this.registrationDate = registrationDate;
    }

    public ShippingModel(Shipping a){
        this.id = a.getId();
        this.trackingCode = a.getTrackingCode();
        this.description = a.getDescription();
        this.price = a.getPrice();
        this.receiptDate = a.getReceiptDate();
        this.shippingDate = a.getShippingDate();
        this.receiptDate = a.getReceiptDate();
        this.registrationDate = a.getRegistrationDate();
        this.customer = new CustomerModel(a.getCustomer());
        this.receiver = new ReceiverModel(a.getReceiver());
        this.shippingStatus = new ShippingStatusModel(a.getShippingStatus());
    }

    public ShippingModel() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTrackingCode() {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        this.trackingCode = trackingCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public byte[] getReceiptDocument() {
        return receiptDocument;
    }

    public void setReceiptDocument(byte[] receiptDocument) {
        this.receiptDocument = receiptDocument;
    }

    public Date getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(Date shippingDate) {
        this.shippingDate = shippingDate;
    }

    public Date getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(Date receiptDate) {
        this.receiptDate = receiptDate;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public CustomerModel getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerModel customer) {
        this.customer = customer;
    }

    public ReceiverModel getReceiver() {
        return receiver;
    }

    public void setReceiver(ReceiverModel receiver) {
        this.receiver = receiver;
    }

    public ShippingStatusModel getShippingStatus() {
        return shippingStatus;
    }

    public void setShippingStatus(ShippingStatusModel shippingStatus) {
        this.shippingStatus = shippingStatus;
    }
}
