package com.restful.service;

import com.restful.entity.Package;
import com.restful.repository.PackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("packageService")
public class PackageService {
    @Autowired
    @Qualifier("packageRepository")
    private PackageRepository packageRepository;

    public boolean registerPackages(List<Package> packages){
        for(Package pack: packages){
            packageRepository.save(pack);
        }
        return true;
    }
}
