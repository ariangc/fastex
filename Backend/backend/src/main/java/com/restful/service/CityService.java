package com.restful.service;

import com.restful.entity.City;
import com.restful.entity.Country;
import com.restful.model.CityModel;
import com.restful.repository.CityRepository;
import com.restful.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("cityService")
public class CityService {
    @Autowired
    @Qualifier("cityRepository")
    private CityRepository cityRepository;

    @Autowired
    @Qualifier("countryRepository")
    private CountryRepository countryRepository;

    public List<CityModel> listCities(){
        List<City> l = cityRepository.findAll();
        List<CityModel> lModel = new ArrayList<CityModel>();
        for(City city: l){
            CityModel x = new CityModel(city);
            lModel.add(x);
        }
        return lModel;
    }

    public List<CityModel> getCitiesByCountry(Long idCountry){
        List<Country> lCountry = countryRepository.findById(idCountry);
        List<City> l = cityRepository.findByCountry(lCountry.get(0));
        List<CityModel> lModel = new ArrayList<CityModel>();
        for(City city: l){
            CityModel x = new CityModel(city);
            lModel.add(x);
        }
        return lModel;
    }
}
