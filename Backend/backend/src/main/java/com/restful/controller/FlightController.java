package com.restful.controller;

import com.restful.service.FlightService;
import com.restful.service.ProgrammedFlightService;
import com.restful.model.ProgrammedFlightModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.List;

@RestController
@RequestMapping("/flight")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
public class FlightController {
    @Autowired
    @Qualifier("flightService")
    private FlightService flightService;
	 
	 @Autowired
	 @Qualifier("programmedFlightService")
	 private ProgrammedFlightService programmedFlightService;

    @CrossOrigin
    @PostMapping("/bounds")
    public @ResponseBody Map<String, Integer> adjustFlights(){
        return flightService.adjustFlights();
    }

	 @CrossOrigin
	 @PostMapping("/getAll")
	 public @ResponseBody Map<String, List<ProgrammedFlightModel>> getAll(){
		 return programmedFlightService.getProgrammedFlights();
	 }
}
