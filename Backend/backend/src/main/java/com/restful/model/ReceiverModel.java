package com.restful.model;


import com.restful.entity.Person;
import com.restful.entity.Receiver;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class ReceiverModel {
    private Long id;
    private String mail;
    private String phone;
    private String phonePrefix;
    private PersonModel person;

    public ReceiverModel(Long id, String mail, String phone, String phonePrefix) {
        this.id = id;
        this.mail = mail;
        this.phone = phone;
        this.phonePrefix = phonePrefix;
    }

    public ReceiverModel(Receiver a){
        this.id = a.getId();
        this.mail = a.getMail();
        this.phone = a.getPhone();
        this.phonePrefix = a.getPhonePrefix();
        this.person = new PersonModel(a.getPerson());
    }

    public ReceiverModel() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public PersonModel getPerson() {
        return person;
    }

    public void setPerson(PersonModel person) {
        this.person = person;
    }
}
