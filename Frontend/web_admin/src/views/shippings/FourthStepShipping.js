import React from "react";

// reactstrap components
import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
} from "reactstrap";
import IconButton from '@material-ui/core/IconButton';
import Search from '@material-ui/icons/Search';
import {apiURL,originWarehouse} from "variables/Variables.js"
// core components
import SideBar from 'components/sidebar/SideBar.js';
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import Loader from 'react-loader-spinner'
import MaterialTable from "material-table";
import 'react-toastify/dist/ReactToastify.css';
import Datetime from 'react-datetime';
import './Shipping.css';

const columns = [
  {
    title: 'Origen',
    field: 'origen'
  },
  {
    title: 'Destino',
    field: 'destino'
  },
  {
    title: 'F.Salida',
    field: 'fechaSalida'
  }, 
  {
    title: 'F.Llegada',
    field: 'fechaLlegada'
  }, 
  {
    title: 'Vuelo',
    field: 'vuelo'
  }
  ];

class FourthStepShipping extends React.Component {
  constructor(props) {
    super(props);
    this.appendLeadingZeroes = this.appendLeadingZeroes.bind(this)
    this.substractDates = this.substractDates.bind(this)

    this.state = {
      flights:[],
      selectedRow:null,
      arrivalDate:"",
      price:0,
      route:null,
      loaderActive:true
    }
}

substractDates(hourstart, minstart, hourend, minend){
  console.log('funcion', hourstart, minstart, hourend, minend)
  var startMin = parseInt(hourstart)*60+parseInt(minstart)
  var endMin = parseInt(hourend)*60+parseInt(minend)
  if(endMin==startMin){
    return 0;
  }
  else if(endMin<startMin){
    //es el dia siguiente
    return (24*60-startMin)+endMin
  }
  else if(endMin>startMin){
    //es el mismo dia
    return endMin-startMin
  }
}
appendLeadingZeroes(n){
  if(n <= 9){
    return "0" + n;
  }
  return n
}


componentWillMount(){
  var body = {idStartCity:originWarehouse.id,idEndCity:this.props.cityAddressee.id}
  axios.post(apiURL.value + 'algorithm/run',body).then((res)=>{
    console.log(res.data);
    if(res.status === 200){
      for (var key in res.data) {
        if(key == "enderror"){
          toast.error('El almacén de destino se encuentra indisponible', { type: toast.TYPE.ERROR, autoClose: 5000 })
          this.setState({loaderActive:false})
        }else if(key == "firstfull"){
          toast.error('El almacén de origen se encuentra indisponible', { type: toast.TYPE.ERROR, autoClose: 5000 })
          this.setState({loaderActive:false})
        }else{
          var flightsAux=[]
          
          res.data.route.forEach(flight => {
            var date = new Date(flight.startDate)
            var minutes = this.substractDates(flight.flight.departureTime.slice(0,2), flight.flight.departureTime.slice(3,5), flight.flight.landingTime.slice(0,2), flight.flight.landingTime.slice(3,5))
            var end = Datetime.moment(date).add(minutes, 'm').toDate()
            var dictFlight = {origen:flight.flight.startCity.name,destino:flight.flight.endCity.name,
              fechaSalida:this.appendLeadingZeroes(this.appendLeadingZeroes(date.getUTCDate()) + '/' + this.appendLeadingZeroes(date.getUTCMonth() + 1) + "/" + date.getUTCFullYear() + " " + date.getUTCHours()) + ":" + this.appendLeadingZeroes(date.getUTCMinutes()),
              fechaLlegada:this.appendLeadingZeroes(this.appendLeadingZeroes(end.getUTCDate()) + '/' + this.appendLeadingZeroes(end.getUTCMonth() + 1) + "/" + end.getUTCFullYear() + " " + end.getUTCHours()) + ":" + this.appendLeadingZeroes(end.getUTCMinutes()),
              vuelo:"PK" + flight.flight.id.toString()}
              flightsAux.push(dictFlight)
          });
          console.log(res.data.route[res.data.route.length-1])
          this.setState({loaderActive:false,route:res.data.route,flights:flightsAux,
            arrivalDate:res.data.route[res.data.route.length-1].startDate.toString().slice(0,10)},() =>{
              this.props.routeFunction(this.state.route)
          })
        }
      }
    }
  })
}

componentDidMount(){
  var body = {idStartCity:parseInt(originWarehouse.id),idEndCity:parseInt(this.props.cityAddressee.id)}
  axios.post(apiURL.value + 'shipping/quote',body).then((res)=>{
    console.log(res.data);
    if(res.status === 200){
      this.setState({price:(res.data.price/100).toFixed(2)},() => {
        this.props.priceFunction(this.state.price)
      })
    }
  })
}

render(){
  return (
    <>
    <div className="App">
          <Col>
            <ToastContainer /> 
            <Form>
              <Row>
                  <h3 className="stepTitle">Ruta de Envío</h3>
              </Row>
              <Row>
                <Col sm={{ size: 3}}>
                  <FormGroup>
                    <label htmlFor="exampleFormControlSelect1">Origen</label>
                    <Input id="originCity" type="text" value={originWarehouse.name}readonly="readonly"></Input>  
                  </FormGroup>
                </Col>
                <Col sm={{ size: 3}}>
                <FormGroup>
                    <label htmlFor="exampleFormControlSelect1">Destino</label>
                    <Input id="destinityCity" type="text" value={this.props.cityAddressee.name+'-'+this.props.cityAddressee.country.name} readonly="readonly"></Input>  
                  </FormGroup>
                </Col>
                <Col sm={{ size: 3}}>
                  <FormGroup>
                    <label htmlFor="exampleFormControlSelect1">Fecha de llegada</label>
                    <Input id="arriveDate" type="text" value={this.state.arrivalDate} readonly="readonly"></Input>  
                  </FormGroup>
                </Col>
                <Col sm={{ size: 3}}>
                <FormGroup>
                    <label htmlFor="exampleFormControlSelect1">Costo</label>
                    <Input id="cost" type="text" value={"$ " + this.state.price} readonly="readonly"></Input>  
                  </FormGroup>
                </Col>
              </Row>
              {this.state.loaderActive?
                <div className='text-center'  style={{width:'100%', weight:'100%', backgroundColor:'#FFFFFF'}}>
                  <Loader
                    type="ThreeDots"
                    color="#03256C"
                    height={100}
                    width={100}
                    timeout={3000000}
                  />
                </div>:<MaterialTable
                title="Hoja de Ruta"
                data={this.state.flights}
                columns={columns}
                onRowClick={(evt, selectedRow) => {
                  (evt.target).ondblclick = () =>{this.setState({selectedRow}, ()=>{console.log(this.state.selectedRow)})
                  }
                  }}
                options={{actionsColumnIndex: -1,filtering:true,
                  rowStyle: rowData => ({
                    backgroundColor: (this.state.selectedRow && this.state.selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
                  })
                }}
                localization={{
                  toolbar: {
                    searchPlaceholder: "Buscar"
                  },
                  body: {
                    emptyDataSourceMessage: 'No hay datos'
                  }
                }}
              />}
            </Form>
          </Col>
                              
  
    </div>       
    </>
  );
  }
}

export default FourthStepShipping;
