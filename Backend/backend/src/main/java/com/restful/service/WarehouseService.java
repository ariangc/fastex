package com.restful.service;

import com.restful.entity.*;
import com.restful.model.*;
import com.restful.repository.*;
import org.apache.catalina.webresources.JarResourceRoot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import com.restful.util.*;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service("warehouseService")
public class WarehouseService {
    @Autowired
    @Qualifier("warehouseRepository")
    private WarehouseRepository warehouseRepository;

    @Autowired
    @Qualifier("warehouseStatusRepository")
    private WarehouseStatusRepository warehouseStatusRepository;

    @Autowired
    @Qualifier("cityRepository")
    private CityRepository cityRepository;

    @Autowired
    @Qualifier("flightRepository")
    private FlightRepository flightRepository;

    @Autowired
    @Qualifier("warehouseHistoryRepository")
    private WarehouseHistoryRepository warehouseHistoryRepository;

    public WarehouseService(){}

    public List<WarehouseModel> getWarehouses(){
        List<Warehouse> l = warehouseRepository.findAll();
        List<WarehouseModel> lModel = new ArrayList<WarehouseModel>();
        for(Warehouse warehouse: l){
            WarehouseModel x = new WarehouseModel(warehouse);
		  		System.out.println(x.getId());
				lModel.add(x);
        }
        return lModel;
    }

    public Map<String, List<WarehouseModel>> getWarehousesByCity(Long idCity){
        Optional<City> lCity = cityRepository.findById(idCity);
        List<Warehouse> l = warehouseRepository.findByCity(lCity.get());
        List<WarehouseModel> lModel = new ArrayList<WarehouseModel>();
        for(Warehouse warehouse: l){
            WarehouseModel x = new WarehouseModel(warehouse);
            if(x.getWarehouseStatus().getId() != 1) lModel.add(x);
        }
        Map<String, List<WarehouseModel>> response = new HashMap<>();
        response.put("warehouse", lModel);
        return response;
    }

    public boolean createWarehouse(Warehouse warehouse){
        warehouseRepository.save(warehouse);
        return true;
    }

    public boolean updateWarehouse(Warehouse warehouse){
        if(warehouseRepository.existsById(warehouse.getId())) {
            warehouseRepository.save(warehouse);
            return true;
        }
        else return false;
    }

    public boolean deleteWarehouse(Warehouse warehouse) {
        Optional<WarehouseStatus> status = warehouseStatusRepository.findById(Long.parseLong("2"));
        warehouse.setWarehouseStatus(status.get());
        warehouseRepository.save(warehouse);
        return true;
    }

    public boolean processFilesSave(String icaoCode) throws IOException, ParseException {
        try {
            //Reading all pathnames
            String[] pathnames;
            File f = new File("./tempFiles/packs/");
            pathnames = f.list();

            //Processing all files
            for (String pathname : pathnames) {
                if(pathname.charAt(pathname.length() - 1) == 'i') continue;
                String icaoOrig = pathname.substring(pathname.length() - 8, pathname.length() - 4);

                List<DayCharge> l = new ArrayList<>();

                String row = new String();
                BufferedReader csvReader = new BufferedReader((new FileReader("./tempFiles/packs/" + pathname)));

                List<String> events = new ArrayList<>();


                Optional<City> startCity = cityRepository.findByAirportCode(icaoOrig);

					 if(!icaoOrig.equals(icaoCode)) continue;

                List<String> allDates = new ArrayList<>();

                while ((row = csvReader.readLine()) != null) {
                    String[] data = row.split("-");

                    String packCode = data[0];
                    String date = data[1];
                    allDates.add(date);
                    String arrivalTime = data[2];
                    String icaoDest = data[3];

                    String timeStamp = date + arrivalTime.substring(0,2) + arrivalTime.substring(3,5) + icaoDest;
                    events.add(timeStamp);
                }
                Collections.sort(events);
                Collections.sort(allDates);
                List<Pair<String, Integer>> ivls = new ArrayList<>();

                Map<String, Integer> totalGood = new HashMap<>();
                Map<String, Integer> totalBad = new HashMap<>();
                Map<Integer, String> idxToDate = new HashMap<>();

                //202006102302SPIMLDZA
                Integer idx = 1;
                for(String event: events){

                    String date = event.substring(0,8);
                    String arrivalTime = event.substring(8,12);
                    String icaoDest = event.substring(12,16);
                    Optional<City> endCity = cityRepository.findByAirportCode(icaoDest);

                    List<Flight> flightList = flightRepository.findByStartCityAndEndCity(startCity.get(), endCity.get());

                    Integer arrivalInt = Integer.parseInt(arrivalTime.substring(0,2)) * 60 + Integer.parseInt(arrivalTime.substring(2,4));

                    Integer minWait = 10000000;

                    for(Flight flight: flightList){
                        String departure = flight.getDepartureTime();
                        departure = departure.substring(0,2) + departure.substring(3,5);
                        Integer departureInt = Integer.parseInt(departure.substring(0,2)) * 60 + Integer.parseInt(departure.substring(2,4));

                        Integer totalWait = 0;

                        if(arrivalInt > departureInt) totalWait = 1440 - (arrivalInt - departureInt);
                        else totalWait = departureInt - arrivalInt;

                        minWait = Integer.min(minWait, totalWait);
                    }
                    Integer departureInt;

                    if(arrivalInt + minWait >= 1440){
                        departureInt = 1439;
                    }
                    else departureInt = arrivalInt + minWait;

                    Integer h = (arrivalInt / 60);
                    Integer m = (arrivalInt % 60);

                    String f1 = date;
                    if(h < 10) f1 = f1.concat("0");
                    f1 = f1.concat(h.toString());
                    if(m < 10) f1 = f1.concat("0");
                    f1 = f1.concat(m.toString());

                    h = (departureInt / 60);
                    m = (departureInt % 60);

                    String f2 = date;
                    if(h < 10) f2 = f2.concat("0");
                    f2 = f2.concat(h.toString());
                    if(m < 10) f2 = f2.concat(m.toString());
                    f2 = f2.concat(m.toString());

                    assert f1.compareTo(f2) <= 0;

                    ivls.add(Pair.of(f1, idx));
                    ivls.add(Pair.of(f2, -idx));
                    idxToDate.put(idx, date);
                    idx++;
                }


                Warehouse warehouse = warehouseRepository.findByCity(startCity.get()).get(0);

                Integer totalCapacity = warehouse.getTotalCapacity();

                ivls.sort(Comparator.comparing(Pair<String, Integer>::getFirst).thenComparing(Pair<String, Integer>::getSecond));


                Integer currentCapacity = 0;


                for(Pair<String,Integer> par: ivls ){
                    Integer index = par.getSecond();
                    if(par.getSecond() < 0) currentCapacity = Integer.max(currentCapacity - 1, 0);
                    else{
                        String date = idxToDate.get(index);
                        if(currentCapacity.equals(totalCapacity)) {
                            Date curDate = new SimpleDateFormat("yyyyMMdd hhmm").parse(date + " " + par.getFirst().substring(8,12));
                            warehouse.setTotalBad(warehouse.getTotalBad() + 1);
                            warehouseRepository.save(warehouse);
                            WarehouseHistory warehouseHistory = new WarehouseHistory(null, currentCapacity, currentCapacity, curDate, false, warehouse);
                            warehouseHistoryRepository.save(warehouseHistory);
                        }
                        else{
                            currentCapacity ++;
                            Date curDate = new SimpleDateFormat("yyyyMMdd hhmm").parse(date + " " + par.getFirst().substring(8,12));
                            WarehouseHistory warehouseHistory = new WarehouseHistory(null, currentCapacity - 1, currentCapacity, curDate, true, warehouse);
                            warehouseHistoryRepository.save(warehouseHistory);
                        }
                    }
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }


    public Map<String, List<DayCharge>> processFiles(String icaoCode) throws IOException, ParseException {
        Map<String, List<DayCharge>> response = new HashMap<>();
		  System.out.println("Entering with " + icaoCode);
        try {
            //Reading all pathnames
            String[] pathnames;
            File f = new File("./tempFiles/packs/");
            pathnames = f.list();

            //Processing all files
            for (String pathname : pathnames) {
                if(pathname.charAt(pathname.length() - 1) == 'i') continue;
                String icaoOrig = pathname.substring(pathname.length() - 8, pathname.length() - 4);
					 System.out.println(icaoOrig + " " + icaoCode);
                
					 if(!icaoCode.equals(icaoOrig)) continue;

					 System.out.println("Processing " + icaoOrig);
                List<DayCharge> l = new ArrayList<>();

                String row = new String();
                BufferedReader csvReader = new BufferedReader((new FileReader("./tempFiles/packs/" + pathname)));

                List<String> events = new ArrayList<>();


                Optional<City> startCity = cityRepository.findByAirportCode(icaoOrig);
					 
                System.out.println("Processing airport: " + icaoOrig);

                List<String> allDates = new ArrayList<>();

                while ((row = csvReader.readLine()) != null) {
                    String[] data = row.split("-");

                    String packCode = data[0];
                    String date = data[1];
                    allDates.add(date);
                    String arrivalTime = data[2];
                    String icaoDest = data[3];

                    String timeStamp = date + arrivalTime.substring(0,2) + arrivalTime.substring(3,5) + icaoDest;
                    events.add(timeStamp);
                }
					 System.out.println("Finished adding events");
                Collections.sort(events);
                Collections.sort(allDates);
                List<Pair<String, Integer>> ivls = new ArrayList<>();

                Map<String, Integer> totalGood = new HashMap<>();
                Map<String, Integer> totalBad = new HashMap<>();
                Map<Integer, String> idxToDate = new HashMap<>();

                //202006102302SPIMLDZA
                Integer idx = 1;
                for(String event: events){

                    String date = event.substring(0,8);
                    String arrivalTime = event.substring(8,12);
                    String icaoDest = event.substring(12,16);
                    Optional<City> endCity = cityRepository.findByAirportCode(icaoDest);

                    List<Flight> flightList = flightRepository.findByStartCityAndEndCity(startCity.get(), endCity.get());

                    Integer arrivalInt = Integer.parseInt(arrivalTime.substring(0,2)) * 60 + Integer.parseInt(arrivalTime.substring(2,4));

                    Integer minWait = 10000000;

                    for(Flight flight: flightList){
                        String departure = flight.getDepartureTime();
                        departure = departure.substring(0,2) + departure.substring(3,5);
                        Integer departureInt = Integer.parseInt(departure.substring(0,2)) * 60 + Integer.parseInt(departure.substring(2,4));

                        Integer totalWait = 0;

                        if(arrivalInt > departureInt) totalWait = 1440 - (arrivalInt - departureInt);
                        else totalWait = departureInt - arrivalInt;

                        minWait = Integer.min(minWait, totalWait);
                    }
                    Integer departureInt;

                    if(arrivalInt + minWait >= 1440){
                        departureInt = 1440;
                    }
                    else departureInt = arrivalInt + minWait;

                    Integer h = (arrivalInt / 60);
                    Integer m = (arrivalInt % 60);

                    String f1 = date;
                    if(h < 10) f1 = f1.concat("0");
                    f1 = f1.concat(h.toString());
                    if(m < 10) f1 = f1.concat("0");
                    f1 = f1.concat(m.toString());

                    h = (departureInt / 60);
                    m = (departureInt % 60);

                    String f2 = date;
                    if(h < 10) f2 = f2.concat("0");
                    f2 = f2.concat(h.toString());
                    if(m < 10) f2 = f2.concat(m.toString());
                    f2 = f2.concat(m.toString());

                    assert f1.compareTo(f2) <= 0;

                    ivls.add(Pair.of(f1, idx));
                    ivls.add(Pair.of(f2, -idx));
                    System.out.println(f1 + " " + f2);
                    idxToDate.put(idx, date);
                    idx++;
                }

					 System.out.println("Finished adding intervals");

                Warehouse warehouse = warehouseRepository.findByCity(startCity.get()).get(0);

                Integer totalCapacity = warehouse.getTotalCapacity();

                ivls.sort(Comparator.comparing(Pair<String, Integer>::getFirst).thenComparing(Pair<String, Integer>::getSecond));


                Integer currentCapacity = 0;

                for(Pair<String,Integer> par: ivls ){
                    System.out.println(currentCapacity.toString() + " " + totalCapacity.toString());
                    Integer index = par.getSecond();
                    if(par.getSecond() < 0) currentCapacity = Integer.max(currentCapacity - 1, 0);
                    else{
                        String date = idxToDate.get(index);
                        if(currentCapacity.equals(totalCapacity)) {
                                Integer curValue = 1;
                                if(totalBad.containsKey(date)) curValue = totalBad.get(date) + 1;
                                totalBad.put(date, curValue);
                        }
                        else{
                            currentCapacity ++;
                            Integer curValue = 1;
                            if(totalGood.containsKey(date)) curValue = totalGood.get(date) + 1;
                            totalGood.put(date, curValue);
                        }
                    }
                }

                System.out.println(ivls.size());
                System.out.println("Finished processing intervals");

                List<DayCharge> finalList = new ArrayList<>();

                for(int i = 0; i < allDates.size(); ++ i){
                    if(i != 0 && allDates.get(i).equals(allDates.get(i-1))) continue;
                    String date = allDates.get(i);
                    String actualDate = date.substring(date.length() - 2, date.length()) + "/" + date.substring(date.length() - 4, date.length() - 2) + "/" + date.substring(0, 4);
                    Integer good = 0, bad = 0;
                    if(totalGood.containsKey(date)) good = totalGood.get(date);
                    if(totalBad.containsKey(date)) bad = totalBad.get(date);
                    finalList.add(new DayCharge(actualDate,good,bad));
                }

                response.put(icaoOrig, finalList);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }

    public Map<String, List<HourCharge>> processFiles(String icaoCode, String selectedDate) throws IOException, ParseException {
        Map<String, List<HourCharge>> response = new HashMap<>();
        System.out.println("Entering with " + icaoCode);
        try {
            //Reading all pathnames
            String[] pathnames;
            File f = new File("./tempFiles/packs/");
            pathnames = f.list();

            //Processing all files
            for (String pathname : pathnames) {
                if(pathname.charAt(pathname.length() - 1) == 'i') continue;
                String icaoOrig = pathname.substring(pathname.length() - 8, pathname.length() - 4);
                System.out.println(icaoOrig + " " + icaoCode);

                if(!icaoCode.equals(icaoOrig)) continue;

                System.out.println("Processing " + icaoOrig);
                List<DayCharge> l = new ArrayList<>();

                String row = new String();
                BufferedReader csvReader = new BufferedReader((new FileReader("./tempFiles/packs/" + pathname)));

                List<String> events = new ArrayList<>();


                Optional<City> startCity = cityRepository.findByAirportCode(icaoOrig);

                System.out.println("Processing airport: " + icaoOrig);

                List<String> allDates = new ArrayList<>();

                while ((row = csvReader.readLine()) != null) {
                    String[] data = row.split("-");

                    String packCode = data[0];
                    String date = data[1];
                    allDates.add(date);
                    String arrivalTime = data[2];
                    String icaoDest = data[3];

                    String timeStamp = date + arrivalTime.substring(0,2) + arrivalTime.substring(3,5) + icaoDest;
                    events.add(timeStamp);
                }
                System.out.println("Finished adding events");
                Collections.sort(events);
                Collections.sort(allDates);
                List<Pair<String, Integer>> ivls = new ArrayList<>();

                Map<String, Integer> totalGood = new HashMap<>();
                Map<String, Integer> totalBad = new HashMap<>();
                Map<Integer, String> idxToDate = new HashMap<>();

                //202006102302SPIMLDZA
                Integer idx = 1;
                for(String event: events){

                    String date = event.substring(0,8);
                    String arrivalTime = event.substring(8,12);
                    String icaoDest = event.substring(12,16);
                    Optional<City> endCity = cityRepository.findByAirportCode(icaoDest);

                    List<Flight> flightList = flightRepository.findByStartCityAndEndCity(startCity.get(), endCity.get());

                    Integer arrivalInt = Integer.parseInt(arrivalTime.substring(0,2)) * 60 + Integer.parseInt(arrivalTime.substring(2,4));

                    Integer minWait = 10000000;

                    for(Flight flight: flightList){
                        String departure = flight.getDepartureTime();
                        departure = departure.substring(0,2) + departure.substring(3,5);
                        Integer departureInt = Integer.parseInt(departure.substring(0,2)) * 60 + Integer.parseInt(departure.substring(2,4));

                        Integer totalWait = 0;

                        if(arrivalInt > departureInt) totalWait = 1440 - (arrivalInt - departureInt);
                        else totalWait = departureInt - arrivalInt;

                        minWait = Integer.min(minWait, totalWait);
                    }
                    Integer departureInt;

                    if(arrivalInt + minWait >= 1440){
                        departureInt = 1439;
                    }
                    else departureInt = arrivalInt + minWait;

                    Integer h = (arrivalInt / 60);
                    Integer m = (arrivalInt % 60);

                    String f1 = date;
                    if(h < 10) f1 = f1.concat("0");
                    f1 = f1.concat(h.toString());
                    if(m < 10) f1 = f1.concat("0");
                    f1 = f1.concat(m.toString());

                    h = (departureInt / 60);
                    m = (departureInt % 60);

                    String f2 = date;
                    if(h < 10) f2 = f2.concat("0");
                    f2 = f2.concat(h.toString());
                    if(m < 10) f2 = f2.concat(m.toString());
                    f2 = f2.concat(m.toString());

                    assert f1.compareTo(f2) <= 0;

                    ivls.add(Pair.of(f1, idx));
                    ivls.add(Pair.of(f2, -idx));
                    System.out.println(f1 + " " + f2);
                    idxToDate.put(idx, date);
                    idx++;
                }

                System.out.println("Finished adding intervals");

                Warehouse warehouse = warehouseRepository.findByCity(startCity.get()).get(0);

                Integer totalCapacity = warehouse.getTotalCapacity();

                ivls.sort(Comparator.comparing(Pair<String, Integer>::getFirst).thenComparing(Pair<String, Integer>::getSecond));


                Integer currentCapacity = 0;

                System.out.println("Selected date: " + selectedDate);

                for(Pair<String,Integer> par: ivls ){
                    System.out.println(currentCapacity.toString() + " " + totalCapacity.toString());
                    Integer index = par.getSecond();
                    if(par.getSecond() < 0) currentCapacity = Integer.max(currentCapacity - 1, 0);
                    else{
                        String date = idxToDate.get(index);
                        System.out.println(date + " " + selectedDate);

                        if(currentCapacity.equals(totalCapacity)) {
                            if(date.equals(selectedDate)) {
                                String hour = par.getFirst().substring(8, 10);
                                Integer curValue = 1;
                                if (totalBad.containsKey(hour)) curValue = totalBad.get(hour) + 1;
                                totalBad.put(hour, curValue);
                            }
                        }
                        else{
                            currentCapacity ++;
                            if(date.equals(selectedDate)) {
                                String hour = par.getFirst().substring(8, 10);
                                Integer curValue = 1;
                                if (totalGood.containsKey(hour)) curValue = totalGood.get(hour) + 1;
                                totalGood.put(hour, curValue);
                            }
                        }
                    }
                }

                System.out.println(ivls.size());
                System.out.println("Finished processing intervals");

                List<HourCharge> finalList = new ArrayList<>();

                for(Integer i = 0; i < 24; ++ i){
                    String hour = "";
                    if(i < 10) hour = hour.concat("0");
                    hour = hour.concat(i.toString());
                    int good = 0, bad = 0;
                    if(totalGood.containsKey(hour)) good = totalGood.get(hour);
                    if(totalBad.containsKey(hour)) bad = totalBad.get(hour);
                    finalList.add(new HourCharge(hour, good, good + bad));
                }
                response.put(icaoCode, finalList);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }

    public Map<String, List<DayCharge>> getHistory(String icaoCode){
        City startCity = cityRepository.findByAirportCode(icaoCode).get();
        Warehouse warehouse = warehouseRepository.findByCity(startCity).get(0);
        List<WarehouseHistory> warehouseHistories = warehouseHistoryRepository.findByWarehouse(warehouse);
        Map<String, Integer> goodMap = new HashMap<>();
        Map<String, Integer> badMap = new HashMap<>();
        List<String> allDates = new ArrayList<>();
        for(WarehouseHistory warehouseHistory: warehouseHistories){
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String actualDate = dateFormat.format(warehouseHistory.getUpdateDate());
            String orderDate = actualDate.substring(6, 10) + actualDate.substring(3, 5) + actualDate.substring(0, 2);
				allDates.add(orderDate);
            Integer value = Integer.parseInt("1");
            if(warehouseHistory.isValid()){
                if(goodMap.containsKey(actualDate)) value = goodMap.get(actualDate) + 1;
                goodMap.put(actualDate, value);
            }
            else{
                if(badMap.containsKey(actualDate)) value = badMap.get(actualDate) + 1;
                badMap.put(actualDate, value);
            }
        }
        List<DayCharge> l  = new ArrayList<>();
        Collections.sort(allDates);
        for(int i = 0; i < allDates.size(); i ++){
            if(i != 0 && allDates.get(i - 1).equals(allDates.get(i))) continue;
				String date = allDates.get(i);
            String actualDate = date.substring(date.length() - 2, date.length()) + "/" + date.substring(date.length() - 4, date.length() - 2) + "/" + date.substring(0, 4);
				int goodn = 0, badn = 0;
				if(goodMap.containsKey(actualDate)) goodn = goodMap.get(actualDate);
				if(badMap.containsKey(actualDate)) badn = badMap.get(actualDate);
            DayCharge x = new DayCharge(actualDate, goodn, badn);
            l.add(x);
        }
        Map<String, List<DayCharge>> response = new HashMap<>();
        response.put("history", l);
        return response;
    }

    public Map<String, Integer> getPrediction(String icaoCode, String pickedDate) throws ParseException{
        String iniDate = "08/03/2020";
        Date start = new SimpleDateFormat("dd/MM/yyyy").parse(iniDate);
        Date picked = new SimpleDateFormat("dd/MM/yyyy").parse(pickedDate);
        long diff = picked.getTime() - start.getTime();
        int x = (int)(diff / (24 * 60 * 60 * 1000));
        float exponent = cityRepository.findByAirportCode(icaoCode).get().getExponent();
        int y = (int)(Math.ceil(Math.pow(x, exponent)) + 21);
        Map<String, Integer> response = new HashMap<>();
        response.put("prediction", y);
        return response;
    }

    public Map<String, String> predictCollision(String icaoCode) throws ParseException{
        String iniDate = "08/03/2020";
        Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse(iniDate);
        int x = 0;
        int capacity = warehouseRepository.findByCity(cityRepository.findByAirportCode(icaoCode).get()).get(0).getTotalCapacity();
        float exponent = cityRepository.findByAirportCode(icaoCode).get().getExponent();
        while(true){
            double y = Math.ceil(Math.pow(x, exponent)) + 21;
            if((double)y > capacity + 1e-5) break;
            System.out.println(x);
				System.out.println(y);
				System.out.println(capacity);
				x ++;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        c.add(Calendar.DAY_OF_MONTH, x);
		  
        Map<String, String> response = new HashMap<>();
        response.put("prediction", sdf.format(c.getTime()));
        return response;
    }

}
