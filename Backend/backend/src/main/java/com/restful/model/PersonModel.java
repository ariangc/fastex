package com.restful.model;

import com.restful.entity.Country;
import com.restful.entity.Person;

import java.util.Date;

public class PersonModel {
    private Long id;
    private String names;
    private String surnames;
    private String document;
    private Date birthday;
    private boolean isCustomer;
    private CountryModel country;
    private DocumentTypeModel documentType;

    public PersonModel(Long id, String names, String surnames, String document, Date birthday, boolean isCustomer) {
        this.id = id;
        this.names = names;
        this.surnames = surnames;
        this.document = document;
        this.birthday = birthday;
        this.isCustomer = isCustomer;
    }

    public PersonModel(Person a){
        this.id = a.getId();
        this.names = a.getNames();
        this.surnames = a.getSurnames();
        this.document = a.getDocument();
        this.birthday = a.getBirthday();
        this.isCustomer = a.isCustomer();
        this.country = new CountryModel(a.getCountry());
        this.documentType = new DocumentTypeModel(a.getDocumentType());
    }

    public PersonModel(){
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }


    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public boolean isCustomer() {
        return isCustomer;
    }

    public void setCustomer(boolean customer) {
        isCustomer = customer;
    }

    public CountryModel getCountry() {
        return country;
    }

    public void setCountry(CountryModel country) {
        this.country = country;
    }

    public DocumentTypeModel getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentTypeModel documentType) {
        this.documentType = documentType;
    }
}
