package com.restful.controller;

import com.restful.model.DocumentTypeModel;
import com.restful.service.DocumentTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/documentType")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
public class DocumentTypeController {
    @Autowired
    @Qualifier("documentTypeService")
    private DocumentTypeService documentTypeService;

    @CrossOrigin
    @PostMapping("/list")
    public @ResponseBody List<DocumentTypeModel> getDocumentTypes(){ return documentTypeService.getDocumentTypes(); }

    @CrossOrigin
    @PostMapping("/listByCountry")
    public @ResponseBody List<DocumentTypeModel> getDocumentTypesByCountry(@RequestBody Map<String, Long> data){
        System.out.println("Id country is: " +data.get("idCountry"));
        return documentTypeService.getDocumentTypesByCountry(data.get("idCountry"));
    }
}
