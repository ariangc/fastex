package com.restful.service;

import com.restful.entity.*;
import com.restful.entity.Package;
import com.restful.model.ProgrammedFlightModel;
import com.restful.model.ShippingModel;
import com.restful.model.ShippingXProgrammedFlightModel;
import com.restful.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("shippingService")
public class ShippingService {
    @Autowired
    @Qualifier("shippingRepository")
    private ShippingRepository shippingRepository;

    @Autowired
    @Qualifier("programmedFlightRepository")
    private ProgrammedFlightRepository programmedFlightRepository;

    @Autowired
    @Qualifier("shippingStatusRepository")
    private ShippingStatusRepository shippingStatusRepository;

    @Autowired
    @Qualifier("cityRepository")
    private CityRepository cityRepository;

    @Autowired
    @Qualifier("warehouseRepository")
    private WarehouseRepository warehouseRepository;

    @Autowired
    @Qualifier("shippingXProgrammedFlightRepository")
    private ShippingXProgrammedFlightRepository shippingXProgrammedFlightRepository;

    public Map<String, ShippingModel> createShipping(Shipping shipping){
        Map<String, ShippingModel> response = new HashMap<>();
        shipping.setRegistrationDate(new Date());
        String trackingCode = "";
        for(int i = 0; i < 10; ++ i){
            Random r = new Random();
            Integer d = r.nextInt(10);
            trackingCode += d.toString();
        }
        Random r = new Random();
        trackingCode += (char)(r.nextInt(26) + 65);
        shipping.setTrackingCode(trackingCode);
        shipping = shippingRepository.save(shipping);
        response.put("shipping", new ShippingModel(shipping));
        return response;
    }

    public Map<String, List<ShippingModel>> listShippings(){
        Map<String, List<ShippingModel>> response = new HashMap<>();
        List<Shipping> l = shippingRepository.findAll();
        List<ShippingModel> lModel = new ArrayList<>();

        for(Shipping shipping: l){
            ShippingModel x = new ShippingModel(shipping);
            if(x.getShippingStatus().getId() != 1) lModel.add(x);
        }
        response.put("shippings", lModel);
        return response;
    }

    public Map<String, List<ShippingModel>> getShippingByTrackingNumber(String trackingCode){
        Map<String, List<ShippingModel>> response = new HashMap<>();
        List<Shipping> l = shippingRepository.findByTrackingCode(trackingCode);
        List<ShippingModel> lModel = new ArrayList<>();
        for(Shipping shipping: l){
            ShippingModel x = new ShippingModel(shipping);
            lModel.add(x);
        }
        response.put("shipping", lModel);
        return response;
    }

    public Map<String, List<ProgrammedFlightModel>> getRoute(Long idShipping){
        Shipping shipping = shippingRepository.findById(idShipping).get();
        List<ShippingXProgrammedFlight> l = shippingXProgrammedFlightRepository.findByShipping(shipping);
        List<ProgrammedFlightModel> lModel = new ArrayList<>();
        for(ShippingXProgrammedFlight shippingXProgrammedFlight: l){
            ShippingXProgrammedFlightModel x = new ShippingXProgrammedFlightModel(shippingXProgrammedFlight);
            lModel.add(x.getProgrammedFlight());
        }
        Map<String, List<ProgrammedFlightModel>> response = new HashMap<>();
        response.put("route", lModel);
        return response;
    }

    public boolean cancelShipping(Shipping shipping){
        if(shipping.getShippingStatus().getId() != 2){
            return false;
        }
        List<ShippingXProgrammedFlight> shippingXProgrammedFlights = shippingXProgrammedFlightRepository.findByShipping(shipping);
        for(ShippingXProgrammedFlight shippingXProgrammedFlight: shippingXProgrammedFlights){
            ProgrammedFlight programmedFlight = shippingXProgrammedFlight.getProgrammedFlight();
            programmedFlight.setFilledCapacity(programmedFlight.getFilledCapacity() - 1);
            programmedFlightRepository.save(programmedFlight);
            Long from = programmedFlight.getFlight().getStartCity().getId();
            City y = cityRepository.findById(from).get();
            Warehouse x = warehouseRepository.findByCity(y).get(0);
            x.setFilledCapacity(x.getFilledCapacity() - 1);
            warehouseRepository.save(x);
        }
        Long to = shippingXProgrammedFlights.get(shippingXProgrammedFlights.size() - 1).getProgrammedFlight().getFlight().getEndCity().getId();
        City y = cityRepository.findById(to).get();
        Warehouse x = warehouseRepository.findByCity(y).get(0);
        x.setFilledCapacity(x.getFilledCapacity() - 1);
        warehouseRepository.save(x);
        shipping.setShippingStatus(shippingStatusRepository.findById(Long.parseLong("1")).get());
        shippingRepository.save(shipping);
        return true;
    }

    public boolean deliverShipping(Shipping shipping){
        if(shipping.getShippingStatus().getId() != 4){
            return false;
        }
        List<ShippingXProgrammedFlight> shippingXProgrammedFlights = shippingXProgrammedFlightRepository.findByShipping(shipping);
        Long to = shippingXProgrammedFlights.get(shippingXProgrammedFlights.size() - 1).getProgrammedFlight().getFlight().getEndCity().getId();
        City y = cityRepository.findById(to).get();
        Warehouse x = warehouseRepository.findByCity(y).get(0);
        x.setFilledCapacity(x.getFilledCapacity() - 1);
        shipping.setShippingStatus(shippingStatusRepository.findById(Long.parseLong("5")).get());
        shippingRepository.save(shipping);
        return true;
    }

    public Map<String, Float> quoteShipping(Long idStartCity, Long idEndCity){
        Map<String, Float> response = new HashMap<>();
        Float latitude1 = cityRepository.findById(idStartCity).get().getLatitude();
        Float latitude2 = cityRepository.findById(idEndCity).get().getLatitude();
        Float longitude1 = cityRepository.findById(idStartCity).get().getLongitude();
        Float longitude2 = cityRepository.findById(idEndCity).get().getLongitude();

        double dlat = latitude2 - latitude1;
        double dlon = longitude2 - longitude1;

        double a = Math.sin(dlat / 2.0) * Math.sin(dlat / 2.0) + Math.cos(latitude1) * Math.cos(latitude2) * Math.sin(dlon/2) * Math.sin(dlon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double res = 6357 * c;
        response.put("price", (float)res);
        return response;
    }

}
