package com.restful.model;

import com.restful.entity.ShippingStatus;

public class ShippingStatusModel {
    private Long id;
    private String description;

    public ShippingStatusModel(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    public ShippingStatusModel(ShippingStatus a){
        this.id = a.getId();
        this.description = a.getDescription();
    }

    public ShippingStatusModel() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
